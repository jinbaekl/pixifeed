﻿using System.Diagnostics;
using System.Windows;
using System.Linq;
using Microsoft.Phone.Scheduler;
using PixifeedTileManager;
using System.Collections.Generic;
using PixifeedTileManager.Model;
using Microsoft.Phone.Shell;
using System;
using System.IO.IsolatedStorage;
using System.IO;
using System.Net;
using Microsoft.Phone.Info;
using HtmlAgilityPack;
using System.Net.NetworkInformation;
using Windows.Networking.Connectivity;

namespace PixifeedTileManager
{
    public class ScheduledAgent : ScheduledTaskAgent
    {
        /// <remarks>
        /// ScheduledAgent 생성자는 UnhandledException 처리기를 초기화합니다.
        /// </remarks>
        static ScheduledAgent()
        {
            // 관리되는 예외 처리기에 알림을 신청합니다.
            Deployment.Current.Dispatcher.BeginInvoke(delegate
            {
                Application.Current.UnhandledException += UnhandledException;
            });
        }

        public static void SaveErrorLog(Exception ex)
        {
            using (IsolatedStorageFile isfm = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (isfm != null)
                {
                    using (IsolatedStorageFileStream isfs = new IsolatedStorageFileStream("exception.txt", System.IO.FileMode.Append,
                        isfm))
                    {
                        if (isfs.CanWrite)
                        {
                            using (StreamWriter sw = new StreamWriter(isfs))
                            {
                                sw.WriteLine();
                                sw.WriteLine("=== Background error {1} on {0} ===", DateTime.Now, ex.GetType().ToString());
                                sw.WriteLine(ex.Message);
                                sw.WriteLine();
                                sw.WriteLine(ex.StackTrace);
                                sw.WriteLine();
                                if (ex.InnerException != null)
                                {
                                    sw.WriteLine("Internal Error: {0}", ex.InnerException.GetType().ToString());
                                    sw.WriteLine(ex.InnerException);
                                }
                                sw.WriteLine();
                                sw.WriteLine();
                                sw.WriteLine("== Mem ==");
                                sw.WriteLine("Limit: {0}MB",
                                    Microsoft.Phone.Info.DeviceStatus.ApplicationMemoryUsageLimit / 1024.0 / 1024.0);
                                sw.WriteLine("Peak: {0}MB",
                                    Microsoft.Phone.Info.DeviceStatus.ApplicationPeakMemoryUsage / 1024.0 / 1024.0);
                                sw.WriteLine("Now: {0}MB",
                                    Microsoft.Phone.Info.DeviceStatus.ApplicationCurrentMemoryUsage / 1024.0 / 1024.0);
                                sw.WriteLine("Total: {0}MB",
                                    Microsoft.Phone.Info.DeviceStatus.DeviceTotalMemory / 1024.0 / 1024.0);
                                sw.WriteLine();
                                sw.WriteLine("== DeviceInfo ==");
                                sw.WriteLine("Manufacture: {0}", Microsoft.Phone.Info.DeviceStatus.DeviceManufacturer);
                                sw.WriteLine("Name: {0}", Microsoft.Phone.Info.DeviceStatus.DeviceName);
                                sw.WriteLine("App: {0}", System.Reflection.Assembly.GetExecutingAssembly().FullName);
                                sw.WriteLine("Devicever: {0}", Microsoft.Phone.Info.DeviceStatus.DeviceFirmwareVersion);
                                sw.WriteLine("OS: {0} {1}", Environment.OSVersion.Platform, Environment.OSVersion.Version);
                                sw.WriteLine("Runtime: {0}", Environment.Version);
                            }
                        }
                    }
                }
            }
        }

        /// 처리되지 않은 예외에 대해 실행할 코드입니다.
        private static void UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            if (Debugger.IsAttached)
            {
                SaveErrorLog(e.ExceptionObject);

                // 처리되지 않은 예외가 발생했습니다. 중단하고 디버거를 실행합니다.
                Debugger.Break();
            }
        }

        DateTime dtRun = DateTime.Now;
        List<string> usedImg = new List<string>();
        string memid = "";
        TileEnv teGlobal = null;
        /// <summary>
        /// 예약된 작업을 실행하는 에이전트입니다.
        /// </summary>
        /// <param name="task">
        /// 호출한 작업입니다.
        /// </param>
        /// <remarks>
        /// 이 메서드는 정기적 작업 또는 리소스를 많이 사용하는 작업이 호출될 때 호출됩니다.
        /// </remarks>
        protected override void OnInvoke(ScheduledTask task)
        {
            //TODO: 백그라운드에서 작업을 수행하는 코드를 추가합니다.
            if (!NetworkInterface.GetIsNetworkAvailable())
            {
                NotifyComplete();
                return;
            }

            teGlobal = new TileEnv();
            if (teGlobal.WiFi)
            {
                ConnectionProfile cp = NetworkInformation.GetInternetConnectionProfile();
                if (cp.NetworkAdapter.IanaInterfaceType != 71)
                {
                    NotifyComplete();
                    return;
                }
            }

            if(teGlobal.ShouldICancel())
            {
                Debug.WriteLine("Abort run.");
                Abort();
                return;
            }
            if (!teGlobal.CanIRun())
            {
                Debug.WriteLine("Cannot run.");
                NotifyComplete();
                return;
            }

            dtRun = DateTime.Now;
            Debug.WriteLine("백그라운드 시작.");
            isf = IsolatedStorageFile.GetUserStoreForApplication();

            int iMmethod = (int)teGlobal.TileType;
            int iCount = teGlobal.Count;
            int iSmethod = (int)teGlobal.PeopleType;

            bool ran = false;
            if (iMmethod < 2)
            {
                RefreshMainTile(iMmethod, iCount);
                ran = true;
            }

            //로그인
            if (IsolatedStorageSettings.ApplicationSettings.Contains("id") && IsolatedStorageSettings.ApplicationSettings.Contains("pass")
                    && !Communication.isLogin)
            {
                string id = HttpUtility.UrlEncode(IsolatedStorageSettings.ApplicationSettings["id"] as string);
                string pass = HttpUtility.UrlEncode(IsolatedStorageSettings.ApplicationSettings["pass"] as string);

                if (id.Length > 0 && pass.Length > 0)
                {
                    Communication.CommunicatePOST("http://www.pixiv.net/login.php", "mode=login&pixiv_id="
                        + id + "&pass="
                        + pass,
                        (re, stuff) =>
                        {
                            if (re >= 0)
                            {
                                //Debug.WriteLine(stuff);
                                int iSError = CommonMethod.SearchTagAttributesEnd("span", "error", 0, stuff);
                                if (iSError > 0)
                                {
                                    UpdateDefaultPrimaryTile();
                                    FinalizeTask(true);
                                }
                                else
                                {
                                    //에러없는 로그인 성공.
                                    Communication.isLogin = (stuff.IndexOf("user.loggedIn = true") > 0); 

                                    HtmlDocument hd = new HtmlDocument();
                                    hd.LoadHtml(stuff);
                                    HtmlNode hnSec = hd.DocumentNode.SelectSingleNode("//section[@class=\"_unit my-profile-unit\"]");
                                    if (hnSec != null)
                                    {
                                        HtmlNode hnA = hnSec.ChildNodes["a"];
                                        if (hnA != null)
                                        {
                                            string link = hnA.GetAttributeValue("href", "");
                                            int iId = link.IndexOf("?id=");
                                            if (iId >= 0)
                                            {
                                                iId += 4;
                                                int iEd = link.IndexOf("&", iId);
                                                if (iEd < iId)
                                                {
                                                    iEd = link.Length;
                                                }
                                                memid = link.Substring(iId, iEd - iId);
                                            }
                                        }
                                    }
                                    hnSec = null;
                                    hd = null;
                                    GC.Collect();

                                    Deployment.Current.Dispatcher.BeginInvoke(delegate
                                    {
                                        if (iMmethod > 1)
                                        {
                                            ran = true;
                                            RefreshMainTile(iMmethod, iCount); //매개변수 method: RefreshMainTile 
                                        }
                                        if (!teGlobal.isLowMemBack)
                                        {
                                            ran = true;
                                            RefreshUserTiles(iSmethod);
                                        }
                                        FinalizeTask(!ran);
                                    });
                                }
                                
                            }
                        });
                }
            }
        }

        #region 메인 타일
        private void RefreshMainTile(int method, int count)
        {
            //0기본 타일/1추천 작품/2최근 모두의 작품/3최근 즐겨찾기 작품
            //4최근 마이픽/5최근 상위 랭킹/6내 북마크 1페이지/

            switch (method)
            {
                case 1: GetRecommendedItems(count);
                    break;
                case 2: GetLFMItems(0, count);
                    break;
                case 3: GetLFMItems(1, count); //최근 즐겨찾기
                    break;
                case 4: GetLFMItems(2, count);
                    break;
                case 5: GetRankItems(count);
                    break;
                case 6: GetFavItems(count);
                    break;
                default: UpdateDefaultPrimaryTile();
                    return;
            }
        }

        private void UpdateDefaultPrimaryTile()
        {
            ShellTile st = ShellTile.ActiveTiles.First();

            CycleTileData cycleTile = new CycleTileData()
            {
                Title = "Pixifeed+",
                Count = 0,
                SmallBackgroundImage = new Uri("/Background.png", UriKind.Relative),
                CycleImages = new Uri[] {
                    new Uri("/Background.png", UriKind.Relative),
                    new Uri("/", UriKind.Relative),
                    new Uri("/", UriKind.Relative),
                    new Uri("/", UriKind.Relative),
                    new Uri("/", UriKind.Relative),
                    new Uri("/", UriKind.Relative),
                    new Uri("/", UriKind.Relative),
                    new Uri("/", UriKind.Relative),
                    new Uri("/", UriKind.Relative)
                }
            };

            st.Update(cycleTile);

            FinalizeTask();
        }

        const string baseuri = "http://www.pixiv.net/new_illust.php";
        const string favuri = "http://www.pixiv.net/bookmark_new_illust.php";
        const string mpuri = "http://www.pixiv.net/mypixiv_new_illust.php";
        private void GetLFMItems(int mi, int count)
        {
            string targeturi = baseuri;
            switch (mi)
            {
                case 1: targeturi = favuri; break;
                case 2: targeturi = mpuri; break;
            }

            Communication.Communicate(targeturi, (suc, res) =>
            {
                if (suc >= 0)
                {
                    List<PictureModel> l = Interpreter.InsideSearchResult(res);
                    if (l != null && l.Count > 0)
                    {
                        ShellTile st = ShellTile.ActiveTiles.First();

                        //Dictionary<string,string> last = new Dictionary<string,string>();
                        List<Uri> lu = new List<Uri>();
                        int ic = 0;
                        foreach (PictureModel pm in l)
                        {
                            if (ic == count)
                            {
                                break;
                            }
                            ic++;
                            if (teGlobal.HighQuality)
                            {
                                if (isf.GetFileNames(@"Shared\ShellContent\*" + pm.ID + "*").Length > 0)
                                {
                                    lu.Add(null);
                                    ImgListToUpdateTile(lu, st, Math.Min(l.Count, count), true);
                                    continue;
                                }
                                Communication.Communicate(string.Format("http://www.pixiv.net/member_illust.php?mode=medium&illust_id={0}", pm.ID),
                                    (a, b) =>
                                    {
                                        if (a >= 0)
                                        {
                                            if (lu.Count <= count)
                                            {
                                                string original = Interpreter.DetailToImg(b);
                                                if (!string.IsNullOrEmpty(original))
                                                {
                                                    string local = "Shared/ShellContent/" + Path.GetFileName(original);

                                                    if (local.IndexOf("?") > 0)
                                                    {
                                                        local = local.Substring(0, local.IndexOf("?"));
                                                    }
                                                    usedImg.Add(Path.GetFileName(pm.Pic));
                                                    lu.Add(new Uri("isostore:/" + local.Replace('\\', '/')));
                                                    RequestDownload(pm, local);
                                                    ImgListToUpdateTile(lu, st, Math.Min(l.Count, count), true);
                                                }
                                            }
                                        }
                                    });
                            }
                            else
                            {
                                if (isf.GetFileNames(@"Shared\ShellContent\*" + pm.ID + "*").Length > 0)
                                {
                                    lu.Add(null);
                                    ImgListToUpdateTile(lu, st, Math.Min(l.Count, count), true);
                                    continue;
                                }
                                else if (lu.Count <= count)
                                {
                                    string original = pm.Pic;
                                    if (!string.IsNullOrEmpty(original))
                                    {
                                        string local = "Shared/ShellContent/" + Path.GetFileName(original);

                                        if (local.IndexOf("?") > 0)
                                        {
                                            local = local.Substring(0, local.IndexOf("?"));
                                        }
                                        usedImg.Add(Path.GetFileName(pm.Pic));
                                        lu.Add(new Uri("isostore:/" + local.Replace('\\', '/')));
                                        RequestDownload(pm, local);
                                        ImgListToUpdateTile(lu, st, Math.Min(l.Count, count), true);
                                    }
                                }
                            }
                        }
                    }
                }
            });
        } // 1-1

        private void GetRankItems(int count)
        {
            string targeturi = "http://www.pixiv.net/ranking.php?mode=daily";

            Communication.Communicate(targeturi, (suc, res) =>
            {
                if (suc >= 0)
                {
                    List<PictureModel> l = Interpreter.AppendRank(res);
                    if (l != null && l.Count > 0)
                    {
                        ShellTile st = ShellTile.ActiveTiles.First();

                        //Dictionary<string,string> last = new Dictionary<string,string>();
                        List<Uri> lu = new List<Uri>();
                        int ic = 0;
                        foreach (PictureModel pm in l)
                        {
                            if (ic == count)
                            {
                                break;
                            }
                            ic++;

                            string local = @"Shared\ShellContent\" + Path.GetFileName(pm.Pic);

                            usedImg.Add(Path.GetFileName(pm.Pic));
                            lu.Add(new Uri("isostore:/" + local.Replace('\\', '/')));
                            RequestDownload(pm, local);

                        }
                        ImgListToUpdateTile(lu, st, Math.Min(l.Count, count));
                    }

                    FinalizeTask();
                }
            });
        } // 1-1

        private void GetRecommendedItems(int count)
        {
            string targeturi = "http://www.pixiv.net";

            Communication.Communicate(targeturi, (suc, res) =>
            {
                if (suc >= 0)
                {
                    List<PictureModel> l = Interpreter.FillWelcome(res);
                    if (l != null && l.Count > 0)
                    {
                        ShellTile st = ShellTile.ActiveTiles.First();

                        //Dictionary<string,string> last = new Dictionary<string,string>();
                        List<Uri> lu = new List<Uri>();
                        int ic = 0;
                        foreach (PictureModel pm in l)
                        {
                            if (ic == count)
                            {
                                break;
                            }
                            ic++;

                            string local = @"Shared\ShellContent\" + Path.GetFileName(pm.Pic);

                            usedImg.Add(Path.GetFileName(pm.Pic));
                            lu.Add(new Uri("isostore:/" + local.Replace('\\', '/')));
                            RequestDownload(pm, local);
                        }
                        ImgListToUpdateTile(lu, st, Math.Min(l.Count, count));
                    }

                    FinalizeTask();
                }
            }, 2);
        } // 1-1

        private void GetFavItems(int count)
        {
            if (string.IsNullOrEmpty(memid))
            {
                FinalizeTask();
                return;
            }
            string targeturi = string.Format("http://www.pixiv.net/bookmark.php?id={0}", memid);

            Communication.Communicate(targeturi, (suc, res) =>
            {
                if (suc >= 0)
                {
                    List<PictureModel> l = Interpreter.GetUserBookmarks(res);
                    if (l != null && l.Count > 0)
                    {
                        ShellTile st = ShellTile.ActiveTiles.First();

                        //Dictionary<string,string> last = new Dictionary<string,string>();
                        List<Uri> lu = new List<Uri>();
                        int ic = 0;
                        foreach (PictureModel pm in l)
                        {
                            if (ic == count)
                            {
                                break;
                            }
                            ic++;
                            
                            string local = @"Shared\ShellContent\" + Path.GetFileName(pm.Pic);

                            usedImg.Add(Path.GetFileName(pm.Pic));
                            lu.Add(new Uri("isostore:/" + local.Replace('\\', '/')));
                            RequestDownload(pm, local);
                        }
                        ImgListToUpdateTile(lu, st, Math.Min(l.Count, count));
                    }

                    FinalizeTask();
                }
            });
        } // 1-1

        private void wc_OpenReadCompleted(object sender, OpenReadCompletedEventArgs e)
        {
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                string filename = (e.UserState as string);
                if(filename.Contains('?'))
                {
                    filename = filename.Substring(0, filename.IndexOf('?'));
                }
                using (IsolatedStorageFileStream stream = new IsolatedStorageFileStream(filename.Replace('/', '\\'), System.IO.FileMode.OpenOrCreate, isf))
                {
                    byte[] buffer = new byte[1024];
                    while (e.Result.Read(buffer, 0, buffer.Length) > 0)
                    {
                        stream.Write(buffer, 0, buffer.Length);
                    }
                }

                if (qpm.Count > 0)
                {
                    qpm.Dequeue();
                }

                FinalizeTask();
            });
        } //1-2

        private void ImgListToUpdateTile(List<Uri> ls, ShellTile st, int count, bool isShrinking = false)
        {
            for (int i = 0; i < ls.Count; i++)
            {
                if (ls[i].OriginalString.Contains('?'))
                {
                    ls[i] = new Uri(ls[i].OriginalString.Substring(0, ls[i].OriginalString.IndexOf('?')));
                }
            }

            if (isShrinking && ls.Count == count)
            {
                while (ls.Count < 9)
                {
                    ls.Add(new Uri("/", UriKind.Relative));
                }

                CycleTileData cycleTile = new CycleTileData()
                {
                    Title = "Pixifeed+",
                    Count = 0,
                    SmallBackgroundImage = new Uri("/Background.png", UriKind.Relative),
                    CycleImages = ls.ToArray()
                };

                st.Update(cycleTile);

                FinalizeTask();
            }
            else if (!isShrinking)
            {
                while (ls.Count > count)
                {
                    ls = ls.GetRange(0, 9);
                }
                while (ls.Count < 9)
                {
                    ls.Add(new Uri("/", UriKind.Relative));
                }

                CycleTileData cycleTile = new CycleTileData()
                {
                    Title = "Pixifeed+",
                    Count = 0,
                    SmallBackgroundImage = new Uri("/Background.png", UriKind.Relative),
                    CycleImages = ls.ToArray()
                };

                st.Update(cycleTile);

                FinalizeTask();
            }
        } //1-3
        #endregion

        #region 유저 타일
        private void RefreshUserTiles(int method)
        {
            //0기본 타일/1사용자의 최근 작품 1개/2사용자의 최근 북마크 1개

            List<ShellTile> lt = new List<ShellTile>();
            foreach (ShellTile stem in ShellTile.ActiveTiles)
            {
                if (stem.NavigationUri.OriginalString.Contains("id="))
                {
                    lt.Add(stem);
                }
            }

            foreach (ShellTile l in lt)
            {
                int iSId = l.NavigationUri.OriginalString.IndexOf("id=");
                if (iSId > 0)
                {
                    iSId += 3;
                    int iEId = l.NavigationUri.OriginalString.Length;
                    string id = l.NavigationUri.OriginalString.Substring(iSId, iEId - iSId);
                    switch (method)
                    {
                        case 1: RefreshUserWork(method, l, id); //최근 작품
                            break;
                        case 2: RefreshUserBM(method, l, id); //최근 즐겨찾기
                            break;
                        default: UpdateDefaultUserTile(l, id);
                            return;
                    }
                }
            }

            FinalizeTask();
        }

        private void RefreshUserWork(int method, ShellTile st, string id)
        {
            string targeturi = string.Format("http://www.pixiv.net/member_illust.php?id={0}", id);

            Communication.Communicate(targeturi, (suc, res) =>
            {
                if (suc >= 0)
                {
                    if(res.IndexOf("user.loggedIn = true") < 0)
                    {
                        Debug.WriteLine("Not Login!!");
                    }

                    PictureModel pm = Interpreter.GetLastWork(res);
                    if (pm == null)
                    {
                        UpdateDefaultUserTile(st, id);
                    }
                    else
                    {
                        string local = @"Shared\ShellContent\" + Path.GetFileName(pm.Pic);

                        usedImg.Add(Path.GetFileName(pm.Pic));
                        Uri u = new Uri("isostore:/" + local.Replace('\\', '/'));
                        if (!isf.FileExists(local))
                        {
                            StackUserToast(id, method, pm);
                        }
                        RequestDownload(pm, local);
                        ImgToUserTile(u, pm.Title, pm.Author, st);
                    }
                }
            });
        }

        List<PictureModel> pmNew = new List<PictureModel>();
        private void StackUserToast(string id, int method, PictureModel pm)
        {
            pmNew.Add(new PictureModel(method == 1 ? "" : null, pm.Title, pm.Author, id));
        }

        private void RefreshUserBM(int method, ShellTile st, string id)
        {
            string targeturi = string.Format("http://www.pixiv.net/bookmark.php?id={0}", id);

            Communication.Communicate(targeturi, (suc, res) =>
            {
                if (suc >= 0)
                {
                    PictureModel pm = Interpreter.GetLastWork(res);
                    if (pm == null)
                    {
                        UpdateDefaultUserTile(st, id);
                    }
                    else
                    {
                        string local = @"Shared\ShellContent\" + Path.GetFileName(pm.Pic);

                        usedImg.Add(Path.GetFileName(pm.Pic));
                        Uri u = new Uri("isostore:/" + local.Replace('\\', '/'));
                        if (!isf.FileExists(local))
                        {
                            StackUserToast(id, method, pm);
                        }
                        RequestDownload(pm, local);
                        ImgToUserTile(u, pm.Title, pm.Author, st);
                    }
                }
            });
        }

        Queue<PictureModel> qpm = new Queue<PictureModel>();
        private void RequestDownload(PictureModel pm, string local)
        {
            if (isf.FileExists(local))
            {
                using(IsolatedStorageFileStream isfs = isf.OpenFile(local, FileMode.Open))
                {
                    if(isfs.Length != 0)
                    {
                        return;
                    }
                }
            }

            qpm.Enqueue(pm);
            WebClient wc = new WebClient();
            wc.Headers[HttpRequestHeader.Referer] = "http://www.pixiv.net/";
            wc.OpenReadCompleted += new OpenReadCompletedEventHandler(wc_OpenReadCompleted);
            wc.OpenReadAsync(new Uri(pm.Pic), local);
        }

        private void UpdateDefaultUserTile(ShellTile st, string id)
        {
            //ImgToUserTile(new Uri("/", UriKind.Relative), "", "", st);
            string targeturi = string.Format("http://www.pixiv.net/member.php?id={0}", id);
            Communication.Communicate(targeturi, (suc, res) =>
            {
                if (suc >= 0)
                {
                    PictureModel pm = Interpreter.GetLastWork(res, true);
                    if (pm != null)
                    {
                        ImgToUserTile(new Uri("/", UriKind.Relative), null, pm.Author, st);
                    }
                }
            });
        }

        private void ImgToUserTile(Uri ti, string title, string author, ShellTile st)
        {
            try
            {
                FlipTileData flipTile = new FlipTileData()
                {
                    BackBackgroundImage = ti,
                    BackContent = string.IsNullOrEmpty(title) ? author : title,
                    BackTitle = "Pixifeed+"
                };

                st.Update(flipTile);

                FinalizeTask();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.GetType().ToString()+" 개인 타일 실패: " + st.NavigationUri.OriginalString);
            }
        }
        #endregion

        System.Threading.Timer tm = null;
        private void FinalizeTask(bool force = false, int timer = 0)
        {
            if (tm == null)
            {
                tm = new System.Threading.Timer((a) =>
                {
                    if (qpm.Count == 0)
                    {
                        FinalizeTask(false, 1);
                    }
                }
                    , null, 2000, System.Threading.Timeout.Infinite);
                return;
            }
            else if(timer == 0)
            {
                tm.Change(3000, System.Threading.Timeout.Infinite);
                return;
            }
            try
            {
                string[] sFiles = isf.GetFileNames(@"Shared\ShellContent\*.*");
                if (sFiles.Length > 0)
                {
                    foreach (string sf in sFiles)
                    {
                        if (usedImg.IndexOf(sf) < 0)
                        {
                            string local = @"Shared\ShellContent\" + sf;
                            //isf.DeleteFile(@"Shared\ShellContent\" + sf);
                            if (isf.FileExists(local))
                            {
                                using (IsolatedStorageFileStream isfs = isf.OpenFile(local, FileMode.Truncate))
                                {
                                    isfs.Flush();
                                }
                            }
                        }
                    }
                }

                if (pmNew.Count > 0 && teGlobal.Toast)
                {
                    ShellToast stoast = null;
                    if (pmNew.Count > 1)
                    {
                        PictureModel pm = pmNew[0];
                        stoast = new ShellToast()
                        {
                            Title = string.IsNullOrEmpty(pm.Author) ? "Pixifeed+" : pm.Author,
                            Content = string.Format(pm.Pic == "" ? Res.LocalString.newworks : Res.LocalString.newbookmarks, pmNew.Count - 1)
                        };
                    }
                    else
                    {
                        PictureModel pm = pmNew[0];
                        stoast = new ShellToast()
                        {
                            Title = string.IsNullOrEmpty(pm.Author) ? "Pixifeed+" : pm.Author,
                            Content = string.Format(pm.Pic == "" ? Res.LocalString.newwork : Res.LocalString.newbookmark, pm.Title),
                            NavigationUri = new Uri("/MainPage.xaml?id=" + pm.ID, UriKind.Relative)
                        };
                    }
                    stoast.Show();
                }

                isf.Dispose();
                Debug.WriteLine("백그라운드 끝: " + (DateTime.Now - dtRun).ToString() + " / " + (DeviceStatus.ApplicationPeakMemoryUsage / 1024 / 1024.0).ToString());
            }
            finally
            {
                NotifyComplete();
            }
            //}
        }  //3

        IsolatedStorageFile isf = null;
        private Dictionary<string,string> KeyToList(string key)
        {
            char[] sep = new char[] { ';' };
            Dictionary<string,string> lst = new Dictionary<string,string>();
            using (IsolatedStorageFileStream isfs = new IsolatedStorageFileStream(string.Format(@"/Shell.{0}.config", key)
            , System.IO.FileMode.OpenOrCreate, isf))
            {
                StreamReader sr = new StreamReader(isfs);
                string[] line = null;
                while ((line = sr.ReadLine().Split(sep)) != null)
                {
                    if (line.Length == 2)
                    {
                        lst.Add(line[0], line[1]);
                    }
                }
            }
            return lst;
        }

        private void ListToKey(string key, Dictionary<string,string> m)
        {
            if (m != null)
            {
                using (IsolatedStorageFileStream isfs = new IsolatedStorageFileStream(string.Format(@"/Shell.{0}.config", key)
                , System.IO.FileMode.Create, isf))
                {
                    StreamWriter sr = new StreamWriter(isfs);
                    foreach (var line in m)
                    {
                        sr.WriteLine(line.Key+";"+line.Value);
                    }
                }
            }
        }

    }
}