﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Scheduler;
using Microsoft.Phone.Shell;
using System.Linq;
using System.IO.IsolatedStorage;
using System.IO;
using System.Text.RegularExpressions;

namespace PixifeedTileManager
{
    public static class CommonMethod
    {
        /// <summary>
        /// 파싱 중 태그와 속성을 검색합니다.
        /// </summary>
        /// <param name="tagname">검색할 태그 이름입니다.</param>
        /// <param name="inside">태그 속에서 찾을 속성입니다.</param>
        /// <param name="stapos">찾기를 시작할 위치입니다.(zerobase)</param>
        /// <param name="rawdata">대상 전체 문자열입니다.</param>
        /// <returns>찾은 결과물의 위치입니다.</returns>
        public static int SearchTagAttributes(string tagname, string inside, int stapos, string rawdata)
        {
            return SearchTagAttributes(tagname, inside, stapos, rawdata.Length, rawdata);
        }

        /// <summary>
        /// 파싱 중 태그와 속성을 검색합니다.
        /// </summary>
        /// <param name="tagname">검색할 태그 이름입니다.</param>
        /// <param name="inside">태그 속에서 찾을 속성입니다.</param>
        /// <param name="stapos">찾기를 시작할 위치입니다.(zerobase)</param>
        /// <param name="endpos">찾기를 제한할 끝 위치입니다.</param>
        /// <param name="rawdata">대상 전체 문자열입니다.</param>
        /// <returns>찾은 결과물의 위치입니다.</returns>
        public static int SearchTagAttributes(string tagname, string inside, int stapos, int endpos, string rawdata)
        {
        gogo:
            int first = rawdata.IndexOf("<" + tagname, stapos, endpos - stapos, StringComparison.InvariantCultureIgnoreCase);
            if (first >= 0)
            {
                int last = rawdata.IndexOf(">", first + 1);
                if (last >= 0)
                {
                    int res = rawdata.IndexOf(inside, first + 1, last - first - 1, StringComparison.InvariantCultureIgnoreCase);
                    if (res >= 0)
                    {
                        return first;
                    }
                    else
                    {
                        if (last + 1 < rawdata.Length)
                        {
                            stapos = last + 1;
                            goto gogo;
                        }
                    }
                }
            }
            return -1;
        }

        /// <summary>
        /// 파싱 중 태그와 속성을 검색하고, 그 태그의 끝 위치를 반환합니다.
        /// </summary>
        /// <param name="tagname">검색할 태그 이름입니다.</param>
        /// <param name="inside">태그 속에서 찾을 속성입니다.</param>
        /// <param name="stapos">찾기를 시작할 위치입니다.(zerobase)</param>
        /// <param name="endpos">찾기를 제한할 끝 위치입니다.</param>
        /// <param name="rawdata">대상 전체 문자열입니다.</param>
        /// <returns>찾은 결과물의 위치입니다.</returns>
        public static int SearchTagAttributesEnd(string tagname, string inside, int stapos, int endpos, string rawdata)
        {
        gogo:
            int first = rawdata.IndexOf("<" + tagname, stapos, endpos - stapos, StringComparison.InvariantCultureIgnoreCase);
            if (first >= 0)
            {
                int last = rawdata.IndexOf(">", first + 1);
                if (last >= 0)
                {
                    int res = rawdata.IndexOf(inside, first + 1, last - first - 1, StringComparison.InvariantCultureIgnoreCase);
                    if (res >= 0)
                    {
                        return last + 1;
                    }
                    else
                    {
                        if (last + 1 < rawdata.Length)
                        {
                            stapos = last + 1;
                            goto gogo;
                        }
                    }
                }
            }
            return -1;
        }

        public static int SearchTagAttributesEnd(string tagname, string inside, int stapos, string rawdata)
        {
            return SearchTagAttributesEnd(tagname, inside, stapos, rawdata.Length, rawdata);
        }

        public static string GetTagAttribute(string key, string tagstr)
        {
            int a = tagstr.IndexOf('<');
            if (a >= 0)
            {
                int b = tagstr.IndexOf('>');
                if (b >= 0)
                {
                    int c = tagstr.IndexOf(' ', a);
                    if (c >= 0)
                    {
                        string[] att = tagstr.Substring(c+1, b - c - 1).Split(new char[] { ' ' });
                        for (int i = 0; i < att.Length; i++)
                        {
                            string inatt = att[i];
                            int iEqual = inatt.IndexOf('=');
                            if (iEqual >= 0)
                            {
                                string skey = inatt.Substring(0, iEqual);
                                if (skey == key)
                                {
                                    string sv = inatt.Substring(iEqual + 1, inatt.Length - iEqual - 1);
                                    char quot = sv[0];
                                    int istart = 0;
                                    int iend = 0;
                                    if (quot == '\'' || quot == '"')
                                    {
                                        istart++;
                                    retry:
                                        iend = sv.LastIndexOf(quot);
                                        if(iend <= 0 && i + 1 < att.Length)
                                        {
                                            i++;
                                            sv += (" " + att[i]);
                                            goto retry;
                                        }
                                    }
                                    else
                                    {
                                        iend = sv.Length;
                                    }
                                    if (iend > 0)
                                    {
                                        return sv.Substring(istart, iend - istart);
                                    }
                                    
                                }
                            }
                        }
                    }
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// 태그로 감싸져 있는 문자열의 바깥쪽 태그만을 모두 삭제합니다.
        /// </summary>
        /// <param name="outer">작업을 할 대상 전체 문자열입니다.</param>
        /// <returns>바깥쪽 짝이 맞는 태그가 제거된 문자열입니다.</returns>
        public static string ExtractTagInside(string outer)
        {
            int tagstart = outer.IndexOf('<');
            if (tagstart >= 0)
            {
                int tagsemiend = outer.IndexOf('>', tagstart + 1);
                if (tagsemiend >= 0)
                {
                    int tagend = outer.LastIndexOf('>');
                    if (tagend >= 0)
                    {
                        int tagsemistart = outer.LastIndexOf('<', tagend - 1);
                        if (tagsemistart >= 0)
                        {
                            return outer.Substring(tagsemiend + 1, tagsemistart - tagsemiend - 1);
                        }
                    }
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// 주어진 문자열 속 모든 태그를 삭제합니다.
        /// </summary>
        /// <param name="target">태그가 섞인 문자열입니다.</param>
        /// <returns>태그 형태를 띤 모든 문자가 제거된 문자열입니다.</returns>
        public static string RemoveAllTags(string target)
        {
            int tagstart = target.IndexOf('<');
            while (tagstart >= 0)
            {
                if (tagstart + 1 >= target.Length)
                {
                    break;
                }

                int tagend = target.IndexOf('>', tagstart);
                if (tagend >= 0)
                {
                    if (char.IsLetter(target[tagstart + 1])
                        || target[tagstart + 1] == '/'
                        || target[tagstart + 1] == '!'
                        || target[tagstart + 1] == '-')
                    {
                        target = target.Remove(tagstart, tagend + 1 - tagstart);
                    }
                    else
                    {
                        if (tagend + 1 <= target.Length)
                        {
                            tagstart = tagend + 1;
                        }
                    }
                }
                else
                {
                    break;
                }
                tagstart = target.IndexOf('<', tagstart);
            }
            target = target.Replace("&nbsp;", "");
            target = target.Replace("  ", "");
            target = target.Replace("	", "");
            return target.Trim();
        }

        public static decimal ExcludeExceptNum(string target)
        {
            if (target.Length == 0)
            {
                return -1;
            }
            else
            {
                return Convert.ToDecimal(Regex.Replace(target, @"[\D]", ""));
            }
        }

        public static string TodayURL()
        {
            return string.Format("{0:0000}{1:00}{2:00}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
        }
    }
}
