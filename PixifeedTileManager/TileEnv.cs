﻿using Microsoft.Phone.Info;
using Microsoft.Phone.Marketplace;
using System.IO.IsolatedStorage;

namespace PixifeedTileManager
{
    public enum TaskFrequency
    {
        NoTask = 0,
        Min30 = 1,
        Min60 = 2,
        Min120 = 3,
        Intensive = 4,
        NotInitialized = -1
    }

    public enum TypeView
    {
        Default = 0,
        Recommended = 1,
        Recent = 2,
        Favorites = 3,
        Mypic = 4,
        Rank = 5,
        Bookmark = 6,
        NotInitialized = -1
    }

    public enum TypePeople
    {
        Default = 0,
        Recent = 1,
        Bookmark = 2,
        NotInitialized = -1
    }

    public class TileEnv
    {
        public TileEnv()
        {

        }

        private object LoadValue(string key)
        { 
            if(IsolatedStorageSettings.ApplicationSettings.Contains(key))
            {
                object m = IsolatedStorageSettings.ApplicationSettings[key];
                return m;
            }
            return null;
        }

        private int LoadInt(string key, int deflt = -1)
        {
            object n = LoadValue(key);
            if(n != null && n is int)
            {
                return (int)n;
            }
            return deflt;
        }

        private string LoadString(string key)
        {
            object n = LoadValue(key);
            if (n != null && n is string)
            {
                return (string)n;
            }
            return null;
        }

        private void SaveValue(string key, object value)
        {
            IsolatedStorageSettings.ApplicationSettings[key] = value;
            IsolatedStorageSettings.ApplicationSettings.Save();
        }

        private void SaveInt(string key, object value)
        {
            IsolatedStorageSettings.ApplicationSettings[key] = (int)value;
            IsolatedStorageSettings.ApplicationSettings.Save();
        }

        private void SaveString(string key, object value)
        {
            IsolatedStorageSettings.ApplicationSettings[key] = (string)value;
            IsolatedStorageSettings.ApplicationSettings.Save();
        }

        private TaskFrequency _freq = TaskFrequency.NotInitialized;
        public TaskFrequency Frequency {
            get
            {
                if (_freq < 0)
                {
                    _freq = (TaskFrequency)LoadInt("taskfreq", -1);
                }
                return _freq;
            }
            set
            {
                _freq = value;
                SaveInt("taskfreq", _freq);
            }
        }

        private int _wifi = -1;
        public bool WiFi
        {
            get
            {
                if (_wifi < 0)
                {
                    _wifi = LoadInt("wifi", 0);
                }
                return _wifi == 1;
            }
            set
            {
                _wifi = value ? 1 : 0;
                SaveInt("wifi", _wifi);
            }
        }

        private int _rcnt = -1;
        public int Runcount
        {
            get
            {
                if (_rcnt < 0)
                {
                    _rcnt = LoadInt("runcount");
                }
                return _rcnt;
            }
            set
            {
                _rcnt = value;
                SaveInt("runcount", _rcnt);
            }
        }

        /// <summary>
        /// ///////////////////////////////
        /// </summary>
        /// 
        //기본 타일/추천 작품/최근 모두의 작품/최근 즐겨찾기 작품
        //            최근 마이픽/최근 상위 랭킹/내 북마크 1페이지
        

        private TypeView _tv = TypeView.NotInitialized;
        public TypeView TileType
        {
            get
            {
                if (_tv < 0)
                {
                    _tv = (TypeView)LoadInt("tiletype", 1);
                }
                return _tv;
            }
            set
            {
                _tv = value;
                SaveInt("tiletype", _tv);
            }
        }

        private int _count = -1;
        public int Count
        {
            get
            {
                if (_count < 0)
                {
                    _count = LoadInt("tilecount", 5);
                }
                return _count;
            }
            set
            {
                _count = value;
                SaveInt("tilecount", _count);
            }
        }

        private int _hq = -1;
        public bool HighQuality
        {
            get
            {
                if (_hq < 0)
                {
                    _hq = LoadInt("highquality", 0);
                }
                return _hq == 1;
            }
            set
            {
                _hq = value ? 1 : 0;
                SaveInt("highquality", _hq);
            }
        }

        /// <summary>
        /// /////////////////////////////
        /// </summary>
        //기본 타일/사용자의 최근 작품 1개/사용자의 최근 북마크 1개
        

        private TypePeople _tp = TypePeople.NotInitialized;
        public TypePeople PeopleType
        {
            get
            {
                if (_tp < 0)
                {
                    _tp = (TypePeople)LoadInt("peopletype",1);
                }
                return _tp;
            }
            set
            {
                _tp = value;
                SaveInt("peopletype", _tp);
            }
        }

        private int _toast = -1;
        public bool Toast
        {
            get
            {
                if (_toast < 0)
                {
                    _toast = LoadInt("toast", 1);
                }
                return _toast == 1;
            }
            set
            {
                _toast = value ? 1 : 0;
                SaveInt("toast", _toast);
            }
        }

        /// <summary>
        /// 환경 설정값에 기본값 적용. NotInitialized가 반환될 경우.
        /// </summary>
        /*public void FillDefaultValues()
        {
            Frequency = TaskFrequency.Min60;
            WiFi = false;
            TileType = TypeView.Recommended;
            Count = 5;
            HighQuality = false;
            PeopleType = TypePeople.Recent;
            Toast = true;
        }*/

        /// <summary>
        /// 정식 버전에 백그라운드 설정이 유효한지 확인하고 무효면 작업 해제.
        /// </summary>
        /// <returns>참이면 작업 해제해야 함.</returns>
        public bool ShouldICancel()
        {
            LicenseInformation li = new LicenseInformation();
            if(li.IsTrial())
            {
                return true;
            }

            if(Frequency == TaskFrequency.NoTask ||
                (TileType == TypeView.Default && PeopleType == TypePeople.Default))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// 주기 설정에 맞게 백그라운드의 실행 가능 여부를 확인.
        /// </summary>
        /// <returns>실행 가능 여부.</returns>
        public bool CanIRun()
        {
            int rc = Runcount;
            if (rc < 0) { rc = 0; }
            Runcount = rc + 1;
            if(Frequency == TaskFrequency.Min60)
            {
                return rc % 2 == 0;
            }
            else if(Frequency == TaskFrequency.Min120)
            {
                return rc % 3 == 0;
            }
            return true;
        }

        private int _lowmem = -1;
        public bool isLowMem
        {
            get
            {
                if(_lowmem < 0)
                {
                    _lowmem = DeviceStatus.ApplicationMemoryUsageLimit < 209715200 ? 1 : 0;
                }
                return _lowmem == 1;
            }
        }

        private int _lowback = -1;
        public bool isLowMemBack
        {
            get
            {
                if (_lowmem < 0)
                {
                    _lowmem = DeviceStatus.ApplicationMemoryUsageLimit < 10485760 ? 1 : 0;
                }
                return _lowmem == 1;
            }
        }
    }
}
