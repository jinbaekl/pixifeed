﻿using System;
using System.Net;
using System.Linq;
using System.Collections.ObjectModel;
using PixifeedTileManager.Model;
using System.Collections.Generic;
using System.Diagnostics;
using HtmlAgilityPack;

namespace PixifeedTileManager
{
    public static class Interpreter
    {
        public static List<PictureModel> InsideSearchResult(string stuff)
        {
            List<PictureModel> lpic = new List<PictureModel>();

            if (stuff.Length > 0)
            {
                HtmlDocument hd = new HtmlDocument();
                hd.LoadHtml(stuff);
                HtmlNodeCollection hncLi = hd.DocumentNode.SelectNodes("//li[contains(@class, 'image-item')]");
                if (hncLi != null)
                {
                    foreach (HtmlNode hnLi in hncLi)
                    {
                        string sID = "", sTitle = "", sAuthor = "", sUri = "", fav = "", resp = "";

                        var hnA = hnLi.SelectSingleNode(".//a[@href]");
                        string href = hnA.GetAttributeValue("href", "");
                        if (href.Length > 0)
                        {
                            int iSid = href.IndexOf("illust_id=");
                            if (iSid >= 0)
                            {
                                iSid += 10;
                                int iEid = href.IndexOf('&', iSid);
                                if (iEid <= 0)
                                {
                                    iEid = href.Length;
                                }
                                sID = href.Substring(iSid, iEid - iSid);
                            }
                        }

                        var hnImg = hnLi.SelectSingleNode(".//img[@data-src or @src]");
                        if (hnImg != null)
                        {
                            string tag = hnImg.GetAttributeValue("data-tags", "");
                            sUri = hnImg.GetAttributeValue("data-src", "");
                            if (string.IsNullOrEmpty(sUri))
                            {
                                sUri = hnImg.GetAttributeValue("src", "");
                            }
                        }

                        var hnTitle = hnLi.SelectSingleNode(".//h1[@class=\"title\"]");
                        if (hnTitle != null)
                        {
                            sTitle = HttpUtility.HtmlDecode(hnTitle.InnerText);
                        }

                        var hnAuthor = hnLi.SelectSingleNode(".//a[@data-user_name]");
                        if (hnAuthor != null)
                        {
                            sAuthor = HttpUtility.HtmlDecode(hnAuthor.InnerText);
                        }

                        var hnBook = hnLi.SelectSingleNode(".//a[contains(@class, 'bookmark-count')]");
                        if (hnBook != null)
                        {
                            fav = HttpUtility.HtmlDecode(hnBook.InnerText);
                        }

                        var hnResp = hnLi.SelectSingleNode(".//a[contains(@class, 'image-response-count')]");
                        if (hnResp != null)
                        {
                            resp = HttpUtility.HtmlDecode(hnResp.InnerText);
                        }

                        PictureModel pmnew = new PictureModel(sUri, sTitle, sAuthor, sID, fav, resp);
                        if (sUri.Length > 0 && sTitle.Length > 0)
                        {
                            lpic.Add(pmnew);
                        }
                    }
                }
            }
            return lpic;
        }

        //사용자 검색
        public static bool SearchUser(ref ObservableCollection<Contact> cm, bool shouldClear, string stuff)
        {
            List<Contact> imc = new List<Contact>();

            if (stuff.Length > 0)
            {
                HtmlDocument hd = new HtmlDocument();
                hd.LoadHtml(stuff);
                HtmlNodeCollection hncLi = hd.DocumentNode.SelectNodes("//li[@class=\"user-recommendation-item\"]");
                if (hncLi != null)
                {
                    foreach (HtmlNode hnLi in hncLi)
                    {
                        string sID = "", sPic = "", sName = "", sDesc = "";
                        HtmlNode hnA = hnLi.ChildNodes["a"];
                        if (hnA != null)
                        {
                            string memanc = hnA.GetAttributeValue("href", "");
                            if (memanc.Length > 0)
                            {
                                int iMemnum = memanc.IndexOf("?id=");
                                if (iMemnum > 0)
                                {
                                    iMemnum += 4;
                                    sID = memanc.Substring(iMemnum, memanc.Length - iMemnum);
                                }
                            }

                            sPic = hnA.GetAttributeValue("data-src", "");
                        }

                        HtmlNode hnH1 = hnLi.ChildNodes["h1"];
                        if (hnH1 != null)
                        {
                            HtmlNode hnAn = hnH1.ChildNodes["a"];
                            if (hnAn != null)
                            {
                                sName = HttpUtility.HtmlDecode(hnAn.InnerText);
                            }
                        }

                        HtmlNode hnP = hnLi.ChildNodes["p"];
                        if (hnP != null)
                        {
                            sDesc = HttpUtility.HtmlDecode(hnP.InnerText);
                        }

                        if (sID.Length > 0 && sName.Length > 0)
                        {
                            imc.Add(new Contact(sPic, sID, sName, sDesc));
                        }
                    }
                }
            }

            if (imc.Count > 0)
            {
                if (shouldClear)
                {
                    cm.Clear();
                }

                foreach (Contact ci in imc)
                {
                    cm.Add(ci);
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool ShowFavoriteUser(ref ObservableCollection<Contact> cm, bool shouldClear, string stuff)
        {
            List<Contact> imc = new List<Contact>();

            HtmlDocument hd = new HtmlDocument();
            hd.LoadHtml(stuff);
            HtmlNodeCollection hncA = hd.DocumentNode.SelectNodes("//ul/li//a[@class]");

            foreach (HtmlNode itt in hncA)
            {
                if (!itt.GetAttributeValue("class", "").Contains("ui-profile-popup"))
                {
                    continue;
                }

                string memnum = "", pic = "", name = "", desc = "";
                /*<a href="member.php?id=3386759" class="ui-profile-popup"
                 * data-user_id="3386759" data-profile_img="http://i2.pixiv.net/img82/profile/ykss35/mobile/7310071_80.jpg"
                 * data-user_name="うぐいす餅">うぐいす餅</a>*/
                memnum = itt.GetAttributeValue("data-user_id", "");
                pic = itt.GetAttributeValue("data-profile_img", "");
                if (string.IsNullOrEmpty(pic))
                {
                    HtmlNode hnImg = hd.DocumentNode.SelectSingleNode(itt.XPath + "//img");
                    if (hnImg != null)
                    {
                        pic = hnImg.GetAttributeValue("src", "");
                        if (string.IsNullOrEmpty(pic))
                        {
                            pic = hnImg.GetAttributeValue("data-src", "");
                        }
                    }
                }
                name = itt.GetAttributeValue("data-user_name", "");
                if (string.IsNullOrEmpty(name))
                {
                    HtmlNode hnName = hd.DocumentNode.SelectSingleNode(itt.XPath + "//h1[@class=\"user-name\"]");
                    name = HttpUtility.HtmlDecode(hnName.InnerText);
                }
                HtmlNode hnDesc = hd.DocumentNode.SelectSingleNode(itt.XPath + "//p[@class=\"user-comment\"]");
                if (hnDesc != null)
                {
                    desc = HttpUtility.HtmlDecode(hnDesc.InnerText);
                }

                if (memnum.Length > 0 && name.Length > 0)
                {
                    var m = new Contact(pic, memnum, HttpUtility.HtmlDecode(name), HttpUtility.HtmlDecode(desc));
                    if (!imc.Contains(m))
                    {
                        imc.Add(m);
                    }
                }
            }


            if (shouldClear)
            {
                cm.Clear();
            }

            if (imc.Count > 0)
            {
                foreach (Contact ci in imc)
                {
                    cm.Add(ci);
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool ShowWorks(ref ObservableCollection<PictureModel> pc, bool shouldClean, string stuff)
        {
            HtmlDocument hd = new HtmlDocument();
            hd.LoadHtml(stuff);
            List<PictureModel> pm = new List<PictureModel>();

            var hncLis = hd.DocumentNode.SelectNodes("//li[contains(@class, 'image-item')]");
            if (hncLis != null)
            {

                foreach (var hnLi in hncLis)
                {
                    string mem = "", uri = "", title = ""; bool ugoira = false;
                    var hnA = hnLi.SelectSingleNode(".//a[@href]");
                    if (hnA != null)
                    {
                        mem = hnA.GetAttributeValue("href", "");
                    }
                    int iID = mem.IndexOf("id=");
                    if (iID > 0)
                    {
                        iID += 3;
                        mem = mem.Substring(iID, mem.Length - iID);
                    }
                    var hnImg = hnLi.SelectSingleNode(".//img[contains(@class, 'thumbnail')]");
                    if (hnImg != null)
                    {
                        uri = hnImg.GetAttributeValue("data-src", "");
                        if(string.IsNullOrEmpty(uri))
                        {
                            uri = hnImg.GetAttributeValue("src", "");
                        }
                        var tag = hnImg.GetAttributeValue("data-tags", "");
                        ugoira = tag.Contains("うごイラ");
                    }

                    var hnTitle = hnLi.SelectSingleNode(".//*[@class=\"title\"]");
                    if (hnTitle != null)
                    {
                        title = HttpUtility.HtmlDecode(hnTitle.InnerText).Trim();
                    }
                    PictureModel pnew = new PictureModel(uri, title, string.Empty, mem);
                    if (!pc.Contains(pnew))
                    {
                        pm.Add(pnew);
                    }
                }
            }
            if (pm.Count > 0)
            {
                if (shouldClean)
                {
                    pc.Clear();
                }
                foreach (PictureModel each in pm)
                {
                    pc.Add(each);
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public static List<PictureModel> AppendRank(string stuff)
        {
            List<PictureModel> lri = new List<PictureModel>();

            HtmlDocument hd = new HtmlDocument();
            hd.LoadHtml(stuff);

            HtmlNodeCollection hncSec = hd.DocumentNode.SelectNodes("//section[@class=\"ranking-item\"]");
            if (hncSec != null)
            {
                foreach (HtmlNode hnS in hncSec)
                {
                    string num = "", img = "", title = "", artist = "", date = "";
                    decimal now = -1, last = 0;
                    bool ugoira = false;
                    HtmlNode hnRank = hnS.SelectSingleNode(".//div[@class=\"rank\"]/h1/a");
                    if (hnRank != null)
                    {
                        now = CommonMethod.ExcludeExceptNum(HttpUtility.HtmlDecode(hnRank.InnerText));
                    }
                    HtmlNode hnLast = hnS.SelectSingleNode(".//div[@class=\"rank\"]/p/a");
                    if (hnLast != null)
                    {
                        last = CommonMethod.ExcludeExceptNum(HttpUtility.HtmlDecode(hnLast.InnerText));
                    }

                    HtmlNode hnImg = hnS.SelectSingleNode(".//img[@data-src]");
                    if (hnImg != null)
                    {
                        var tag = hnImg.GetAttributeValue("data-tags", "");
                        if (tag.Contains("うごイラ"))
                        {
                            ugoira = true;
                        }
                        img = hnImg.GetAttributeValue("data-src", "");
                        if (string.IsNullOrEmpty(img))
                        {
                            img = hnImg.GetAttributeValue("src", "");
                        }
                    }

                    HtmlNode hnTitle = hnS.SelectSingleNode(".//h2/a");
                    if (hnTitle != null)
                    {
                        string anchor = hnTitle.GetAttributeValue("href", "");
                        int iSID = anchor.IndexOf("id=");
                        if (iSID > 0)
                        {
                            iSID += 3;
                            //num
                            int iAfter = anchor.IndexOf('&', iSID);
                            if (iAfter >= iSID)
                            {
                                num = anchor.Substring(iSID, iAfter - iSID);
                            }
                            else
                            {
                                num = anchor.Substring(iSID, anchor.Length - iSID);
                            }
                        }
                        title = HttpUtility.HtmlDecode(hnTitle.InnerText);
                    }

                    HtmlNode hnMem = hnS.SelectSingleNode(".//a[@data-user_name]/span");
                    if (hnMem != null)
                    {
                        artist = HttpUtility.HtmlDecode(hnMem.InnerText);
                    }

                    date = hnS.GetAttributeValue("data-date", "");

                    if (!string.IsNullOrEmpty(num))
                    {
                        lri.Add(new PictureModel(img, title, artist, num));
                    }
                }
            }

            return lri;
        }

        public static List<PictureModel> FillWelcome(string stuff)
        {
            List<PictureModel> lri = new List<PictureModel>();

            HtmlDocument hd = new HtmlDocument();
            hd.LoadHtml(stuff);
            HtmlNodeCollection hnc = hd.DocumentNode.SelectNodes("//ul[@class=\"ui-brick\"]/li/a/img");
            if (hnc != null)
            {
                foreach (HtmlNode hn in hnc)
                {
                    if (hn.Name == "img")
                    {
                        string img = hn.GetAttributeValue("data-src", "");
                        if (string.IsNullOrEmpty(img))
                        {
                            img = hn.GetAttributeValue("src", "");
                        }
                        if (!string.IsNullOrEmpty(img))
                        {
                            lri.Add(new PictureModel(img, null, null, null));
                        }
                    }
                }
            }

            return lri;
        }

        public static string DetailToImg(string stuff)
        {
            try
            {
                Communication.isLogin = (stuff.IndexOf("loggedIn = true") >= 0);

                HtmlDocument hd = new HtmlDocument();
                hd.LoadHtml(stuff);

                HtmlNode hnImg = hd.DocumentNode.SelectSingleNode("//div[@class=\"works_display\"]/a/img");
                if (hnImg != null)
                {
                    string m = hnImg.GetAttributeValue("data-src", "");
                    if (string.IsNullOrEmpty(m))
                    {
                        m = hnImg.GetAttributeValue("src", "");
                    }
                    GC.Collect();
                    return m;
                }
            }
            catch
            {
                Debug.WriteLine("Error: Parsing detail Error.");
            }

            return "";
        }

        public static List<PictureModel> GetUserBookmarks(string stuff)
        {
            List<PictureModel> lpm = new List<PictureModel>();


            HtmlDocument hd = new HtmlDocument();
            hd.LoadHtml(stuff);

            HtmlNodeCollection hncLi = hd.DocumentNode.SelectNodes("//li[contains(@class, 'image-item')]");
            if (hncLi != null)
            {

                foreach (HtmlNode hn in hncLi)
                {
                    string uri = "", title = "", author = "", id = "", fav = "", resp = "";
                    bool ugoira = false;
                    #region 이미지 부분
                    HtmlNode hnA = hn.ChildNodes["a"];
                    if (hnA != null)
                    {
                        string link = hnA.GetAttributeValue("href", "");
                        int iId = link.IndexOf("illust_id=");
                        if (iId >= 0)
                        {
                            iId += 10;
                            int iEd = link.IndexOf("&", iId);
                            if (iEd < iId)
                            {
                                iEd = link.Length;
                            }
                            id = link.Substring(iId, iEd - iId);
                        }

                        var hnTitle = hn.SelectSingleNode(".//h1[@class=\"title\"]");
                        if (hnTitle != null)
                        {
                            title = HttpUtility.HtmlDecode(hnTitle.InnerText);
                        }

                        HtmlNode img = hn.SelectSingleNode(".//img[@data-src or @src]");
                        if (img != null)
                        {
                            string tag = img.GetAttributeValue("data-tags", "");
                            if (tag.Contains("うごイラ"))
                            {
                                ugoira = true;
                            }
                            uri = img.GetAttributeValue("data-src", "");
                            if (string.IsNullOrEmpty(uri))
                            {
                                uri = hn.GetAttributeValue("src", "");
                            }
                        }
                    }
                    #endregion

                    #region 작성자 부분
                    HtmlNode hnAuthor = hn.SelectSingleNode(".//a[@data-user_name]");
                    if (hnAuthor != null)
                    {
                        author = HttpUtility.HtmlDecode(hnAuthor.InnerText);
                    }
                    #endregion

                    #region 북마크 부분
                    HtmlNodeCollection hncI = hd.DocumentNode.SelectNodes(".//i[@class]");
                    if (hncI != null)
                    {
                        foreach (HtmlNode hnI in hncI)
                        {
                            if (hnI.GetAttributeValue("class", "").IndexOf("bookmark-") >= 0)
                            {
                                HtmlNode p = hnI.ParentNode;
                                fav = HttpUtility.HtmlDecode(p.InnerText);
                            }
                            else if (hnI.GetAttributeValue("class", "").IndexOf("response-") >= 0)
                            {
                                HtmlNode p = hnI.ParentNode;
                                resp = HttpUtility.HtmlDecode(p.InnerText);
                            }
                        }
                    }
                    #endregion

                    if (!string.IsNullOrEmpty(uri) && !string.IsNullOrEmpty(title))
                    {
                        PictureModel pm = null;
                        pm = new PictureModel(uri, title, author, id, fav, resp);
                        lpm.Add(pm);
                    }
                }
            }            

            return lpm;
        }

        internal static PictureModel GetLastWork(string res, bool nameonly = false)
        {
            HtmlDocument hd = new HtmlDocument();
            hd.LoadHtml(res);

            string userid = "", stitle = "", simg = "";
            HtmlNode hnId = hd.DocumentNode.SelectSingleNode("//div[@class=\"_unit profile-unit\"]/a/h1");
            if (hnId != null)
            {
                userid = HttpUtility.HtmlDecode(hnId.InnerText);
                if(nameonly)
                {
                    return new PictureModel(null, null, userid, null);
                }
            }

            HtmlNode hn = hd.DocumentNode.SelectSingleNode("//li[contains(@class, 'image-item')]");
            if (hn != null)
            {
                /*string link = hnA.GetAttributeValue("href", "");
                int iId = link.IndexOf("illust_id=");
                if (iId >= 0)
                {
                    iId += 10;
                    int iEd = link.IndexOf("&", iId);
                    if (iEd < iId)
                    {
                        iEd = link.Length;
                    }
                    id = link.Substring(iId, iEd - iId);
                }*/

                string mem = "", uri = "", title = ""; bool ugoira = false;
                var hnA = hn.SelectSingleNode(".//a[@href]");
                if (hnA != null)
                {
                    mem = hnA.GetAttributeValue("href", "");
                }
                int iID = mem.IndexOf("id=");
                if (iID > 0)
                {
                    iID += 3;
                    mem = mem.Substring(iID, mem.Length - iID);
                }
                var hnImg = hn.SelectSingleNode(".//img[contains(@class, 'thumbnail')]");
                if (hnImg != null)
                {
                    uri = hnImg.GetAttributeValue("data-src", "");
                    if (string.IsNullOrEmpty(uri))
                    {
                        uri = hnImg.GetAttributeValue("src", "");
                    }
                    var tag = hnImg.GetAttributeValue("data-tags", "");
                    ugoira = tag.Contains("うごイラ");
                }

                var hnTitle = hn.SelectSingleNode(".//*[@class=\"title\"]");
                if (hnTitle != null)
                {
                    title = HttpUtility.HtmlDecode(hnTitle.InnerText).Trim();
                }
                return new PictureModel(uri, title, string.Empty, mem);
                    
            }
            return null;
        }
    }
}


