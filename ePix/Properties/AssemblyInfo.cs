﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ePix")]
[assembly: AssemblyDescription("yeon")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("yygworld")]
[assembly: AssemblyProduct("ePix")]
[assembly: AssemblyCopyright("Copyright © yygworld 2012")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
//[assembly: Guid("e623fec8-32a3-4d35-89e5-2a7328e355ea")]
[assembly: Guid("3d18ae29-3556-4816-998d-abbc3e974387")]
//[assembly: Guid("cde294bc-f0a7-49cb-bdd9-817459577875")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.22.15.1130")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: NeutralResourcesLanguageAttribute("en-US")]
