﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Input;
using System.Collections.ObjectModel;
using ePix.Model;
using Microsoft.Phone.Tasks;
using System.Windows.Controls.Primitives;
using System.Windows.Media.Imaging;

namespace ePix
{
    public partial class Comment : PhoneApplicationPage
    {
        public Comment()
        {
            InitializeComponent();
        }

        ObservableCollection<CommentItem> ocComment = new ObservableCollection<CommentItem>();
        ObservableCollection<EmojiModel> ocEmoji = new ObservableCollection<EmojiModel>();
        ObservableCollection<StampModel> ocStamp = new ObservableCollection<StampModel>();

        string _id = null;
        string _membernum = null;
        private int nextpg;
        private int flast;
        string id
        {
            get
            {
                return string.IsNullOrEmpty(_id) ?
                    (PhoneApplicationService.Current.State.ContainsKey("illust_id") ?
                    PhoneApplicationService.Current.State["illust_id"].ToString() : string.Empty)
                    : _id;
            }
            set
            {
                _id = value;
                PhoneApplicationService.Current.State["illust_id"] = _id;
            }
        }
        string membernum
        {
            get
            {
                return string.IsNullOrEmpty(_membernum) ?
                    (PhoneApplicationService.Current.State.ContainsKey("m_id") ?
                    PhoneApplicationService.Current.State["m_id"].ToString() : string.Empty)
                    : _membernum;
            }
            set
            {
                _membernum = value;
                PhoneApplicationService.Current.State["m_id"] = _membernum;
            }
        }

        public static string stampstr = "";
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (NavigationContext.QueryString.ContainsKey("id"))
            {
                id = NavigationContext.QueryString["id"];
            }
            if (NavigationContext.QueryString.ContainsKey("mid"))
            {
                membernum = NavigationContext.QueryString["mid"];
            }

            llsComment.ItemsSource = ocComment;
            llsEmoji.ItemsSource = ocEmoji;
            llsStamp.ItemsSource = ocStamp;

            if (RichTextViewer.dicEmoji == null)
            {
                    RichTextViewer.dicEmoji = new Dictionary<string, string>();
            }
            if (RichTextViewer.dicEmoji.Count == 0)
            {
                if (ocEmoji.Count == 0)
                {
                    List<EmojiModel> lm = Interpreter.EnumEmoji(stampstr);

                    foreach (EmojiModel em in lm)
                    {
                        RichTextViewer.dicEmoji.Add(em.Name, em.Pic);
                    }
                }
            }
            if (ocComment.Count == 0)
            {
                RenewComments();
            }

            if (Communication.isLogin)
            {
                tcWriting.IsHitTestVisible = true;
                tboxComment.IsReadOnly = false;
                tboxComment.Hint = Res.LocalString.commenthere;
            }
            else
            {
                tcWriting.IsHitTestVisible = false;
                tboxComment.IsReadOnly = true;
                tboxComment.Hint = Res.LocalString.nocomment;
            }
        }

        private void RenewComments(int page = 1)
        {
            pbComment.Visibility = System.Windows.Visibility.Visible;
            pbComment.IsIndeterminate = true;
            string request = page == 1 ? string.Format("i_id={0}&u_id={1}", id, membernum)
                : string.Format("i_id={0}&u_id={1}&p={2}", id, membernum, page);
            Communication.CommunicatePOST("http://www.pixiv.net/rpc_comment_history.php",
                request, (suc, res) =>
                {
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        try
                        {
                            bool isMore = false;
                            tbNothing.Visibility = System.Windows.Visibility.Collapsed;
                            List<CommentItem> lc = Interpreter.ShowComments(res, out isMore);
                            if (page <= 1)
                            {
                                ocComment.Clear();
                            }
                            if (lc != null && lc.Count > 0)
                            {
                                foreach (CommentItem c in lc)
                                {
                                    ocComment.Add(c);
                                }
                            }

                            pbComment.Visibility = System.Windows.Visibility.Collapsed;
                            pbComment.IsIndeterminate = false;

                            if (ocComment.Count == 0)
                            {
                                tbNothing.Visibility = System.Windows.Visibility.Visible;
                            }

                            if (isMore)
                            {
                                nextpg = page + 1;
                            }
                        }
                        catch
                        {
                        }
                    });
                }, false, false);
        }

        private void tboxComment_GotFocus(object sender, RoutedEventArgs e)
        {
            if (!Communication.isFull)
            {
                if (MessageBox.Show(Res.LocalString.fullverdesc, Res.LocalString.fullver, MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                {
                    MarketplaceDetailTask mdt = new MarketplaceDetailTask();
                    mdt.Show();
                }
            }
            if (ocEmoji.Count == 0)
            {
                List<EmojiModel> lm = Interpreter.EnumEmoji(stampstr);

                foreach (EmojiModel l in lm)
                {
                    ocEmoji.Add(l);
                }
            }
            llsEmoji.Visibility = System.Windows.Visibility.Visible;
        }

        private void ViewportControl_ViewportChanged(object sender, System.Windows.Controls.Primitives.ViewportChangedEventArgs e)
        {
            ViewportControl vc = sender as ViewportControl;
            //lastview = vc.Viewport;
            if (ocComment != null && ocComment.Count > 0 && vc.Viewport.Bottom >= vc.Bounds.Bottom - 140 && flast != nextpg)
            {
                //int ipg = pivLat.SelectedIndex;
                try
                {
                    flast = nextpg;
                    RenewComments(nextpg);
                }
                catch { }
            }
        }

        private void tboxComment_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (!Communication.isFull)
                {
                    return;
                }
                if (Communication.isLogin && (sender as TextBox).Text.Trim().Length > 0 && (sender as TextBox).Text.Length <= 140)
                {
                    tcWriting.IsHitTestVisible = false;
                    (sender as TextBox).IsEnabled = false;
                    /*string postdata = string.Format("mode=comment_save&tt={0}&illust_id={1}&comment={2}",
                        Interpreter.token, id, HttpUtility.UrlEncode(tboxComment.Text));*/
                    PostComment("comment", HttpUtility.UrlEncode(tboxComment.Text));
                }
            }
        }

        private void PostComment(string type, string content)
        {
            string postdata = "";
            if (type == "stamp")
            {
                postdata = string.Format("type={0}&illust_id={1}&author_user_id={2}&stamp_id={3}&tt={4}",
                type, id, membernum, content, Interpreter.token);
            }
            else
            {
                postdata = string.Format("type={0}&illust_id={1}&author_user_id={2}&comment={3}&tt={4}",
                type, id, membernum, content, Interpreter.token);
            }
            Communication.CommunicatePOST("http://www.pixiv.net/rpc/post_comment.php",
                postdata, (a, b) =>
                {
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        tcWriting.IsHitTestVisible = true;
                        tboxComment.IsEnabled = true;
                        if (a >= 0 && !string.IsNullOrEmpty(b))
                        {
                            CmReturn cmr = JSONParser.JSONToCmRes(b);
                            if (cmr.error)
                            {
                                MessageBox.Show(cmr.message);
                            }
                            else
                            {
                                tboxComment.Text = string.Empty;
                            }
                            RenewComments();
                        }
                        else
                        {
                            MessageBox.Show(Res.LocalString.noconnection, Res.LocalString.errorpixifeed, MessageBoxButton.OK);
                        }
                    });
                }, false, false);
        }

        private void tcWriting_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ocStamp.Count == 0)
            {
                if (tcWriting != null && e.AddedItems.Count > 0)
                {
                    if (tcWriting.SelectedIndex == 1)
                    {
                        List<StampModel> lm = Interpreter.EnumStamps(stampstr);

                        foreach (StampModel l in lm)
                        {
                            ocStamp.Add(l);
                        }
                    }
                }
            }
        }

        private void Grid_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (!Communication.isFull)
            {
                return;
            }
            if (Communication.isLogin)
            {
                StampModel sm = ((sender as Grid).DataContext as StampModel);

                tcWriting.IsHitTestVisible = false;
                tboxComment.IsEnabled = false;
                PostComment("stamp", sm.Id);
            }
        }

        private void Image_ImageOpened(object sender, RoutedEventArgs e)
        {
            Image i = (sender as Image);
            if (i.Source != null)
            {
                i.Height = (i.Source as BitmapSource).PixelHeight;
            }
        }

        private void llsComment_MouseEnter(object sender, MouseEventArgs e)
        {
            llsEmoji.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void Grid2_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (!Communication.isFull)
            {
                return;
            }
            if (Communication.isLogin)
            {
                llsEmoji.Focus();
                EmojiModel sm = ((sender as Grid).DataContext as EmojiModel);
                string toadd = "(" + sm.Name + ")";
                int c = tboxComment.SelectionStart < 0 ? tboxComment.Text.Length : tboxComment.SelectionStart;
                if (tboxComment.Text.Length + toadd.Length <= 140)
                {
                    tboxComment.Text = tboxComment.Text.Insert(c, toadd);
                }
                tboxComment.Focus();
                tboxComment.SelectionStart = Math.Min(c + toadd.Length, tboxComment.Text.Length);
            }
        }
    }
}