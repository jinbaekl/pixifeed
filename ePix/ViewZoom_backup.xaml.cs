﻿using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.IsolatedStorage;
using System.Net;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ePix
{
    public class PicMeta
    {
        public PicMeta(int wid, int hei, long size)
        {
            Width = wid;
            Height = hei;
            if (size < 1024)
            {
                Size = string.Format("{0} Bytes", size);
            }
            else if (size < 1024 * 1024)
            {
                Size = string.Format("{0} KB", Math.Round(size / 1024.0, 2));
            }
            else if (size < 1024 * 1024 * 1024)
            {
                Size = string.Format("{0} MB", Math.Round(size / 1024.0 / 1024.0, 2));
            }
        }

        public int Width { get; set; }
        public int Height { get; set; }
        /*public int Type { get; set; }
        public int Bitrate { get; set; }*/
        public string Size { get; set; }
    }

    public partial class ViewZoomOld : PhoneApplicationPage
    {
        public ViewZoomOld()
        {
            InitializeComponent();

            this.Language = System.Windows.Markup.XmlLanguage.GetLanguage(System.Globalization.CultureInfo.CurrentUICulture.Name);

            if (ApplicationBar.Buttons.Count >= 1)
            {
                (ApplicationBar.Buttons[0] as ApplicationBarIconButton).Text = Res.LocalString.save;
            }
        }

        const string biguri = "http://www.pixiv.net/member_illust.php?mode={0}&illust_id={1}";
        string id = string.Empty;
        string type = "big";
        int currentImage = 0;

        List<string> bigimages = new List<string>();
        List<string> smallimgs = new List<string>();
        Dictionary<int,PicMeta> imgmeta = new Dictionary<int,PicMeta>();
        //List<System.Net.HttpWebRequest> lrequest = new List<System.Net.HttpWebRequest>();
        List<WebClient> lrequest = new List<WebClient>();
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (NavigationContext.QueryString.ContainsKey("mode"))
            {
                type = NavigationContext.QueryString["mode"];
            }
            else
            {
                if (NavigationService.CanGoBack)
                {
                    NavigationService.GoBack();
                }
                return;
            }
            if (NavigationContext.QueryString.ContainsKey("illust_id") && (e.NavigationMode == System.Windows.Navigation.NavigationMode.New ||
                ic < smallimgs.Count)
                && NavigationContext.QueryString.ContainsKey("url"))
            {
                id = NavigationContext.QueryString["illust_id"];
                string url = NavigationContext.QueryString["url"];
                SystemTray.SetIsVisible(this, true);
                SystemTray.SetProgressIndicator(this, new ProgressIndicator()
                {
                    IsIndeterminate = true,
                    IsVisible = true,
                    Text = Res.LocalString.retfull
                });
                Communication.Communicate(string.Format(biguri, type, id), (suc, res) =>
                {
                    if (suc >= 0)
                    {
                        this.Dispatcher.BeginInvoke(() =>
                        {
                            bigimages.Clear();
                            if (type == "manga")
                            {
                                bigimages = Interpreter.EnumBigImages(res);
                            }
                            else
                            {
                                bigimages.Add(Interpreter.SingleBigImage(res));
                            }
                            ManageImages();
                        });
                    }
                    else
                    {
                        this.Dispatcher.BeginInvoke(() =>
                        {
                            if (NavigationService.CanGoBack)
                            {
                                NavigationService.GoBack();
                            }
                        });
                    }
                }, true, url);
            }
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            if(lrequest != null && lrequest.Count > 0)
            {
                for(int i = 0; i < lrequest.Count ; i++)
                {
                    try
                    {
                        if (lrequest[i] != null)
                        {
                            //lrequest[i].Abort();
                            lrequest[i].CancelAsync();
                            lrequest.Remove(lrequest[i]);
                        }
                    }
                    catch
                    { }
                }
            }

            base.OnNavigatedFrom(e);            
        }

        bool imginit = false;
        int ic = 0, ia = 0;
        List<int> lprog = new List<int>();
        public void ManageImages()
        {
            smallimgs.Clear();
            imgmeta.Clear();
            lprog.Clear();
            for (int i = 0; i < bigimages.Count; i++)
            {
                string s = bigimages[i];
                smallimgs.Add(s);
                lprog.Add(0);
                if (NavigationContext.QueryString.ContainsKey("return") && !string.IsNullOrEmpty(NavigationContext.QueryString["return"]) && NavigationService.CanGoBack)
                {
                    break;
                }
            }

            ic = 0; ia = 0;
            try
            {
                using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    if (!isf.DirectoryExists("zoom"))
                    {
                        isf.CreateDirectory("zoom");
                    }
                    else
                    {
                        string[] m = isf.GetFileNames(@"zoom\*.img");
                        foreach (string n in m)
                        {
                            isf.DeleteFile(@"zoom\"+n);
                        }
                    }
                }
            }
            catch { }

            if (SystemTray.ProgressIndicator != null)
            {
                SystemTray.ProgressIndicator.Text = string.Format(Res.LocalString.bigready, smallimgs.Count);
            }
            lrequest.Clear();
            foreach (string img in smallimgs)
            {
                //System.Net.HttpWebRequest hwr = Communication.CommunicateBinary(img, (suc, stream, respuri) =>
                WebClient hwr = Communication.CommunicateBinary(img, (suc, stream, respuri) =>
                {
                    #region old

                    int im = 0;
                    if (respuri.Length > 0)
                    {
                        im = smallimgs.IndexOf(respuri);
                        if (im < 0)
                        {
                            return;
                        }
                    }
                    else
                    {
                        return;
                    }
                    if (suc >= 0)
                    {
                        this.Dispatcher.BeginInvoke(() =>
                        {
                            using (MemoryStream ms = new MemoryStream())
                            {
                                stream.CopyTo(ms);
                                stream.Close();
                                using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                                {
                                    string tname = string.Format(@"zoom\{0}.img", im);
                                    if (!isf.DirectoryExists("zoom"))
                                    {
                                        isf.CreateDirectory("zoom");
                                    }
                                    else
                                    {
                                        try
                                        {
                                            if (isf.FileExists(tname))
                                            {
                                                isf.DeleteFile(tname);
                                            }
                                        }
                                        catch { }
                                    }
                                    using (IsolatedStorageFileStream isfs = isf.OpenFile(tname,
                                        System.IO.FileMode.Create))
                                    {
                                        ms.Position = 0;
                                        ms.CopyTo(isfs);
                                    }
                                }
                                ms.Position = 0;
                                if (MakeMiddleImage(ms, im))
                                {
                                    if (im == 0)
                                    {
                                        if (NavigationContext.QueryString.ContainsKey("return") && !string.IsNullOrEmpty(NavigationContext.QueryString["return"]) && NavigationService.CanGoBack)
                                        {
                                            btnSave_Click(null, null);
                                        }
                                        else
                                        {
                                            LoadMiddleImage(0);
                                        }
                                    }
                                }
                            }

                            ic++;
                            if (SystemTray.ProgressIndicator != null)
                            {
                                SystemTray.ProgressIndicator.Text = ia > 0 ? string.Format(Res.LocalString.bigdownfail, ic, smallimgs.Count, ia)
                                    : string.Format(Res.LocalString.bigdown, ic, smallimgs.Count);
                                if (ic == smallimgs.Count && ia == 0)
                                {
                                    SystemTray.ProgressIndicator.Text = "";
                                    SystemTray.SetProgressIndicator(this, null);
                                    SystemTray.SetIsVisible(this, false);
                                }
                            }
                        });
                    }
                    else
                    {
                        this.Dispatcher.BeginInvoke(() =>
                        {
                            ia++;
                            if (SystemTray.ProgressIndicator != null)
                            {
                                SystemTray.ProgressIndicator.Text = string.Format(Res.LocalString.bigdownfail, ic, smallimgs.Count, ia);
                            }
                        });
                    }

                    #endregion
                }, (prog, file) =>
                {
                    int i = smallimgs.IndexOf(file);
                    lprog[i] = prog;

                    int j = 0;
                    foreach(int k in lprog)
                    {
                        j += k;
                    }

                    ProgressIndicator pi = SystemTray.ProgressIndicator;
                    if(pi != null)
                    {
                        pi.IsIndeterminate = false;
                        pi.Value = j / (double)(100*lprog.Count);
                    }
                });
                lrequest.Add(hwr);
            }
        }

        private static WriteableBitmap CropImage(WriteableBitmap source,
                                                         int xOffset, int yOffset,
                                                         int width, int height)
        {
            // Get the width of the source image
            var sourceWidth = source.PixelWidth;

            // Get the resultant image as WriteableBitmap with specified size
            var result = new WriteableBitmap(width, height);

            // Create the array of bytes
            for (var x = 0; x <= height - 1; x++)
            {
                var sourceIndex = xOffset + (yOffset + x) * sourceWidth;
                var destinationIndex = x * width;

                Array.Copy(source.Pixels, sourceIndex, result.Pixels, destinationIndex, width);
            }
            return result;
        }

        private bool MakeMiddleImage(Stream sr, int num)
        {
            try
            {
                sr.Position = 0;
                WriteableBitmap wb = new WriteableBitmap(0, 0);
                wb.SetSource(sr);
                imgmeta.Add(num, new PicMeta(wb.PixelWidth, wb.PixelHeight, sr.Length));
                sr.Position = 0;
                //sr.Position = 0;
                //byte[] test = ShrinkImage(sr);                
                //using (MemoryStream ms = new MemoryStream(sr))
                //{
                    //using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                    //{
                        //using (IsolatedStorageFileStream isfs = isf.OpenFile(string.Format(@"zoom\{0}_mid.img", num), FileMode.Create))
                        //{
                        //    sr.CopyTo(isfs);
                        //}
                    //}
                //}
                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool LoadMiddleImage(int targetnum)
        {
            bool isBlack = (Application.Current.Resources["PhoneDarkThemeVisibility"] as Visibility?)
                                            == System.Windows.Visibility.Visible;
            if (imgmeta == null || imgmeta.Count <= targetnum)
            {
                SystemTray.SetProgressIndicator(this, null);
                SystemTray.SetIsVisible(this, false);
                return false;
            }
            try
            {
                using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream isfs = isf.OpenFile(@"zoom\zoom.htm", FileMode.Create))
                    {
                        using(StreamWriter sw = new StreamWriter(isfs))
                        {
                            sw.WriteLine("<html><head><script>function nexts(){window.external.notify(\"next\");}function privs(){window.external.notify(\"priv\");}</script><meta id=\"viewport\" name=\"viewport\""
                            + string.Format("content=\"width={0}px; height={1}px; user-scalable=yes\"/></head>", imgmeta[targetnum].Width, imgmeta[targetnum].Height+30)
                            +"<body style=\"background-color:"+(isBlack ? "black" : "white")+"; color:"+(isBlack ? "white" : "black")
                            +"; margin:0;\"><div style=\"display: table-cell;align: center;vertical-align: middle; background-color:white;\">"
                            + string.Format("<img src=\"{0}.img\">", targetnum) + "</div>"
                            + (targetnum < smallimgs.Count - 1 ? "&nbsp;<a style=\"color:gray; font-size: 50px;\" onclick=\"nexts()\">Next&nbsp;&gt;</a><br/>&nbsp;" : "")
                            + "<p style=\"font-size: 20px;\">" + string.Format("{0} x {1} : {2}", imgmeta[targetnum].Width, imgmeta[targetnum].Height, imgmeta[targetnum].Size) + "</p>"
                            + (targetnum > 0 ? "<a style=\"color:gray; font-size: 50px;\" onclick=\"privs()\">&lt;&nbsp;Priv</a><br/>" : "")
                            + "</body></html>");
                        }
                    }
                }
                wbZoom.IsScriptEnabled = true;
                wbZoom.Navigate(new Uri(@"zoom\zoom.htm", UriKind.Relative));
                //wbZoom.Navigate(new Uri(string.Format(@"zoom\{0}_mid.img", targetnum), UriKind.Relative));
            /*using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if(!isf.FileExists(string.Format(@"zoom\{0}_mid.img", targetnum)))
                {
                    return false;
                }
                BitmapImage bi = new BitmapImage();
                using (IsolatedStorageFileStream isfs = isf.OpenFile(string.Format(@"zoom\{0}_mid.img", targetnum), FileMode.Open))
                {
                    bi.SetSource(isfs);
                    iImg.Source = bi;
                    iImg.Margin = new Thickness((this.ActualWidth / 2.0) - (bi.PixelWidth / 2.0),
                        (this.ActualHeight / 2.0) - (bi.PixelHeight / 2.0), bi.PixelWidth, bi.PixelHeight);
                    if (bi.PixelHeight >= bi.PixelWidth)
                    {
                        minZoom = Math.Min(800 / (double)bi.PixelHeight, 1.0);
                    }
                    else
                    {
                        minZoom = Math.Min(480 / (double)bi.PixelWidth, 1.0);
                    }
                    transImg.CenterX = bi.PixelWidth / 2.0;
                    transImg.CenterY = bi.PixelHeight / 2.0;
                }
            }*/
            //transImg.ScaleX = transImg.ScaleY = minZoom;
            //transImg.TranslateX = transImg.TranslateY = 0;
            btnLeft.IsEnabled = true;
            btnRight.IsEnabled = true;
            if (ApplicationBar.Buttons.Count >= 1)
            {
                (ApplicationBar.Buttons[0] as ApplicationBarIconButton).IsEnabled = true;
            }
            btnLeft.Visibility = (smallimgs.Count > 1) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
            btnRight.Visibility = (smallimgs.Count > 1) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
            tbPage.Text = string.Format("{0} / {1}", targetnum + 1, smallimgs.Count);
            bdPage.Visibility = smallimgs.Count > 1 ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
            //bdControl.Visibility = smallimgs.Count > 1 ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
            bdControl.Visibility = System.Windows.Visibility.Collapsed;
            btnLeft.Opacity = (targetnum == 0) ? 0 : 1;
            btnRight.Opacity = (targetnum == smallimgs.Count - 1) ? 0 : 1;
            }catch{
                SystemTray.SetProgressIndicator(this, null);
                SystemTray.SetIsVisible(this, false);
                return false;
            }
            return true;
        }

        public byte[] ShrinkImage(Stream org)
        {
            return ShrinkImage(org, 0);
        }

        public byte[] ShrinkImage(Stream org, int width)
        {
            /*WriteableBitmap wbtest = new WriteableBitmap(300,3000);
            Extensions.LoadJpeg(wbtest, org);*/

            
            BitmapImage bi = new BitmapImage();
            bi.SetSource(org);

            const int maxsize = 2000;
            int cx = 0, cy = 0;
            int orgwid = bi.PixelWidth;
            int orghei = bi.PixelHeight;
            if (width <= 0)
            {
                if (orgwid >= orghei)
                {
                    cy = Math.Min(orghei, maxsize);
                    cx = Convert.ToInt32(orgwid * (cy / (double)orghei));
                }
                else
                {
                    cx = Math.Min(orgwid, maxsize);
                    cy = Convert.ToInt32(orghei * (cx / (double)orgwid));
                }
            }
            else
            {
                cx = Math.Min(orgwid, width);
                cy = Convert.ToInt32(orghei * (cx / (double)orgwid));
            }
            
            Image image = new Image();
            image.Source = bi;

            WriteableBitmap wb = new WriteableBitmap((int)cx, (int)cy);
            ScaleTransform transform = new ScaleTransform();
            transform.ScaleX = cx / bi.PixelWidth;
            transform.ScaleY = cy / bi.PixelHeight;
            wb.Render(image, transform);
            wb.Invalidate();
            

            MemoryStream ms = new MemoryStream();
            wb.SaveJpeg(ms, cx, cy, 0, 95);

            byte[] resb = new byte[ms.Length];
            ms.Position = 0;
            ms.Read(resb, 0, resb.Length);

            return resb;
        }

        public BitmapSource ShrinkImageToBi(Stream org, int width)
        {
            /*WriteableBitmap wbtest = new WriteableBitmap(300,3000);
            Extensions.LoadJpeg(wbtest, org);*/


            BitmapImage bi = new BitmapImage();
            bi.SetSource(org);

            const int maxsize = 2000;
            int cx = 0, cy = 0;
            int orgwid = bi.PixelWidth;
            int orghei = bi.PixelHeight;
            if (width <= 0)
            {
                if (orgwid >= orghei)
                {
                    cy = Math.Min(orghei, maxsize);
                    cx = Convert.ToInt32(orgwid * (cy / (double)orghei));
                }
                else
                {
                    cx = Math.Min(orgwid, maxsize);
                    cy = Convert.ToInt32(orghei * (cx / (double)orgwid));
                }
            }
            else
            {
                cx = Math.Min(orgwid, width);
                cy = Convert.ToInt32(orghei * (cx / (double)orgwid));
            }

            Image image = new Image();
            image.Source = bi;

            WriteableBitmap wb = new WriteableBitmap((int)cx, (int)cy);
            ScaleTransform transform = new ScaleTransform();
            transform.ScaleX = cx / bi.PixelWidth;
            transform.ScaleY = cy / bi.PixelHeight;
            wb.Render(image, transform);
            wb.Invalidate();


            MemoryStream ms = new MemoryStream();
            wb.SaveJpeg(ms, cx, cy, 0, 95);

            return wb;
        }


        private void btnBack_Click(object sender, EventArgs e)
        {
            if (NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            int currentnum = Math.Max(currentImage, 0);
            try
            {
                using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream isfs = isf.OpenFile(string.Format(@"zoom\{0}.img", currentnum), FileMode.Open))
                    {
                        BitmapImage bi = new BitmapImage();
                        bi.CreateOptions = BitmapCreateOptions.None;
                        bi.SetSource(isfs);
                        WriteableBitmap wb = new WriteableBitmap(bi);
                        MemoryStream ms = new MemoryStream();
                        wb.SaveJpeg(ms, bi.PixelWidth, bi.PixelHeight, 0, 100);
                        ms.Position = 0;

                        MediaLibrary mb = new MediaLibrary();
                        Picture pic = mb.SavePicture(string.Format("{0}.jpg",id), ms);
                        ms.Close();
                        if (pic != null)
                        {
                            MessageBox.Show(Res.LocalString.savethumbok,
                                Res.LocalString.alert, MessageBoxButton.OK);
                        }
                        mb.Dispose();
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(Res.LocalString.errorbang + Environment.NewLine + ex.GetType().ToString(),
                    Res.LocalString.errorpixifeed, MessageBoxButton.OK);
            }

            if(NavigationContext.QueryString.ContainsKey("return"))
            {
                if(!string.IsNullOrEmpty(NavigationContext.QueryString["return"]) && NavigationService.CanGoBack)
                {
                    NavigationService.GoBack();
                }
            }
        }

        private void btnLeft_Click(object sender, RoutedEventArgs e)
        {
            if (currentImage-1 >= 0 && currentImage-1 < smallimgs.Count)
            {
                currentImage--;
                if (!LoadMiddleImage(currentImage))
                {
                    currentImage++;
                }
            }
        }

        private void btnRight_Click(object sender, RoutedEventArgs e)
        {
            if (currentImage + 1 >= 0 && currentImage + 1 < smallimgs.Count)
            {
                currentImage++;
                if (!LoadMiddleImage(currentImage))
                {
                    currentImage--;
                }
            }
        }

        private void PhoneApplicationPage_OrientationChanged(object sender, OrientationChangedEventArgs e)
        {
            switch (e.Orientation)
            {
                case PageOrientation.LandscapeRight:
                case PageOrientation.PortraitUp: bdControl.Margin = new Thickness(0, 18, 22, 0); break;
                case PageOrientation.LandscapeLeft: bdControl.Margin = new Thickness(0, 18, 82, 0); break;
            }
        }

        private void wbZoom_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            wbZoom.Opacity = 1;
        }

        private void wbZoom_Navigating(object sender, NavigatingEventArgs e)
        {
            wbZoom.Opacity = 0;
        }

        private void wbZoom_lbDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            bdPage.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void wbZoom_ScriptNotify(object sender, NotifyEventArgs e)
        {
            if (e.Value == "priv")
            {
                if (currentImage - 1 >= 0 && currentImage - 1 < smallimgs.Count)
                {
                    currentImage--;
                    if (!LoadMiddleImage(currentImage))
                    {
                        currentImage++;
                    }
                }
            }
            else if (e.Value == "next")
            {
                if (currentImage + 1 >= 0 && currentImage + 1 < smallimgs.Count)
                {
                    currentImage++;
                    if (!LoadMiddleImage(currentImage))
                    {
                        currentImage--;
                    }
                }
            }
        }
    }

    public class byteToImageConverter : IValueConverter
    {
        public BitmapImage ConvertByteArrayToBitMapImage(byte[] imageByteArray)
        {
            BitmapImage img = new BitmapImage();
            img.CreateOptions = BitmapCreateOptions.None;
            MemoryStream memStream = new MemoryStream(imageByteArray);
            img.SetSource(memStream);
            return img;
        }


        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            BitmapImage img = new BitmapImage();
            if (value != null)
            {
                img = this.ConvertByteArrayToBitMapImage(value as byte[]);
            }
            return img;
        }
        
        // No need to implement converting back on a one-way binding
        public object ConvertBack(object value, Type targetType,
        object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [DataContract]
    public class ImgRaw
    {
        [DataMember]
        public byte[] Mo { get; set; }

        public string Nam { get; set; }

        public ImgRaw(byte[] from)
        {
            Mo = from;
        }
    }
}