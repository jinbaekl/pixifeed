﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using Microsoft.Phone.Info;
using Microsoft.Phone.Scheduler;

namespace ePix
{
    public partial class Settings : PhoneApplicationPage
    {
        public Settings()
        {
            InitializeComponent();
        }
        
        private void btnBack_Click(object sender, EventArgs e)
        {
            if (NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }
        }

        PixifeedTileManager.TileEnv te = new PixifeedTileManager.TileEnv();

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            tbLowmem.Visibility = te.isLowMem ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
            lpUser.IsEnabled = !te.isLowMem;
            tgToast.IsEnabled = !te.isLowMem;
            tgHQ.IsEnabled = !te.isLowMem;

            string periodicTaskName = "PixifeedTask";
            // Obtain a reference to the period task, if one exists
            PeriodicTask periodicTask = ScheduledActionService.Find(periodicTaskName) as PeriodicTask;

            // If the task already exists and background agents are enabled for the
            // application, you must remove the task and then add it again to update 
            // the schedule
            if (periodicTask != null)
            {
                try
                {
                    tbLast.Text = string.Format("Last run: {0}"
                    + Environment.NewLine + "Last error: {1}" + Environment.NewLine + "Scheduled: {2}"
                    + Environment.NewLine + "Expiration time: {3}",
                    periodicTask.LastScheduledTime, periodicTask.LastExitReason, periodicTask.IsScheduled,
                    periodicTask.ExpirationTime);
                }
                catch (Exception)
                {
                }
            }

            if (e.NavigationMode == NavigationMode.New)
            {
                tgHQ.Checked -= tgHQ_Checked;
                tgHQ.Unchecked -= tgHQ_Checked;
                tgToast.Checked -= tgHQ_Checked;
                tgToast.Unchecked -= tgHQ_Checked;
                tgWifi.Checked -= tgHQ_Checked;
                tgWifi.Unchecked -= tgHQ_Checked;

                lpUpdate.SelectionChanged -= lpUpdate_SelectionChanged;
                lpLive.SelectionChanged -= lpUpdate_SelectionChanged;
                lpCount.SelectionChanged -= lpUpdate_SelectionChanged;
                lpUser.SelectionChanged -= lpUpdate_SelectionChanged;

                if (lpUpdate.Items.Count == 0)
                {
                    lpUpdate.Items.Add(Res.LocalString.set_lpno);
                    lpUpdate.Items.Add(Res.LocalString.set_lp30);
                    lpUpdate.Items.Add(Res.LocalString.set_lp60);
                    lpUpdate.Items.Add(Res.LocalString.set_lp120);
                    //lpUpdate.Items.Add(Res.LocalString.set_lpintensive);
                }

                if (lpLive.Items.Count == 0)
                {
                    lpLive.Items.Add(Res.LocalString.set_lptile);
                    lpLive.Items.Add(Res.LocalString.set_lptile1);
                    lpLive.Items.Add(Res.LocalString.set_lptile2);
                    lpLive.Items.Add(Res.LocalString.set_lptile3);
                    lpLive.Items.Add(Res.LocalString.set_lptile4);
                    lpLive.Items.Add(Res.LocalString.set_lptile5);
                    lpLive.Items.Add(Res.LocalString.set_lptile6);
                }

                if (lpUser.Items.Count == 0)
                {
                    lpUser.Items.Add(Res.LocalString.set_lpuser);
                    lpUser.Items.Add(Res.LocalString.set_lpuser1);
                    lpUser.Items.Add(Res.LocalString.set_lpuser2);
                }


                if (lpUpdate.Items.Count > (int)te.Frequency)
                {
                    lpUpdate.SelectedIndex = Math.Max(0,(int)te.Frequency);
                }
                if (lpLive.Items.Count > (int)te.TileType)
                {
                    lpLive.SelectedIndex = (int)te.TileType;
                }
                if (lpUser.Items.Count > (int)te.PeopleType)
                {
                    lpUser.SelectedIndex = (int)te.PeopleType;
                }
                if (lpCount.Items.Contains(te.Count.ToString()))
                {
                    lpCount.SelectedIndex = lpCount.Items.IndexOf(te.Count.ToString());
                }

                tgHQ.IsChecked = te.HighQuality;
                tgToast.IsChecked = te.Toast;
                tgWifi.IsChecked = te.WiFi;

                tgHQ.Visibility = (te.TileType == PixifeedTileManager.TypeView.Recent || te.TileType == PixifeedTileManager.TypeView.Favorites
                || te.TileType == PixifeedTileManager.TypeView.Mypic) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
                
                this.Dispatcher.BeginInvoke(() =>
                {
                    tgHQ.Checked += tgHQ_Checked;
                    tgHQ.Unchecked += tgHQ_Checked;
                    tgToast.Checked += tgHQ_Checked;
                    tgToast.Unchecked += tgHQ_Checked;
                    tgWifi.Checked += tgHQ_Checked;
                    tgWifi.Unchecked += tgHQ_Checked;

                    lpUpdate.SelectionChanged += lpUpdate_SelectionChanged;
                    lpLive.SelectionChanged += lpUpdate_SelectionChanged;
                    lpCount.SelectionChanged += lpUpdate_SelectionChanged;
                    lpUser.SelectionChanged += lpUpdate_SelectionChanged;
                });
            }
        }

        void tgHQ_Checked(object sender, RoutedEventArgs e)
        {
            ToggleSwitch ts = sender as ToggleSwitch;
            if(ts != null)
            {
                bool b = ts.IsChecked == true;
                switch(ts.Name)
                {
                    case "tgHQ": te.HighQuality = b; break;
                    case "tgToast": te.Toast = b; break;
                    case "tgWifi": te.WiFi = b; break;
                }
            }
        }

        void lpUpdate_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListPicker lp = sender as ListPicker;
            if(lp == null)
            {
                return;
            }
            switch(lp.Name)
            {
                case "lpUpdate":
                    te.Frequency = (PixifeedTileManager.TaskFrequency)lp.SelectedIndex;

                    if (te.Frequency == PixifeedTileManager.TaskFrequency.NoTask)
                    {
                        string periodicTaskName = "PixifeedTask";
                        // Obtain a reference to the period task, if one exists
                        PeriodicTask periodicTask = ScheduledActionService.Find(periodicTaskName) as PeriodicTask;

                        // If the task already exists and background agents are enabled for the
                        // application, you must remove the task and then add it again to update 
                        // the schedule
                        if (periodicTask != null)
                        {
                            try
                            {
                                ScheduledActionService.Remove(periodicTaskName);
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }
                    else
                    {
                        string periodicTaskName = "PixifeedTask";
                        // Obtain a reference to the period task, if one exists
                        PeriodicTask periodicTask = ScheduledActionService.Find(periodicTaskName) as PeriodicTask;

                        // If the task already exists and background agents are enabled for the
                        // application, you must remove the task and then add it again to update 
                        // the schedule
                        if (periodicTask == null)
                        {
                            try
                            {
                                periodicTask = new PeriodicTask(periodicTaskName);
                                periodicTask.Description = "Pixifeed tile updator";
                                periodicTask.ExpirationTime = DateTime.Now.AddDays(13);

                                ScheduledActionService.Add(periodicTask);
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }

                    break;
                case "lpLive":
                    te.TileType = (PixifeedTileManager.TypeView)lp.SelectedIndex;
                    break;
                case "lpCount":
                    te.Count = Convert.ToInt32(lp.Items[lp.SelectedIndex]);
                    break;
                case "lpUser":
                    te.PeopleType = (PixifeedTileManager.TypePeople)lp.SelectedIndex;
                    break;
            }

            tgHQ.Visibility = (te.TileType == PixifeedTileManager.TypeView.Recent || te.TileType == PixifeedTileManager.TypeView.Favorites
                || te.TileType == PixifeedTileManager.TypeView.Mypic) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            bool fail = false;
            if(!Communication.isFull)
            {
                fail = true;
                if(MessageBox.Show(Res.LocalString.fullverdesc, Res.LocalString.fullver, MessageBoxButton.OKCancel)
                    == MessageBoxResult.OK)
                {
                    MarketplaceDetailTask mdt = new MarketplaceDetailTask();
                    mdt.Show();
                }
            }

            if (fail && NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }
        }
    }
}