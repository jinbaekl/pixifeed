﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Globalization;
using System.IO;
using System.Text;
using Microsoft.Phone.Marketplace;
using System.IO.IsolatedStorage;
using Microsoft.Phone.Tasks;
using System.Security;
using System.Threading.Tasks;

namespace ePix
{
    internal class GZipWebResponse : WebResponse
    {
        WebResponse response;
        internal GZipWebResponse(WebResponse resp)
        {
            response = resp;
        }
        public override System.IO.Stream GetResponseStream()
        {
            Stream st = response.GetResponseStream();
            //st.Position = 0;
            Stream sy = new Ionic.Zlib.GZipStream(st,Ionic.Zlib.CompressionMode.Decompress); //Uncompress
            //sy.Position = 0;
            return sy;
        }

        public override Uri ResponseUri
        {
            get
            {
                return response.ResponseUri;
            }
        }
    }

    public class ExtendedWebClient : WebClient
    {
        public CookieContainer CookieContainer { get; set; }

        [SecuritySafeCritical]
        public ExtendedWebClient()
        {
            //this.CookieContainer = new CookieContainer();
        }

        protected override WebRequest GetWebRequest(Uri address)
        {
            WebRequest request = base.GetWebRequest(address);

            if (request is HttpWebRequest)
                (request as HttpWebRequest).CookieContainer = this.CookieContainer;

            return request;
        }
    }

    public static class Communication
    {
        public static string useragent = string.Format("Mozilla/5.0 (Windows Phone OS 8.0; {1} {2}; Trident/7.0; Touch; rv:11.0) like Gecko", "1.0", Environment.OSVersion.Platform, Environment.OSVersion.Version, Environment.Version);
        private const string accpt = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
        public static string lasturl = "http://www.pixiv.net";
        public static int isAllowed = 1;
        public static int _isFull = -1;
        public static bool isFull
        {
            get
            {
                if (_isFull < 0)
                {
                    /*if (System.Diagnostics.Debugger.IsAttached)
                    {
                        _isFull = 0;
                    }
                    else
                    {*/
                        LicenseInformation li = new LicenseInformation();
                        _isFull = li.IsTrial() ? 0 : 1;
                    /*}*/
                }
                return (_isFull == 1);
            }
        }

        public static int _isInvert = -1;
        public static bool isInvert
        {
            get
            {
                /*if (_isInvert < 0)
                {
                    if (IsolatedStorageSettings.ApplicationSettings.Contains("invert"))
                    {
                        bool invert = (bool)IsolatedStorageSettings.ApplicationSettings["invert"];
                        _isInvert = invert ? 1 : 0;
                    }
                }
                return (_isInvert == 1);*/
                return false;
            }
        }

        private static bool _refresh = false;
        public static bool needRefresh { get { return _refresh; } set { _refresh = value; } }
        private static bool _login = false;
        public static bool isLogin { get { return _login; } set { _refresh = true; _login = value; } }
        public static bool? _t = null;
        public static bool Te
        {
            get
            {
                if (_t.HasValue)
                {
                    return _t.Value;
                }
                else
                {
                    if (IsolatedStorageSettings.ApplicationSettings.Contains("t"))
                    {
                        int m = (int)IsolatedStorageSettings.ApplicationSettings["t"];
                        _t = (m == 19);
                    }
                    else
                    {
                        _t = false;
                    }
                    return _t.Value;
                }
            }
        }
        public static CookieContainer ccMain = new CookieContainer();
        public static NetworkCredential ncMain = new NetworkCredential();
        public static HttpWebRequest Communicate(string target, Action<int, string> after)
        {
            return Communicate(target, after, true, string.Empty);
        }

        public static HttpWebRequest Communicate(string target, Action<int, string> after, bool updateReferer)
        {
            return Communicate(target, after, updateReferer, string.Empty);
        }

        public static HttpWebRequest Communicate(string target, Action<int, string> after, bool updateReferer, string speReferer)
        {
            if (isAllowed < 1)
            {
                after(-1, Res.LocalString.restrictedconnection);
                return null;
            }
            try
            {
                HttpWebRequest hwr = WebRequest.CreateHttp(target);
                //hwr.AllowAutoRedirect
                hwr.Method = "GET";
                hwr.Accept = accpt;
                hwr.CookieContainer = ccMain;
                hwr.Headers[HttpRequestHeader.CacheControl] = "max-age=0";
                hwr.Headers[HttpRequestHeader.AcceptEncoding] = "gzip";
                hwr.UserAgent = useragent;
                hwr.Credentials = ncMain;
                if(speReferer.Length > 0)
                {
                    hwr.Headers[HttpRequestHeader.Referer] = speReferer;
                }
                else
                {
                    hwr.Headers[HttpRequestHeader.Referer] = lasturl;
                }
                hwr.Headers[HttpRequestHeader.AcceptLanguage] = CultureInfo.CurrentCulture.Name;
                hwr.Headers[HttpRequestHeader.IfModifiedSince] = DateTime.UtcNow.ToString("r");
                hwr.BeginGetResponse((a) =>
                {
                    try
                    {
                        if (a == null || hwr == null)
                        {
                            return;
                        }
                        if (a.IsCompleted)
                        {
                            var resp = hwr.EndGetResponse(a);
                            if (resp.Headers[HttpRequestHeader.ContentEncoding] != null && resp.Headers[HttpRequestHeader.ContentEncoding].Contains("gzip"))
                                resp = new GZipWebResponse(resp); //If gzipped response, uncompress
                            if (updateReferer)
                            {
                                lasturl = resp.ResponseUri.OriginalString;
                            }
                            using (var stream = resp.GetResponseStream())
                            {
                                using (StreamReader sr = new StreamReader(stream))
                                {
                                    string got = sr.ReadToEnd();
                                    after(0, got);
                                }
                            }
                        }
                    }
                    catch (WebException ex)
                    {
                        after(-1, ex.Status.ToString());
                    }
                    catch (Exception ex)
                    {
                        after(-2, ex.GetType().ToString());
                    }
                }, null);
                return hwr;
            }
            catch (WebException ex)
            {
                after(-1, ex.Status.ToString());
            }
            catch (Exception ex)
            {
                after(-2, ex.GetType().ToString());
            }
            return null;
        }

        public static HttpWebRequest CommunicateWithoutCookie(string target, Action<int, string> after, bool updateReferer, string speReferer)
        {
            if (isAllowed < 1)
            {
                after(-1, Res.LocalString.restrictedconnection);
                return null;
            }
            try
            {
                HttpWebRequest hwr = WebRequest.CreateHttp(target);
                //hwr.AllowAutoRedirect
                hwr.Method = "GET";
                hwr.Accept = accpt;
                hwr.Headers[HttpRequestHeader.CacheControl] = "max-age=0";
                hwr.Headers[HttpRequestHeader.AcceptEncoding] = "gzip";
                hwr.UserAgent = useragent;
                hwr.Credentials = ncMain;
                if (speReferer.Length > 0)
                {
                    hwr.Headers[HttpRequestHeader.Referer] = speReferer;
                }
                else
                {
                    hwr.Headers[HttpRequestHeader.Referer] = lasturl;
                }
                hwr.Headers[HttpRequestHeader.AcceptLanguage] = CultureInfo.CurrentCulture.Name;
                hwr.Headers[HttpRequestHeader.IfModifiedSince] = DateTime.UtcNow.ToString("r");
                hwr.BeginGetResponse((a) =>
                {
                    try
                    {
                        if (a == null || hwr == null)
                        {
                            return;
                        }
                        if (a.IsCompleted)
                        {
                            var resp = hwr.EndGetResponse(a);
                            if (resp.Headers[HttpRequestHeader.ContentEncoding] != null && resp.Headers[HttpRequestHeader.ContentEncoding].Contains("gzip"))
                                resp = new GZipWebResponse(resp); //If gzipped response, uncompress
                            if (updateReferer)
                            {
                                lasturl = resp.ResponseUri.OriginalString;
                            }
                            using (var stream = resp.GetResponseStream())
                            {
                                using (StreamReader sr = new StreamReader(stream))
                                {
                                    string got = sr.ReadToEnd();
                                    after(0, got);
                                }
                            }
                        }
                    }
                    catch (WebException ex)
                    {
                        after(-1, ex.Status.ToString());
                    }
                    catch (Exception ex)
                    {
                        after(-2, ex.GetType().ToString());
                    }
                }, null);
                return hwr;
            }
            catch (WebException ex)
            {
                after(-1, ex.Status.ToString());
            }
            catch (Exception ex)
            {
                after(-2, ex.GetType().ToString());
            }
            return null;
        }

        public static HttpWebRequest CommunicatePOST(string target, string postdata, Action<int, string> after)
        {
            return CommunicatePOST(target, postdata, after, true, false);
        }

        public static HttpWebRequest CommunicatePOST(string target, string postdata, Action<int, string> after, bool updateReferer, bool json)
        {
            if (isAllowed < 1)
            {
                after(-1, Res.LocalString.restrictedconnection);
                return null;
            }
            try
            {
                HttpWebRequest hwr = WebRequest.CreateHttp(target);
                //hwr.AllowAutoRedirect
                hwr.Method = "POST";
                hwr.CookieContainer = ccMain;
                hwr.UserAgent = useragent;
                //hwr.Accept = accpt;
                hwr.Credentials = ncMain;
                hwr.Headers[HttpRequestHeader.Referer] = lasturl;
                hwr.Headers[HttpRequestHeader.AcceptEncoding] = "gzip";
                hwr.Headers["Origin"] = "http://www.pixiv.net";
                hwr.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
                if (json)
                {
                    hwr.Accept = "application/json, text/javascript, */*; q=0.01";
                    hwr.Headers["X-Requested-With"] = "XMLHttpRequest";
                }
                else
                {
                    hwr.Headers[HttpRequestHeader.CacheControl] = "max-age=0";
                    hwr.Accept = accpt;
                    hwr.Headers[HttpRequestHeader.IfModifiedSince] = DateTime.UtcNow.ToString("r");
                }
                hwr.Headers[HttpRequestHeader.AcceptLanguage] = CultureInfo.CurrentCulture.Name;
                hwr.BeginGetRequestStream((n) =>
                {
                    using (var reqstream = hwr.EndGetRequestStream(n))
                    {
                        using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(postdata)))
                        {
                            ms.Position = 0;
                            ms.CopyTo(reqstream);
                        }
                    }

                    hwr.BeginGetResponse((a) =>
                    {
                        try
                        {
                            if (a == null || hwr == null)
                            {
                                return;
                            }
                            if (a.IsCompleted)
                            {
                                var resp = hwr.EndGetResponse(a);
                                if (resp.Headers[HttpRequestHeader.ContentEncoding] != null && resp.Headers[HttpRequestHeader.ContentEncoding].Contains("gzip"))
                                    resp = new GZipWebResponse(resp); //If gzipped response, uncompress
                                if (updateReferer)
                                {
                                    lasturl = resp.ResponseUri.OriginalString;
                                }
                                using (var stream = resp.GetResponseStream())
                                {
                                    using (StreamReader sr = new StreamReader(stream))
                                    {
                                        string got = sr.ReadToEnd();
                                        after(0, got);
                                    }
                                }
                            }
                        }
                        catch (WebException ex)
                        {
                            after(-1, ex.Status.ToString());
                        }
                        catch (Exception ex)
                        {
                            after(-2, ex.GetType().ToString());
                        }
                    }, null);
                }, null);
                return hwr;
            }
            catch (WebException ex)
            {
                after(-1, ex.Status.ToString());
            }
            catch (Exception ex)
            {
                after(-2, ex.GetType().ToString());
            }
            return null;
        }

        public static HttpWebRequest CommunicateLogin(string target, string postdata, Action<int, string> after)
        {
            if (isAllowed < 1)
            {
                after(-1, Res.LocalString.restrictedconnection);
                return null;
            }
            try
            {
                HttpWebRequest hwr = WebRequest.CreateHttp(target);
                //hwr.AllowAutoRedirect
                hwr.Method = "POST";
                hwr.CookieContainer = ccMain;
                hwr.UserAgent = useragent;
                //hwr.Accept = accpt;
                hwr.Accept = "text/html, application/xhtml+xml, */*";
                hwr.Credentials = ncMain;
                hwr.Headers[HttpRequestHeader.Referer] = "http://www.pixiv.net/";
                hwr.Headers[HttpRequestHeader.AcceptEncoding] = "gzip";
                hwr.Headers[HttpRequestHeader.CacheControl] = "no-cache";
                hwr.ContentType = "application/x-www-form-urlencoded";
                hwr.Headers[HttpRequestHeader.IfModifiedSince] = DateTime.UtcNow.ToString("r");
                hwr.Headers[HttpRequestHeader.AcceptLanguage] = CultureInfo.CurrentCulture.Name;
                hwr.BeginGetRequestStream((n) =>
                {
                    using (var reqstream = hwr.EndGetRequestStream(n))
                    {
                        using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(postdata)))
                        {
                            ms.Position = 0;
                            ms.CopyTo(reqstream);
                        }
                    }

                    hwr.BeginGetResponse((a) =>
                    {
                        try
                        {
                            if (a == null || hwr == null)
                            {
                                return;
                            }
                            if (a.IsCompleted)
                            {
                                var resp = hwr.EndGetResponse(a);
                                if (resp.Headers[HttpRequestHeader.ContentEncoding] != null && resp.Headers[HttpRequestHeader.ContentEncoding].Contains("gzip"))
                                    resp = new GZipWebResponse(resp); //If gzipped response, uncompress
                                
                                using (var stream = resp.GetResponseStream())
                                {
                                    using (StreamReader sr = new StreamReader(stream))
                                    {
                                        string got = sr.ReadToEnd();
                                        after(0, got);
                                    }
                                }
                            }
                        }
                        catch (WebException ex)
                        {
                            after(-1, ex.Status.ToString());
                        }
                        catch (Exception ex)
                        {
                            after(-2, ex.GetType().ToString());
                        }
                    }, null);
                }, null);
                return hwr;
            }
            catch (WebException ex)
            {
                after(-1, ex.Status.ToString());
            }
            catch (Exception ex)
            {
                after(-2, ex.GetType().ToString());
            }
            return null;
        }

        public static HttpWebRequest CommunicateBinary(string target, Action<int, Stream, string> after)
        {
            if (isAllowed < 1)
            {
                after(-1, null, Res.LocalString.restrictedconnection);
                return null;
            }

            try
            {
                HttpWebRequest hwr = WebRequest.CreateHttp(target);
                //hwr.AllowAutoRedirect
                hwr.Method = "GET";
                hwr.Accept = accpt;
                hwr.CookieContainer = ccMain;
                hwr.Headers[HttpRequestHeader.AcceptEncoding] = "gzip";
                hwr.Headers[HttpRequestHeader.CacheControl] = "max-age=0";
                hwr.UserAgent = useragent;
                hwr.Headers[HttpRequestHeader.Referer] = lasturl;
                hwr.Headers[HttpRequestHeader.AcceptLanguage] = CultureInfo.CurrentCulture.Name;
                //hwr.Headers[HttpRequestHeader.IfModifiedSince] = DateTime.UtcNow.ToString("r");
                hwr.BeginGetResponse((a) =>
                {
                    try
                    {
                        if (a == null || hwr == null)
                        {
                            return;
                        }
                        if (a.IsCompleted)
                        {
                            var resp = hwr.EndGetResponse(a);
                            if (resp.Headers[HttpRequestHeader.ContentEncoding] != null && resp.Headers[HttpRequestHeader.ContentEncoding].Contains("gzip"))
                                resp = new GZipWebResponse(resp); //If gzipped response, uncompress
                            //lasturl = resp.ResponseUri.OriginalString;
                            using (var stream = resp.GetResponseStream())
                            {
                                MemoryStream ms = new MemoryStream();
                                stream.CopyTo(ms);
                                ms.Position = 0;
                                after(0, ms, resp.ResponseUri.OriginalString);
                            }
                        }
                    }
                    catch (WebException)
                    {
                        after(-1, null, string.Empty);
                    }
                    catch (Exception)
                    {
                        after(-2, null, string.Empty);
                    }
                }, null);
                return hwr;
            }
            catch (WebException)
            {
                after(-1, null, string.Empty);
            }
            catch (Exception)
            {
                after(-2, null, string.Empty);
            }
            return null;
        }

        public static WebClient CommunicateBinary(string target, Action<int, Stream, string> after, Action<int, string> progress)
        {
            if (isAllowed < 1)
            {
                after(-1, null, Res.LocalString.restrictedconnection);
                return null;
            }

            try
            {
                ExtendedWebClient wc = new ExtendedWebClient();
                wc.Headers[HttpRequestHeader.Accept] = accpt;
                wc.CookieContainer = ccMain;
                wc.Headers[HttpRequestHeader.AcceptEncoding] = "gzip";
                wc.Headers[HttpRequestHeader.CacheControl] = "max-age=0";
                wc.Headers[HttpRequestHeader.UserAgent] = useragent;
                wc.Headers[HttpRequestHeader.Referer] = lasturl;
                wc.Headers[HttpRequestHeader.AcceptLanguage] = CultureInfo.CurrentCulture.Name;
                wc.DownloadProgressChanged += (a,b) =>
                {
                    if(progress != null)
                    {
                        progress(b.ProgressPercentage, target);
                    }
                };
                wc.OpenReadCompleted += (n,a) =>
                {
                    try
                    {
                        if (a == null)
                        {
                            return;
                        }
                        if (a.Cancelled)
                        {
                            after(-1,null,"Cancelled");
                        }
                        else
                        {
                            if(wc.IsBusy)
                            {
                                return;
                            }
                            Stream sy = a.Result;
                            if (wc.ResponseHeaders[HttpRequestHeader.ContentEncoding] != null && wc.ResponseHeaders[HttpRequestHeader.ContentEncoding].Contains("gzip"))
                                sy = new Ionic.Zlib.GZipStream(sy,Ionic.Zlib.CompressionMode.Decompress); //If gzipped response, uncompress
                            //lasturl = resp.ResponseUri.OriginalString;
                            MemoryStream ms = new MemoryStream();
                            sy.CopyTo(ms);
                            ms.Position = 0;
                            after(0, ms, target);
                        }
                    }
                    catch (WebException)
                    {
                        after(-1, null, string.Empty);
                    }
                    catch (Exception)
                    {
                        after(-2, null, string.Empty);
                    }
                };
                wc.OpenReadAsync(new Uri(target));
                return wc;
            }
            catch (WebException)
            {
                after(-1, null, string.Empty);
            }
            catch (Exception)
            {
                after(-2, null, string.Empty);
            }
            return null;
        }

        public static HttpWebRequest CommunicateJSON(string target, Action<int, string> after)
        {
            if (isAllowed < 1)
            {
                after(-1, Res.LocalString.restrictedconnection);
                return null;
            }
            try
            {
                HttpWebRequest hwr = WebRequest.CreateHttp(target);
                //hwr.AllowAutoRedirect
                hwr.Method = "GET";
                hwr.Accept = "application/json, text/javascript, */*; q=0.01";
                hwr.CookieContainer = ccMain;
                hwr.Headers[HttpRequestHeader.CacheControl] = "max-age=0";
                hwr.Headers[HttpRequestHeader.AcceptEncoding] = "gzip";
                hwr.UserAgent = useragent;
                hwr.Headers["X-Requested-With"] = "XMLHttpRequest";
                hwr.Headers[HttpRequestHeader.Referer] = lasturl;
                hwr.Headers[HttpRequestHeader.AcceptLanguage] = CultureInfo.CurrentCulture.Name;
                hwr.Headers[HttpRequestHeader.IfModifiedSince] = DateTime.UtcNow.ToString("r");
                hwr.BeginGetResponse((a) =>
                {
                    try
                    {
                        if (a == null || hwr == null)
                        {
                            return;
                        }
                        if (a.IsCompleted)
                        {
                            var resp = hwr.EndGetResponse(a);
                            if (resp.Headers[HttpRequestHeader.ContentEncoding] != null && resp.Headers[HttpRequestHeader.ContentEncoding].Contains("gzip"))
                                resp = new GZipWebResponse(resp); //If gzipped response, uncompress
                            lasturl = resp.ResponseUri.OriginalString;
                            using (var stream = resp.GetResponseStream())
                            {
                                using (StreamReader sr = new StreamReader(stream))
                                {
                                    string got = sr.ReadToEnd();
                                    after(0, got);
                                }
                            }
                        }
                    }
                    catch (WebException ex)
                    {
                        after(-1, ex.Status.ToString());
                    }
                    catch (Exception ex)
                    {
                        after(-2, ex.GetType().ToString());
                    }
                }, null);
                return hwr;
            }
            catch (WebException ex)
            {
                after(-1, ex.Status.ToString());
            }
            catch (Exception ex)
            {
                after(-2, ex.GetType().ToString());
            }
            return null;
        }

  

    }
}
