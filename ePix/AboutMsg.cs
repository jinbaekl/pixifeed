﻿using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Xml;

namespace ePix
{

    /// <summary>
    /// AlertParser는 공지를 트위터에서 검색해서 가져옵니다.
    /// </summary>
    public class AlertParser
    {
        WebClient wc;
        EventHandler<ResultEventArgs> ehMon;
        public EventHandler<ResultEventArgs> Completed { get { return ehMon; } set { ehMon = value; } }
        public string LastMsg { get; set; }
        public string ThisMsg { get; set; }
        public string Category { get; set; }

        public class ResultEventArgs : EventArgs
        {
            public string result { get; set; }

            public ResultEventArgs(string s)
            {
                result = s;
            }
        }

        public AlertParser()
        {
            //
            // TODO: 여기에 생성자 논리를 추가합니다.
            //
            LastMsg = "";
            Category = string.Empty;
            wc = new WebClient();
            wc.DownloadStringCompleted += wc_DownloadStringCompleted;
        }

        void wc_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            if (e.Error == null && !e.Cancelled)
            {
                string sR = e.Result;
                sR = ParseMessage(sR);
                ThisMsg = sR;
                if (ehMon != null)
                {
                    if (LastMsg.Length == 0 || (LastMsg != sR))
                    {
                        Completed(this, new ResultEventArgs(sR));
                    }
                }
            }
        }

        private string ParseMessage(string original)
        {
            try
            {
                if (string.IsNullOrEmpty(original))
                {
                    return string.Empty;
                }
                string res = "";
                byte[] ba = Encoding.UTF8.GetBytes(original);
                MemoryStream ms = new MemoryStream(ba);
                ms.Position = 0;
                XmlReader xr = XmlReader.Create(ms);
                string result = string.Empty;
                while (xr.ReadToFollowing("item"))
                {
                    if (xr.ReadToFollowing("description"))
                    {
                        var assembly = Assembly.GetExecutingAssembly().FullName;
                        string[] rawver = assembly.Split('=')[1].Split(',')[0].Split('.');
                        string thisver = string.Format("{0}.{1}", rawver[0], rawver[1]);
                        double dver = Convert.ToDouble(thisver);

                        res = xr.ReadInnerXml();
                        res = HttpUtility.HtmlDecode(res);
                        if (res.Length > 0)
                        {
                            int iSVer = res.IndexOf("<minver>");
                            if (iSVer >= 0)
                            {
                                int iSSVer = iSVer + "<minver>".Length;
                                int iEVer = res.IndexOf("</minver>");
                                if (iEVer > iSSVer)
                                {
                                    string version = res.Substring(iSSVer, iEVer - iSSVer);
                                    try
                                    {
                                        double nver = Convert.ToDouble(version);
                                        if (nver > dver)
                                        {
                                            //요구 버전보다 낮다.
                                            return string.Empty;
                                        }
                                        else
                                        {
                                            res = res.Remove(iSVer, iEVer + "</minver>".Length - iSVer);
                                        }
                                    }
                                    catch { }
                                }
                            }

                            iSVer = res.IndexOf("<maxver>");
                            if (iSVer >= 0)
                            {
                                int iSSVer = iSVer + "<maxver>".Length;
                                int iEVer = res.IndexOf("</maxver>");
                                if (iEVer > iSSVer)
                                {
                                    string version = res.Substring(iSSVer, iEVer - iSSVer);
                                    try
                                    {
                                        double nver = Convert.ToDouble(version);
                                        if (nver > dver)
                                        {
                                            //요구 버전보다 낮다.
                                            return string.Empty;
                                        }
                                        else
                                        {
                                            res = res.Remove(iSVer, iEVer + "</maxver>".Length - iSVer);
                                        }
                                    }
                                    catch { }
                                }
                            }

                            const string langtag = "lang:{0}";
                            string searchkey = string.Format(langtag, CultureInfo.CurrentUICulture.TwoLetterISOLanguageName).ToLower();
                            int iSSearch = res.IndexOf("<" + searchkey + ">");
                            if (iSSearch < 0)
                            {
                                searchkey = "lang:en";
                                iSSearch = res.IndexOf("<" + searchkey + ">");
                            }

                            if (iSSearch >= 0)
                            {
                                int iESearch = res.IndexOf("</" + searchkey + ">");
                                if (iESearch > iSSearch)
                                {
                                    iSSearch += ("<" + searchkey + ">").Length;
                                    result = res.Substring(iSSearch, iESearch - iSSearch);
                                }
                            }
                            else
                            {
                                result = res;
                            }

                            int iDSearch = result.IndexOf("<div");
                            if (iDSearch > 0)
                            {
                                result = result.Remove(iDSearch);
                            }
                            result = result.Replace("<br>", Environment.NewLine);
                        }
                    }

                    if (xr.ReadToFollowing("category"))
                    {
                        string cat = xr.ReadInnerXml().Trim();
                        if (cat == Category)
                        {
                            return result;
                        }
                    }
                }
                return string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }

        public static void Retrive(Action<string> refresh)
        {
            try
            {
                var assembly = Assembly.GetExecutingAssembly().FullName;
                var ver = assembly.Split('=')[1].Split(',')[0].Split('.');
                string uri = string.Format("http://yeon.me/api/alert.php?id={0}&ver={1}.{2}&lang={3}",
                    "pixifeed", ver[0], ver[1], CultureInfo.CurrentUICulture.TwoLetterISOLanguageName.ToLower());
                Communication.Communicate(uri, (suc, res) =>
                {
                    if (suc >= 0 && res.Length > 0)
                    {
                        if (refresh != null)
                        {
                            refresh(HttpUtility.HtmlDecode(res));
                        }
                    }
                });
            }
            catch
            {

            }
        }
    }
}