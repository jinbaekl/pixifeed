﻿using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace ePix
{
    public partial class UserResult : PhoneApplicationPage
    {
        public UserResult()
        {
            InitializeComponent();

            this.Language = System.Windows.Markup.XmlLanguage.GetLanguage(System.Globalization.CultureInfo.CurrentUICulture.Name);
        }

        ObservableCollection<Contact> ocUser = new ObservableCollection<Contact>();
        const string uritemplate = "http://www.pixiv.net/search_user.php?s_mode=s_usr&nick={0}";
        const string urisug = "http://www.pixiv.net/search_user.php";
        string keyword = string.Empty;
        int nextpg = 1;
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (ocUser != null && ocUser.Count == 0)
            {
                if ((Application.Current.Resources["PhoneDarkThemeVisibility"] as Visibility?) == System.Windows.Visibility.Visible ^ Communication.isInvert)
                {
                    LayoutRoot.Background = new SolidColorBrush(Color.FromArgb(255, 37, 39, 41));
                    SystemTray.BackgroundColor = Color.FromArgb(255, 37, 39, 41);
                    SystemTray.ForegroundColor = Colors.White;
                }

                if (NavigationContext.QueryString.ContainsKey("q"))
                {
                    keyword = NavigationContext.QueryString["q"];
                }

                if (lbUser.ItemsSource == null)
                {
                    lbUser.ItemsSource = ocUser;
                }
                
                RefreshUsersPage(1);
            }
        }

        bool working = false;
        private void RefreshUsersPage(int page)
        {
            if (working)
            {
                return;
            }
            working = true;

            SystemTray.SetProgressIndicator(this, new ProgressIndicator()
            {
                IsIndeterminate = true,
                IsVisible = true,
                Text = Res.LocalString.retuser
            });
            nextpg = page;
            tbNothing.Visibility = System.Windows.Visibility.Collapsed;

            string targeturi = "";
            if (keyword.Length > 0)
            {
                targeturi = string.Format(uritemplate, HttpUtility.UrlEncode(keyword));
            }
            else
            {
                targeturi = urisug;
            }

            if(page > 1)
            {
                targeturi += "&p="+page.ToString();
            }

            Communication.Communicate(targeturi, (suc, res) =>
            {
                if (suc >= 0)
                {
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        Communication.isLogin = (res.IndexOf("user.loggedIn = true") > 0);
                        if(!Communication.isLogin)
                        {
                            MessageBox.Show(Res.LocalString.errorprofile, Res.LocalString.errorpixifeed, MessageBoxButton.OK);
                            if(NavigationService.CanGoBack)
                            {
                                NavigationService.GoBack();
                            }
                            return;
                        }
                        if (Interpreter.SearchUser(ref ocUser, (page == 1), res))
                        {
                            nextpg = page + 1;
                        }
                        else
                        {
                            if (page == 1)
                            {
                                tbNothing.Visibility = System.Windows.Visibility.Visible;
                            }
                        }
                    });
                }
                else
                {
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        try
                        {
                            MessageBox.Show(Res.LocalString.errorbang+ Environment.NewLine +
                                (res != "UnknownError" ? res : Res.LocalString.noconnection),
                                Res.LocalString.errorpixifeed, MessageBoxButton.OK);
                            if (NavigationService.CanGoBack)
                            {
                                NavigationService.GoBack();
                            }
                        }
                        catch { }
                    });
                }
                this.Dispatcher.BeginInvoke(() =>
                    {
                        SystemTray.SetProgressIndicator(this, null);

                        if (page == 1)
                        {
                            if (ocUser.Count > 0 && lbUser.ItemsSource == ocUser)
                            {
                                lbUser.ScrollTo(ocUser[0]);
                            }
                            else
                            {
                                lbUser.ItemsSource = ocUser;
                            }
                        }
                    });
                working = false;
            });
        }

        private void Grid_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Contact cm = (sender as Grid).DataContext as Contact;
            if (cm != null)
            {
                NavigationService.Navigate(new Uri("/People.xaml?id="
                    + cm.user_id, UriKind.Relative));
            }
        }


        private void ApplicationBarIconButton_Click(object sender, EventArgs e)
        {
            if (NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }
        }

        Rect lastview = Rect.Empty;
        int flast = -1;
        private void ViewportControl_ViewportChanged(object sender, ViewportChangedEventArgs e)
        {
            ViewportControl vc = sender as ViewportControl;
            lastview = vc.Viewport;
            if (ocUser != null && ocUser.Count > 0 && vc.Viewport.Bottom >= vc.Bounds.Bottom - 140 && flast != nextpg)
            {
                try
                {
                    RefreshUsersPage(nextpg);
                    flast = nextpg;
                }
                catch { }
            }
        }
    }
}