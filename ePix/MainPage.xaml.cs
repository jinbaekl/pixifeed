﻿using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;
using System.IO.IsolatedStorage;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Microsoft.Phone.Tasks;
using Microsoft.Phone.Net.NetworkInformation;
using System.Windows.Media;
using Windows.Networking.Connectivity;
using System.Windows.Media.Imaging;
using System.IO;
using HtmlAgilityPack;
using ePix.Model;
using System.Text;
using Microsoft.Phone.Scheduler;
using System.Diagnostics;
using System.Windows.Navigation;
using System.Windows.Media.Animation;

namespace ePix
{
    public partial class MainPage : PhoneApplicationPage
    {
        MainViewModel mvm = null;
        bool renew1 = false;
        bool init = false;

        // Constructor
        public MainPage()
        {
            InitializeComponent();
            //WebRequest.RegisterPrefix("http://", SharpGIS.WebRequestCreator.GZip);

            this.Language = System.Windows.Markup.XmlLanguage.GetLanguage(CultureInfo.CurrentUICulture.Name);
            hbTranslate.Content = string.Format(Res.LocalString.translatemore, CultureInfo.CurrentUICulture.EnglishName);
            hbTranslate.Visibility = (CultureInfo.CurrentUICulture.IsNeutralCulture && CultureInfo.CurrentUICulture.Name != "en-US")
                ? System.Windows.Visibility.Visible :
                System.Windows.Visibility.Collapsed;
            DeviceNetworkInformation.NetworkAvailabilityChanged += DeviceNetworkInformation_NetworkAvailabilityChanged;
            System.Net.NetworkInformation.NetworkChange.NetworkAddressChanged += NetworkChange_NetworkAddressChanged;

            mvm = new MainViewModel(this);
            lmRecomm.ItemsSource = mvm.ocRecomm;
            //            lbSuggest.ItemsSource = mvm.ocSuggest;

            if (ApplicationBar.Buttons.Count >= 3)
            {
                (ApplicationBar.Buttons[0] as ApplicationBarIconButton).Text = Res.LocalString.info;
                (ApplicationBar.Buttons[1] as ApplicationBarIconButton).Text = Res.LocalString.bsettings;
                (ApplicationBar.Buttons[2] as ApplicationBarIconButton).Text = Res.LocalString.refresh;
            }

            if (lpMethod.Items.Count >= 4)
            {
                lpMethod.Items[0] = Res.LocalString.bytag;
                lpMethod.Items[1] = Res.LocalString.bytitle;
                lpMethod.Items[2] = Res.LocalString.byid;
                lpMethod.Items[3] = Res.LocalString.inuser;
                lpMethod.Items[4] = Res.LocalString.byaid;
            }

            init = true;


        }

        void NetworkChange_NetworkAddressChanged(object sender, EventArgs e)
        {
            CheckConnection();
        }

        void DeviceNetworkInformation_NetworkAvailabilityChanged(object sender, NetworkNotificationEventArgs e)
        {
            CheckConnection();
        }

        /*private static void SetProperty(object instance, string name, object value)
        {
            var setMethod = instance.GetType().GetProperty(name).GetSetMethod();
            setMethod.Invoke(instance, new object[] { value });
        }*/

        //{Binding Path=localRes.pixiv, Source={StaticResource localString}}
        private void gbLogin_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (Communication.isLogin)
            {
                if (gbLogin.Tag != null)
                {
                    NavigationService.Navigate(new Uri("/People.xaml?id=" + gbLogin.Tag.ToString(), UriKind.Relative));
                }
            }
            else
            {
                NavigationService.Navigate(new Uri("/Login.xaml", UriKind.Relative));
            }
        }

        private void gbSearch_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (grSearch.Visibility == System.Windows.Visibility.Visible)
            {
                grSearch.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                grSearch.Visibility = System.Windows.Visibility.Visible;
            }
            e.Handled = true;
        }

        private void Panorama_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            grSearch.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void phoneApplicationPage_BackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (grSearch.Visibility == System.Windows.Visibility.Visible)
                {
                    e.Cancel = true;
                    grSearch.Visibility = System.Windows.Visibility.Collapsed;
                    return;
                }
            }
            catch { }
        }

        bool norefresh_nav = false;
        protected override void OnNavigatingFrom(System.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            if(e.Uri.OriginalString.Contains("Picker"))
            {
                norefresh_nav = true;
            }
            else
            {
                norefresh_nav = false;
            }

            base.OnNavigatingFrom(e);
        }

        bool welcome = false;
        List<string> lsSearch = new List<string>();
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            //트라이얼 확인 후 표시
            tbTrial.Visibility = Communication.isFull ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;            
            this.Dispatcher.BeginInvoke(() =>
            {
                //연결 확인
                CheckConnection();
            });

            if(norefresh_nav)
            {
                return;
            }

            if ((Application.Current.Resources["PhoneDarkThemeVisibility"] as Visibility?) == System.Windows.Visibility.Visible ^ Communication.isInvert)
            {
                //                pvMain.Background = new SolidColorBrush(Color.FromArgb(255, 98, 128, 150));
                pvMain.Background = new SolidColorBrush(Color.FromArgb(255, 37, 39, 41));
                SystemTray.BackgroundColor = Color.FromArgb(255, 37, 39, 41);
                SystemTray.ForegroundColor = Colors.White;
            } //배경 색상 지정

            if (Windows.Phone.ApplicationModel.ApplicationProfile.Modes == Windows.Phone.ApplicationModel.ApplicationProfileModes.Alternate || !IsolatedStorageSettings.ApplicationSettings.Contains("frun"))
            {
                NavigationService.Navigate(new Uri("/Warn.xaml", UriKind.Relative));
                return;
            } //첫 실행 or 어린이 모드 확인 후 경고창으로 점프. 끝.

            RefreshServerState(true, () =>
            {
                if (e.NavigationMode == System.Windows.Navigation.NavigationMode.New || e.NavigationMode == System.Windows.Navigation.NavigationMode.Refresh)
                {
                    if (NavigationContext.QueryString.ContainsKey("id"))
                    {
                        if (Communication.isLogin)
                        {
                            NavigationService.Navigate(new Uri("/People.xaml?id=" + NavigationContext.QueryString["id"],
                                UriKind.Relative));
                            return;
                        }
                        else
                        {
                            MessageBox.Show(Res.LocalString.errorprofile, Res.LocalString.errorpixifeed, MessageBoxButton.OK);
                        }
                    }
                    else if (NavigationContext.QueryString.ContainsKey("search"))
                    {
                        string[] sword = NavigationContext.QueryString["search"].Split(new char[] { '|' });
                        if (sword.Length > 2)
                        {
                            string keyword = "";
                            for (int i = 2; i < sword.Length; i++)
                            {
                                keyword += sword[i];
                            }
                            NavigationService.Navigate(new Uri(
                                string.Format("/SearchResult.xaml?tag={0}&m={1}&q={2}", sword[0], sword[1], keyword),
                                    UriKind.Relative));
                            return;
                        }
                    }
                    else if (NavigationContext.QueryString.ContainsKey("view"))
                    {
                        NavigationService.Navigate(new Uri("/ViewDetail.xaml?id=" + NavigationContext.QueryString["view"],
                            UriKind.Relative));
                        return;
                    }
                }
                ReadyMainPage(mvm.AllStuff);
            });

            if (NavigationContext.QueryString.ContainsKey("clearhistory") && e.NavigationMode == System.Windows.Navigation.NavigationMode.New)
            { //디테일뷰xaml에서 홈 버튼으로 넘어온 경우
                try
                {
                    while (NavigationService.CanGoBack)
                    {
                        NavigationService.RemoveBackEntry();
                    }
                }
                catch { }
                init = false;
            }

            using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (isf != null)
                {
                    btnError.Visibility = isf.FileExists("exception.txt") ? Visibility.Visible : Visibility.Collapsed;
                }
            }
                        

            SetBackgroundAgent();

            tboxKeyword.MinimumPrefixLength = 0;


            base.OnNavigatedTo(e);
        }

        void tbNotice_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (tbNotice.Tag == null)
            {
                IsolatedStorageSettings.ApplicationSettings["dismiss_alert"] = tbNotice.Text;
                IsolatedStorageSettings.ApplicationSettings.Save();
                if(sb.GetCurrentState() == ClockState.Active)
                {
                    sb.Stop();
                }
                sb.Children.Clear();
                bdNotice.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                MarketplaceDetailTask mdt = new MarketplaceDetailTask();
                mdt.Show();
            }
        }
        Storyboard sb = new Storyboard();

        private void SetBackgroundAgent()
        {
            string periodicTaskName = "PixifeedTask";
            // Obtain a reference to the period task, if one exists
            PeriodicTask periodicTask = ScheduledActionService.Find(periodicTaskName) as PeriodicTask;

            // If the task already exists and background agents are enabled for the
            // application, you must remove the task and then add it again to update 
            // the schedule
            if (periodicTask != null)
            {
                try
                {
                    if (periodicTask.ExpirationTime > DateTime.Now.AddDays(2)
                        && (periodicTask.LastExitReason == AgentExitReason.Completed || periodicTask.LastExitReason == AgentExitReason.None))
                    {
                        if (Debugger.IsAttached)
                        {
                            ScheduledActionService.LaunchForTest(periodicTaskName, new TimeSpan(0, 0, 30));
                        }

                        return;
                    }

                    ScheduledActionService.Remove(periodicTaskName);
                }
                catch (Exception)
                {
                }
            }

            PixifeedTileManager.TileEnv te = new PixifeedTileManager.TileEnv();
            if (te.Frequency == PixifeedTileManager.TaskFrequency.NotInitialized)
            {
                te.Frequency = PixifeedTileManager.TaskFrequency.NoTask;
                if (Communication.isFull)
                {
                    if (MessageBox.Show(Res.LocalString.confirm_task, Res.LocalString.confirm_taskcap, MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                    {
                        NavigationService.Navigate(new Uri("/Settings.xaml", UriKind.Relative));
                    }
                }
                return;
            }
            else if (te.Frequency == PixifeedTileManager.TaskFrequency.NoTask)
            {
                return;
            }

            if (!Communication.isFull)
            {
                return;
            }

            periodicTask = new PeriodicTask(periodicTaskName);
            periodicTask.Description = "Pixifeed tile updator";
            periodicTask.ExpirationTime = DateTime.Now.AddDays(13);

            // Place the call to Add in a try block in case the user has disabled agents.
            try
            {
                ScheduledActionService.Add(periodicTask);
                ScheduledActionService.LaunchForTest(periodicTaskName, TimeSpan.FromSeconds(15));
            }
            catch (InvalidOperationException exception)
            {
                Debug.WriteLine("Background Agent Error!");
                if (exception.Message.Contains("BNS Error: The action is disabled"))
                {
                    //MessageBox.Show("Background agents for this application have been disabled by the user.");
                    //agentsAreEnabled = false;
                }
                if (exception.Message.Contains("BNS Error: The maximum number of ScheduledActions of this type have already been added."))
                {
                    // No user action required. The system prompts the user when the hard limit of periodic tasks has been reached.
                }
            }
            catch (SchedulerServiceException)
            {
                // No user action required.
            }
        }

        private void CheckConnection()
        {
            this.Dispatcher.BeginInvoke(() =>
            {
                try
                {
                    ConnectionProfile cp = NetworkInformation.GetInternetConnectionProfile();
                    if (!NetworkInterface.GetIsNetworkAvailable())
                    {
                        Communication.isAllowed = 1;
                        tbPaid.Visibility = Visibility.Visible;
                        tbPaid.Text = Res.LocalString.noconnection;
                    }
                    else if (cp.NetworkAdapter.IanaInterfaceType != 71) //Not Wi-Fi
                    {
                        tbPaid.Visibility = Visibility.Visible;
                        ConnectionCost cc = cp.GetConnectionCost();
                        if (Communication.isAllowed != 2 && (cc.NetworkCostType == NetworkCostType.Fixed || cc.NetworkCostType == NetworkCostType.Variable)
                            && (cc.ApproachingDataLimit || cc.OverDataLimit || cc.Roaming))
                        {
                            Communication.isAllowed = 0;
                            tbPaid.Text = Res.LocalString.restrictedconnection + Res.LocalString.ignoreconnection;
                        }
                        else
                        {
                            if (Communication.isAllowed != 2)
                            {
                                Communication.isAllowed = 1;
                            }
                            tbPaid.Text = Res.LocalString.paidconnection;
                        }
                    }
                    else
                    {
                        Communication.isAllowed = 1;
                        tbPaid.Visibility = Visibility.Collapsed;
                    }
                }
                catch { }
            });
        }

        private void RefreshSuggestUsers()
        {
            if (mvm.AllStuff.Length > 0)
            {
                Interpreter.SuggestUsers(ref mvm.ocSuggest, mvm.AllStuff);
            }
        }

        private void RefreshServerState(bool silent, Action after = null)
        {
            ProgressIndicator pi = new ProgressIndicator()
            {
                IsIndeterminate = true,
                Text = Res.LocalString.retmain,
                IsVisible = true
            };
            SystemTray.ProgressIndicator = pi;
            SystemTray.SetIsVisible(this, true);
            Communication.Communicate("http://www.pixiv.net/", (res, stuff) =>
            {
                if (res >= 0)
                {
                    mvm.AllStuff = stuff;
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        Communication.isLogin = (stuff.IndexOf("user.loggedIn = true") > 0);
                        if (!Communication.isLogin && IsolatedStorageSettings.ApplicationSettings.Contains("id") && IsolatedStorageSettings.ApplicationSettings.Contains("pass"))
                        {
                            RenewImages();

                            string mid = HttpUtility.UrlEncode(IsolatedStorageSettings.ApplicationSettings["id"] as string);
                            string pass = HttpUtility.UrlEncode(IsolatedStorageSettings.ApplicationSettings["pass"] as string);
                            Login.LoginSilent(mid, pass, this, () =>
                            {
                                this.Dispatcher.BeginInvoke(() =>
                                {
                                    Communication.Communicate("http://www.pixiv.net/", (com2, result2) =>
                                    {
                                        if (com2 >= 0)
                                        {
                                            this.Dispatcher.BeginInvoke(() =>
                                            {
                                                mvm.AllStuff = result2;
                                                if (after != null)
                                                {
                                                    after();
                                                }
                                                else
                                                {
                                                    ReadyMainPage(result2);
                                                }
                                                SystemTray.SetIsVisible(this, false);
                                                SystemTray.ProgressIndicator = null;
                                            });
                                        }
                                    });
                                });
                            });
                            return;
                        }
                        else
                        {
                            mvm.AllStuff = stuff;
                            if (after != null)
                            {
                                after();
                                this.Dispatcher.BeginInvoke(() =>
                                {
                                    SystemTray.SetIsVisible(this, false);
                                    SystemTray.ProgressIndicator = null;
                                });
                            }
                            else
                            {
                                ReadyMainPage(stuff);
                            }
                        }
                    });
                }
                else
                {
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        try
                        {
                            if (!silent)
                            {
                                MessageBox.Show(Res.LocalString.errorbang + Environment.NewLine +
                                    (stuff != "UnknownError" ? stuff : Res.LocalString.noconnection),
                                    Res.LocalString.errorpixifeed, MessageBoxButton.OK);
                            }
                        }
                        catch { }
                        SystemTray.SetIsVisible(this, false);
                        SystemTray.ProgressIndicator = null;
                    });
                }
            });
        }

        private void ReadyMainPage(string stuff)
        {
            Interpreter.TodayBanner(stuff, (ban, tap) =>
            {
                this.Dispatcher.BeginInvoke(() =>
                {
                    tapbanner = tap;
                    if (ban != null)
                    {
                        if (imgBan.Source == null)
                        {
                            BitmapSource bs = new BitmapImage(new Uri(ban));
                            imgBan.Source = bs;
                            tbTitle.Opacity = 0;
                        }
                    }
                    else
                    {
                        imgBan.Source = null;
                        tbTitle.Opacity = 1;
                    }
                });
            });
            Communication.isLogin = (stuff.IndexOf("user.loggedIn = true") > 0);

            HtmlDocument hd = new HtmlDocument();
            hd.LoadHtml(stuff);
            HtmlNode hnSec = hd.DocumentNode.SelectSingleNode("//section[@class=\"_unit my-profile-unit\"]");
            if (hnSec != null)
            {
                HtmlNode hnA = hnSec.ChildNodes["a"];
                if (hnA != null)
                {
                    string link = hnA.GetAttributeValue("href", "");
                    int iId = link.IndexOf("?id=");
                    if (iId >= 0)
                    {
                        iId += 4;
                        int iEd = link.IndexOf("&", iId);
                        if (iEd < iId)
                        {
                            iEd = link.Length;
                        }
                        gbLogin.Tag = link.Substring(iId, iEd - iId);
                    }
                }
            }


            tbLogin.Text = Communication.isLogin ? Res.LocalString.myinfo : Res.LocalString.signin;
            phImg.Header = Communication.isLogin ? Res.LocalString.newone : Res.LocalString.recommend;
            gbSuggest.Visibility = Communication.isLogin ? Visibility.Visible : System.Windows.Visibility.Collapsed;
            RefreshSuggestUsers();
            Communication.needRefresh = false;
            renew1 = true;

            if (pvMain.SelectedIndex == 1)
            {
                RenewImages();
                renew1 = false;
            }
        }

        string tapbanner = null;
        void imgBan_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if(tapbanner != null && tapbanner.StartsWith("http"))
            {
                NavigationService.Navigate(new Uri("/Browser.xaml?uri=" + HttpUtility.UrlEncode(tapbanner), UriKind.Relative));
            }
        }

        private void Panorama_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (mvm != null)
            {
                if (pvMain.SelectedIndex == 0)
                {
                    this.ApplicationBar.IsVisible = true;
                    if (gdAd.Tag == null && !Communication.isFull)
                    {
                        adDuplexAd.Visibility = System.Windows.Visibility.Visible;
                        gdAd.Visibility = System.Windows.Visibility.Visible;
                    }
                }
                else
                {
                    this.ApplicationBar.IsVisible = false;
                    grSearch.Visibility = System.Windows.Visibility.Collapsed;
                    adDuplexAd.Visibility = System.Windows.Visibility.Collapsed;
                    gdAd.Visibility = System.Windows.Visibility.Collapsed;
                }
                if (mvm.AllStuff.Length > 0)
                {
                    if (pvMain.SelectedItem == piTag)
                    {
                        lbTag.ItemsSource = null;
                        Interpreter.HotTags(ref mvm.ocTag, mvm.AllStuff);
                        lbTag.ItemsSource = mvm.ocTag;
                    }
                    else
                    {
                        if (renew1 || mvm.ocRecomm.Count == 0)
                        {
                            RenewImages();
                            renew1 = false;
                        }
                        btnMore.Visibility = Communication.isLogin ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
                    }
                }
            }
        }

        private void RenewImages()
        {
            int iLast = -1;
            if (lmRecomm.SelectedItem != null)
            {
                iLast = mvm.ocRecomm.IndexOf(lmRecomm.SelectedItem as PictureModel);
            }

            bool isGrouped = false;
            Interpreter.LatestImages(ref mvm.ocRecomm, mvm.AllStuff, out isGrouped);
            if (isGrouped)
            {
                lmRecomm.IsGroupingEnabled = true;
                if(mvm.ocRecomm_back != null && mvm.ocRecomm_back.Count > 0)
                {
                    foreach(var item in mvm.ocRecomm_back)
                    {
                        mvm.ocRecomm.Add(item);
                    }
                }
                lmRecomm.ItemsSource = Grouping<PictureModel>.CreateGroups(mvm.ocRecomm,
                    (a) => { return a.Category; });
            }
            else
            {
                lmRecomm.IsGroupingEnabled = false;
                mvm.ocRecomm_back.Clear();
                mvm.ocRecomm_back.AddRange(mvm.ocRecomm);
                lmRecomm.ItemsSource = mvm.ocRecomm;
            }
        }

        private void gbRank_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/RankPage.xaml", UriKind.Relative));
        }

        private void liUser_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Contact cm = (sender as StackPanel).DataContext as Contact;
            if (cm != null)
            {
                NavigationService.Navigate(new Uri("/People.xaml?id="
                    + cm.user_id, UriKind.Relative));
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshServerState(false);
        }

        private void lbSuggest_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((sender as ListBox).SelectedItem != null)
            {
                Contact cm = (sender as ListBox).SelectedItem as Contact;
                if (cm != null)
                {
                    NavigationService.Navigate(new Uri("/People.xaml?id="
                        + cm.user_id, UriKind.Relative));
                }
            }
        }

        private void btnInfo_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/AboutPage.xaml", UriKind.Relative));
        }

        private void gbSuggest_Tap_1(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/PeopleList.xaml", UriKind.Relative));
        }

        private void tboxKeyword_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && (sender as AutoCompleteBox).Text.Length > 0)
            {
                if (lpMethod.SelectedIndex >= 0 && lpMethod.SelectedIndex < 2)
                {
                    string[] method = new string[] { "s_tag", "s_tc" };
                    NavigationService.Navigate(new Uri("/SearchResult.xaml?m=" + HttpUtility.UrlEncode(method[lpMethod.SelectedIndex]) +
                        "&q=" + HttpUtility.UrlEncode((sender as AutoCompleteBox).Text), UriKind.Relative));
                }
                else if (lpMethod.SelectedIndex == 2)
                {
                    int dummy = 0;
                    if (Int32.TryParse(tboxKeyword.Text, out dummy))
                    {
                        NavigationService.Navigate(new Uri("/ViewDetail.xaml?id=" + tboxKeyword.Text, UriKind.Relative));
                    }
                }
                else if (lpMethod.SelectedIndex == 3)
                {
                    NavigationService.Navigate(new Uri("/UserResult.xaml?q=" + HttpUtility.UrlEncode((sender as AutoCompleteBox).Text), UriKind.Relative));
                }
                else if (lpMethod.SelectedIndex == 4)
                {
                    int dummy = 0;
                    if (Int32.TryParse(tboxKeyword.Text, out dummy))
                    {
                        NavigationService.Navigate(new Uri("/People.xaml?id=" + tboxKeyword.Text, UriKind.Relative));
                    }
                }
            }
        }

        private void tbTrial_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            MarketplaceDetailTask mdt = new MarketplaceDetailTask();
            mdt.Show();
        }

        private void tboxKeyword_Populating(object sender, PopulatingEventArgs e)
        {
            e.Cancel = true;
            if (lpMethod.SelectedIndex != 2)
            {
                SearchKeyword();
            }
        }

        private void SearchKeyword()
        {
            try
            {
                if (Interpreter.token.Length == 0 || tboxKeyword.SearchText.Length == 0)
                {
                    return;
                }
                int iSkeyword = tboxKeyword.SearchText.LastIndexOfAny(new char[] { ' ' });
                iSkeyword = (iSkeyword < 0) ? 0 : iSkeyword;
                string prior = tboxKeyword.SearchText.Substring(0, iSkeyword);
                iSkeyword = (iSkeyword > 0) ? iSkeyword + 1 : 0;
                string query = HttpUtility.UrlEncode(tboxKeyword.SearchText.Remove(0, iSkeyword));
                Communication.Communicate(
                    string.Format("http://www.pixiv.net/rpc/cps.php?keyword={0}&tt={1}",
                    query, Interpreter.token)
                    ,
                    (a, b) =>
                    {
                        if (a >= 0 && b.StartsWith("{"))
                        {
                            List<string> lk = JSONParser.JSONToKeywords(b, prior);
                            this.Dispatcher.BeginInvoke(() =>
                            {
                                tboxKeyword.ItemsSource = lk;
                                tboxKeyword.PopulateComplete();
                            });
                        }
                    },
                    false);
            }
            catch
            {

            }

        }

        private void tbPaid_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (Communication.isAllowed == 0)
            {
                Communication.isAllowed = 2;
                tbPaid.Visibility = System.Windows.Visibility.Collapsed;
                CheckConnection();
                RefreshServerState(false);
            }
        }

        private void htbError_Click(object sender, RoutedEventArgs e)
        {
            string m = string.Empty;
            using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (isf != null)
                {
                    using (IsolatedStorageFileStream isfs = new IsolatedStorageFileStream("exception.txt", System.IO.FileMode.Open,
                             isf))
                    {
                        if (isfs.CanRead)
                        {
                            using (StreamReader sw = new StreamReader(isfs))
                            {
                                m = sw.ReadToEnd();
                            }
                        }
                    }
                    isf.DeleteFile("exception.txt");
                }
            }
            if (m.Length > 0)
            {
                EmailComposeTask emt = new EmailComposeTask();
                emt.Subject = "[앱]"+(Communication.isFull ? "Pixifeed+": "Pixifeed")+" error reporting!";
                emt.To = "yygworld@outlook.com";
                emt.Body = Res.LocalString.reporting + Environment.NewLine + Environment.NewLine + m;
                emt.Show();
            }
        }

        private void hbTranslate_Click(object sender, RoutedEventArgs e)
        {
            EmailComposeTask ect = new EmailComposeTask();
            ect.Subject = "Sending translating page";
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Here is a link to document page of translating:");
            sb.AppendLine("http://goo.gl/9xKQIi");
            sb.AppendLine();
            sb.AppendLine("Send this mail to yourself to contribute on your desktop computer.");
            ect.Body = sb.ToString();
            ect.Show();
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Settings.xaml", UriKind.Relative));
        }

        private void phoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            if (!NavigationContext.QueryString.ContainsKey("id"))
            {
                bool run = false;
                if (PhoneApplicationService.Current.State.ContainsKey("run")
                    && PhoneApplicationService.Current.State["run"] != null)
                {
                    run = (bool)PhoneApplicationService.Current.State["run"];
                }
                if (run && ((int)IsolatedStorageSettings.ApplicationSettings["count"]) % 5 == 4)
                {
                    if (!welcome)
                    {
                        if (!Communication.isFull)
                        {
                            HyperlinkButton hyperlinkButton = new HyperlinkButton()
                            {
                                Content = Res.LocalString.letsfullbtn,
                                Margin = new Thickness(0, 28, 0, 8),
                                HorizontalAlignment = HorizontalAlignment.Left
                            };
                            TiltEffect.SetIsTiltEnabled(hyperlinkButton, true);
                            hyperlinkButton.Click += (a, b) =>
                            {
                                MarketplaceDetailTask mdt = new MarketplaceDetailTask();
                                mdt.Show();
                            };
                            this.BackKeyPress -= phoneApplicationPage_BackKeyPress;
                            CustomMessageBox cmb = new CustomMessageBox()
                            {
                                Language = System.Windows.Markup.XmlLanguage.GetLanguage(CultureInfo.CurrentUICulture.Name),
                                Caption = Res.LocalString.letsfull,
                                Message = string.Format(Res.LocalString.letsfullcap, (DateTime.Now.Year - 1992)),
                                LeftButtonContent = Res.LocalString.ok,
                                IsRightButtonEnabled = false,
                                IsLeftButtonEnabled = true,
                                Content = hyperlinkButton
                            };
                            cmb.Dismissed += (a, b) =>
                            {
                                //this.BackKeyPress += phoneApplicationPage_BackKeyPress;

                                if (!IsolatedStorageSettings.ApplicationSettings.Contains("star"))
                                {
                                    CustomMessageBox cmbm = new CustomMessageBox()
                                    {
                                        Language = System.Windows.Markup.XmlLanguage.GetLanguage(CultureInfo.CurrentUICulture.Name),
                                        Caption = Res.LocalString.ratetitle,
                                        Message = Res.LocalString.ratedesc.Replace("/", Environment.NewLine),
                                        LeftButtonContent = Res.LocalString.rate,
                                        RightButtonContent = Res.LocalString.later,
                                        IsRightButtonEnabled = true,
                                        IsLeftButtonEnabled = true
                                    };
                                    cmbm.Dismissed += (am, bm) =>
                                    {
                                        if (bm.Result == CustomMessageBoxResult.LeftButton)
                                        {
                                            MarketplaceReviewTask mrt = new MarketplaceReviewTask();
                                            mrt.Show();
                                            IsolatedStorageSettings.ApplicationSettings["star"] = true;
                                        }
                                        //this.BackKeyPress += phoneApplicationPage_BackKeyPress;
                                        adDuplexAd.Visibility = Communication.isFull ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
                                        gdAd.Visibility = Communication.isFull ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
                                    };
                                    this.BackKeyPress -= phoneApplicationPage_BackKeyPress;
                                    cmbm.Show();
                                }
                                else if (gdAd.Tag == null && pvMain.SelectedIndex == 0)
                                {
                                    adDuplexAd.Visibility = Communication.isFull ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
                                    gdAd.Visibility = Communication.isFull ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
                                }
                            };
                            cmb.Show();
                        }
                        else if (!IsolatedStorageSettings.ApplicationSettings.Contains("star"))
                        {
                            CustomMessageBox cmb = new CustomMessageBox()
                            {
                                Language = System.Windows.Markup.XmlLanguage.GetLanguage(CultureInfo.CurrentUICulture.Name),
                                Caption = Res.LocalString.ratetitle,
                                Message = Res.LocalString.ratedesc.Replace("/", Environment.NewLine),
                                LeftButtonContent = Res.LocalString.rate,
                                RightButtonContent = Res.LocalString.later,
                                IsRightButtonEnabled = true,
                                IsLeftButtonEnabled = true
                            };
                            cmb.Dismissed += (a, b) =>
                            {
                                if (b.Result == CustomMessageBoxResult.LeftButton)
                                {
                                    MarketplaceReviewTask mrt = new MarketplaceReviewTask();
                                    mrt.Show();
                                    IsolatedStorageSettings.ApplicationSettings["star"] = true;
                                }
                                //this.BackKeyPress += phoneApplicationPage_BackKeyPress;

                                if (gdAd.Tag == null && pvMain.SelectedIndex == 0)
                                {
                                    adDuplexAd.Visibility = Communication.isFull ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
                                    gdAd.Visibility = Communication.isFull ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
                                }
                            };
                            this.BackKeyPress -= phoneApplicationPage_BackKeyPress;
                            cmb.Show();
                        }
                        else if (gdAd.Tag == null && pvMain.SelectedIndex == 0)
                        {
                            adDuplexAd.Visibility = Communication.isFull ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
                            gdAd.Visibility = Communication.isFull ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
                        }
                        welcome = true;
                    }
                    PhoneApplicationService.Current.State["run"] = false;
                }
                else if (gdAd.Tag == null && pvMain.SelectedIndex == 0)
                {
                    adDuplexAd.Visibility = Communication.isFull ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
                    gdAd.Visibility = Communication.isFull ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
                }
            }
            else if(gdAd.Tag == null && pvMain.SelectedIndex == 0)
            {
                adDuplexAd.Visibility = Communication.isFull ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
                gdAd.Visibility = Communication.isFull ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
            }

            bdNotice.Visibility = System.Windows.Visibility.Collapsed;
            tbNotice.Tap -= tbNotice_Tap;
            AlertParser.Retrive((a) =>
            {
                this.Dispatcher.BeginInvoke(() =>
                {
                    if (string.IsNullOrEmpty(a) || (IsolatedStorageSettings.ApplicationSettings.Contains("dismiss_alert") && IsolatedStorageSettings.ApplicationSettings["dismiss_alert"].ToString() == a))
                    {
                        bdNotice.Visibility = System.Windows.Visibility.Collapsed;
                        return;
                    }

                    if (a.Trim().StartsWith("<"))
                    {
                        return;
                    }

                    bdNotice.Visibility = System.Windows.Visibility.Visible;
                    tbNotice.Tap += tbNotice_Tap;

                    if (a.StartsWith("*"))
                    {
                        tbNotice.Text = a.Substring(1, a.Length - 1);
                        tbNotice.Tag = "*";
                    }
                    else
                    {
                        tbNotice.Text = a;
                        tbNotice.Tag = null;
                    }

                    if (sb.GetCurrentState() == ClockState.Active)
                    {
                        sb.Stop();
                    }
                    if (sb.Children.Count == 0)
                    {
                        DoubleAnimation da = new DoubleAnimation();
                        da.From = 480;
                        da.To = -tbNotice.ActualWidth;
                        //da.AutoReverse = true;
                        da.Duration = new Duration(new TimeSpan(0, 0, Convert.ToInt32((480 + tbNotice.ActualWidth) / 60)));
                        da.RepeatBehavior = new RepeatBehavior(2);
                        Storyboard.SetTarget(da, ctNotice);
                        Storyboard.SetTargetProperty(da, new PropertyPath("(CompositeTransform.TranslateX)"));
                        sb.Children.Add(da);
                    }
                    sb.Begin();
                });
            });
        }

        private void bdAdClose_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            gdAd.Tag = "ff";
            adDuplexAd.Visibility = System.Windows.Visibility.Collapsed;
            gdAd.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void phoneApplicationPage_Unloaded(object sender, RoutedEventArgs e)
        {
            if (sb != null)
            {
                sb.Stop();
                sb.Children.Clear();
            }
        }

    }
}