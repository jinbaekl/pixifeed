﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Scheduler;
using Microsoft.Phone.Shell;
using System.Linq;
using System.IO.IsolatedStorage;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.Phone.Info;

namespace ePix
{
    public enum Resolutions { WVGA, WXGA, HD720p, HD1080p };

    public static class ResolutionHelper
    {
        static private Size _size;

        private static bool IsWvga
        {
            get
            {
                return App.Current.Host.Content.ScaleFactor == 100;
            }
        }

        private static bool IsWxga
        {
            get
            {
                return App.Current.Host.Content.ScaleFactor == 160;
            }
        }

        private static bool Is720p
        {
            get
            {
                return (App.Current.Host.Content.ScaleFactor == 150 && !Is1080p);
            }
        }

        private static bool Is1080p
        {
            get
            {
                if (_size.Width == 0)
                {
                    try
                    {
                        _size = (Size)DeviceExtendedProperties.GetValue("PhysicalScreenResolution");
                    }
                    catch (Exception)
                    {
                        _size.Width = 0;
                    }
                }
                return _size.Width == 1080;
            }
        }

        public static Resolutions CurrentResolution
        {
            get
            {
                if (IsWvga) return Resolutions.WVGA;
                else if (IsWxga) return Resolutions.WXGA;
                else if (Is720p) return Resolutions.HD720p;
                else if (Is1080p) return Resolutions.HD1080p;
                else throw new InvalidOperationException("Unknown resolution");
            }
        }

        public static double ScaleFactor
        {
            get
            {
                if (_size.Width == 0)
                {
                    try
                    {
                        _size = (Size)DeviceExtendedProperties.GetValue("PhysicalScreenResolution");
                    }
                    catch (Exception)
                    {
                        _size.Width = 480;
                    }
                }
                return _size.Width / 480.0;
            }
        }

        public static Size Size
        {
            get
            {
                if (_size.Width == 0)
                {
                    try
                    {
                        _size = (Size)DeviceExtendedProperties.GetValue("PhysicalScreenResolution");
                    }
                    catch (Exception)
                    {
                        _size.Width = 480;
                    }
                }
                return _size;
            }
        }
    }

    public static class CommonMethod
    {
        /// <summary>
        /// 파싱 중 태그와 속성을 검색합니다.
        /// </summary>
        /// <param name="tagname">검색할 태그 이름입니다.</param>
        /// <param name="inside">태그 속에서 찾을 속성입니다.</param>
        /// <param name="stapos">찾기를 시작할 위치입니다.(zerobase)</param>
        /// <param name="rawdata">대상 전체 문자열입니다.</param>
        /// <returns>찾은 결과물의 위치입니다.</returns>
        public static int SearchTagAttributes(string tagname, string inside, int stapos, string rawdata)
        {
            return SearchTagAttributes(tagname, inside, stapos, rawdata.Length, rawdata);
        }

        /// <summary>
        /// 파싱 중 태그와 속성을 검색합니다.
        /// </summary>
        /// <param name="tagname">검색할 태그 이름입니다.</param>
        /// <param name="inside">태그 속에서 찾을 속성입니다.</param>
        /// <param name="stapos">찾기를 시작할 위치입니다.(zerobase)</param>
        /// <param name="endpos">찾기를 제한할 끝 위치입니다.</param>
        /// <param name="rawdata">대상 전체 문자열입니다.</param>
        /// <returns>찾은 결과물의 위치입니다.</returns>
        public static int SearchTagAttributes(string tagname, string inside, int stapos, int endpos, string rawdata)
        {
        gogo:
            int first = rawdata.IndexOf("<" + tagname, stapos, endpos - stapos, StringComparison.InvariantCultureIgnoreCase);
            if (first >= 0)
            {
                int last = rawdata.IndexOf(">", first + 1);
                if (last >= 0)
                {
                    int res = rawdata.IndexOf(inside, first + 1, last - first - 1, StringComparison.InvariantCultureIgnoreCase);
                    if (res >= 0)
                    {
                        return first;
                    }
                    else
                    {
                        if (last + 1 < rawdata.Length)
                        {
                            stapos = last + 1;
                            goto gogo;
                        }
                    }
                }
            }
            return -1;
        }

        /// <summary>
        /// 파싱 중 태그와 속성을 검색하고, 그 태그의 끝 위치를 반환합니다.
        /// </summary>
        /// <param name="tagname">검색할 태그 이름입니다.</param>
        /// <param name="inside">태그 속에서 찾을 속성입니다.</param>
        /// <param name="stapos">찾기를 시작할 위치입니다.(zerobase)</param>
        /// <param name="endpos">찾기를 제한할 끝 위치입니다.</param>
        /// <param name="rawdata">대상 전체 문자열입니다.</param>
        /// <returns>찾은 결과물의 위치입니다.</returns>
        public static int SearchTagAttributesEnd(string tagname, string inside, int stapos, int endpos, string rawdata)
        {
        gogo:
            int first = rawdata.IndexOf("<" + tagname, stapos, endpos - stapos, StringComparison.InvariantCultureIgnoreCase);
            if (first >= 0)
            {
                int last = rawdata.IndexOf(">", first + 1);
                if (last >= 0)
                {
                    int res = rawdata.IndexOf(inside, first + 1, last - first - 1, StringComparison.InvariantCultureIgnoreCase);
                    if (res >= 0)
                    {
                        return last + 1;
                    }
                    else
                    {
                        if (last + 1 < rawdata.Length)
                        {
                            stapos = last + 1;
                            goto gogo;
                        }
                    }
                }
            }
            return -1;
        }

        public static int SearchTagAttributesEnd(string tagname, string inside, int stapos, string rawdata)
        {
            return SearchTagAttributesEnd(tagname, inside, stapos, rawdata.Length, rawdata);
        }

        public static string GetTagAttribute(string key, string tagstr)
        {
            int a = tagstr.IndexOf('<');
            if (a >= 0)
            {
                int b = tagstr.IndexOf('>');
                if (b >= 0)
                {
                    int c = tagstr.IndexOf(' ', a);
                    if (c >= 0)
                    {
                        string[] att = tagstr.Substring(c+1, b - c - 1).Split(new char[] { ' ' });
                        for (int i = 0; i < att.Length; i++)
                        {
                            string inatt = att[i];
                            int iEqual = inatt.IndexOf('=');
                            if (iEqual >= 0)
                            {
                                string skey = inatt.Substring(0, iEqual);
                                if (skey == key)
                                {
                                    string sv = inatt.Substring(iEqual + 1, inatt.Length - iEqual - 1);
                                    char quot = sv[0];
                                    int istart = 0;
                                    int iend = 0;
                                    if (quot == '\'' || quot == '"')
                                    {
                                        istart++;
                                    retry:
                                        iend = sv.LastIndexOf(quot);
                                        if(iend <= 0 && i + 1 < att.Length)
                                        {
                                            i++;
                                            sv += (" " + att[i]);
                                            goto retry;
                                        }
                                    }
                                    else
                                    {
                                        iend = sv.Length;
                                    }
                                    if (iend > 0)
                                    {
                                        return sv.Substring(istart, iend - istart);
                                    }
                                    
                                }
                            }
                        }
                    }
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// 태그로 감싸져 있는 문자열의 바깥쪽 태그만을 모두 삭제합니다.
        /// </summary>
        /// <param name="outer">작업을 할 대상 전체 문자열입니다.</param>
        /// <returns>바깥쪽 짝이 맞는 태그가 제거된 문자열입니다.</returns>
        public static string ExtractTagInside(string outer)
        {
            int tagstart = outer.IndexOf('<');
            if (tagstart >= 0)
            {
                int tagsemiend = outer.IndexOf('>', tagstart + 1);
                if (tagsemiend >= 0)
                {
                    int tagend = outer.LastIndexOf('>');
                    if (tagend >= 0)
                    {
                        int tagsemistart = outer.LastIndexOf('<', tagend - 1);
                        if (tagsemistart >= 0)
                        {
                            return outer.Substring(tagsemiend + 1, tagsemistart - tagsemiend - 1);
                        }
                    }
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// 주어진 문자열 속 모든 태그를 삭제합니다.
        /// </summary>
        /// <param name="target">태그가 섞인 문자열입니다.</param>
        /// <returns>태그 형태를 띤 모든 문자가 제거된 문자열입니다.</returns>
        public static string RemoveAllTags(string target)
        {
            int tagstart = target.IndexOf('<');
            while (tagstart >= 0)
            {
                if (tagstart + 1 >= target.Length)
                {
                    break;
                }

                int tagend = target.IndexOf('>', tagstart);
                if (tagend >= 0)
                {
                    if (char.IsLetter(target[tagstart + 1])
                        || target[tagstart + 1] == '/'
                        || target[tagstart + 1] == '!'
                        || target[tagstart + 1] == '-')
                    {
                        target = target.Remove(tagstart, tagend + 1 - tagstart);
                    }
                    else
                    {
                        if (tagend + 1 <= target.Length)
                        {
                            tagstart = tagend + 1;
                        }
                    }
                }
                else
                {
                    break;
                }
                tagstart = target.IndexOf('<', tagstart);
            }
            target = target.Replace("&nbsp;", "");
            target = target.Replace("  ", "");
            target = target.Replace("	", "");
            return target.Trim();
        }

        public static decimal ExcludeExceptNum(string target)
        {
            if (target.Length == 0)
            {
                return -1;
            }
            else
            {
                return Convert.ToDecimal(Regex.Replace(target, @"[\D]", ""));
            }
        }

        public static string TodayURL()
        {
            return string.Format("{0:0000}{1:00}{2:00}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
        }

        static PeriodicTask periodicTask;
        static string periodicTaskName = "realtimeKPX";
        public static bool agentsEnabled = false;

        public static bool StartPeriodicAgent(object target)
        {
            // Variable for tracking enabled status of background agents for this app.
            //agentsAreEnabled = true;

            // Obtain a reference to the period task, if one exists
            periodicTask = ScheduledActionService.Find(periodicTaskName) as PeriodicTask;

            // If the task already exists and background agents are enabled for the
            // application, you must remove the task and then add it again to update 
            // the schedule
            if (periodicTask != null)
            {
                RemoveAgent();
            }

            periodicTask = new PeriodicTask(periodicTaskName);
            // The description is required for periodic agents. This is the string that the user
            // will see in the background services Settings page on the device.
            periodicTask.Description = "실시간 전력망 정보를 주기적으로 가져와서 라이브 타일 뒷면에 나타냅니다.";

            // Place the call to Add in a try block in case the user has disabled agents.
            try
            {
                ScheduledActionService.Add(periodicTask);
                //PeriodicStackPanel.DataContext = periodicTask;

                // If debugging is enabled, use LaunchForTest to launch the agent in one minute.
#if DEBUG
                ScheduledActionService.LaunchForTest(periodicTaskName, TimeSpan.FromSeconds(10));
#endif
                return true;
            }
            catch (InvalidOperationException exception)
            {
                agentsEnabled = false;
                if (exception.Message.Contains("BNS Error: The action is disabled"))
                {
                    if (target != null)
                    {
                        (target as DependencyObject).Dispatcher.BeginInvoke(() =>
                        {
                            MessageBox.Show("사용자 설정에 의해 전력망예보 라이브 타일 실행이 금지되었습니다. 휴대폰 설정 -> 응용프로그램 -> 백그라운드 작업에서 확인해보세요.");
                        });
                    }
                    //PeriodicCheckBox.IsChecked = false;
                }

                if (exception.Message.Contains("BNS Error: The maximum number of ScheduledActions of this type have already been added."))
                {
                    // No user action required. The system prompts the user when the hard limit of periodic tasks has been reached.
                }
                //PeriodicCheckBox.IsChecked = false;
                return false;
            }
            catch (SchedulerServiceException)
            {
                // No user action required.
                //PeriodicCheckBox.IsChecked = false;
                agentsEnabled = false;
                return false;
            }
        }

        public static void RemoveAgent()
        {
            try
            {
                StandardTileData std = new StandardTileData()
                {
                    Title = "전력망예보",
                    BackgroundImage = new Uri("/Background.png", UriKind.Relative),
                    BackTitle = null,
                    Count = 0,
                    BackBackgroundImage = new Uri("/", UriKind.Relative),
                    BackContent = null
                };
                ShellTile.ActiveTiles.First().Update(std);
            }
            catch{}
            try{
                ScheduledActionService.Remove(periodicTaskName);
            }
            catch (Exception)
            {
            }
        }

        /*public static string SaveToIsolatedStorage(int type, string target, int itemnum, Action<int, string> setAfter)
        {
            using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (type == 0) //Image
                {
                    if (!isf.DirectoryExists("Pics"))
                    {
                        isf.CreateDirectory("Pics");
                    }
                    string cachefile = @"\Pics\" + System.IO.Path.GetFileName(target.ToString()).Replace("?", "").Replace("&", "").Replace("=", "_");
                    if (isf.FileExists(cachefile))
                    {
                        return cachefile;
                    }

                    Communication.CommunicateBinary(target.ToString(),
                        (status, result) =>
                        {
                            if (status >= 0)
                            {
                                using (IsolatedStorageFile isfm = IsolatedStorageFile.GetUserStoreForApplication())
                                {
                                    using (IsolatedStorageFileStream isfs = isfm.OpenFile(cachefile, System.IO.FileMode.OpenOrCreate))
                                    {
                                        result.CopyTo(isfs);
                                        result.Close();
                                        setAfter(itemnum, cachefile);
                                    }
                                }
                            }
                        });
                    return cachefile;
                }
                return null;
            }
        }*/
    }
}
