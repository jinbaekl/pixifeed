﻿using ePix.Model;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Info;
using Microsoft.Phone.Shell;
using System;
using System.Diagnostics;
using System.IO;
using System.IO.IsolatedStorage;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Navigation;

namespace ePix
{
    class AssociationUriMapper : UriMapperBase
    {
        private string tempUri;

        public override Uri MapUri(Uri uri)
        {
            tempUri = System.Net.HttpUtility.UrlDecode(uri.ToString());

            // URI association launch for contoso.
            if (tempUri.Contains("pixiv:illust_id="))
            {
                // Get the category ID (after "CategoryID=").
                int categoryIdIndex = tempUri.IndexOf("illust_id=") + 10;
                string categoryId = tempUri.Substring(categoryIdIndex);
                if(string.IsNullOrEmpty(categoryId))
                {
                    return new Uri("/MainPage.xaml", UriKind.Relative);
                }
                // Map the show products request to ShowProducts.xaml
                return new Uri("/MainPage.xaml?view=" + categoryId, UriKind.Relative);
            }
            else if (tempUri.Contains("pixiv:member_id="))
            {
                int categoryIdIndex = tempUri.IndexOf("member_id=") + 10;
                string categoryId = tempUri.Substring(categoryIdIndex);
                if (string.IsNullOrEmpty(categoryId))
                {
                    return new Uri("/MainPage.xaml", UriKind.Relative);
                }
                return new Uri("/MainPage.xaml?id=" + categoryId, UriKind.Relative);
            }
            else if (tempUri.Contains("pixiv:search="))
            {
                int categoryIdIndex = tempUri.IndexOf("search=") + 7;
                string categoryId = tempUri.Substring(categoryIdIndex);
                if (string.IsNullOrEmpty(categoryId))
                {
                    return new Uri("/MainPage.xaml", UriKind.Relative);
                }
                return new Uri("/MainPage.xaml?search=0|1|" + categoryId, UriKind.Relative);
            }
            else if (tempUri.Contains("pixiv:"))
            {
                // Get the category ID (after "CategoryID=").
                int categoryIdIndex = tempUri.IndexOf("pixiv:") + 6;
                string categoryId = tempUri.Substring(categoryIdIndex);
                if (string.IsNullOrEmpty(categoryId))
                {
                    return new Uri("/MainPage.xaml", UriKind.Relative);
                }
                else
                {
                    int num = -1;
                    if(int.TryParse(categoryId, out num))
                    {
                        return new Uri("/MainPage.xaml?view=" + num.ToString(), UriKind.Relative);
                    }
                    else
                    {
                        return new Uri("/MainPage.xaml?search=0|1|" + categoryId, UriKind.Relative);
                    }
                }
                // Map the show products request to ShowProducts.xaml
                return new Uri("/MainPage.xaml?view=" + categoryId, UriKind.Relative);
            }

            if (tempUri.StartsWith("/Protocol"))
            {
                // Otherwise perform normal launch.
                return new Uri("/MainPage.xaml", UriKind.Relative);
            }
            else
            {
                return uri;
            }
        }
    }

    public partial class App : Application
    {
        /// <summary>
        /// Provides easy access to the root frame of the Phone Application.
        /// </summary>
        /// <returns>The root frame of the Phone Application.</returns>
        public static PhoneApplicationFrame RootFrame { get; private set; }
        System.Threading.Timer tm = null;

        /// <summary>
        /// Constructor for the Application object.
        /// </summary>
        public App()
        {
            // Global handler for uncaught exceptions. 
            UnhandledException += Application_UnhandledException;

            //Application.Current.Resources["MyBackgroundBrush"] = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
            //Application.Current.Resources["PhoneForegroundBrush"] = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));

            //Application.Current.Resources.Remove("MyBackgroundBrush");
            //Application.Current.Resources.Add("MyBackgroundBrush", new SolidColorBrush(Color.FromArgb(255, 0, 0, 0)));
            //Application.Current.Resources.Remove("PhoneForegroundBrush");
            //Application.Current.Resources.Add("PhoneForegroundBrush", new SolidColorBrush(Color.FromArgb(255, 255, 255, 255)));

            // Standard Silverlight initialization
            InitializeComponent();

            // Phone-specific initialization
            InitializePhoneApplication();

            // Show graphics profiling information while debugging.
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // Display the current frame rate counters.
                //Application.Current.Host.Settings.EnableFrameRateCounter = true;

                // Show the areas of the app that are being redrawn in each frame.
                //Application.Current.Host.Settings.EnableRedrawRegions = true;

                // Enable non-production analysis visualization mode, 
                // which shows areas of a page that are handed off to GPU with a colored overlay.
                //Application.Current.Host.Settings.EnableCacheVisualization = true;

                // Disable the application idle detection by setting the UserIdleDetectionMode property of the
                // application's PhoneApplicationService object to Disabled.
                // Caution:- Use this under debug mode only. Application that disables user idle detection will continue to run
                // and consume battery power when the user is not using the phone.
                PhoneApplicationService.Current.UserIdleDetectionMode = IdleDetectionMode.Disabled;

                
            }
            if (System.Diagnostics.Debugger.IsAttached)
            {
                //Perodic report or manage of memory
                tm = new System.Threading.Timer((a) =>
                {
                    //GC.Collect();
                    if (System.Diagnostics.Debugger.IsAttached)
                    {
                        Debug.WriteLine("{0}, {1}, {2}, {3}",
                        DeviceStatus.ApplicationCurrentMemoryUsage / 1024.0 / 1024.0,
                        DeviceStatus.ApplicationPeakMemoryUsage / 1024.0 / 1024.0,
                        DeviceStatus.ApplicationMemoryUsageLimit / 1024.0 / 1024.0,
                        DeviceStatus.DeviceTotalMemory / 1024.0 / 1024.0);
                    }
                }, null, 0, (System.Diagnostics.Debugger.IsAttached) ? 1000 : 3000);
            }
        }

        // Code to execute when the application is launching (eg, from Start)
        // This code will not execute when the application is reactivated
        private void Application_Launching(object sender, LaunchingEventArgs e)
        {
            int cnt = 0;
            if (IsolatedStorageSettings.ApplicationSettings.Contains("count")
                && IsolatedStorageSettings.ApplicationSettings["count"] != null
                && IsolatedStorageSettings.ApplicationSettings["count"] is int)
            {
                cnt = (int)IsolatedStorageSettings.ApplicationSettings["count"];
            }
            cnt++;
            IsolatedStorageSettings.ApplicationSettings["count"] = cnt;
            IsolatedStorageSettings.ApplicationSettings.Save();
            PhoneApplicationService.Current.State["run"] = true;
            ticklauch = DateTime.Now;
        }

        // Code to execute when the application is activated (brought to foreground)
        // This code will not execute when the application is first launched
        private void Application_Activated(object sender, ActivatedEventArgs e)
        {
            Communication._isFull = -1;
        }

        // Code to execute when the application is deactivated (sent to background)
        // This code will not execute when the application is closing
        private void Application_Deactivated(object sender, DeactivatedEventArgs e)
        {
        }

        // Code to execute when the application is closing (eg, user hit Back)
        // This code will not execute when the application is deactivated
        private void Application_Closing(object sender, ClosingEventArgs e)
        {
        }

        // Code to execute if a navigation fails
        private void RootFrame_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // A navigation has failed; break into the debugger
                System.Diagnostics.Debugger.Break();
            }
        }

        // Code to execute on Unhandled Exceptions
        private void Application_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            SaveErrorLog(e.ExceptionObject);

            if (System.Diagnostics.Debugger.IsAttached)
            {
                // An unhandled exception has occurred; break into the debugger
                System.Diagnostics.Debugger.Break();
            }
        }

        public static void SaveErrorLog(Exception ex)
        {
            using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (isf != null)
                {
                    using (IsolatedStorageFileStream isfs = new IsolatedStorageFileStream("exception.txt", System.IO.FileMode.Append,
                        isf))
                    {
                        if (isfs.CanWrite)
                        {
                            using (StreamWriter sw = new StreamWriter(isfs))
                            {
                                sw.WriteLine();
                                sw.WriteLine("=== {1} on {0} ===", DateTime.Now, ex.GetType().ToString());
                                sw.WriteLine(ex.Message);
                                sw.WriteLine(ex.HResult);
                                sw.WriteLine();
                                sw.WriteLine(ex.StackTrace);
                                sw.WriteLine();
                                if (ex.InnerException != null)
                                {
                                    sw.WriteLine("Internal Error: {0}", ex.InnerException.GetType().ToString());
                                    sw.WriteLine(ex.InnerException);
                                }
                                sw.WriteLine();
                                sw.WriteLine("== Current state ==");
                                sw.WriteLine("Apptick: {0}", (DateTime.Now - ticklauch).ToString("g"));
                                try
                                {
                                    if (RootFrame != null)
                                    {
                                        sw.WriteLine(RootFrame.CurrentSource.OriginalString);
                                    }
                                }
                                catch (Exception exe) { sw.WriteLine("Unknown as following error: " + exe.ToString()); }
                                sw.WriteLine();
                                sw.WriteLine("== Mem ==");
                                sw.WriteLine("Limit: {0}MB",
                                    Microsoft.Phone.Info.DeviceStatus.ApplicationMemoryUsageLimit / 1024.0 / 1024.0);
                                sw.WriteLine("Peak: {0}MB",
                                    Microsoft.Phone.Info.DeviceStatus.ApplicationPeakMemoryUsage / 1024.0 / 1024.0);
                                sw.WriteLine("Now: {0}MB",
                                    Microsoft.Phone.Info.DeviceStatus.ApplicationCurrentMemoryUsage / 1024.0 / 1024.0);
                                sw.WriteLine("Total: {0}MB",
                                    Microsoft.Phone.Info.DeviceStatus.DeviceTotalMemory / 1024.0 / 1024.0);
                                sw.WriteLine();
                                sw.WriteLine("== DeviceInfo ==");
                                sw.WriteLine("Manufacture: {0}", Microsoft.Phone.Info.DeviceStatus.DeviceManufacturer);
                                sw.WriteLine("Name: {0}", Microsoft.Phone.Info.DeviceStatus.DeviceName);
                                sw.WriteLine("App: {0}", System.Reflection.Assembly.GetExecutingAssembly().FullName);
                                sw.WriteLine("Devicever: {0}", Microsoft.Phone.Info.DeviceStatus.DeviceFirmwareVersion);
                                sw.WriteLine("OS: {0} {1}", Environment.OSVersion.Platform, Environment.OSVersion.Version);
                                sw.WriteLine("Runtime: {0}", Environment.Version);
                            }
                        }
                    }
                }
            }
        }

        #region Phone application initialization

        // Avoid double-initialization
        private bool phoneApplicationInitialized = false;
        private static DateTime _ticklauch = DateTime.MinValue;
        private static DateTime ticklauch
        {
            get
            {
                if (_ticklauch == DateTime.MinValue)
                {
                    if (PhoneApplicationService.Current.State.ContainsKey("tick"))
                    {
                        _ticklauch = (DateTime)PhoneApplicationService.Current.State["tick"];
                    }
                }
                return _ticklauch;
            }
            set
            {
                _ticklauch = value;
                PhoneApplicationService.Current.State["tick"] = _ticklauch;
            }
        }

        // Do not add any additional code to this method
        private void InitializePhoneApplication()
        {
            if (phoneApplicationInitialized)
                return;

            // Create the frame but don't set it as RootVisual yet; this allows the splash
            // screen to remain active until the application is ready to render.
            RootFrame = new TransitionFrame();
            RootFrame.Navigated += CompleteInitializePhoneApplication;
            RootFrame.Navigating += RootFrame_Navigating;
            RootFrame.Navigated += RootFrame_Navigated;

            // Assign the URI-mapper class to the application frame.
            RootFrame.UriMapper = new AssociationUriMapper();

            // Handle navigation failures
            RootFrame.NavigationFailed += RootFrame_NavigationFailed;

            // Ensure we don't initialize again
            phoneApplicationInitialized = true;
        }

        void RootFrame_Navigated(object sender, NavigationEventArgs e)
        {
            //2번의 if문에서 안 걸린다면 Navigated가 연달아 발생합니다.
            if(e.NavigationMode == NavigationMode.New && reseting)
            {
                reseting = false;
                norepair = false;
                if (e.Uri.OriginalString.Contains("MainPage.xaml"))
                {
                    while (RootFrame.RemoveBackEntry() != null) ;
                }
                //백 버튼 히스토리를 초기화합니다. 저는 어차피 세컨 타일도 메인 페이지를 거치기 때문에 이렇게 처리해도
                //메인 페이지로 한 번은 나올 수 있습니다.
                //만약 세컨 타일이 메인이 아닌 다른 페이지로 연결되어 있고, 백 버튼 눌러서 메인으로 나가게 구현하시려면,
                //최상위 메인을 남기시든지 다른 방법을 찾으셔야 할 겁니다.
                
                //백 버튼 히스토리를 지워주지 않으면 재실행해도 히스토리가 마구마구 쌓여나가기 때문에 혼란스러워지실겁니다.
            }

            //* 탐색 추적용
            /*IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication();
            if (isf != null)
            {
                using (IsolatedStorageFileStream isfs = new IsolatedStorageFileStream("exception.txt", System.IO.FileMode.Append,
                    isf))
                {
                    if (isfs.CanWrite)
                    {
                        using (StreamWriter sw = new StreamWriter(isfs))
                        {
                            sw.WriteLine("Navigated:");
                            sw.WriteLine(e.IsNavigationInitiator ? "Inside" : "Outside");
                            sw.WriteLine(e.NavigationMode);
                            sw.WriteLine(e.Uri);
                            sw.WriteLine();
                        }
                    }
                }
                isf.Dispose();
            }*/
            //**
        }

        bool reseting = false, norepair = false;
        void RootFrame_Navigating(object sender, NavigatingCancelEventArgs e)
        {
            //1. 타일이나 앱을 재실행하려 하면 e.NMode = Reset, e.Uri = 이전 페이지로 우선 Navigate합니다.
            if (e.NavigationMode == NavigationMode.Reset)
            {
                reseting = true;
                //이를 통해 곧 새로운 페이지(아마 타일에 따라 메인페이지)로 이동할 것임을 알 수 있습니다.
                //아무 것도 안 하고 탈출.

                if(e.Uri.OriginalString.Contains("Settings.xaml") || e.Uri.OriginalString.IndexOf("MainPage.xaml?") >= 0)
                {
                    // Settings.xaml이나 바로가기가 딸린(인수 포함) 메인 페이지는 복구하지 않음.
                    // (마지막 페이지 예외 지정) 2에서 활용
                    norepair = true;
                }

                int cnt = 0; //실행 카운트 추가
                if (IsolatedStorageSettings.ApplicationSettings.Contains("count")
                    && IsolatedStorageSettings.ApplicationSettings["count"] != null
                    && IsolatedStorageSettings.ApplicationSettings["count"] is int)
                {
                    cnt = (int)IsolatedStorageSettings.ApplicationSettings["count"];
                }
                cnt++;
                IsolatedStorageSettings.ApplicationSettings["count"] = cnt;
                //return;
            }
            //2. 곧 이어 e.NMode = New, e.Uri = 새로운 페이지로 다시 이벤트가 발생합니다.
            else if (e.NavigationMode == NavigationMode.New && reseting)
            {
                if (e.Uri.OriginalString.EndsWith("MainPage.xaml")
                    && Windows.Phone.ApplicationModel.ApplicationProfile.Modes != Windows.Phone.ApplicationModel.ApplicationProfileModes.Alternate
                    && !norepair)
                    //어린이 모드가 아니고, 메인 페이지로 탐색한다면. (메인 타일이나 앱 실행시) + 마지막 페이지가 예외가 아니라면
                {
                    //하지만 메인 페이지로 간다면 무시하고 이전 페이지를 그대로 보여줍니다.
                    //프라이머리 타일은 항상 메인페이지로 연결되니까, 이 때 탐색을 취소하는 겁니다.
                    //이렇게 하면 멀티태스킹으로 활성화한 것과 똑같은 결과가 됩니다.
                    //탐색을 취소하고 탈출. 1번 -> 2번 없이 위 Navigated로 이동합니다.
                    
                    //같은 페이지면 무조건 재탐색하지 않도록 만들기 위해서는 1번에서 Uri를 변수에 저장해뒀다가
                    //2번에서 Uri를 확인해서 동일하면 이와같이 탈출하도록 만드실 수 있습니다.
                    reseting = false;
                    e.Cancel = true;
                }
                //그러나 세컨 타일 등으로 다른 페이지 탐색이 지정되어 있으면 여기선 아무 것도 안 합니다.
            }

            //* 탐색 추적용
            /*IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication();
            if (isf != null)
            {
                using (IsolatedStorageFileStream isfs = new IsolatedStorageFileStream("exception.txt", System.IO.FileMode.Append,
                    isf))
                {
                    if (isfs.CanWrite)
                    {
                        using (StreamWriter sw = new StreamWriter(isfs))
                        {
                            sw.WriteLine("Navigating:");
                            sw.WriteLine(e.IsNavigationInitiator ? "Inside" : "Outside");
                            sw.WriteLine(e.NavigationMode);
                            sw.WriteLine(e.Uri);
                            sw.WriteLine(e.IsCancelable ? "Cancelable" : "Non-cancelable");
                            sw.WriteLine(e.Cancel ? "Canceled":"Accepted");
                            sw.WriteLine();
                        }
                    }
                }
                isf.Dispose();
            }*/
            //**
        }

        // Do not add any additional code to this method
        private void CompleteInitializePhoneApplication(object sender, NavigationEventArgs e)
        {
            // Set the root visual to allow the application to render
            if (RootVisual != RootFrame)
                RootVisual = RootFrame;

            // Remove this handler since it is no longer needed
            RootFrame.Navigated -= CompleteInitializePhoneApplication;
        }

        #endregion

        private void image_ImageFailed(object sender, ExceptionRoutedEventArgs e)
        {
            //Debug.WriteLine("Image Failed");
        }

        private void Grid_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            try
            {
                PictureModel pm = (sender as Grid).DataContext as PictureModel;
                if (pm != null)
                {
                    (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/ViewDetail.xaml?id="
                        + pm.ID + "&thumb=" + System.Net.HttpUtility.UrlEncode(pm.Pic), UriKind.Relative));
                }
            }
            catch
            { } // Nothing available
        }

        private void UserGrid_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Contact cm = (sender as Grid).DataContext as Contact;
            if (cm != null)
            {
                (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/People.xaml?id="
                    + cm.user_id, UriKind.Relative));
            }
        }

        internal static void SetTranslationToken(Models.AdmAccessToken token)
        {
            IsolatedStorageSettings.ApplicationSettings["admAccessToken"] = token;
        }
    }
}