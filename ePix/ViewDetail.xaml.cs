﻿using ePix.Model;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Info;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using Microsoft.Xna.Framework.Media;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.IsolatedStorage;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using ePix.Services;
using System.Globalization;
using System.IO;

namespace ePix
{
    public partial class ViewDetail : PhoneApplicationPage
    {
        DetailViewModel dvm = new DetailViewModel();
        public ViewDetail()
        {
            InitializeComponent();

            if (this.Orientation == PageOrientation.LandscapeLeft || this.Orientation == PageOrientation.LandscapeRight)
            {
                bdSpace.Visibility = System.Windows.Visibility.Collapsed;
                spDesc.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                bdSpace.Visibility = System.Windows.Visibility.Visible;
                spDesc.Visibility = System.Windows.Visibility.Visible;
            }

            this.Language = System.Windows.Markup.XmlLanguage.GetLanguage(System.Globalization.CultureInfo.CurrentUICulture.Name);

            //dvm = new DetailViewModel();

            if (ApplicationBar.Buttons.Count >= 2)
            {
                (ApplicationBar.Buttons[0] as ApplicationBarIconButton).Text = Res.LocalString.bookmark;
                (ApplicationBar.Buttons[1] as ApplicationBarIconButton).Text = Res.LocalString.home;
            }

            if (ApplicationBar.MenuItems.Count >= 3)
            {
                (ApplicationBar.MenuItems[0] as ApplicationBarMenuItem).Text = Res.LocalString.share;
                (ApplicationBar.MenuItems[1] as ApplicationBarMenuItem).Text = Res.LocalString.openbrowser;
                (ApplicationBar.MenuItems[2] as ApplicationBarMenuItem).Text = Res.LocalString.gotomenu;
            }
        }

        string queryuri = string.Empty;
        string id = string.Empty;
        const string uriformat = "http://www.pixiv.net/member_illust.php?mode=medium&illust_id={0}";

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            if(ugosb != null)
            {
                ugosb.Pause();
            }

            if (e.NavigationMode != System.Windows.Navigation.NavigationMode.New)
            {
                for (int i = 0; i < lB.Count; i++)
                {
                    lB[i] = null;
                }
                if (ugosb != null)
                {
                    ugosb.Stop();
                    ugosb.Children.Clear();
                }

                lB.Clear();
                GC.Collect();
            }
            base.OnNavigatedFrom(e);
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (string.IsNullOrEmpty(id) && NavigationContext.QueryString.ContainsKey("id"))
            {
                id = NavigationContext.QueryString["id"];
                queryuri = string.Format(uriformat, id);
            }
            else if (e.NavigationMode == System.Windows.Navigation.NavigationMode.Reset
                || e.NavigationMode == System.Windows.Navigation.NavigationMode.Back)
            {
                if (e.NavigationMode == System.Windows.Navigation.NavigationMode.Reset)
                {
                    svMain.ScrollToVerticalOffset(0);
                }

                if (ugosb != null && ugosb.Children.Count > 0)
                {
                    ugosb.Resume();
                }
                return;
            }

            if(e.NavigationMode == System.Windows.Navigation.NavigationMode.New)
            {
                svMain.Visibility = System.Windows.Visibility.Collapsed;
            }

            

            /*if (DeviceStatus.ApplicationMemoryUsageLimit <= DeviceStatus.ApplicationCurrentMemoryUsage + 104857600)
            {
                MessageBox.Show(Res.LocalString.memorycritical);
                if (NavigationService.CanGoBack)
                {
                    NavigationService.GoBack();
                }
                return;
            }*/

            if (NavigationContext.QueryString.ContainsKey("thumb"))
            {
                dvm.Thumb = HttpUtility.UrlDecode(NavigationContext.QueryString["thumb"]);
            }

            UpdateArticle(id);
        }

        private void UpdateArticle(string sid)
        {
            /*if (DeviceStatus.ApplicationMemoryUsageLimit <= DeviceStatus.ApplicationCurrentMemoryUsage + 104857600)
            {
                MessageBox.Show(Res.LocalString.memorycritical);
                if (NavigationService.CanGoBack)
                {
                    NavigationService.GoBack();
                }
                return;
            }*/

            id = sid;
            queryuri = string.Format(uriformat, sid);

            ProgressIndicator pi = new ProgressIndicator();
            pi.Text = Res.LocalString.retpicture;
            pi.IsIndeterminate = true;
            pi.IsVisible = true;
            SystemTray.SetProgressIndicator(this, pi);

            if (ugosb != null)
            {
                ugosb.Stop();
                ugosb.Children.Clear();
            }

            imgMain.Source = null;
            spDesc.Opacity = 0;
            tbUgoira.Visibility = System.Windows.Visibility.Collapsed;

            Communication.Communicate(queryuri, (com, result) =>
            {
                if (com >= 0)
                {
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        Communication.isLogin = (result.IndexOf("user.loggedIn = true") > 0);
                        if (!Communication.isLogin && IsolatedStorageSettings.ApplicationSettings.Contains("id") && IsolatedStorageSettings.ApplicationSettings.Contains("pass"))
                        {
                            string mid = HttpUtility.UrlEncode(IsolatedStorageSettings.ApplicationSettings["id"] as string);
                            string pass = HttpUtility.UrlEncode(IsolatedStorageSettings.ApplicationSettings["pass"] as string);
                            Login.LoginSilent(mid, pass, this, () =>
                            {
                                this.Dispatcher.BeginInvoke(() =>
                                    {
                                        Communication.Communicate(queryuri, (com2, result2) =>
                                        {
                                            this.Dispatcher.BeginInvoke(() =>
                                                {
                                                    ReadyWork(result2);
                                                });
                                        });
                                    });
                            });
                        }
                        else
                        {
                            ReadyWork(result);
                        }
                    });
                }
                else
                {
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        try
                        {
                            MessageBox.Show(Res.LocalString.errorbang + Environment.NewLine +
                                (result != "UnknownError" ? result : Res.LocalString.noconnection),
                                Res.LocalString.errorpixifeed, MessageBoxButton.OK);
                            if (NavigationService.CanGoBack)
                            {
                                NavigationService.GoBack();
                            }
                        }
                        catch { }
                    });
                }

                this.Dispatcher.BeginInvoke(() =>
                {
                    if (imgMain.Source != null)
                    {
                        SystemTray.SetProgressIndicator(this, null);
                    }
                });
            });
        }

        string stampstr = "";
        private void ReadyWork(string result)
        {
            tbTap.Text = Res.LocalString.tapimage;
            tbTap.Foreground = new SolidColorBrush(Colors.Gray);
            if (ApplicationBar.Buttons.Count >= 1)
            {
                (ApplicationBar.Buttons[0] as ApplicationBarIconButton).IsEnabled = false;
            }
            rtStar.ValueChanged -= rtStar_ValueChanged;
            rtStar.Visibility = System.Windows.Visibility.Collapsed;
            tbRCom.Visibility = System.Windows.Visibility.Collapsed;
            imgMain.Opacity = 0;
            tbLong.Visibility = System.Windows.Visibility.Collapsed;
            dvm.Rate_max = 0;
            dvm.Rate_good = 0;
            dvm.Type = null;
            Interpreter.ViewDetail(dvm, bdDummy, pvView, this.ActualWidth, true, (v) =>
            {
                this.Dispatcher.BeginInvoke(() =>
                {
                    this.DataContext = null;
                    this.DataContext = dvm;
                    pbGrade.Maximum = dvm.Rate_max;
                    pbGrade.Value = dvm.Rate_good;
                });
            }, result);
            this.DataContext = dvm;
            pbGrade.Maximum = dvm.Rate_max;
            pbGrade.Value = dvm.Rate_good;

            if (result.Contains("pixiv.context.ugokuIllustData"))
            {
                dvm.Pic = null;
                dvm.Pics = Interpreter.GetUgoiraData(result);
                
                ReadyUgoira();
                if (Communication.isLogin)
                {
                   // dvm.Type = "?mode=ugoira_view";
                }
            }
            else
            {
                tbUgoira.Visibility = System.Windows.Visibility.Collapsed;
            }

            if (dvm != null)
            {
                if (string.IsNullOrEmpty(dvm.Pic))
                {
                    if (dvm.Pics == null)
                    {
                        imgMain.Tap -= imgMain_Tap;
                        if ((dvm.Caption == null || dvm.Caption != null && !dvm.Caption.Contains("error")))
                        {
                            MessageBox.Show(Res.LocalString.errorcont, Res.LocalString.errorpixifeed,
                                MessageBoxButton.OK);
                        }

                        if (NavigationService.CanGoBack)
                        {
                            NavigationService.GoBack();
                        }
                        return;
                    }
                    else
                    {
                        tbTap.Text = Res.LocalString.imsi_ugoira;
                        tbTap.Foreground = Application.Current.Resources["PhoneAccentBrush"] as SolidColorBrush;
/*
                        if (queryuri.Length > 0 && dvm != null)
                        {
                            WebBrowserTask wbt = new WebBrowserTask();
                            wbt.Uri = new Uri(queryuri);
                            wbt.Show();
                        }*/
                    }
                }

                lbTags.ItemsSource = dvm.ocTags;
                pvView.CanSwipeLeft = (dvm.PriorNum.Length != 0);
                pvView.CanSwipeRight = (dvm.NextNum.Length != 0);

                if (storyMe.GetCurrentState() == System.Windows.Media.Animation.ClockState.Stopped || dvm.Pics != null)
                {
                    pfMe.Opacity = 1;
                    pfMe.Visibility = System.Windows.Visibility.Visible;
                    pfMe.IsIndeterminate = dvm.Pics == null;
                    spDesc.Opacity = 0;
                    imgThumb.Opacity = 1;
                    imgMain.Opacity = 0;
                    bdDummy.Background = new SolidColorBrush(Colors.Gray);
                    bdDummy.MinHeight = 0;
                }

                if (storyDesc.GetCurrentState() != System.Windows.Media.Animation.ClockState.Active)
                {
                    svMain.Visibility = Visibility.Visible;
                    if (this.ApplicationBar.Buttons.Count >= 1 && Communication.isLogin
                        && result.IndexOf("class=\"add-bookmark _button") >= 0)
                    {
                        (this.ApplicationBar.Buttons[0] as ApplicationBarIconButton).IsEnabled = true;
                    }

                    if((dvm.Pic != null && !dvm.Pic.Contains("r-18")) //가려졌는감
                        && ApplicationBar.Buttons.Count < 3 && dvm.Pics == null)
                    {
                        ApplicationBarIconButton abib = new ApplicationBarIconButton(new Uri("/Images/appbar.save.rest.png", UriKind.Relative));
                        abib.Text = Res.LocalString.save;
                        abib.Click += (obj,arg) => {
                            if (dvm.Type == null || dvm.Type.Length < 0)
                            {
                                return;
                            }
                            if (dvm.Type.IndexOf("mode") < 0)
                            {
                                if (MessageBox.Show(Res.LocalString.noexpand,
                                    Res.LocalString.restricted, MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                                {
                                    if (dvm.Pic.Length > 0)
                                    {
                                        Communication.CommunicateBinary(dvm.Pic, (suc, stream, res) =>
                                        {
                                            if (suc >= 0)
                                            {
                                                this.Dispatcher.BeginInvoke(() =>
                                                {
                                                    MediaLibrary ml = new MediaLibrary();
                                                    ml.SavePicture(string.Format("{0}_thumb.jpg", id), stream);
                                                    MessageBox.Show(Res.LocalString.savethumbok, Res.LocalString.pixivok, MessageBoxButton.OK);
                                                });
                                            }
                                            else
                                            {
                                                this.Dispatcher.BeginInvoke(() =>
                                                {
                                                    MessageBox.Show(Res.LocalString.noconnection, Res.LocalString.errorpixifeed, MessageBoxButton.OK);
                                                });
                                            }
                                        });
                                    }
                                }
                            }
                            else
                            {
                                if (dvm.Type.Contains("illust_id="))
                                {
                                    NavigationService.Navigate(new Uri("/ViewZoom.xaml" + dvm.Type + "&title=" + HttpUtility.UrlEncode(dvm.Title) + "&url=" + HttpUtility.UrlEncode(string.Format(uriformat, id)) + "&return=now", UriKind.Relative));
                                }
                                else
                                {
                                    NavigationService.Navigate(new Uri("/ViewZoom.xaml" + dvm.Type + "&title=" + HttpUtility.UrlEncode(dvm.Title) + "&url=" + HttpUtility.UrlEncode(string.Format(uriformat, id)) + "&illust_id=" + id + "&return=now", UriKind.Relative));
                                }
                            }
                        };
                        ApplicationBar.Buttons.Insert(1,abib);
                    }
                    imgMain.Tap -= imgMain_Tap;
                    imgMain.Tap += imgMain_Tap;
                    storyDesc.Stop();
                    storyDesc.BeginTime = new TimeSpan(0, 0, 1);
                    storyDesc.Begin();
                }

                hbRate.Visibility = result.IndexOf("pixiv.context.rated = false") > 0 &&
                result.IndexOf("pixiv.context.self = false") > 0 ?
                    System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
                hbTranslate.Visibility = System.Windows.Visibility.Visible;

                if(result.IndexOf("pixiv.user.id =") > 0)
                {
                    int iSU = result.IndexOf("pixiv.user.id =");
                    iSU += 16;
                    int iEU = result.IndexOf('\n', iSU);
                    if (iEU > iSU)
                    {
                        string sId = result.Substring(iSU, iEU - iSU);
                        sId = Regex.Replace(sId, @"[^\w]","");
                        rtStar.Tag = sId;
                    }
                }

                if (imgMain.Source != null || dvm.Pics != null)
                {
                    SystemTray.SetProgressIndicator(this, null);
                }
                int iSStamp = result.IndexOf("pixiv.context.stampSeries");
                if(iSStamp > 0)
                {
                    int iEStamp = result.IndexOf("</script>", iSStamp);
                    if(iEStamp > iSStamp)
                    {
                        stampstr = result.Substring(iSStamp, iEStamp - iSStamp);
                    }
                }

                if (!Communication.Te && dvm.ocTags.Contains(new TagItem() { TagName = "R-18" }))
                {
                    /*this.DataContext = null;
                    dvm.Thumb = null;
                    dvm.Pic = "http://fake.dd/r-18";
                    this.DataContext = dvm;*/
                    imgMain.Tap -= imgMain_Tap;

                    MessageBox.Show(Res.LocalString.r18_warning);
                    if (id == NavigationContext.QueryString["id"] && NavigationService.CanGoBack)
                    {
                        NavigationService.GoBack();
                    }

                    if (ApplicationBar.Buttons.Count >= 3)
                    {
                        ApplicationBar.Buttons.RemoveAt(1);
                    }
                    (ApplicationBar.MenuItems[1] as ApplicationBarMenuItem).IsEnabled = false;
                    //return;
                }
                else
                {
                    (ApplicationBar.MenuItems[1] as ApplicationBarMenuItem).IsEnabled = true;
                }
            }
            else
            {
                if (NavigationService.CanGoBack)
                {
                    NavigationService.GoBack();
                }
            }
        }

        List<BitmapImage> lB = new List<BitmapImage>();
        Storyboard ugosb = null;
        private void ReadyUgoira()
        {
            pfMe.Maximum = 100;
            tbUgoira.Visibility = System.Windows.Visibility.Visible;
            if(dvm.Pics != null && !string.IsNullOrEmpty(dvm.Pics.src))
            {
                tbUgoira.Text = Res.LocalString.ugoiradown;
                pfMe.IsIndeterminate = false;
                Communication.CommunicateBinary(dvm.Pics.src,
                    (a, b, c) =>
                    {
                        this.Dispatcher.BeginInvoke(() =>
                        {
                            try
                            {
                                if (a >= 0)
                                {
                                    tbUgoira.Text = Res.LocalString.ugoirafilling;
                                    this.UpdateLayout();
                                    lB.Clear();
                                    UnZipper unzip = new UnZipper(b);
                                    List<string> lsf = unzip.FileNamesInZip().ToList();
                                    foreach (Model.Frame mf in dvm.Pics.frames)
                                    {
                                        string FileName = mf.file;
                                        if (lsf.Contains(FileName))
                                        {
                                            BitmapImage bi = new BitmapImage();
                                            bi.SetSource(unzip.GetFileStream(FileName));
                                            lB.Add(bi);
                                        }
                                    }
                                    if (lB.Count == dvm.Pics.frames.Count)
                                    {
                                        ugosb = new Storyboard();

                                        ObjectAnimationUsingKeyFrames anim = new ObjectAnimationUsingKeyFrames();

                                        int lastdelay = 0;
                                        for (int i = 0; i < dvm.Pics.frames.Count; i++)
                                        {
                                            DiscreteObjectKeyFrame obj = new DiscreteObjectKeyFrame();

                                            obj.Value = lB[i];
                                            obj.KeyTime = TimeSpan.FromMilliseconds(lastdelay);
                                            lastdelay += dvm.Pics.frames[i].delay;
                                            anim.KeyFrames.Add(obj);
                                        }
                                        anim.Duration = new Duration(TimeSpan.FromMilliseconds(lastdelay));
                                        anim.RepeatBehavior = RepeatBehavior.Forever;
                                        Storyboard.SetTarget(anim, imgMain);
                                        Storyboard.SetTargetProperty(anim, new PropertyPath(Image.SourceProperty));

                                        ugosb.Children.Add(anim);
                                        ugosb.Begin();

                                        imgMain.Opacity = 1;


                                        tbUgoira.Text = string.Format(Res.LocalString.ugoiradesc,
                                            dvm.Pics.frames.Count,
                                            anim.Duration.TimeSpan.ToString(@"mm\:ss\.fff"),
                                            Math.Round(dvm.Pics.frames.Count / anim.Duration.TimeSpan.TotalSeconds)
                                            //Math.Round(Math.Sqrt(bavg / dvm.Pics.frames.Count))
                                            );
                                    }
                                    else
                                    {
                                        tbUgoira.Text = "_" + Res.LocalString.ugoiraerror;
                                    }
                                    unzip.Dispose();
                                }
                                else
                                {
                                    tbUgoira.Text = Res.LocalString.ugoiraerror;
                                }
                            }
                            catch
                            {
                                tbUgoira.Text = Res.LocalString.ugoiraerror;
                            }
                            pfMe.IsIndeterminate = false;
                            pfMe.Value = 0;
                            pfMe.Visibility = System.Windows.Visibility.Collapsed;
                        });
                    }, (prog, file) =>
                    {
                        this.Dispatcher.BeginInvoke(() =>
                        {
                            pfMe.Value = prog;
                        });
                    });
            }
            else
            {
                MessageBox.Show(Res.LocalString.ugoiraerror, Res.LocalString.errorpixifeed, MessageBoxButton.OK);
            }
        }

        private void imgMain_ImageOpened(object sender, RoutedEventArgs e)
        {
            tbMsg.Visibility = System.Windows.Visibility.Collapsed;
            if (storyMe.GetCurrentState() != System.Windows.Media.Animation.ClockState.Active)
            {
                storyMe.Begin();
            }

            SystemTray.SetProgressIndicator(this, null);
        }

        private void imgMain_ImageFailed(object sender, ExceptionRoutedEventArgs e)
        {
            tbMsg.Text = e.ErrorException.GetType().ToString();
            tbMsg.Visibility = System.Windows.Visibility.Visible;
        }

        private void HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            string me = "";
            if ((sender as HyperlinkButton) != null)
            {
                me = ((sender as HyperlinkButton).Content as string);
            }

            int i = 0;
            bool joongbok = false;
            foreach (System.Windows.Navigation.JournalEntry je in NavigationService.BackStack)
            {
                if (je != null)
                {
                    i++;
                    if (HttpUtility.UrlDecode(je.Source.ToString()).IndexOf("="+me) > 0)
                    {
                        joongbok = true;
                    }
                }
            }

            if (joongbok)
            {
                for (int n = 0; n < i - 1; n++)
                {
                    if (NavigationService.CanGoBack)
                    {
                        NavigationService.RemoveBackEntry();
                    }
                }
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            if (NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/MainPage.xaml?clearhistory=1", UriKind.Relative));
        }

        private void btnMore_Click(object sender, EventArgs e)
        {
            if ((gdCmt.RenderTransform as CompositeTransform).TranslateY == 0)
            {
                (this.Resources["cOff"] as Storyboard).Begin();
            }
            else
            {
                this.SupportedOrientations = SupportedPageOrientation.Portrait;
                ptbComment.Text = "";
                StringBuilder sb = new StringBuilder();
                foreach (TagItem ti in dvm.ocTags)
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(" ");
                    }
                    sb.Append(ti.TagName);
                }
                ptbTag.Text = sb.ToString();
                ckPrivate.IsChecked = false;

                (this.Resources["cOn"] as Storyboard).Begin();
            }
        }

        private void btnComment_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(id))
            {
                Comment.stampstr = stampstr;
                NavigationService.Navigate(new Uri(string.Format("/Comment.xaml?id={0}&mid={1}", id, dvm.membernum
                    ), UriKind.Relative));
            }
        }

        private void menShare_Click(object sender, EventArgs e)
        {
            if (queryuri.Length > 0 && dvm != null)
            {
                ShareLinkTask slt = new ShareLinkTask();
                slt.LinkUri = new Uri(queryuri);
                slt.Title = dvm.Title;
                slt.Message = Res.LocalString.sharemsg;
                slt.Show();
            }
        }

        private void menShareEmail_Click(object sender, EventArgs e)
        {
            if (queryuri.Length > 0 && dvm != null)
            {
                EmailComposeTask slt = new EmailComposeTask();
                slt.Subject = dvm.Title;
                slt.Body = Res.LocalString.sharemsg+Environment.NewLine + Environment.NewLine
                    + queryuri;
                slt.Show();
            }
        }

        private void menBrowse_Click(object sender, EventArgs e)
        {
            if (queryuri.Length > 0 && dvm != null)
            {
                WebBrowserTask wbt = new WebBrowserTask();
                wbt.Uri = new Uri(queryuri);
                wbt.Show();
            }
        }

        private void menGo_Click(object sender, EventArgs e)
        {
            grGoto.Visibility = System.Windows.Visibility.Visible;
            this.ApplicationBar.IsVisible = false;
            tbNum.Text = id;
            tbNum.SelectionStart = 0;
            tbNum.SelectionLength = tbNum.Text.Length;
            tbNum.Focus();
        }

        private void tbNum_LostFocus(object sender, RoutedEventArgs e)
        {
            grGoto.Visibility = System.Windows.Visibility.Collapsed;
            this.ApplicationBar.IsVisible = true;
        }

        private void tbNum_KeyDown_1(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Enter:
                try
                {
                    if (Convert.ToInt32(tbNum.Text) > 0)
                    {
                        grGoto.Visibility = System.Windows.Visibility.Collapsed;
                        this.ApplicationBar.IsVisible = true;
                        NavigationService.Navigate(new Uri("/ViewDetail.xaml?id=" + tbNum.Text, UriKind.Relative));
                    }
                }
                catch
                { }
                break;
                case Key.D0:
                case Key.D1:
                case Key.D2:
                case Key.D3:
                case Key.D4:
                case Key.D5:
                case Key.D6:
                case Key.D7:
                case Key.D8:
                case Key.D9:
                case Key.Back:
                case Key.Delete:
                break;

                default:
                e.Handled = true;
                return;
            }
        }

        private void PhoneApplicationPage_OrientationChanged_1(object sender, OrientationChangedEventArgs e)
        {
            if (e.Orientation == PageOrientation.LandscapeLeft || e.Orientation == PageOrientation.LandscapeRight)
            {
                bdSpace.Visibility = System.Windows.Visibility.Collapsed;
                spDesc.Visibility = System.Windows.Visibility.Collapsed;
                spDesc.Opacity = 0;
                bdDummy.Width = double.NaN;
                bdDummy.Height = double.NaN;
                imgMain.Height = this.ActualHeight;
                imgMain.Width = double.NaN;
                imgThumb.Height = this.ActualHeight;
                imgThumb.Width = double.NaN;
                svMain.VerticalScrollBarVisibility = ScrollBarVisibility.Disabled;
            }
            else
            {
                bdSpace.Visibility = System.Windows.Visibility.Visible;
                spDesc.Visibility = System.Windows.Visibility.Visible;
                bdDummy.Width = double.NaN;
                bdDummy.Height = double.NaN;
                imgMain.Height = double.NaN;
                imgMain.Width = this.ActualWidth;
                imgThumb.Height = double.NaN;
                imgThumb.Width = this.ActualWidth;
                svMain.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
                if (storyDesc.GetCurrentState() == ClockState.Filling)
                {
                    storyDesc.BeginTime = TimeSpan.Zero;
                    storyDesc.Begin();
                }
            }
            //nx : ny = cx: cy
        }

        private void rtStar_ValueChanged(object sender, EventArgs e)
        {
            if (!Communication.isFull)
            {
                if (MessageBox.Show(Res.LocalString.fullverdesc, Res.LocalString.fullver,
                    MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                {
                    MarketplaceDetailTask mdt = new MarketplaceDetailTask();
                    mdt.Show();
                }
                return;
            }

            if (rtStar.Tag == null)
            {
                rtStar.Visibility = System.Windows.Visibility.Collapsed;
                return;
            }
            rtStar.ValueChanged -= rtStar_ValueChanged;
            //MessageBox.Show(rtStar.Value.ToString());
            Debug.WriteLine("mode=save&i_id={0}&u_id={1}&qr=0&score={2}&tt={3}",
                id, rtStar.Tag.ToString(), Convert.ToInt32(rtStar.Value), Interpreter.token);
            Communication.CommunicatePOST("http://www.pixiv.net/rpc_rating.php",
                string.Format("mode=save&i_id={0}&u_id={1}&qr=0&score={2}&tt={3}",
                id, rtStar.Tag.ToString(), Convert.ToInt32(rtStar.Value), Interpreter.token),
                (a,b) => {
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        if (a >= 0)
                        {
                            rtStar.IsEnabled = false;
                            tbRCom.Visibility = System.Windows.Visibility.Visible;
                        }
                    });
                }, false, true);
        }

        private void ContentControl_SwipeLeft(object sender, EventArgs e)
        {
            svMain.ScrollToVerticalOffset(0);
            btnComment.Content = Res.LocalString.showcomment;
            if (dvm.PriorNum != null)
            {
                if (dvm.PriorNum.Length <= 0)
                {
                    return;
                }
                imgThumb.Source = null;
                imgMain.Source = null;
                bdDummy.Background = new SolidColorBrush(Colors.Gray);
                string prior = dvm.PriorNum;
                string priorpic = dvm.Prior;
                if (dvm.PriorModel != null)
                {
                    dvm.PriorModel.Pic = null;
                    dvm = dvm.PriorModel;
                    bdDummy.MinHeight = 200;
                    bdDummy.Width = double.NaN;
                    bdDummy.Height = double.NaN;
                }
                else
                {
                    dvm = new DetailViewModel();
                    dvm.Thumb = null;
                    dvm.Pic = null;
                }
                UpdateArticle(prior);
                if (string.IsNullOrEmpty(dvm.Pic) && priorpic != null && priorpic.Length > 0)
                {
                    //this.DataContext = null;
                    //dvm.Thumb = priorpic;
                    //dvm.Pic = priorpic;
                    //this.DataContext = dvm;
                }
            }
        }

        private void ContentControl_SwipeRight(object sender, EventArgs e)
        {
            svMain.ScrollToVerticalOffset(0);
            btnComment.Content = Res.LocalString.showcomment;
            if (dvm.NextNum != null)
            {
                if (dvm.NextNum.Length <= 0)
                {
                    return;
                }
                imgThumb.Source = null;
                imgMain.Source = null;
                bdDummy.Background = new SolidColorBrush(Colors.Gray);
                string next = dvm.NextNum;
                string nextpic = dvm.Next;
                if (dvm.NextModel != null)
                {
                    dvm.NextModel.Pic = null;
                    dvm = dvm.NextModel;
                    bdDummy.MinHeight = 200;
                    bdDummy.Width = double.NaN;
                    bdDummy.Height = double.NaN;
                }
                else
                {
                    dvm = new DetailViewModel();
                    dvm.Thumb = null;
                    dvm.Pic = null;
                }
                UpdateArticle(next);
                if (string.IsNullOrEmpty(dvm.Pic) && nextpic != null && nextpic.Length > 0)
                {
                    //this.DataContext = null;
                    //dvm.Thumb = nextpic;
                    //dvm.Pic = nextpic;
                    //this.DataContext = dvm;
                }
            }
        }

        private void storyMe_Completed(object sender, EventArgs e)
        {
            pfMe.Visibility = System.Windows.Visibility.Collapsed;
            pfMe.IsIndeterminate = false;
            SystemTray.SetProgressIndicator(this, null);
        }

        private void imgMain_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if(dvm.Type == "error")
            {
                if (dvm.Pics == null)
                {
                    MessageBox.Show(Res.LocalString.errorzoom, Res.LocalString.errorpixifeed, MessageBoxButton.OK);
                }
                return;
            }
            if (dvm.Type == null || dvm.Type.Length < 0)
            {
                return;
            }
            if (dvm.Type.IndexOf("mode") < 0) //비회원
            {
                if (MessageBox.Show(Res.LocalString.noexpand,
                    Res.LocalString.restricted, MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                {
                    if (dvm.Pic.Length > 0)
                    {
                        Communication.CommunicateBinary(dvm.Pic, (suc, stream, res) =>
                        {
                            if (suc >= 0)
                            {
                                this.Dispatcher.BeginInvoke(() =>
                                {
                                    MediaLibrary ml = new MediaLibrary();
                                    ml.SavePicture(string.Format("{0}_thumb.jpg", id), stream);
                                    MessageBox.Show(Res.LocalString.savethumbok, Res.LocalString.pixivok, MessageBoxButton.OK);
                                });
                            }
                            else
                            {
                                this.Dispatcher.BeginInvoke(() =>
                                {
                                    MessageBox.Show(Res.LocalString.noconnection, Res.LocalString.errorpixifeed, MessageBoxButton.OK);
                                });
                            }
                        });
                    }
                }
            }
            else //회원
            {                
                this.SupportedOrientations = SupportedPageOrientation.Portrait;
                if (dvm.Type.Contains("illust_id="))
                {
                    NavigationService.Navigate(new Uri("/ViewZoom.xaml" + dvm.Type + "&title=" + HttpUtility.UrlEncode(dvm.Title) + "&url=" + HttpUtility.UrlEncode(string.Format(uriformat, id)), UriKind.Relative));
                }
                else
                {
                    NavigationService.Navigate(new Uri("/ViewZoom.xaml" + dvm.Type + "&title=" + HttpUtility.UrlEncode(dvm.Title) + "&url=" + HttpUtility.UrlEncode(string.Format(uriformat, id)) + "&illust_id="+id, UriKind.Relative));
                }
            }
        }

        private void storyDesc_Completed(object sender, EventArgs e)
        {
            if (storyMe.GetCurrentState() == System.Windows.Media.Animation.ClockState.Stopped)
            {
                //storyMe.Begin();
            }
            if (imgMain.ActualHeight > imgMain.ActualWidth * 2 || imgThumb.ActualHeight > imgThumb.ActualWidth * 2)
            {
                tbLong.Visibility = Visibility.Visible;
            }
        }

        private void HideComment()
        {
            (this.Resources["cOff"] as Storyboard).Begin();
            svMain.Focus();
        }

        private void PhoneApplicationPage_BackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if ((gdCmt.RenderTransform as CompositeTransform).TranslateY == 0)
            {
                e.Cancel = true;
                HideComment();
                return;
            }
        }
        TranslationService _translator = null;
        string second_translate = "";
        private void hbTranslate_Click(object sender, RoutedEventArgs e)
        {
            if(string.IsNullOrEmpty(dvm.Title))
            {
                return;
            }
            hbTranslate.IsHitTestVisible = false;
            string todo = dvm.Title;            
            second_translate = dvm.Caption != null && dvm.Caption.Trim().Length > 0 ? dvm.Caption : "--";
            //Communication.TranslateModal(todo);
            if (_translator == null)
            {
                _translator = new TranslationService();
                _translator.TranslationComplete += _translator_TranslationComplete;
                _translator.TranslationFailed += _translator_TranslationFailed;
            }
            _translator.GetTranslation(todo, string.Empty, CultureInfo.CurrentUICulture.TwoLetterISOLanguageName);
        }

        void _translator_TranslationFailed(object sender, TranslationFailedEventArgs e)
        {
            this.Dispatcher.BeginInvoke(() =>
            {
                hbTranslate.Visibility = System.Windows.Visibility.Collapsed;
                hbTranslate.IsHitTestVisible = true;
                MessageBox.Show(e.ErrorDescription);
            });
        }

        void _translator_TranslationComplete(object sender, TranslationCompleteEventArgs e)
        {
            this.Dispatcher.BeginInvoke(() =>
            {
                if(second_translate.Length > 0)
                {
                    dvm.Title = HttpUtility.HtmlDecode(e.Translation);
                    if (second_translate != "--")
                    {
                        string n = second_translate;
                        second_translate = "";
                        _translator.GetTranslation(n, string.Empty, CultureInfo.CurrentUICulture.TwoLetterISOLanguageName);
                        return;
                    }
                }
                else
                {
                    dvm.Caption = HttpUtility.HtmlDecode(e.Translation);
                }
                this.DataContext = null;
                this.DataContext = dvm;
                hbTranslate.Visibility = System.Windows.Visibility.Collapsed;
                hbTranslate.IsHitTestVisible = true;
                this.Focus();
            });

        }

        private void hbRate_Click(object sender, RoutedEventArgs e)
        {
            if (rtStar.Visibility == System.Windows.Visibility.Collapsed)
            {

                rtStar.Visibility = System.Windows.Visibility.Visible;
                rtStar.Value = 0;
                rtStar.IsEnabled = true;
                rtStar.ValueChanged += rtStar_ValueChanged;
            }
            else
            {
                rtStar.Visibility = System.Windows.Visibility.Collapsed;
                rtStar.IsEnabled = false;
                rtStar.ValueChanged -= rtStar_ValueChanged;
            }
        }

        private void okbtn_Click(object sender, RoutedEventArgs e)
        {            
            string bt = ptbTag.Text;
            string[] bts = bt.Split(new char[] { ' ' });
            if (bts.Length > 10)
            {
                MessageBox.Show(Res.LocalString.manytag, Res.LocalString.errorpixifeed, MessageBoxButton.OK);
                return;
            }
            string bcm = ptbComment.Text;
            okbtn.IsEnabled = false;
            string postdata = string.Format("mode=add&tt={0}&id={1}&type=illust&from_sid=&comment={2}&tag={3}&restrict={4}",
                Interpreter.token, id, HttpUtility.UrlEncode(bcm), HttpUtility.UrlEncode(bt), (ckPrivate.IsChecked == true) ? 
                "1" : "0");
            Communication.CommunicatePOST("http://www.pixiv.net/bookmark_add.php",
                postdata, (suc, res) =>
                {
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        okbtn.IsEnabled = true;
                        if (suc >= 0)
                        {
                            HideComment();
                            (this.ApplicationBar.Buttons[0] as ApplicationBarIconButton).IsEnabled = false;
                        }
                    });
                }, false, false);
        }

        private void cancelbtn_Click(object sender, RoutedEventArgs e)
        {
            HideComment();
        }

        private void Storyboard_Completed(object sender, EventArgs e)
        {
            this.SupportedOrientations = SupportedPageOrientation.PortraitOrLandscape;
        }
    }
}