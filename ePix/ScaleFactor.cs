﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace ePix
{
    public static class ScaleFactor
    {
        public static Size DisplayResolution
        {
            get
            {
                if (Environment.OSVersion.Version.Major < 8)
                    return new Size(480, 800);
                int scaleFactor = (int)GetProperty(Application.Current.Host.Content, "ScaleFactor");
                switch (scaleFactor)
                {
                    case 100:
                        return new Size(480, 800);
                    case 150:
                        return new Size(720, 1280);
                    case 160:
                        return new Size(768, 1280);
                }
                return new Size(480, 800);
            }
        }

        public static double Scale
        {
            get
            {
                if (Environment.OSVersion.Version.Major < 8)
                {
                    return 1.0;
                }
                int scaleFactor = (int)GetProperty(Application.Current.Host.Content, "ScaleFactor");
                return scaleFactor / 100.0;
            }
        }

        private static object GetProperty(object instance, string name)
        {
            var getMethod = instance.GetType().GetProperty(name).GetGetMethod();
            return getMethod.Invoke(instance, null);
        }  

    }
}
