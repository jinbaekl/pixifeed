﻿using HtmlAgilityPack;
using System.Collections.Generic;
using System.Net;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Media;

namespace System.Windows.Controls
{
    public class RichTextViewer : RichTextBox
    {
        #region 리치텍스트 이모티콘
        public const string RichTextPropertyName = "RichText";

        public static readonly DependencyProperty RichTextProperty =
            DependencyProperty.Register(RichTextPropertyName,
                                        typeof(string),
                                        typeof(RichTextBox),
                                        new PropertyMetadata(
                                            new PropertyChangedCallback
                                                (RichTextPropertyChanged)));

        public static Dictionary<string, string> dicEmoji
        {
            get;
            set;
        }

        public RichTextViewer()
        {
            IsReadOnly = true;
            Background = new SolidColorBrush { Opacity = 0 };
            FontSize = 24;
            //BorderThickness = new Thickness(0);
            //Padding = new Thickness(0);
        }

        public string RichText
        {
            get { return (string)GetValue(RichTextProperty); }
            set { SetValue(RichTextProperty, value); }
        }

        //string[] 

        private static void RichTextPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            ((RichTextBox)dependencyObject).Blocks.Clear();
            if (dependencyPropertyChangedEventArgs.NewValue == null || !(dependencyPropertyChangedEventArgs.NewValue is string))
            {
                return;
            }
            string original = (string)dependencyPropertyChangedEventArgs.NewValue;
            int i = original.IndexOf('(');
            int j = 0;
            int k = 0;
            Paragraph p = new Paragraph();
            while(i >= k)
            {
                j = original.IndexOf(')', i);
                if(j > i)
                {
                    

                    string eword = original.Substring(i + 1, j - i - 1);



                    Run r = new Run() { Text = original.Substring(k, i - k) };
                    if (dicEmoji != null && dicEmoji.ContainsKey(eword))
                    {
                        if (!string.IsNullOrEmpty(r.Text))
                        {
                            p.Inlines.Add(r);
                        }
                        string euri = dicEmoji[eword];
                        InlineUIContainer iu = new InlineUIContainer();
                        Image im = new Image();
                        im.SetValue(ePix.ImageProperties.SmallSourceWCRProperty, new Uri(euri));
                        im.Height = 56;
                        im.Opacity = 1;
                        iu.Child = im;
                        p.Inlines.Add(iu);
                    }
                    else
                    {
                        i = original.IndexOf('(', j);
                        continue;
                    }

                    k = j+1;
                    i = original.IndexOf('(', j);
                }
                else
                {
                    i = -1;
                }
            }

            if (k < original.Length)
            {
                Run r = new Run() { Text = original.Substring(k, original.Length - k) };
                p.Inlines.Add(r);
            }

            ((RichTextBox)dependencyObject).Blocks.Add(p);
            //((RichTextBox)dependencyObject).Blocks.Add(
            //    XamlReader.Load((string)dependencyPropertyChangedEventArgs.NewValue) as Paragraph);

        }
        #endregion

        #region 리치텍스트 문맥
        public const string RichTextSimplePropertyName = "RichTextSimple";

        public static readonly DependencyProperty RichTextSimpleProperty =
            DependencyProperty.Register(RichTextSimplePropertyName,
                                        typeof(string),
                                        typeof(RichTextBox),
                                        new PropertyMetadata(
                                            new PropertyChangedCallback
                                                (RichTextSimplePropertyChanged)));

        public string RichTextSimple
        {
            get { return (string)GetValue(RichTextSimpleProperty); }
            set { SetValue(RichTextSimpleProperty, value); }
        }

        //string[] 

        private static void RichTextSimplePropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            BlockCollection b = ((RichTextBox)dependencyObject).Blocks;
            b.Clear();
            Paragraph p = new Paragraph();
            if (dependencyPropertyChangedEventArgs.NewValue == null || !(dependencyPropertyChangedEventArgs.NewValue is string))
            {
                return;
            }
            string original = (string)dependencyPropertyChangedEventArgs.NewValue;
            HtmlDocument hd = new HtmlDocument();
            hd.LoadHtml(original);
            if (hd.DocumentNode != null && hd.DocumentNode.ChildNodes != null)
            {
                InnerTags(p, hd.DocumentNode.ChildNodes);
            }
            b.Add(p);
        }

        private static void InnerTags(Paragraph p, HtmlNodeCollection hnc, bool bold = false, bool italic = false,
            bool underline = false, Brush bm = null)
        {
            if (hnc == null)
            {
                return;
            }
            foreach (HtmlNode hn in hnc)
            {
                if (hn.Name == "#text")
                {
                    Run toadd = new Run()
                        {
                            Text = HttpUtility.HtmlDecode(hn.InnerText),
                            FontSize = 26,
                            FontWeight = bold ? FontWeights.Bold : FontWeights.Normal,
                            FontStyle = italic ? FontStyles.Italic : FontStyles.Normal,
                        };
                    if(bm != null)
                    {
                        toadd.Foreground = bm;
                    }
                    p.Inlines.Add(toadd);
                }
                else if (hn.Name == "br")
                {
                    p.Inlines.Add(new LineBreak());
                }
                else if (hn.Name == "strong" || hn.Name == "b")
                {
                    InnerTags(p, hn.ChildNodes, true, italic, underline, bm);
                }
                else if (hn.Name == "em" || hn.Name == "i")
                {
                    InnerTags(p, hn.ChildNodes, bold, true, underline, bm);
                }
                else if (hn.Name == "u")
                {
                    InnerTags(p, hn.ChildNodes, bold, italic, true, bm);
                }
                else if(hn.Name == "span")
                {
                    string style = hn.GetAttributeValue("style","");
                    int im = style.IndexOf("color:#");
                    if (im >= 0)
                    {
                        im += 7;
                        int ie = style.IndexOf(';', im);
                        if (ie > im)
                        {
                            string hex = style.Substring(im, ie - im);
                            SolidColorBrush myBrush = null;
                            if (hex.Length >= 6)
                            {
                                byte r = (byte)(Convert.ToUInt32(hex.Substring(0, 2), 16));
                                byte g = (byte)(Convert.ToUInt32(hex.Substring(2, 2), 16));
                                byte b = (byte)(Convert.ToUInt32(hex.Substring(4, 2), 16));

                                myBrush = new SolidColorBrush(Color.FromArgb(255, r, g, b));
                            }
                            InnerTags(p, hn.ChildNodes, bold, italic, underline, myBrush);
                        }
                        else
                        {
                            InnerTags(p, hn.ChildNodes, bold, italic, underline);
                        }
                    }
                    else if(style.Contains("rgb("))
                    {
                        int io = style.IndexOf("(");
                        if(io > 0)
                        {
                            io++;
                            int ie = style.IndexOf(")", io);
                            string[] decolors = style.Substring(io, ie - io).Split(',');
                            SolidColorBrush myBrush = null;
                            if(decolors.Length == 3)
                            {
                                byte r = (byte)Convert.ToInt32(decolors[0].Trim());
                                byte g = (byte)Convert.ToInt32(decolors[1].Trim());
                                byte b = (byte)Convert.ToInt32(decolors[2].Trim());

                                myBrush = new SolidColorBrush(Color.FromArgb(255, r, g, b));
                            }
                            InnerTags(p, hn.ChildNodes, bold, italic, underline, myBrush);
                        }
                    }
                    else
                    {
                        InnerTags(p, hn.ChildNodes, bold, italic, underline);
                    }
                }
                else if (hn.Name == "a")
                {
                    string href = hn.GetAttributeValue("href", "");
                    if (href.IndexOf("jump.php") >= 0)
                    {
                        int iSQ = href.IndexOf('?');
                        if (iSQ >= 0)
                        {
                            href = HttpUtility.UrlDecode(href.Substring(iSQ + 1, href.Length - iSQ - 1));
                        }
                    }
                    else if(href.StartsWith("javascript:"))
                    {
                        continue;
                    }
                    else if (!href.StartsWith("http"))
                    {
                        href = "http://www.pixiv.net/" + href;
                    }
                    try
                    {
                        Hyperlink hl = new Hyperlink()
                        {
                            NavigateUri = new Uri(href),
                            TargetName = "_blank"
                        };
                        int iSid = href.IndexOf("illust_id=");
                        if (iSid > 0)
                        {
                            iSid += 10;
                            int iEid = href.IndexOf('&', iSid);
                            if (iEid < 0)
                            {
                                iEid = href.Length;
                            }
                            if (iEid > iSid)
                            {
                                string num = href.Substring(iSid, iEid - iSid);
                                hl.NavigateUri = new Uri(string.Format("/ViewDetail.xaml?id={0}", num), UriKind.Relative);
                                hl.TargetName = "";
                            }
                        }
                        if (href.IndexOf("member") >= 0)
                        {
                            iSid = href.IndexOf("?id=");
                            if (iSid > 0)
                            {
                                iSid += 4;
                                int iEid = href.IndexOf('&', iSid);
                                if (iEid < 0)
                                {
                                    iEid = href.Length;
                                }
                                if (iEid > iSid)
                                {
                                    string num = href.Substring(iSid, iEid - iSid);
                                    hl.NavigateUri = new Uri(string.Format("/People.xaml?id={0}", num), UriKind.Relative);
                                    hl.TargetName = "";
                                }
                            }
                        }
                        hl.Inlines.Add(new Run()
                        {
                            Text = HttpUtility.HtmlDecode(hn.InnerText),
                            FontSize = 26,
                            FontWeight = bold ? FontWeights.Bold : FontWeights.Normal,
                            FontStyle = italic ? FontStyles.Italic : FontStyles.Normal
                            //Foreground = new SolidColorBrush(Color.FromArgb(255, 37, 143, 184))
                        });
                        p.Inlines.Add(hl);
                    }
                    catch
                    { //Uri Error
                    }
                }
            }
        }
        #endregion
    }
}