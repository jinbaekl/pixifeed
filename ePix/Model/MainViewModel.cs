﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using ePix.Model;
using System.Collections.Generic;

namespace ePix
{
    public class MainViewModel
    {
       // public string _
        public ObservableCollection<PictureModel> ocRecomm = new ObservableCollection<PictureModel>();
        public List<PictureModel> ocRecomm_back = new List<PictureModel>();
        public ObservableCollection<Contact> ocSuggest = new ObservableCollection<Contact>();
        public ObservableCollection<TagItem> ocTag = new ObservableCollection<TagItem>();

        MainPage mp = null;

        private string _allmainstuff = "";
        public string AllStuff { get { return _allmainstuff; } set { _allmainstuff = value; } }

        public MainViewModel(MainPage mm)
        {
            mp = mm;
        }
    }
}
