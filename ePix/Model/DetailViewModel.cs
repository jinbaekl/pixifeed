﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace ePix.Model
{
    public class DetailViewModel : INotifyPropertyChanged
    {
        public string Pic { get; set; } //

        public PixivContext Context { get; set; }
        public IllustData Pics { get; set; }

        public string PriorNum { get; set; }
        public string NextNum { get; set; }
        public string Prior { get; set; }
        public string Next { get; set; }

        public bool isManga { get; set; }

        public DetailViewModel PriorModel { get; set; }
        public DetailViewModel NextModel { get; set; }

        public string Thumb { get; set; }
        public string Title
        {
            get;
            set;
        } //
        public string Name { get; set; } //
        public string TimeStamp { get; set; } //
        public string Tool { get; set; } //
        public string Rate { get; set; } //
        public string Caption { get; set; } //
        public string membernum = "0"; //
        public Uri Member
        {
            get
            {
                return new Uri("/People.xaml?id=" + membernum, UriKind.Relative);
            }
        }
        //public int Rate_view { get; set; }
        public int Rate_max { get; set; }
        public int Rate_good { get; set;}
        public Visibility Rate_visual
        {
            get
            {
                return Rate_max > 0 ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public string Type { get; set; }
        //public string AllComments { get; set; }

        //public ObservableCollection<string> ocContent = new ObservableCollection<string>();
        public ObservableCollection<TagItem> ocTags = new ObservableCollection<TagItem>(); //

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    public class TagItem
    {
        public Uri NavUri
        {
            get
            {
                return new Uri("/SearchResult.xaml?"+
                    (isLocalized ? string.Empty : "tag=1&")
                    +"q=" + HttpUtility.UrlEncode(TagName), UriKind.Relative);
            }
        }
        public string TagName { get; set; }
        public int _tagclass = 0;
        public double TagSize
        {
            get
            {
                switch (_tagclass)
                {
                    //case 6: return 20;
                    case 5: return 23;
                    case 4: return 26.6;
                    case -2: case 3: return 30;
                    case 2: return 30;
                    case 1: return 33.2;
                    default: return 20;
                }
            }
        }
        public SolidColorBrush TagColor
        {
            get
            {
                Color c = new Color();
                switch (_tagclass)
                {
                    //case 6: return 20;
                    case 5: c = Color.FromArgb(255, 88, 124, 151); break;
                    case 4: c = Color.FromArgb(255, 88, 124, 151); break;
                    case 3: c = Color.FromArgb(255, 88, 124, 151); break;
                    case 2: c = Color.FromArgb(255, 62, 92, 113); break;
                    case 1: c = Color.FromArgb(255, 62, 92, 113); break;
                    case -2: c = Color.FromArgb(255, 151, 124, 88); break;
                    default: c = Color.FromArgb(255, 94, 158, 206); break;
                }
                return new SolidColorBrush(c);
            }
        }
        public bool isnSelf;
        public FontStyle TagWeight
        {
            get
            {
                return isnSelf ? FontStyles.Italic : FontStyles.Normal;
            }
        }

        public string TagImage
        {
            get;
            set;
        }

        public string TagLabel
        {
            get;
            set;
        }

        public bool isLocalized
        {
            get;
            set;
        }

        public override bool Equals(object obj)
        {
            if(obj != null)
            {
                return (obj as TagItem).TagName == TagName;
            }
            else
            {
                return false;
            }
        }
    }

    public class CommentItem
    {
        public string Name { get; set; }
        public string _nameid = "";
        public string NameLink { get { return "/People.xaml?id=" + _nameid; } }
        public string TimeStamp { get; set; }
        public string Body { get; set; }
        public string Profile { get; set; }
        public string Stamp { get; set; }
        public bool Host { get; set; }

        public System.Windows.Visibility BarVisibility
        {
            get
            {
                return Host ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
            }
        }

        public System.Windows.Visibility NameVisibility
        {
            get
            {
                return
                    string.IsNullOrEmpty(_nameid) ? System.Windows.Visibility.Collapsed : Visibility.Visible;
            }
        }

        public System.Windows.Visibility BodyVisibility
        {
            get
            {
                return
                    string.IsNullOrEmpty(Body) ? System.Windows.Visibility.Collapsed : Visibility.Visible;
            }
        }

        public System.Windows.Visibility StampVisibility
        {
            get
            {
                return
                    !string.IsNullOrEmpty(Body) ? System.Windows.Visibility.Collapsed : Visibility.Visible;
            }
        }
    }
}
