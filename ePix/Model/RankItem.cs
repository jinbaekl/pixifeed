﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Expression.Media;

namespace ePix.Model
{
    public class RankItem
    {
        public decimal Rank { get; set; }
        public decimal LastRank { get; set; }
        public string Pic { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string Date { get; set; }
        public string ID { get; set; }
        public Visibility LastAvailable { get { return _lav; } }
        private Visibility _lav = Visibility.Collapsed;
        public Visibility UgoiraVisibility { get { return _ugo ? Visibility.Visible : Visibility.Collapsed; } }
        public Visibility MultiVisibility { get { return _mul ? Visibility.Visible : Visibility.Collapsed; } }
        private bool _ugo = false;
        private bool _mul = false;
        private ArrowOrientation _ao = ArrowOrientation.Right;
        public RankItem(string date, string artist, string img, string num, string title, decimal p, decimal p_2, bool ugo, bool mul)
        {
            this.Date = date;
            this.Name = artist;
            this.Pic = img;
            this.ID = num;
            this.Title = title;
            this.Rank = p;
            this.LastRank = p_2;
            _lav = p_2 > 0 ? Visibility.Visible : Visibility.Collapsed;
            this._ugo = ugo;
            this._mul = mul;
            if (Rank < LastRank && LastRank > 0)
            {
                _ao = ArrowOrientation.Up;
            }
            else if (Rank > LastRank)
            {
                _ao = ArrowOrientation.Down;
            }
            else
            {
                _ao = ArrowOrientation.Right;
            }
        }

        public ArrowOrientation Dir
        {
            get { return _ao; }
        }
    }
}
