﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Media.Imaging;
using System.ComponentModel;

namespace ePix.Model
{
    public class PictureModel
    {
        string _pic = "";
        public string Pic { get { return _pic; } set { 
            _pic = value; } }
        public string Title { get; set; }
        public string Author { get; set; }

        public string ID { get; set; }

        public string Tags { get; set; }

        public string _favs;
        public string Favs
        {
            get
            {
                return "✫ " + _favs;
            }
        }

        public string _resp;
        public string Resp
        {
            get
            {
                return "⤷ " + _resp;
            }
        }

        public string Category
        {
            get;
            set;
        }

        public Visibility FavVisibility
        {
            get
            {
                return (string.IsNullOrEmpty(_favs)) ? Visibility.Collapsed : Visibility.Visible;
            }
        }
        public Visibility RespVisibility
        {
            get
            {
                return (string.IsNullOrEmpty(_resp)) ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        public Visibility TitleVisibility
        {
            get
            {
                return string.IsNullOrEmpty(Title) ? Visibility.Collapsed : Visibility.Visible;
            }
        }
        public Visibility AuthorVisibility
        {
            get
            {
                return string.IsNullOrEmpty(Author) ? Visibility.Collapsed : Visibility.Visible;
            }
        }
        public Visibility UgoiraVisibility
        {
            get
            {
                return _ugo ? Visibility.Visible : Visibility.Collapsed;
            }
        }
        public bool _ugo = false;
        public Visibility MultiVisibility
        {
            get
            {
                return _mul ? Visibility.Visible : Visibility.Collapsed;
            }
        }
        public bool _mul = false;

        public PictureModel(string picuri, string title, string author, string id)
        {
            /*BitmapImage bi = new BitmapImage(picuri);
            bi.CreateOptions = BitmapCreateOptions.DelayCreation;
            Pic = bi;*/
            Pic = picuri;
            Title = title;
            Author = author;
            ID = id;
        }

        public PictureModel(string picuri, string title, string author, string id, string fav, string resp)
        {
            /*BitmapImage bi = new BitmapImage(picuri);
            bi.CreateOptions = BitmapCreateOptions.DelayCreation;
            Pic = bi;*/
            Pic = picuri;
            Title = title;
            Author = author;
            ID = id;
            _favs = fav;
            _resp = resp;
        }

        public PictureModel(string picuri, string title, string author, string id, string fav)
        {
            /*BitmapImage bi = new BitmapImage(picuri);
            bi.CreateOptions = BitmapCreateOptions.DelayCreation;
            Pic = bi;*/
            Pic = picuri;
            Title = title;
            Author = author;
            ID = id;
            _favs = fav;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            if (obj is PictureModel)
            {
                PictureModel pm = obj as PictureModel;
                return this.ID == pm.ID;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public class StampModel
    {
        public string Pic
        {
            get;
            set;
        }

        public string Id
        {
            get;
            set;
        }
    }

    public class EmojiModel
    {
        public string Pic
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }
    }
}
