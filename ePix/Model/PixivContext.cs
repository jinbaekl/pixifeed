﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ePix.Model
{
    public class PixivContext
    {
        public string illustId { get; set; }
        public string illustTitle { get; set; }
        public string userId { get; set; }
        public string userName { get; set; }
        public string hasQuestionnaire { get; set; }
        public string embedId { get; set; }
        public string Explicit { get; set; }
        public IllustData ugokuIllustData { get; set; }
    }

    public class Frame
    {
        public string file { get; set; }
        public int delay { get; set; }
    }

    public class IllustData
    {
        public string src { get; set; }
        public string mime_type { get; set; }
        public List<Frame> frames { get; set; }
    }

    public class Body
    {
        public string html { get; set; }
    }

    public class CmReturn
    {
        public bool error { get; set; }
        public string message { get; set; }
        public Body body { get; set; }
    }

    public class StampInfo
    {
        public string slug { get; set; }
        public string name { get; set; }
        public List<int> stamps { get; set; }
    }

    public class EmojiInfo
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}