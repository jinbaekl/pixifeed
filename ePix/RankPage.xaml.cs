﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using ePix.Model;
using System.Collections.ObjectModel;
using System.Collections;
using System.Windows.Controls.Primitives;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Info;

namespace ePix
{
    public partial class RankPage : PhoneApplicationPage
    {
        public RankPage()
        {
            InitializeComponent();

            this.Language = System.Windows.Markup.XmlLanguage.GetLanguage(System.Globalization.CultureInfo.CurrentUICulture.Name);

            lbMain.ItemsSource = ocMain;
            /*lbWeekly.ItemsSource = ocWeekly;
            lbMonthly.ItemsSource = ocMonthly;
            lbRookie.ItemsSource = ocRookie;
            lbOriginal.ItemsSource = ocOriginal;*/

            rbEv.IsChecked = true;
            rbEv.Checked += RadioButton_Checked;
            rbMen.Checked += RadioButton_Checked;
            rbWomen.Checked += RadioButton_Checked;
        }

        ObservableCollection<RankItem> ocMain = new ObservableCollection<RankItem>();
        /*ObservableCollection<RankItem> ocDaily = new ObservableCollection<RankItem>();
        ObservableCollection<RankItem> ocWeekly = new ObservableCollection<RankItem>();
        ObservableCollection<RankItem> ocMonthly = new ObservableCollection<RankItem>();
        ObservableCollection<RankItem> ocRookie = new ObservableCollection<RankItem>();
        ObservableCollection<RankItem> ocOriginal = new ObservableCollection<RankItem>();*/

        const string rankform = "http://www.pixiv.net/ranking.php?mode={0}";
        int lastpg = 1;
        bool r18 = false;
        string[] nametype = new string[] { "daily", "weekly", "monthly", "rookie", "original", "daily&content=ugoira" };
        string[] r18type = new string[] { "daily_r18", "weekly_r18", "daily_r18&content=ugoira" };
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if ((Application.Current.Resources["PhoneDarkThemeVisibility"] as Visibility?) == System.Windows.Visibility.Visible ^ Communication.isInvert)
            {
                LayoutRoot.Background = new SolidColorBrush(Color.FromArgb(255, 37, 39, 41));
                pvMain.Background = LayoutRoot.Background;
                SystemTray.BackgroundColor = Color.FromArgb(255, 37, 39, 41);
                SystemTray.ForegroundColor = Colors.White;
            }

            if (e.NavigationMode == System.Windows.Navigation.NavigationMode.New)
            {
                ShowRank(0, 1);
            }
        }

        bool working = false;
        private void ShowRank(int ipg, int p)
        {
            int plength = r18 ? r18type.Length : nametype.Length;
            if (working || ipg >= plength)
            {
                return;
            }

            /*if (DeviceStatus.ApplicationMemoryUsageLimit <= DeviceStatus.ApplicationCurrentMemoryUsage + 157286400)
            {
                MessageBox.Show(Res.LocalString.memorycritical);
                return;
            }*/

            working = true;
            SystemTray.SetProgressIndicator(this, new ProgressIndicator()
            {
                IsVisible = true,
                IsIndeterminate = true,
                Text = Res.LocalString.retrank
            });

            string method = r18 ? "daily_r18" : "daily";
            if (ipg == 0)
            {
                if(rbMen.IsChecked == true)
                {
                    method = r18 ? "male_r18" : "male";
                }
                else if(rbWomen.IsChecked == true)
                {
                    method = r18 ? "female_r18" : "female";
                }
                else
                {
                     method = r18 ? "daily_r18" : "daily"; 
                }
            }
            else if (ipg < nametype.Length)
            {
                method = r18 ? r18type[ipg] : nametype[ipg];
            }
            string auri = string.Format(rankform, method);
            if (p > 1)
            {
                auri += "&p=" + p.ToString();
            }
            else
            {
                uppg = 1;
            }

            Communication.Communicate(auri, (suc, res) =>
            {
                if (suc >= 0)
                {
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        btnR18.Visibility = Interpreter.IsR_18(res) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
                        bool isOK = false;
                        isOK = Interpreter.AppendRank(ref ocMain, (p == 1), res);
                        if (isOK)
                        {
                            lastpg = p + 1;
                        }
                        else
                        {
                            uppg = lastpg - 1;
                            //MessageBox.Show(Res.LocalString.endpage, Res.LocalString.alert, MessageBoxButton.OK);
                        }
                        working = false;
                        SystemTray.SetProgressIndicator(this, null);
                    });
                }
                else
                {
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        working = false;
                        SystemTray.SetProgressIndicator(this, null);
                        try
                        {
                            MessageBox.Show(Res.LocalString.errorbang + Environment.NewLine +
                                (res != "UnknownError" ? res : Res.LocalString.noconnection),
                                Res.LocalString.errorpixifeed, MessageBoxButton.OK);
                            /*if (NavigationService.CanGoBack)
                            {
                                NavigationService.GoBack();
                            }*/
                        }
                        catch { }
                    });
                }
            });
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (e.NavigationMode == System.Windows.Navigation.NavigationMode.Back)
            {
                ocMain.Clear();
            }
            GC.Collect();
            base.OnNavigatedFrom(e);
        }

        private void pvMain_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ocMain.Clear();
            GC.Collect();
            lastpg = 0;
            if (pvMain.SelectedIndex >= 0)
            {
                PivotItem li = e.RemovedItems[0] as PivotItem;
                PivotItem pi = e.AddedItems[0] as PivotItem;
                if (li != null)
                {
                    Grid gpi = li.Content as Grid;

                    if (gpi != null)
                    {
                        if (!gpi.Name.StartsWith("gdList"))
                        {
                            gpi = gpi.Children.FirstOrDefault((obj) => { return (obj as FrameworkElement).Name.StartsWith("gdList"); }) as Grid;
                        }
                        if (gpi != null && gpi.Children.Count > 0)
                        {
                            gpi.Children.Clear();
                        }
                        gpi.InvalidateArrange();
                        gpi.InvalidateMeasure();
                        gpi.OnApplyTemplate();
                    }

                    if (pi != null)
                    {
                        gpi = pi.Content as Grid;
                        if (gpi != null)
                        {
                            if (!gpi.Name.StartsWith("gdList"))
                            {
                                gpi = gpi.Children.FirstOrDefault((obj) => { return (obj as FrameworkElement).Name.StartsWith("gdList"); }) as Grid;
                            }
                            if (gpi != null)
                            {
                                if (gpi.Children.Count > 0)
                                {
                                    gpi.Children.Clear();
                                }
                                gpi.Children.Add(lbMain);
                                gpi.InvalidateArrange();
                                gpi.InvalidateMeasure();
                                gpi.OnApplyTemplate();
                            }
                        }
                    }
                }

                bool shouldRef = ocMain.Count == 0;
                if (shouldRef)
                {
                    ShowRank(pvMain.SelectedIndex, 1);
                }

                btnUgoira.Visibility = pvMain.SelectedIndex >= 0 && pvMain.Items[pvMain.SelectedIndex] == piUgoira ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
            }
        }

        private void Grid_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            RankItem rm = (sender as Grid).DataContext as RankItem;
            if (rm != null)
            {
                NavigationService.Navigate(new Uri("/ViewDetail.xaml?id="
                    + rm.ID + "&thumb=" + System.Net.HttpUtility.UrlEncode(rm.Pic), UriKind.Relative));
            }
        }

        private void lbDaily_MouseEnter(object sender, MouseEventArgs e)
        {
            //tbAlert.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            if (NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }
        }

        int uppg = 1;
        private void ViewportControl_ViewportChanged(object sender, ViewportChangedEventArgs e)
        {
            ViewportControl vc = sender as ViewportControl;
            if (ocMain != null && ocMain.Count > 0 && vc.Viewport.Bottom >= vc.Bounds.Bottom - 140)
            {
                int ipg = 0;
                try
                {
                    ipg = pvMain.SelectedIndex;
                    if (uppg != lastpg)
                    {
                        uppg = lastpg;
                        ShowRank(ipg, lastpg);
                    }
                }
                catch { }
            }
        }

        private void btnR18_Click(object sender, RoutedEventArgs e)
        {
            r18 = !r18;
            pvMain.Title = r18 ? Res.LocalString.grank + " " + Res.LocalString.r18 : Res.LocalString.grank;
            btnR18.Content = r18 ? Res.LocalString.r18_no : Res.LocalString.r18;
            //btnUgoira.Visibility = r18 ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
            ocMain.Clear();
            if (pvMain.SelectedIndex != 0)
            {
                pvMain.SelectedIndex = 0;
            }
            else
            {
                ShowRank(0, 1);
            }
            pvMain.SelectionChanged -= pvMain_SelectionChanged;
            if(r18)
            {
                if (pvMain.Items.Contains(piOriginal))
                {
                    pvMain.Items.Remove(piOriginal);
                }
                if (pvMain.Items.Contains(piRookie))
                {
                    pvMain.Items.Remove(piRookie);
                }
                if (pvMain.Items.Contains(piMonthly))
                {
                    pvMain.Items.Remove(piMonthly);
                }
            }
            else
            {
                if (pvMain.Items.Contains(piUgoira))
                {
                    pvMain.Items.Remove(piUgoira);
                }
                if (!pvMain.Items.Contains(piMonthly))
                {
                    pvMain.Items.Add(piMonthly);
                }
                if (!pvMain.Items.Contains(piRookie))
                {
                    pvMain.Items.Add(piRookie);
                }
                if (!pvMain.Items.Contains(piOriginal))
                {
                    pvMain.Items.Add(piOriginal);
                }
                if (!pvMain.Items.Contains(piUgoira))
                {
                    pvMain.Items.Add(piUgoira);
                }
            }
            pvMain.SelectionChanged += pvMain_SelectionChanged;
            
        }

        private void btnUgoira_Click(object sender, RoutedEventArgs e)
        {
            if (pvMain.Items.Contains(piUgoira))
            {
                pvMain.SelectedIndex = pvMain.Items.IndexOf(piUgoira);
            }
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            ShowRank(0, 1);
        }
    }
}