﻿using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using System;
using System.Globalization;
using System.IO.IsolatedStorage;
using System.Reflection;
using System.Text;
using System.Windows;

namespace ePix
{
    public partial class AboutPage : PhoneApplicationPage
    {
        public AboutPage()
        {
            InitializeComponent();

            this.Language = System.Windows.Markup.XmlLanguage.GetLanguage(System.Globalization.CultureInfo.CurrentUICulture.Name);

            if (ApplicationBar.Buttons.Count >= 1)
            {
                (ApplicationBar.Buttons[0] as ApplicationBarIconButton).Text = Res.LocalString.review;
            }

            if(ApplicationBar.MenuItems.Count >= 1)
            {
                (ApplicationBar.MenuItems[0] as ApplicationBarMenuItem).Text = Res.LocalString.other;
            }
        }

        private void btnReview_Click(object sender, EventArgs e)
        {
            MarketplaceReviewTask mrt = new MarketplaceReviewTask();
            mrt.Show();
        }

        private void btnOther_Click(object sender, EventArgs e)
        {
            MarketplaceSearchTask mst = new MarketplaceSearchTask();
            mst.ContentType = MarketplaceContentType.Applications;
            mst.SearchTerms = "yygworld";
            mst.Show();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            if (NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            var assembly = Assembly.GetExecutingAssembly().FullName;
            tbVersion.Text = assembly.Split('=')[1].Split(',')[0];

            if (IsolatedStorageSettings.ApplicationSettings.Contains("lsenabled"))
            {
                tgRecent.IsChecked = (bool)IsolatedStorageSettings.ApplicationSettings["lsenabled"];
            }
            if (IsolatedStorageSettings.ApplicationSettings.Contains("invert"))
            {
                tgInvert.IsChecked = (bool)IsolatedStorageSettings.ApplicationSettings["invert"];
            }
        }

        private void HyperlinkButton_Click_1(object sender, RoutedEventArgs e)
        {
            /*AlertParser ap = new AlertParser();
            ap.LastMsg = "";
            ap.Completed += new EventHandler<AlertParser.ResultEventArgs>((o, r) =>
            {
                if (r.result.Length > 0)
                {
                    MessageBox.Show(r.result, Res.LocalString.noticetitle, MessageBoxButton.OK);
                    IsolatedStorageSettings.ApplicationSettings["lm"] = r.result;
                }
            });
            ap.Category = "pixifeed";
            ap.Retrive();*/
            EmailComposeTask ect = new EmailComposeTask();
            ect.To = "yygworld@outlook.com";
            ect.Subject = "[앱]"+ (Communication.isFull ? "Pixifeed+" : "Pixifeed") +" contact";
            StringBuilder sw = new StringBuilder();
            sw.AppendLine();
            sw.AppendLine();
            sw.AppendLine("== DeviceInfo ==");
            sw.AppendLine(string.Format("Manufacture: {0}", Microsoft.Phone.Info.DeviceStatus.DeviceManufacturer));
            sw.AppendLine(string.Format("Name: {0}", Microsoft.Phone.Info.DeviceStatus.DeviceName));
            sw.AppendLine(string.Format("App: {0}", System.Reflection.Assembly.GetExecutingAssembly().FullName));
            sw.AppendLine(string.Format("Devicever: {0}", Microsoft.Phone.Info.DeviceStatus.DeviceFirmwareVersion));
            sw.AppendLine(string.Format("OS: {0} {1}", Environment.OSVersion.Platform, Environment.OSVersion.Version));
            sw.AppendLine(string.Format("Runtime: {0}", Environment.Version));
            ect.Body = sw.ToString();
            ect.Show();
        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            if (IsolatedStorageSettings.ApplicationSettings.Contains("lsearch"))
            {
                IsolatedStorageSettings.ApplicationSettings.Remove("lsearch");
            }
        }

        private void tgRecent_Checked(object sender, RoutedEventArgs e)
        {
            IsolatedStorageSettings.ApplicationSettings["lsenabled"] = tgRecent.IsChecked;
        }

        private void tgInvert_Checked(object sender, RoutedEventArgs e)
        {
            IsolatedStorageSettings.ApplicationSettings["invert"] = tgInvert.IsChecked;
            IsolatedStorageSettings.ApplicationSettings.Save();

            Communication._isInvert = -1;
        }

        int chec = -1;
        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            if(chec < 0)
            {
                if (IsolatedStorageSettings.ApplicationSettings.Contains("t"))
                {
                    chec = (int)IsolatedStorageSettings.ApplicationSettings["t"];
                }
                else
                {
                    chec = 0;
                }
            }

            if (chec == 20)
            {
                chec = 0;
            }
            else
            {
                chec++;
            }
            if (chec == 0)
            {
                tgInvert.Header = "Test option (not affected)";
            }
            else
            {
                tgInvert.Header = string.Format("Test option method {0} (not affected)", chec);
            }
            Communication._t = null;
            IsolatedStorageSettings.ApplicationSettings["t"] = chec;
            IsolatedStorageSettings.ApplicationSettings.Save();
        }
    }
}