﻿using ePix.Model;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System;
using System.Collections;
using System.IO.IsolatedStorage;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Linq;
using System.Windows.Media.Imaging;
using System.IO;

namespace ePix
{
    public partial class People : PhoneApplicationPage
    {
        public People()
        {
            InitializeComponent();
            cmMain = new ContactModel();

            this.Language = System.Windows.Markup.XmlLanguage.GetLanguage(System.Globalization.CultureInfo.CurrentUICulture.Name);

            if (ApplicationBar.Buttons.Count >= 2)
            {
                (ApplicationBar.Buttons[0] as ApplicationBarIconButton).Text = Res.LocalString.addfav;
                (ApplicationBar.Buttons[1] as ApplicationBarIconButton).Text = Res.LocalString.pin;
            }
        }
        const string urlformat = "http://www.pixiv.net/member.php?id={0}";
        string id = string.Empty;
        ContactModel cmMain = null;
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if ((Application.Current.Resources["PhoneDarkThemeVisibility"] as Visibility?) == System.Windows.Visibility.Visible ^ Communication.isInvert)
            {
                pivPeople.Background = new SolidColorBrush(Color.FromArgb(255, 37, 39, 41));
                SystemTray.BackgroundColor = Color.FromArgb(255, 37, 39, 41);
                SystemTray.ForegroundColor = Colors.White;
            }

            if (NavigationContext.QueryString.ContainsKey("id"))
            {
                id = NavigationContext.QueryString["id"];
            }
            if (string.IsNullOrEmpty(cmMain.Nick))
            {
                GetUserInfo();
            }
        }

        private void GetUserInfo()
        {
            SystemTray.SetProgressIndicator(this, new ProgressIndicator()
            {
                Text = Res.LocalString.retpeople,
                IsIndeterminate = true,
                IsVisible = true
            });
            if (!Communication.isLogin && !IsolatedStorageSettings.ApplicationSettings.Contains("id"))
            {
                MessageBox.Show(Res.LocalString.errorprofile, Res.LocalString.errorpixifeed, MessageBoxButton.OK);
                if (NavigationService.CanGoBack)
                {
                    NavigationService.GoBack();
                }
                return;
            }
            string uri = string.Format(urlformat, id);
            Communication.Communicate(uri, (suc, res) =>
            {
                this.Dispatcher.BeginInvoke(() =>
                {
                    After(uri, suc, res, true);
                    return;
                });
            }, false);
        }

        private void After(string uri, int suc, string res, bool first)
        {
            if (suc >= 0)
            {
                if (cmMain.ocContactInfo != null)
                {
                    cmMain.ocContactInfo.Clear();
                }
                Communication.isLogin = (res.IndexOf("user.loggedIn = true") > 0);
                if (Interpreter.ViewProfile(ref cmMain, res))
                {
                    this.DataContext = null;
                    this.DataContext = cmMain;
                }
                else if (first && !Communication.isLogin && IsolatedStorageSettings.ApplicationSettings.Contains("id") && IsolatedStorageSettings.ApplicationSettings.Contains("pass"))
                {
                    string mid = HttpUtility.UrlEncode(IsolatedStorageSettings.ApplicationSettings["id"] as string);
                    string pass = HttpUtility.UrlEncode(IsolatedStorageSettings.ApplicationSettings["pass"] as string);
                    Login.LoginSilent(mid, pass, this, () =>
                    {
                        this.Dispatcher.BeginInvoke(() =>
                        {
                            Communication.Communicate(uri, (com2, result2) =>
                            {
                                this.Dispatcher.BeginInvoke(() =>
                                {
                                    After(uri, com2, result2, false);
                                });
                            });
                        });
                    });
                }
                else
                {
                    MessageBox.Show(Res.LocalString.errorprofile, Res.LocalString.errorpixifeed, MessageBoxButton.OK);
                    if (NavigationService.CanGoBack)
                    {
                        NavigationService.GoBack();
                    }
                    return;
                }
                lbMain.ItemsSource = cmMain.ocContactInfo;

                if (res.IndexOf("my-profile-url-unit") >= 0)
                {
                    //내 프로필
                    btnSignout.Visibility = System.Windows.Visibility.Visible;
                    lpFavorite.Visibility = System.Windows.Visibility.Visible;
                }
                else
                {
                    //남의 프로필
                    btnSignout.Visibility = System.Windows.Visibility.Collapsed;
                    lpFavorite.Visibility = System.Windows.Visibility.Collapsed;

                    if (imf = Interpreter.IsUserFavorite(res))
                    {
                        (ApplicationBar.Buttons[0] as ApplicationBarIconButton).IsEnabled = true;
                        (ApplicationBar.Buttons[0] as ApplicationBarIconButton).IconUri = new Uri("/Images/favs.del.png", UriKind.Relative);
                        (ApplicationBar.Buttons[0] as ApplicationBarIconButton).Text = Res.LocalString.delfav;
                    }
                    else
                    {
                        (ApplicationBar.Buttons[0] as ApplicationBarIconButton).IsEnabled = true;
                        (ApplicationBar.Buttons[0] as ApplicationBarIconButton).IconUri = new Uri("/Images/favs.addto.png", UriKind.Relative);
                        (ApplicationBar.Buttons[0] as ApplicationBarIconButton).Text = Res.LocalString.addfav;
                    }
                }
            }
            else
            {
                MessageBox.Show(Res.LocalString.errorpcon, Res.LocalString.errorpixifeed, MessageBoxButton.OK);
                if (NavigationService.CanGoBack)
                {
                    NavigationService.GoBack();
                }
            }
            SystemTray.SetProgressIndicator(this, null);
        }

        private void Image_ImageOpened(object sender, RoutedEventArgs e)
        {
            (sender as Image).Opacity = 1;
        }

        int nextpg = 1;
        int nextpg2 = 1;
        private void pivPeople_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cmMain.ocWorks.Clear();
            GC.Collect();
            nextpg = 0;
            flast = 0;

            if (pivPeople.SelectedIndex == 1)
            {
                if (gdList1.Children.Count > 0)
                {
                    gdList1.Children.Clear();
                    gdList0.Children.Add(lbWorks);
                }
            }
            else if (pivPeople.SelectedIndex == 2)
            {
                if (lpFavorite.Items.Count == 0)
                {
                    lpFavorite.Items.Add(Res.LocalString.ppublic);
                    lpFavorite.Items.Add(Res.LocalString.pprivate);
                }

                if (gdList0.Children.Count > 0)
                {
                    gdList0.Children.Clear();
                    gdList1.Children.Add(lbWorks);
                }
            }
            else
            {
                tbNothing.Visibility = System.Windows.Visibility.Collapsed;
            }

            if (pivPeople.SelectedIndex == 1 || pivPeople.SelectedIndex == 2)
            {
                RefreshWorks(1);
                this.ApplicationBar.Mode = ApplicationBarMode.Minimized;
            }
            else
            {
                this.ApplicationBar.Mode = ApplicationBarMode.Default;
            }
        }

        bool working = false;
        private void RefreshWorks(int newpg)
        {
            if (working)
            {
                return;
            }
            working = true;
            tbNothing.Visibility = System.Windows.Visibility.Collapsed;

            SystemTray.SetProgressIndicator(this, new ProgressIndicator()
            {
                IsIndeterminate = true,
                IsVisible = true,
                Text = Res.LocalString.retworks
            });

            bool isFavorite = pivPeople.SelectedIndex == 2;
            string targeturi = isFavorite ? string.Format("http://www.pixiv.net/bookmark.php?id={0}", id)
                : string.Format("http://www.pixiv.net/member_illust.php?id={0}", id);
            if (isFavorite && lpFavorite.Visibility == System.Windows.Visibility.Visible && lpFavorite.SelectedIndex == 1)
            {
                targeturi += "&rest=hide";
            }

            if (newpg > 1)
            {
                targeturi += string.Format("&p={0}", newpg);
            }

            Communication.Communicate(targeturi, (suc, res) =>
            {
                if (suc >= 0)
                {
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        if (isFavorite ? Interpreter.GetUserBookmarks(ref cmMain.ocWorks, (newpg == 1), res)
                            : Interpreter.ShowWorks(ref cmMain.ocWorks, (newpg == 1), res))
                        {
                            nextpg = newpg + 1;
                        }
                        else if (newpg == 1 && pivPeople.SelectedIndex > 0 && pivPeople.SelectedIndex < 3)
                        {
                            tbNothing.Visibility = System.Windows.Visibility.Visible;
                        }
                        lbWorks.ItemsSource = cmMain.ocWorks;
                    });
                }
                working = false;
                this.Dispatcher.BeginInvoke(() =>
                    {
                        SystemTray.SetProgressIndicator(this, null);
                        if (cmMain.ocWorks.Count > 0 && newpg == 1)
                        {
                            lbWorks.ScrollTo(cmMain.ocWorks[0]);
                        }
                    });
            });
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            if (NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }
        }

        private void btnPin_Click(object sender, EventArgs e)
        {
            if (cmMain != null && !string.IsNullOrEmpty(cmMain.Pic))
            {
                WebClient wc = new WebClient();
                wc.OpenReadCompleted += new OpenReadCompletedEventHandler(wc_OpenReadCompleted);
                wc.OpenReadAsync(new Uri(cmMain.Pic));
            }
        }

        void wc_OpenReadCompleted(object sender, OpenReadCompletedEventArgs e)
        {
            try
            {
                if(e.Error != null)
                {
                    if (e.Error is WebException)
                    {
                        MessageBox.Show(Res.LocalString.errortileun + Environment.NewLine + Environment.NewLine
                    + Res.LocalString.noconnection, Res.LocalString.errortile, MessageBoxButton.OK);
                    }
                    else
                    {
                        MessageBox.Show(Res.LocalString.errortileun + Environment.NewLine + Environment.NewLine
                        + e.Error.GetType().ToString(), Res.LocalString.errortile, MessageBoxButton.OK);
                    }
                    return;
                }

                var file = IsolatedStorageFile.GetUserStoreForApplication();

                if (!file.DirectoryExists(@"Shared\ShellContent\People"))
                {
                    file.CreateDirectory(@"Shared\ShellContent\People");
                }

                BitmapImage bi = new BitmapImage();
                //작은 타일 변경 필요함.
                bi.SetSource(e.Result);
                WriteableBitmap wb = CreateTileImage(bi, cmMain.Nick);
                SaveTileImage(wb, string.Format(@"Shared\ShellContent\People\{0}.png",
                    id));
                wb = CreateSmallTileImage(bi);
                SaveTileImage(wb, string.Format(@"Shared\ShellContent\People\{0}_small.png",
                    id));

                file.Dispose();
                FlipTileData ftd = new FlipTileData()
                {
                    SmallBackgroundImage = new Uri(string.Format(@"isostore:/Shared/ShellContent/People/{0}_small.png",
                    id)),
                    BackTitle = string.Empty,
                    BackContent = string.Empty,
                    Title = cmMain.Nick,
                    BackgroundImage = new Uri(string.Format(@"isostore:/Shared/ShellContent/People/{0}.png",
                    id))
                };
                ShellTile.Create(new Uri("/MainPage.xaml?id=" + id, UriKind.Relative), ftd, false);
            }
            catch (InvalidOperationException)
            {
                MessageBox.Show(Res.LocalString.errortiledup, Res.LocalString.errortile, MessageBoxButton.OK);
            }
            catch (Exception ex)
            {
                MessageBox.Show(Res.LocalString.errortileun + Environment.NewLine + Environment.NewLine
                    + ex.GetType().ToString(), Res.LocalString.errortile, MessageBoxButton.OK);
            }
        }

        private WriteableBitmap CreateTileImage(BitmapImage img, string text)
        {
            var image = new Image { Source = img, Stretch = Stretch.UniformToFill, Width = 336, Height = 336 };
            var icon = new Image { Source = new BitmapImage() { CreateOptions = BitmapCreateOptions.None }, Stretch = Stretch.Fill, Width = 62, Height = 62 };

            System.Windows.Resources.StreamResourceInfo sri = Application.GetResourceStream(new Uri("ApplicationIcon.png", UriKind.Relative));
            (icon.Source as BitmapImage).SetSource(sri.Stream);

            Canvas.SetLeft(image, 0);
            Canvas.SetTop(image, 0);

            TextBlock tb = new TextBlock()
            {
                Text = text,
                FontSize = 30
            };

            System.Windows.Shapes.Rectangle rec = new System.Windows.Shapes.Rectangle()
            {
                Width = 336,
                Height = tb.ActualHeight+20,
                Fill = new SolidColorBrush(Color.FromArgb(127,0,0,0)),
                Stroke = new SolidColorBrush(Colors.Transparent),
                StrokeThickness = 0
            };

            //b.Child = tb;
            rec.UpdateLayout();
            image.UpdateLayout();

            // Measure the actual size of the TextBlock, this is with
            // the characters full hight/width (includning char spaceing)
            var width = tb.ActualWidth;
            var height = tb.ActualHeight;

            // Place the text in the lower right corner
            Canvas.SetLeft(rec, 0);
            Canvas.SetTop(rec, 336 - height - 20);

            Canvas.SetLeft(icon, 0);
            Canvas.SetTop(icon, 336 - height - 20 - icon.ActualHeight);

            TextBlock tbp = new TextBlock()
            {
                Text = Res.LocalString.user,
                FontSize = 32,
                Foreground = new SolidColorBrush(Colors.White)
            };

            System.Windows.Shapes.Rectangle recs = new System.Windows.Shapes.Rectangle()
            {
                Width = tbp.ActualWidth+20,
                Height = tbp.ActualHeight+20,
                Fill = new SolidColorBrush(Color.FromArgb(127, 0, 0, 0)),
                Stroke = new SolidColorBrush(Colors.Transparent),
                StrokeThickness = 0
            };

            Canvas.SetLeft(tbp, icon.ActualWidth+10);
            Canvas.SetTop(tbp, 336 - height - 20 - tbp.ActualHeight - 10);
            Canvas.SetLeft(recs, icon.ActualWidth);
            Canvas.SetTop(recs, 336 - height - 20 - tbp.ActualHeight - 20);

            // Create a canvas and place the image and text on it
            var canvas = new Canvas
            {
                Height = 176,
                Width = 176
            };

            // This is where the images is placed
            // based on the Canvas.SetLeft &amp; SetTop values
            canvas.Children.Add(image);
            canvas.Children.Add(icon);
            canvas.Children.Add(recs);
            canvas.Children.Add(tbp);
            canvas.Children.Add(rec);
            canvas.UpdateLayout();

            // Render all of it to the WritableBitmap
            var wbmp = new WriteableBitmap(336, 336);

            wbmp.Render(canvas, null);
            wbmp.Invalidate();

            CompensateForRender(wbmp.Pixels);
            wbmp.Invalidate();

            return wbmp;
        }

        private WriteableBitmap CreateSmallTileImage(BitmapImage img)
        {
            var image = new Image { Source = img, Stretch = Stretch.UniformToFill, Width = 336, Height = 336 };
            var icon = new Image { Source = new BitmapImage() { CreateOptions = BitmapCreateOptions.None }, Stretch = Stretch.Fill, Width = 62, Height = 62 };

            System.Windows.Resources.StreamResourceInfo sri = Application.GetResourceStream(new Uri("ApplicationIcon.png", UriKind.Relative));
            (icon.Source as BitmapImage).SetSource(sri.Stream);

            Canvas.SetLeft(image, 0);
            Canvas.SetTop(image, 0);
            image.UpdateLayout();

            Canvas.SetLeft(icon, 0);
            Canvas.SetTop(icon, 336 - icon.ActualHeight);

            // Create a canvas and place the image and text on it
            var canvas = new Canvas
            {
                Height = 176,
                Width = 176
            };

            // This is where the images is placed
            // based on the Canvas.SetLeft &amp; SetTop values
            canvas.Children.Add(image);
            canvas.Children.Add(icon);
            canvas.UpdateLayout();

            // Render all of it to the WritableBitmap
            var wbmp = new WriteableBitmap(336, 336);

            wbmp.Render(canvas, null);
            wbmp.Invalidate();

            CompensateForRender(wbmp.Pixels);
            wbmp.Invalidate();

            return wbmp;
        }

        private void SaveTileImage(WriteableBitmap wbmp, string path)
        {
            using (var store = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (store.FileExists(path))
                    store.DeleteFile(path);
                var stream = store.OpenFile(path, FileMode.OpenOrCreate);
                wbmp.WritePNG(stream);
                stream.Close();
            }
        }

        public void CompensateForRender(int[] bitmapPixels)
{
    if (bitmapPixels.Length == 0) return;

    for (int i = 0; i > bitmapPixels.Length; i++)
    {
        uint pixel = unchecked((uint)bitmapPixels[i]);

        double a = (pixel >> 24) & 255;
        if ((a == 255) || (a == 0)) continue;

        double r = ( pixel >> 16 ) & 255;
        double g = ( pixel >> 8 ) & 255;
        double b = ( pixel ) & 255;

        double factor = 255 / a;
        uint newR = (uint)Math.Round(r * factor);
        uint newG = (uint)Math.Round(g * factor);
        uint newB = (uint)Math.Round(b * factor);

        // compose
        bitmapPixels[i] = unchecked((int)(( pixel & 0xFF000000 ) | ( newR << 16 ) | ( newG << 8 ) | newB ));
    }
        }

        bool imf = false;
        private void btnFav_Click(object sender, EventArgs e)
        {
            if (imf)
            {
                Communication.CommunicatePOST("http://www.pixiv.net/rpc_group_setting.php",
                    string.Format("mode=del&type=bookuser&id={0}&tt={1}", id, Interpreter.token),
                    (a, b) =>
                    {
                        if (b.Length > 0)
                        {
                            this.Dispatcher.BeginInvoke(() =>
                            {
                                GetUserInfo();
                            });
                        }
                    });
            }
            else
            {
                CustomMessageBox cm = new CustomMessageBox()
                {
                    Caption = Res.LocalString.pfavcap,
                    Message = Res.LocalString.pfavtxt,
                    LeftButtonContent = Res.LocalString.ppublic,
                    RightButtonContent = Res.LocalString.pprivate,
                    Language = this.Language
                };
                SystemTray.BackgroundColor = (Color)Application.Current.Resources["PhoneChromeColor"];
                SystemTray.ForegroundColor = (Color)Application.Current.Resources["PhoneForegroundColor"];
                SystemTray.Opacity = 1;
                cm.Dismissed += cm_Dismissed;
                cm.Show();
            }
        }

        const string uriaf = "http://www.pixiv.net/bookmark_add.php";
        const string afparam = "mode=add&type=user&user_id={0}&tt={1}&from_sid=&restrict={2}&left_column=OK";
        private Rect lastview;
        private int flast;
        void cm_Dismissed(object sender, DismissedEventArgs e)
        {
            SystemTray.BackgroundColor = Colors.White;
            SystemTray.ForegroundColor = Colors.Black;
            SystemTray.Opacity = 0;
            if (e.Result == CustomMessageBoxResult.None)
            {
                return;
            }
            int isw = e.Result == CustomMessageBoxResult.RightButton ? 1 : 0;
            string eparam = string.Format(afparam, id, Interpreter.token, isw);
            Communication.CommunicatePOST(uriaf, eparam, (a, b) =>
            {
                if (a >= 0)
                {
                    int iSCU = CommonMethod.SearchTagAttributes("div", "complete-unit", 0, b);
                    if (iSCU > 0)
                    {
                        int iSCH = CommonMethod.SearchTagAttributesEnd("h1", "column-title", iSCU, b);
                        if (iSCH > iSCU)
                        {
                            int iECH = b.IndexOf("</h", iSCH);
                            if (iECH > iSCH)
                            {
                                string s = HttpUtility.HtmlDecode(CommonMethod.RemoveAllTags(b.Substring(iSCH, iECH - iSCH)));
                                this.Dispatcher.BeginInvoke(() =>
                                {
                                    MessageBox.Show(s, "Pixifeed", MessageBoxButton.OK);
                                    GetUserInfo();
                                });
                            }
                        }
                    }
                }
            }, false, false);
        }

        private void ViewportControl_ViewportChanged(object sender, ViewportChangedEventArgs e)
        {
            ViewportControl vc = sender as ViewportControl;
            lastview = vc.Viewport;
            if (cmMain != null && cmMain.ocWorks != null && cmMain.ocWorks.Count > 0 && vc.Viewport.Bottom >= vc.Bounds.Bottom - 140 && flast != nextpg)
            {
                int ipg = pivPeople.SelectedIndex;
                try
                {
                    flast = nextpg;
                    RefreshWorks(nextpg);
                }
                catch { }
            }
        }

        private void btnSignout_Click(object sender, RoutedEventArgs e)
        {
            btnSignout.IsEnabled = false;
            Communication.Communicate("http://www.pixiv.net/logout.php", (suc, res) =>
            {
                this.Dispatcher.BeginInvoke(() =>
                {
                    try
                    {
                        if (suc >= 0)
                        {
                            Communication.isLogin = false;
                            Communication.needRefresh = true;
                            if (IsolatedStorageSettings.ApplicationSettings.Contains("id"))
                            {
                                IsolatedStorageSettings.ApplicationSettings.Remove("id");
                            }
                            if (IsolatedStorageSettings.ApplicationSettings.Contains("pass"))
                            {
                                IsolatedStorageSettings.ApplicationSettings.Remove("pass");
                            }
                            IsolatedStorageSettings.ApplicationSettings.Save();

                            if (NavigationService.CanGoBack)
                            {
                                NavigationService.GoBack();
                            }
                        }
                        else
                        {
                            btnSignout.IsEnabled = true;
                            MessageBox.Show(Res.LocalString.errorlogout, Res.LocalString.errorpixifeed, MessageBoxButton.OK);
                        }
                    }
                    catch (Exception ex)
                    {
                        btnSignout.IsEnabled = true;
                        MessageBox.Show(Res.LocalString.errorlogout, ex.GetType().ToString(), MessageBoxButton.OK);
                    }
                });
            });
        }

        private void lp_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (pivPeople.SelectedIndex == 2 && lpFavorite != null)
            {
                RefreshWorks(1);
            }
        }

    }
}