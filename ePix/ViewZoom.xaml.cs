﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO;
using System.Windows.Media.Imaging;
using System.IO.IsolatedStorage;
using Microsoft.Xna.Framework.Media;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using Nokia.Graphics.Imaging;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace ePix
{
    public class PicMeta
    {
        public PicMeta(int wid, int hei, long size)
        {
            Width = wid;
            Height = hei;
            if (size < 1024)
            {
                Size = string.Format("{0} Bytes", size);
            }
            else if (size < 1024 * 1024)
            {
                Size = string.Format("{0} KB", Math.Round(size / 1024.0, 2));
            }
            else if (size < 1024 * 1024 * 1024)
            {
                Size = string.Format("{0} MB", Math.Round(size / 1024.0 / 1024.0, 2));
            }
        }

        public int Width { get; set; }
        public int Height { get; set; }
        public int Type { get; set; }
        public string TypeStr { get; set; }
        public string Ext { get; set; }
        /*public int Bitrate { get; set; }*/
        public string Size { get; set; }
    }

    public class PicTile
    {
        public BitmapImage Pic { get; set; }
        public string Page { get; set; }
        public int pg { get; set; }
    }

    public partial class ViewZoom : PhoneApplicationPage
    {
        ePix.Model.IllustData idata = null;

        public ViewZoom()
        {
            InitializeComponent();

            this.Language = System.Windows.Markup.XmlLanguage.GetLanguage(System.Globalization.CultureInfo.CurrentUICulture.Name);

            if (ApplicationBar.Buttons.Count >= 4)
            {
                (ApplicationBar.Buttons[0] as ApplicationBarIconButton).Text = Res.LocalString.back;
                (ApplicationBar.Buttons[1] as ApplicationBarIconButton).Text = Res.LocalString.tile;
                (ApplicationBar.Buttons[2] as ApplicationBarIconButton).Text = Res.LocalString.save;
                (ApplicationBar.Buttons[3] as ApplicationBarIconButton).Text = Res.LocalString.next;
            }
        }

        const string biguri = "http://www.pixiv.net/member_illust.php?mode={0}&illust_id={1}";
        string id = string.Empty;
        string type = "big";
        int currentImage = 0;

        List<string> bigimages = new List<string>();
        List<string> smallimgs = new List<string>();
        Dictionary<int, PicMeta> imgmeta = new Dictionary<int, PicMeta>();
        //List<System.Net.HttpWebRequest> lrequest = new List<System.Net.HttpWebRequest>();
        List<WebClient> lrequest = new List<WebClient>();
        List<PicTile> lpTiles = new List<PicTile>();
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (NavigationContext.QueryString.ContainsKey("mode"))
            {
                type = NavigationContext.QueryString["mode"];
            }
            else
            {
                if (NavigationService.CanGoBack)
                {
                    NavigationService.GoBack();
                }
                return;
            }

            if (NavigationContext.QueryString.ContainsKey("title"))
            {
                tbTitle.Text = NavigationContext.QueryString["title"];
            }
            if (NavigationContext.QueryString.ContainsKey("illust_id") && wbView.Source == null
                && NavigationContext.QueryString.ContainsKey("url"))
            {
                id = NavigationContext.QueryString["illust_id"];
                string url = NavigationContext.QueryString["url"];
                SystemTray.SetIsVisible(this, true);
                SystemTray.SetProgressIndicator(this, new ProgressIndicator()
                {
                    IsIndeterminate = true,
                    IsVisible = true,
                    Text = Res.LocalString.retfull
                });
                if (type == "single")
                {
                    if (NavigationContext.QueryString.ContainsKey("fullsize"))
                    {
                        string fullsize = NavigationContext.QueryString["fullsize"];

                        if (!string.IsNullOrEmpty(fullsize))
                        {
                            bigimages.Clear();
                            bigimages.Add(fullsize);
                            ManageImages();
                        }
                        else
                        {
                            MessageBox.Show(Res.LocalString.errorcont, Res.LocalString.errorpixifeed, MessageBoxButton.OK);
                            if (NavigationService.CanGoBack)
                            {
                                NavigationService.GoBack();
                            }
                            return;
                        }
                    }
                }
                else
                {
                    Communication.Communicate(string.Format(biguri, type, id), (suc, res) =>
                    {
                        if (suc >= 0)
                        {
                            this.Dispatcher.BeginInvoke(() =>
                            {
                                bigimages.Clear();
                                if (type == "ugoira_view")
                                {
                                    idata = Interpreter.GetUgoiraData(res);
                                }
                                else if (type == "manga")
                                {
                                    bigimages = Interpreter.EnumBigImages(res);
                                }
                                else //이제 이곳은 작동하지 않음. 위에서 처리.
                                {
                                    bigimages.Add(Interpreter.SingleBigImage(res));
                                }
                                if (bigimages.Count > 0)
                                {
                                    ManageImages();
                                }
                                else if (idata != null && !string.IsNullOrEmpty(idata.src))
                                {
                                    ManageUgoira();
                                }
                                else
                                {
                                    MessageBox.Show(Res.LocalString.errorcont, Res.LocalString.errorpixifeed, MessageBoxButton.OK);
                                    if (NavigationService.CanGoBack)
                                    {
                                        NavigationService.GoBack();
                                    }
                                    return;
                                }
                            });
                        }
                        else
                        {
                            this.Dispatcher.BeginInvoke(() =>
                            {
                                if (NavigationService.CanGoBack)
                                {
                                    NavigationService.GoBack();
                                }
                            });
                        }
                    }, true, url);
                }
            }
        }

        private void ManageUgoira()
        {
            /*
            if (idata != null && !string.IsNullOrEmpty(idata.src))
            {
                Communication.CommunicateBinary(idata.src,
                    (a, b, c) =>
                    {
                        this.Dispatcher.BeginInvoke(async () =>
                        {
                            try
                            {
                                if (a >= 0)
                                {
                                    if (SystemTray.ProgressIndicator != null)
                                    {
                                        SystemTray.ProgressIndicator.Text = Res.LocalString.ugoirafilling;
                                    }
                                    UnZipper unzip = new UnZipper(b);
                                    List<IImageProvider> lsIp = new List<IImageProvider>();
                                    List<string> lsf = unzip.FileNamesInZip().ToList();
                                    foreach (ePix.Model.Frame mf in idata.frames)
                                    {
                                        string FileName = mf.file;
                                        if (lsf.Contains(FileName))
                                        {
                                            Stream st = unzip.GetFileStream(FileName);
                                            StreamImageSource sis = new StreamImageSource(st);
                                            if(lsIp.Count < 40)
                                            {
                                                lsIp.Add(sis);
                                            }
                                        }
                                    }
                                    if (lsIp.Count == idata.frames.Count)
                                    {
                                        using(var renderer = new GifRenderer())
                                        {
                                            renderer.Sources = lsIp;
                                            var m = await renderer.RenderAsync();
                                            using(IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                                            {
                                                using(IsolatedStorageFileStream isfs = isf.OpenFile("tobe.gif",FileMode.Create))
                                                {
                                                    
                                                    isfs.Write()
                                                    ws.
                                                }
                                            }
                                        }


                                        
                                    }
                                    else
                                    {
                                        if (SystemTray.ProgressIndicator != null)
                                        {
                                            SystemTray.ProgressIndicator.Text = "_" + Res.LocalString.ugoiraerror;
                                        }
                                    }
                                    unzip.Dispose();
                                }
                                else
                                {
                                    if (SystemTray.ProgressIndicator != null)
                                    {
                                        SystemTray.ProgressIndicator.Text = Res.LocalString.ugoiraerror;
                                    }
                                }
                            }
                            catch
                            {
                                if (SystemTray.ProgressIndicator != null)
                                {
                                    SystemTray.ProgressIndicator.Text = Res.LocalString.ugoiraerror;
                                }
                            }
                            if (SystemTray.ProgressIndicator != null)
                            {
                                SystemTray.ProgressIndicator.IsIndeterminate = false;
                                SystemTray.ProgressIndicator.IsVisible = false;
                                SystemTray.ProgressIndicator.Text = Res.LocalString.ugoiraerror;
                            }
                        });
                    }, (prog, file) =>
                    {
                        this.Dispatcher.BeginInvoke(() =>
                        {
                            if (SystemTray.ProgressIndicator != null)
                            {
                                SystemTray.ProgressIndicator.Value = prog / 100.0;
                            }
                        });
                    });
            }*/
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (lrequest != null && lrequest.Count > 0)
            {
                for (int i = 0; i < lrequest.Count; i++)
                {
                    try
                    {
                        if (lrequest[i] != null)
                        {
                            //lrequest[i].Abort();
                            lrequest[i].CancelAsync();
                            lrequest.Remove(lrequest[i]);
                        }

                        if (sbRotate != null)
                        {
                            sbRotate.Stop();
                            sbRotate.Children.Clear();
                        }

                        if (e.IsNavigationInitiator)
                        {
                            lpTiles.Clear();
                            lbTiles.ItemsSource = null;
                        }
                    }
                    catch
                    { }
                }
            }

            base.OnNavigatedFrom(e);
        }

        bool imginit = false;
        int ic = 0, ia = 0;
        List<int> lprog = new List<int>();
        public void ManageImages()
        {
            smallimgs.Clear();
            imgmeta.Clear();
            lprog.Clear();
            for (int i = 0; i < bigimages.Count; i++)
            {
                string s = bigimages[i];
                smallimgs.Add(s);
                lprog.Add(0);
                if (NavigationContext.QueryString.ContainsKey("return") && !string.IsNullOrEmpty(NavigationContext.QueryString["return"]) && NavigationService.CanGoBack)
                {
                    break;
                }
            }

            ic = 0; ia = 0;
            try
            {
                using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    if (!isf.DirectoryExists("zoom"))
                    {
                        isf.CreateDirectory("zoom");
                    }
                    else
                    {
                        string[] m = isf.GetFileNames(@"zoom\*.img");
                        foreach (string n in m)
                        {
                            isf.DeleteFile(@"zoom\" + n);
                        }
                    }
                }
            }
            catch { }

            if (SystemTray.ProgressIndicator != null)
            {
                SystemTray.ProgressIndicator.Text = string.Format(Res.LocalString.bigready, smallimgs.Count);
            }
            lrequest.Clear();
            tbError.Visibility = System.Windows.Visibility.Collapsed;
            MetaPanel.Visibility = System.Windows.Visibility.Collapsed;
            bdPage.Visibility = smallimgs.Count > 1 ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
            foreach (string img in smallimgs)
            {
                //System.Net.HttpWebRequest hwr = Communication.CommunicateBinary(img, (suc, stream, respuri) =>
                WebClient hwr = Communication.CommunicateBinary(img, (suc, stream, respuri) =>
                {
                    #region old

                    int im = 0;
                    if (respuri.Length > 0)
                    {
                        im = smallimgs.IndexOf(respuri);
                        if (im < 0)
                        {
                            return;
                        }
                    }
                    else
                    {
                        return;
                    }
                    if (suc >= 0)
                    {
                        this.Dispatcher.BeginInvoke(() =>
                        {
                            using (MemoryStream ms = new MemoryStream())
                            {
                                stream.CopyTo(ms);
                                stream.Close();
                                using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                                {
                                    string tname = string.Format(@"zoom\{0}.img", im);
                                    if (!isf.DirectoryExists("zoom"))
                                    {
                                        isf.CreateDirectory("zoom");
                                    }
                                    else
                                    {
                                        try
                                        {
                                            if (isf.FileExists(tname))
                                            {
                                                isf.DeleteFile(tname);
                                            }
                                        }
                                        catch { }
                                    }
                                    using (IsolatedStorageFileStream isfs = isf.OpenFile(tname,
                                        System.IO.FileMode.Create))
                                    {
                                        ms.Position = 0;
                                        ms.CopyTo(isfs);
                                    }
                                }
                                ms.Position = 0;
                                if (MakeMiddleImage(ms, im))
                                {
                                    if (im == 0)
                                    {
                                        if (NavigationContext.QueryString.ContainsKey("return") && !string.IsNullOrEmpty(NavigationContext.QueryString["return"]) && NavigationService.CanGoBack)
                                        {
                                            btnSave_Click(null, null);
                                        }
                                        else
                                        {
                                            LoadMiddleImage(0);
                                        }
                                    }
                                }
                            }

                            ic++;
                            if (SystemTray.ProgressIndicator != null)
                            {
                                SystemTray.ProgressIndicator.Text = ia > 0 ? string.Format(Res.LocalString.bigdownfail, ic, smallimgs.Count, ia)
                                    : string.Format(Res.LocalString.bigdown, ic, smallimgs.Count);
                                if (ic == smallimgs.Count && ia == 0)
                                {
                                    SystemTray.ProgressIndicator.Text = "";
                                    SystemTray.SetProgressIndicator(this, null);
                                    SystemTray.SetIsVisible(this, false);
                                    if (smallimgs.Count > 1)
                                    {
                                        (ApplicationBar.Buttons[1] as ApplicationBarIconButton).IsEnabled = true;
                                    }
                                }
                            }
                        });
                    }
                    else
                    {
                        this.Dispatcher.BeginInvoke(() =>
                        {
                            ia++;
                            if (SystemTray.ProgressIndicator != null)
                            {
                                SystemTray.ProgressIndicator.Text = string.Format(Res.LocalString.bigdownfail, ic, smallimgs.Count, ia);
                            }
                        });
                    }

                    #endregion
                }, (prog, file) =>
                {
                    int i = smallimgs.IndexOf(file);
                    lprog[i] = prog;

                    int j = 0;
                    foreach (int k in lprog)
                    {
                        j += k;
                    }

                    ProgressIndicator pi = SystemTray.ProgressIndicator;
                    if (pi != null)
                    {
                        pi.IsIndeterminate = false;
                        pi.Value = j / (double)(100 * lprog.Count);
                    }
                });
                lrequest.Add(hwr);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            int currentnum = Math.Max(currentImage, 0);
            try
            {
                using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream isfs = isf.OpenFile(string.Format(@"zoom\{0}.img", currentnum), FileMode.Open))
                    {
                        BitmapImage bi = new BitmapImage();
                        bi.CreateOptions = BitmapCreateOptions.None;
                        bi.SetSource(isfs);
                        WriteableBitmap wb = new WriteableBitmap(bi);
                        MemoryStream ms = new MemoryStream();
                        wb.SaveJpeg(ms, bi.PixelWidth, bi.PixelHeight, 0, 100);
                        ms.Position = 0;

                        MediaLibrary mb = new MediaLibrary();
                        Picture pic = mb.SavePicture(string.Format("{0}.{1}", id, imgmeta[currentnum].Ext), ms);
                        ms.Close();
                        if (pic != null)
                        {
                            MessageBox.Show(Res.LocalString.savethumbok,
                                Res.LocalString.alert, MessageBoxButton.OK);
                        }
                        mb.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Res.LocalString.errorbang + Environment.NewLine + ex.GetType().ToString(),
                    Res.LocalString.errorpixifeed, MessageBoxButton.OK);
            }

            if (NavigationContext.QueryString.ContainsKey("return"))
            {
                if (!string.IsNullOrEmpty(NavigationContext.QueryString["return"]) && NavigationService.CanGoBack)
                {
                    NavigationService.GoBack();
                }
            }
        }

        private static WriteableBitmap CropImage(WriteableBitmap source,
                                                         int xOffset, int yOffset,
                                                         int width, int height)
        {
            // Get the width of the source image
            var sourceWidth = source.PixelWidth;

            // Get the resultant image as WriteableBitmap with specified size
            var result = new WriteableBitmap(width, height);

            // Create the array of bytes
            for (var x = 0; x <= height - 1; x++)
            {
                var sourceIndex = xOffset + (yOffset + x) * sourceWidth;
                var destinationIndex = x * width;

                Array.Copy(source.Pixels, sourceIndex, result.Pixels, destinationIndex, width);
            }
            return result;
        }

        private bool MakeMiddleImage(Stream sr, int num)
        {
            try
            {
                sr.Position = 0;
                WriteableBitmap wb = new WriteableBitmap(0, 0);
                wb.SetSource(sr);
                imgmeta.Add(num, new PicMeta(wb.PixelWidth, wb.PixelHeight, sr.Length));
                sr.Position = 0;

                if (smallimgs.Count > 1)
                {
                    BitmapImage bi = new BitmapImage();
                    bi.DecodePixelHeight = 170;
                    bi.SetSource(sr);

                    var pt = new PicTile()
                    {
                        Pic = bi,
                        pg = num,
                        Page = (num + 1).ToString()
                    };

                    if (num <= lpTiles.Count)
                    {
                        lpTiles.Insert(num, pt);
                    }
                    else
                    {
                        lpTiles.Add(pt);
                    }
                    sr.Position = 0;
                }
                //sr.Position = 0;
                //byte[] test = ShrinkImage(sr);                
                //using (MemoryStream ms = new MemoryStream(sr))
                //{
                //using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                //{
                //using (IsolatedStorageFileStream isfs = isf.OpenFile(string.Format(@"zoom\{0}_mid.img", num), FileMode.Create))
                //{
                //    sr.CopyTo(isfs);
                //}
                //}
                //}
                return true;
            }
            catch
            {
                return false;
            }
        }

        private async Task<bool> LoadMiddleImage(int targetnum)
        {
            bool isBlack = (Application.Current.Resources["PhoneDarkThemeVisibility"] as Visibility?)
                                            == System.Windows.Visibility.Visible;
            if (imgmeta == null || imgmeta.Count <= targetnum)
            {
                SystemTray.SetProgressIndicator(this, null);
                SystemTray.SetIsVisible(this, false);
                return false;
            }
            try
            {
                using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    wbView.Visibility = System.Windows.Visibility.Collapsed;
                    if (!isf.FileExists(string.Format(@"zoom\{0}.img", targetnum)))
                    {
                        return false;
                    }
                    double maxThickness = Math.Max(this.ActualWidth, this.ActualHeight) * ResolutionHelper.ScaleFactor;
                    using (IsolatedStorageFileStream isfs = isf.OpenFile(string.Format(@"\zoom\{0}.img", targetnum), FileMode.Open))
                    {
                        isfs.Position = 0;
                        using (var sis = new StreamImageSource(isfs))
                        {
                            await sis.PreloadAsync();
                            ImageProviderInfo info = null;

                            Task.Run(async () => { info = await sis.GetInfoAsync(); }).Wait();

                            if (!info.ImageSize.IsEmpty)
                            {
                                imgmeta[targetnum].Width = Convert.ToInt32(info.ImageSize.Width);
                                imgmeta[targetnum].Height = Convert.ToInt32(info.ImageSize.Height);
                            }

                            imgmeta[currentImage].Type = (int)sis.ImageFormat;
                            switch (sis.ImageFormat)
                            {
                                case ImageFormat.Bmp: imgmeta[currentImage].TypeStr = "Bitmap"; imgmeta[currentImage].Ext = "bmp"; break;
                                case ImageFormat.Gif: imgmeta[currentImage].TypeStr = "GIF"; imgmeta[currentImage].Ext = "gif"; break;
                                case ImageFormat.Jpeg: imgmeta[currentImage].TypeStr = "Jpeg"; imgmeta[currentImage].Ext = "jpg"; break;
                                case ImageFormat.Png: imgmeta[currentImage].TypeStr = "PNG"; imgmeta[currentImage].Ext = "png"; break;
                                case ImageFormat.Tiff: imgmeta[currentImage].TypeStr = "Tiff"; imgmeta[currentImage].Ext = "tif"; break;
                                case ImageFormat.Undefined: imgmeta[currentImage].TypeStr = "Undefined"; imgmeta[currentImage].Ext = "img"; break;
                                case ImageFormat.Wbmp: imgmeta[currentImage].TypeStr = "Wbmp"; imgmeta[currentImage].Ext = "bmp"; break;
                            }
                        }
                        isfs.Position = 0;
                    }
                    using (IsolatedStorageFileStream isfs2 = isf.OpenFile(@"\zoom\zoom.htm", FileMode.Create))
                    {
                        StreamWriter sw = new StreamWriter(isfs2);
                        sw.WriteLine("<!DOCTYPE html><html><head><script type=\"text/javascript\">function ab() { window.external.notify(\"wtf\"); }</script><meta name=\"viewport\" content=\"width=" + imgmeta[targetnum].Width + "\"/></head><body onclick=\"ab()\" style=\"margin: 0; background-color: black;\">"
                        + string.Format("<div style=\"background-color: white;\"><img src=\"{0}.img\"/></div>", targetnum)
                        + "</body></html>");
                        sw.Flush();
                    }
                    wbView.Navigate(new Uri("zoom/zoom.htm", UriKind.Relative));
                }
                if (ApplicationBar.Buttons.Count >= 4)
                {
                    (ApplicationBar.Buttons[0] as ApplicationBarIconButton).IsEnabled = (smallimgs.Count > 1 && currentImage > 0);
                    //(ApplicationBar.Buttons[1] as ApplicationBarIconButton).IsEnabled = true;
                    (ApplicationBar.Buttons[2] as ApplicationBarIconButton).IsEnabled = true;
                    (ApplicationBar.Buttons[3] as ApplicationBarIconButton).IsEnabled = (smallimgs.Count > 1 && currentImage < smallimgs.Count - 1);

                    int i = 0;
                    if (smallimgs.Count == 1)
                    {
                        ApplicationBar.Buttons.RemoveAt(3);
                        ApplicationBar.Buttons.RemoveAt(1);
                        ApplicationBar.Buttons.RemoveAt(0);
                    }
                }
                else if (ApplicationBar.Buttons.Count == 1)
                {
                    (ApplicationBar.Buttons[0] as ApplicationBarIconButton).IsEnabled = true;
                }
                tbPage.Text = (targetnum + 1).ToString();
                tbCount.Text = (smallimgs.Count).ToString();
                //tbPage.Text = string.Format("{0} / {1}", targetnum + 1, smallimgs.Count);
                //bdPage.Visibility = smallimgs.Count > 1 ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
                //bdControl.Visibility = smallimgs.Count > 1 ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
                //bdControl.Visibility = System.Windows.Visibility.Collapsed;
                //btnLeft.Opacity = (targetnum == 0) ? 0 : 1;
                //btnRight.Opacity = (targetnum == smallimgs.Count - 1) ? 0 : 1;

                tbMeta.Text = string.Format("{0}, {1}x{2}, {3}", imgmeta[targetnum].TypeStr, imgmeta[targetnum].Width, imgmeta[targetnum].Height, imgmeta[targetnum].Size);
                new Thickness(0, 0, this.Orientation == PageOrientation.PortraitUp ? 0 : 72, this.Orientation == PageOrientation.PortraitUp ? 72 : 0);
                MetaPanel.Visibility = System.Windows.Visibility.Visible;
            }
            catch (Exception ex)
            {
                SystemTray.SetProgressIndicator(this, null);
                SystemTray.SetIsVisible(this, false);
                tbError.Visibility = System.Windows.Visibility.Visible;
                tbError.Text += Environment.NewLine + ex.GetType().ToString();
                return false;
            }
            return true;
        }

        double origX = 1.0, origY = 1.0, transX = 0, transY = 0;
        bool isPinch = false;

        /*private void ImageRoot_ManipulationDelta(object sender, System.Windows.Input.ManipulationDeltaEventArgs e)
        {
            var transform = (CompositeTransform)imgThumb.RenderTransform;
            if (e.PinchManipulation != null)
            {
                isPinch = true;

                var originalCenter = e.PinchManipulation.Original.Center;
                var newCenter = e.PinchManipulation.Current.Center;
                var lnCenter = new Point(originalCenter.X + (newCenter.X - originalCenter.X) / 2, originalCenter.Y + (newCenter.Y - originalCenter.Y) / 2);
                transform.CenterX = lnCenter.X - ((Application.Current.Host.Content.ActualWidth - imgThumb.ActualWidth + transX) / 2.0);
                transform.CenterY = lnCenter.Y - ((Application.Current.Host.Content.ActualHeight - imgThumb.ActualHeight + transY) / 2.0);

                // Scale Manipulation
                transform.ScaleX = Math.Max(1.0, origX * e.PinchManipulation.CumulativeScale);
                transform.ScaleY = Math.Max(1.0, origY * e.PinchManipulation.CumulativeScale);
                // Translate manipulation
                
                
                transform.TranslateX = transX + (newCenter.X - originalCenter.X) / 2;
                transform.TranslateY = transY + (newCenter.Y - originalCenter.Y) / 2;

                // Rotation manipulation
                //transform.Rotation = angleBetween2Lines(
                //    e.PinchManipulation.Current,
                //    e.PinchManipulation.Original);

                // end 
            }
            else if(!isPinch)
            {
                transform.TranslateX = transX + e.CumulativeManipulation.Translation.X;
                transform.TranslateY = transY + e.CumulativeManipulation.Translation.Y;
            }

            MetaPanel.Visibility = transform.ScaleX > 1.0 ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
            e.Handled = true;
        }*/

        // copied from http://www.developer.nokia.com/Community/Wiki/Real-time_rotation_of_the_Windows_Phone_8_Map_Control
        public static double angleBetween2Lines(PinchContactPoints line1, PinchContactPoints line2)
        {
            if (line1 != null && line2 != null)
            {
                double angle1 = Math.Atan2(line1.PrimaryContact.Y - line1.SecondaryContact.Y,
                                           line1.PrimaryContact.X - line1.SecondaryContact.X);
                double angle2 = Math.Atan2(line2.PrimaryContact.Y - line2.SecondaryContact.Y,
                                           line2.PrimaryContact.X - line2.SecondaryContact.X);
                return (angle1 - angle2) * 180 / Math.PI;
            }
            else { return 0.0; }
        }

        /*private void ImageRoot_ManipulationStarted(object sender, ManipulationStartedEventArgs e)
        {
            isPinch = false;
            var transform = (CompositeTransform)imgThumb.RenderTransform;
            origX = transform.ScaleX;
            origY = transform.ScaleY;
            transX = transform.TranslateX;
            transY = transform.TranslateY;
        }*/

        Storyboard sbRotate = new Storyboard();
        private void PhoneApplicationPage_OrientationChanged(object sender, OrientationChangedEventArgs e)
        {
            /*CompositeTransform ct = (imgThumb.RenderTransform as CompositeTransform);
            ct.TranslateX = 0;
            ct.TranslateY = 0;
            ct.ScaleX = 0;
            ct.ScaleY = 0;*/

            /*imgThumb.Width = this.ActualWidth;
            if (lbTiles.Visibility != System.Windows.Visibility.Visible)
            {
                LoadMiddleImage(currentImage);
            }*/

            sbRotate.Stop();
            sbRotate.Children.Clear();
            Timeline tl = ThicknessAnimation.Create(MetaPanel, Grid.MarginProperty, new Duration(new TimeSpan(0, 0, 0, 0, 400)),
                new Thickness(MetaPanel.Margin.Left, MetaPanel.Margin.Top, MetaPanel.Margin.Right, MetaPanel.Margin.Bottom),
                new Thickness(
                    e.Orientation == PageOrientation.LandscapeRight ? 72 : 0,
                    0,
                    e.Orientation == PageOrientation.LandscapeLeft ? 72 : 0,
                    e.Orientation == PageOrientation.PortraitUp ? 72 : 0
                    ));
            sbRotate.Children.Add(tl);
            sbRotate.Begin();
        }

        private async void btnBack_Click(object sender, EventArgs e)
        {
            if (currentImage - 1 >= 0 && currentImage - 1 < smallimgs.Count)
            {
                currentImage--;
                if (!(await LoadMiddleImage(currentImage)))
                {
                    currentImage++;
                }
            }
        }

        private void btnTile_Click(object sender, EventArgs e)
        {
            if (lbTiles.Visibility == System.Windows.Visibility.Visible)
            {
                lbTiles.Visibility = System.Windows.Visibility.Collapsed;
                this.ApplicationBar.IsVisible = true;
                this.SupportedOrientations = SupportedPageOrientation.PortraitOrLandscape;
            }
            else
            {
                lbTiles.ItemsSource = null;
                lpTiles.Sort((a, b) => { return (a.pg - b.pg); });

                lbTiles.ItemsSource = lpTiles;
                lbTiles.Visibility = System.Windows.Visibility.Visible;
                this.ApplicationBar.IsVisible = false;
                this.SupportedOrientations = SupportedPageOrientation.Portrait;
            }
        }

        private async void btnNext_Click(object sender, EventArgs e)
        {
            if (currentImage + 1 >= 0 && currentImage + 1 < smallimgs.Count)
            {
                currentImage++;
                if (!(await LoadMiddleImage(currentImage)))
                {
                    currentImage--;
                }
            }
        }

        async private void Grid_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (sender is Grid)
            {
                var idx = lpTiles.IndexOf((sender as Grid).DataContext as PicTile);
                if (idx >= 0)
                {
                    lbTiles.Visibility = System.Windows.Visibility.Collapsed;
                    this.ApplicationBar.IsVisible = true;
                    this.SupportedOrientations = SupportedPageOrientation.PortraitOrLandscape;
                    currentImage = idx;
                    await LoadMiddleImage(idx);
                }
            }
        }

        private void PhoneApplicationPage_BackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (lbTiles.Visibility == System.Windows.Visibility.Visible)
            {
                e.Cancel = true;
                lbTiles.Visibility = System.Windows.Visibility.Collapsed;
                this.ApplicationBar.IsVisible = true;
                this.SupportedOrientations = SupportedPageOrientation.PortraitOrLandscape;
            }
        }

        private void WebBrowser_ManipulationCompleted(object sender, ManipulationCompletedEventArgs e)
        {

        }

        private void wbView_Navigating(object sender, NavigatingEventArgs e)
        {
            wbView.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void wbView_Navigated(object sender, NavigationEventArgs e)
        {
            wbView.Visibility = System.Windows.Visibility.Visible;
        }

        private void wbView_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            tbError.Visibility = System.Windows.Visibility.Visible;
        }

        private void wbView_ScriptNotify(object sender, NotifyEventArgs e)
        {
            if (e.Value == "wtf")
            {
                if (MetaPanel.Visibility == System.Windows.Visibility.Visible)
                {
                    MetaPanel.Visibility = System.Windows.Visibility.Collapsed;
                    ApplicationBar.IsVisible = false;
                }
                else
                {
                    MetaPanel.Visibility = System.Windows.Visibility.Visible;
                    ApplicationBar.IsVisible = true;
                }
            }
        }
    }
}