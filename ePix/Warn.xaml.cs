﻿using Microsoft.Phone.Controls;
using System.Globalization;
using System.IO.IsolatedStorage;
using System.Windows;
using System.Windows.Navigation;

namespace ePix
{
    public partial class Warn : PhoneApplicationPage
    {
        public Warn()
        {
            InitializeComponent();
        }

        public static bool first = false;

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.Language = System.Windows.Markup.XmlLanguage.GetLanguage(CultureInfo.CurrentUICulture.Name);

            base.OnNavigatedTo(e);
        }

        private void HyperlinkButton_Click_1(object sender, RoutedEventArgs e)
        {
            if (Windows.Phone.ApplicationModel.ApplicationProfile.Modes == Windows.Phone.ApplicationModel.ApplicationProfileModes.Alternate)
            {
                MessageBox.Show(Res.LocalString.kidmode, Res.LocalString.kidtitle, MessageBoxButton.OK);
                return;
            }

            if (NavigationService.CanGoBack)
            {
                first = true;
                IsolatedStorageSettings.ApplicationSettings["frun"] = true;
                NavigationService.GoBack();
            }
        }

        /*private void CheckBox_Checked_1(object sender, RoutedEventArgs e)
        {
            hbMain.Visibility = System.Windows.Visibility.Visible;
            svMain.ScrollToVerticalOffset(svMain.ExtentHeight - svMain.ViewportHeight);
            svMain.UpdateLayout();
        }

        private void CheckBox_Unchecked_1(object sender, RoutedEventArgs e)
        {
            hbMain.Visibility = System.Windows.Visibility.Collapsed;
            svMain.ScrollToVerticalOffset(0);
        }*/

        private void PhoneApplicationPage_BackKeyPress_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            while (NavigationService.CanGoBack)
            {
                NavigationService.RemoveBackEntry();
            }
        }
    }
}