﻿using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Navigation;
using System.Linq;
using System.Windows.Controls.Primitives;
using LLSSample;

namespace ePix
{
    public partial class PeopleList : PhoneApplicationPage
    {
        public PeopleList()
        {
            InitializeComponent();

            //lbSuggest.ItemsSource = ocSugs;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.Language = XmlLanguage.GetLanguage(CultureInfo.CurrentUICulture.Name);
            if ((Application.Current.Resources["PhoneDarkThemeVisibility"] as Visibility?) == System.Windows.Visibility.Visible ^ Communication.isInvert)
            {
                LayoutRoot.Background = new SolidColorBrush(Color.FromArgb(255, 37, 39, 41));
                pvResult.Background = LayoutRoot.Background;
                SystemTray.BackgroundColor = Color.FromArgb(255, 37, 39, 41);
                SystemTray.ForegroundColor = Colors.White;
            }

            if (pvResult.SelectedIndex == 0)
            {
                ShowSuggestUser();
            }
            else
            {
                RefreshUsersPage(pvResult.SelectedIndex == 1, 1);
            }

            base.OnNavigatedTo(e);
        }

        ObservableCollection<Contact> ocSugs = new ObservableCollection<Contact>();
        int nextpg = 1;
        const string urisug = "http://www.pixiv.net/rpc/recommender.php?type=user&sample_users={0}&num_recommendations=100&nc=1&tt={1}";
        const string uripic = "http://www.pixiv.net/mypixiv_all.php?type=user&rest={0}&p={1}";
        const string urifav = "http://www.pixiv.net/bookmark.php?type=user&rest={0}&p={1}";
        private bool working;
        private void ShowSuggestUser()
        {
            SystemTray.SetProgressIndicator(this, new ProgressIndicator()
            {
                IsIndeterminate = true,
                IsVisible = true,
                Text = Res.LocalString.retuser
            });
            tbNothing.Visibility = System.Windows.Visibility.Collapsed;

            string sugu = string.Format(urisug, Interpreter.sugs, Interpreter.token);
            Communication.Communicate(sugu, (suc, res) =>
            {
                if (suc >= 0)
                {
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        List<Contact> lc = JSONParser.JSONToContacts(res);
                        if (lc.Count == 0 && pvResult.SelectedIndex == 0)
                        {
                            tbNothing.Visibility = System.Windows.Visibility.Visible;
                        }
                        else
                        {
                            int selectedidx = -1;
                            if (lbSuggest.SelectedItem != null)
                            {
                                selectedidx = ocSugs.IndexOf(lbSuggest.SelectedItem as Contact);
                            }
                            ocSugs.Clear();
                            for (int i = 0; i < lc.Count; i++)
                            {
                                ocSugs.Add(lc[i]);
                            }
                            lbSuggest.IsGroupingEnabled = true;
                            lbSuggest.HideEmptyGroups = true;
                            lbSuggest.ItemsSource = AlphaGrouping<Contact>.CreateGroups(ocSugs,
                                    CultureInfo.CurrentUICulture, (Contact c) =>
                                    {
                                        return c.user_name;
                                    }, true);

                            if (selectedidx >= 0 && ocSugs.Count > 0)
                            {
                                lbSuggest.ScrollTo(ocSugs[selectedidx]);
                            }
                        }
                    });
                }
                else
                {
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        try
                        {
                            MessageBox.Show(Res.LocalString.errorbang+ Environment.NewLine +
                                (res != "UnknownError" ? res : Res.LocalString.noconnection),
                                Res.LocalString.errorpixifeed, MessageBoxButton.OK);
                            if (NavigationService.CanGoBack)
                            {
                                NavigationService.GoBack();
                            }
                        }
                        catch { }
                    });
                }
                this.Dispatcher.BeginInvoke(() =>
                    {
                        SystemTray.SetProgressIndicator(this, null);
                    });
                //working = false;
            });
        }

        private void RefreshUsersPage(bool mypic, int page)
        {
            try
            {
                if (working)
                {
                    return;
                }
                working = true;

                SystemTray.SetProgressIndicator(this, new ProgressIndicator()
                {
                    IsIndeterminate = true,
                    IsVisible = true,
                    Text = Res.LocalString.retuser
                });
                //nextpg = page;
                tbNothing.Visibility = System.Windows.Visibility.Collapsed;

                string targeturi = string.Format(mypic ? uripic : urifav,
                    (mypic ? lpMypic.SelectedIndex : lpFav.SelectedIndex) == 1 ? "hide" : "show", page);

                //ObservableCollection<Contact> ocObj = pvResult.SelectedIndex == 1 ? ocPubs : ocPris;
                ObservableCollection<Contact> ocObj = ocSugs;

                if (lbSuggest.ItemsSource != ocSugs)
                {
                    lbSuggest.IsGroupingEnabled = false;
                    lbSuggest.HideEmptyGroups = false;
                    lbSuggest.ItemsSource = ocSugs;
                }

                Communication.Communicate(targeturi, (suc, res) =>
                {
                    if (suc >= 0)
                    {
                        this.Dispatcher.BeginInvoke(() =>
                        {
                            try
                            {
                                if (page == 1)
                                {
                                    int selectedidx = -1;
                                    if (ocObj.Count > 0 && lbSuggest.SelectedItem != null)
                                    {
                                        selectedidx = ocObj.IndexOf(lbSuggest.SelectedItem as Contact);
                                    }

                                    if (Interpreter.ShowFavoriteUser(ref ocObj, true, res))
                                    {
                                        nextpg = page + 1;
                                        //lbSuggest.ItemsSource = AlphaGrouping<Contact>.CreateGroups(ocObj,
                                        //    CultureInfo.CurrentUICulture, (Contact c) =>
                                        //    {
                                        //        return c.user_name;
                                        //   }, true);

                                    }
                                    else
                                    {
                                        if (ocSugs.Count == 0)
                                        {
                                            tbNothing.Visibility = System.Windows.Visibility.Visible;
                                        }
                                    }

                                    if (selectedidx >= 0 && ocSugs.Count > 0)
                                    {
                                        lbSuggest.ScrollTo(ocSugs[selectedidx]);
                                    }
                                }
                                else
                                {
                                    if (Interpreter.ShowFavoriteUser(ref ocObj, false, res))
                                    {
                                        nextpg = page + 1;
                                    }
                                    else
                                    {
                                        if (ocSugs.Count == 0)
                                        {
                                            tbNothing.Visibility = System.Windows.Visibility.Visible;
                                        }
                                    }
                                }
                            }
                            catch { }
                        });
                    }
                    else
                    {
                        this.Dispatcher.BeginInvoke(() =>
                        {
                            try
                            {
                                MessageBox.Show(Res.LocalString.errorbang + Environment.NewLine +
                                    (res != "UnknownError" ? res : Res.LocalString.noconnection),
                                    Res.LocalString.errorpixifeed, MessageBoxButton.OK);
                                if (NavigationService.CanGoBack)
                                {
                                    NavigationService.GoBack();
                                }
                            }
                            catch { }
                        });
                    }
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        SystemTray.SetProgressIndicator(this, null);
                    });
                    working = false;
                });
            }
            catch
            { }
        }

        private void pvResult_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            uppg = 0;
            ocSugs.Clear();
            GC.Collect();
            nextpg = 0;
            if (pvResult.SelectedIndex >= 0)
            {
                PivotItem li = e.RemovedItems[0] as PivotItem;
                PivotItem pi = e.AddedItems[0] as PivotItem;
                if (li != null)
                {
                    Grid gpi = li.Content as Grid;

                    if (gpi != null)
                    {
                        if (!gpi.Name.StartsWith("gdList"))
                        {
                            gpi = gpi.Children.FirstOrDefault((obj) => { return (obj as FrameworkElement).Name.StartsWith("gdList"); }) as Grid;
                        }
                        if (gpi != null && gpi.Children.Count > 0)
                        {
                            gpi.Children.Clear();
                        }
                        gpi.InvalidateArrange();
                        gpi.InvalidateMeasure();
                        gpi.OnApplyTemplate();
                    }

                    if (pi != null)
                    {
                        gpi = pi.Content as Grid;
                        if (gpi != null)
                        {
                            if (!gpi.Name.StartsWith("gdList"))
                            {
                                ListPicker lp = gpi.Children.FirstOrDefault((obj) => { return (obj is ListPicker); }) as ListPicker;
                                if (lp.Items.Count == 0)
                                {
                                    lp.Items.Add(Res.LocalString.ppublic);
                                    lp.Items.Add(Res.LocalString.pprivate);
                                }
                                gpi = gpi.Children.FirstOrDefault((obj) => { return (obj as FrameworkElement).Name.StartsWith("gdList"); }) as Grid;
                            }
                            if (gpi != null)
                            {
                                if (gpi.Children.Count > 0)
                                {
                                    gpi.Children.Clear();
                                }
                                gpi.Children.Add(lbSuggest);
                                gpi.InvalidateArrange();
                                gpi.InvalidateMeasure();
                                gpi.OnApplyTemplate();
                            }
                        }
                    }
                }
            }

            tbNothing.Visibility = System.Windows.Visibility.Collapsed;
            if (pvResult.SelectedIndex == 0)
            {
                ShowSuggestUser();
            }
            else if (pvResult.SelectedIndex == 1)
            {
                RefreshUsersPage(true, 1);
            }
            else if (pvResult.SelectedIndex == 2)
            {
                RefreshUsersPage(false, 1);
            }
        }

        private void Grid_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Contact cm = (sender as Grid).DataContext as Contact;
            if (cm != null)
            {
                NavigationService.Navigate(new Uri("/People.xaml?id="
                    + cm.user_id, UriKind.Relative));
            }
        }

        private void lbSuggest_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                Contact cm = e.AddedItems[0] as Contact;
                if (cm != null)
                {
                    NavigationService.Navigate(new Uri("/People.xaml?id="
                        + cm.user_id, UriKind.Relative));
                }
            }
        }

        private void lp_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (pvResult != null && pvResult.SelectedIndex > 0 && (sender as ListPicker) != null && (sender as ListPicker).SelectedIndex >= 0)
            {
                uppg = 0;
                nextpg = 0;
                RefreshUsersPage(pvResult.SelectedIndex == 1, 1);
            }
        }

        int uppg = 0;
        private void ViewportControl_ViewportChanged(object sender, System.Windows.Controls.Primitives.ViewportChangedEventArgs e)
        {
            ViewportControl vc = sender as ViewportControl;
            if (ocSugs != null && ocSugs.Count > 0 && vc.Viewport.Bottom >= vc.Bounds.Bottom - 140)
            {
                int ipg = 0;
                try
                {
                    ipg = pvResult.SelectedIndex;
                    if (ipg > 0 && uppg != nextpg)
                    {
                        uppg = nextpg;
                        RefreshUsersPage(ipg == 1, nextpg);
                    }
                }
                catch { }
            }
        }
    }
}