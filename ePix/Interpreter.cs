﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using ePix.Model;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Data;
using System.IO;
using System.Windows.Media.Imaging;
using System.IO.IsolatedStorage;
using System.Text.RegularExpressions;
using System.Globalization;
using Microsoft.Phone.Controls;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using HtmlAgilityPack;
using System.Text;
using System.Threading.Tasks;

namespace ePix
{
    public static class Interpreter
    {
        //메인 페이지 최근 항목
        public static void LatestImages(ref ObservableCollection<PictureModel> pic, string stuff, out bool isGrouped, int recom = -1)
        {
            try
            {
                List<PictureModel> lpic = new List<PictureModel>();
                isGrouped = false;

                HtmlDocument hd = new HtmlDocument();
                hd.LoadHtml(stuff);

                int iSUl = CommonMethod.SearchTagAttributes("ul", "linkStyleWorks", 0, stuff);
                if (iSUl < 0)
                {
                    iSUl = CommonMethod.SearchTagAttributes("div", "newindex-bg-container", 0, stuff);
                }

                #region 비회원
                if (iSUl >= 0 || recom == 1)
                {
                    //비회원

                    iSUl = CommonMethod.SearchTagAttributes("ul", "ui-brick", iSUl, stuff);


                    int iEUl = stuff.IndexOf("</ul>", iSUl);
                    if (iEUl > iSUl)
                    {
                        string part = stuff.Substring(iSUl, iEUl - iSUl);
                        string[] pats = part.Split(new string[] { "</li>" }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string pa in pats)
                        {
                            string sID = "", sTitle = "", sUri = "";
                            #region 앵커 위치
                            int iSA = pa.IndexOf("<a");
                            if (iSA >= 0)
                            {

                                int iEA = pa.IndexOf('>', iSA + 1);
                                if (iEA > iSA)
                                {
                                    string href = CommonMethod.GetTagAttribute("href", pa.Substring(iSA, iEA - iSA + 1));
                                    if (href.Length > 0)
                                    {
                                        int iSid = href.IndexOf("illust_id=");
                                        if (iSid >= 0)
                                        {
                                            iSid += 10;
                                            int iEid = href.IndexOf('&', iSid);
                                            if (iEid <= 0)
                                            {
                                                iEid = href.Length;
                                            }
                                            sID = href.Substring(iSid, iEid - iSid);
                                        }
                                    }
                                }
                            }
                            #endregion
                            #region 이미지 위치
                            int iSI = pa.IndexOf("<img");
                            if (iSI >= 0)
                            {

                                int iEI = pa.IndexOf('>', iSI + 1);
                                if (iEI > iSI)
                                {
                                    string inimg = pa.Substring(iSI, iEI - iSI + 1);
                                    string src = CommonMethod.GetTagAttribute("src", inimg);
                                    if (src.Length > 0)
                                    {
                                        if (src.IndexOf("http://") < 0)
                                        {
                                            src = src.Insert(0, "http://www.pixiv.net/");
                                        }
                                    }
                                    sUri = src;
                                    string alt = CommonMethod.GetTagAttribute("alt", inimg);
                                    sTitle = HttpUtility.HtmlDecode(alt);
                                }
                            }
                            #endregion

                            if (sUri.Length > 0)
                            {
                                lpic.Add(new PictureModel(sUri, sTitle, string.Empty, sID)
                                {
                                    Category = Res.LocalString.recommend
                                }
                                    );
                            }
                        }
                    }
                }
                #endregion
                else
                {
                    #region 회원

                    //int iSDiv = CommonMethod.SearchTagAttributes("ul", "class=\"image-items\"", 0, stuff);

                    HtmlNodeCollection hnc = hd.DocumentNode.SelectNodes("//div[@id=\"item-container\"]/section[@class]");
                    if (hnc != null)
                    {
                        foreach (HtmlNode hnSec in hnc)
                        {
                            if (!hnSec.GetAttributeValue("class", "").Contains("item"))
                            {
                                continue;
                            }
                            string category = "";
                            HtmlNode hnHeader = hd.DocumentNode.SelectSingleNode(hnSec.XPath + "/header/h1/a");
                            if (hnHeader != null)
                            {
                                category = HttpUtility.HtmlDecode(hnHeader.InnerText.Trim());
                                isGrouped = true;
                            }

                            HtmlNodeCollection hncSec = hnSec.SelectNodes(hnSec.XPath + "//li[contains(@class, 'image-item')]");
                            if (hncSec != null)
                            {
                                foreach (HtmlNode hnLi in hncSec)
                                {
                                    bool ugoira = false, multi = false;
                                    string sID = "", sTitle = "", sAuthor = "", sUri = "";
                                    HtmlNode ic = hnLi.SelectSingleNode(".//a[@class]");
                                    if (ic != null)
                                    {
                                        string href = ic.GetAttributeValue("href", "");
                                        if (href.Length > 0)
                                        {
                                            int iSid = href.IndexOf("illust_id=");
                                            if (iSid >= 0)
                                            {
                                                iSid += 10;
                                                int iEid = href.IndexOf('&', iSid);
                                                if (iEid <= 0)
                                                {
                                                    iEid = href.Length;
                                                }
                                                sID = href.Substring(iSid, iEid - iSid);
                                            }
                                        }

                                        HtmlNode div = ic.ChildNodes["div"];
                                        if (div != null)
                                        {
                                            ugoira = div.GetAttributeValue("class", "").Contains("ugoku-illust");

                                            var hnUgoA = hnLi.SelectSingleNode(".//a[contains(@class, 'ugoku-illust')]");
                                            if (hnUgoA != null)
                                            {
                                                ugoira = true;
                                            }

                                            var hnM = hnLi.SelectSingleNode(".//a[contains(@class, 'multiple')]");
                                            if (hnM != null)
                                            {
                                                multi = true;
                                            }

                                            HtmlNode img = div.ChildNodes["img"];
                                            if (img != null)
                                            {
                                                string r18check = img.GetAttributeValue("data-tags", "");
                                                if (r18check.Contains("R-18") && !Communication.Te)
                                                {
                                                    sUri = "http://fake.dd/r-18";
                                                }
                                                else
                                                {
                                                    sUri = img.GetAttributeValue("data-src", "");
                                                    if (string.IsNullOrEmpty(sUri))
                                                    {
                                                        sUri = img.GetAttributeValue("src", "");
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            HtmlNode img = ic.ChildNodes["img"];
                                            if (img != null)
                                            {
                                                string r18check = img.GetAttributeValue("data-tags", "");
                                                if (r18check.Contains("R-18") && !Communication.Te)
                                                {
                                                    sUri = "http://fake.dd/r-18";
                                                }
                                                else
                                                {
                                                    sUri = img.GetAttributeValue("data-src", "");
                                                    if (string.IsNullOrEmpty(sUri))
                                                    {
                                                        sUri = img.GetAttributeValue("src", "");
                                                    }
                                                }
                                            }
                                        }
                                        HtmlNode h1 = hnLi.SelectSingleNode(".//h1");
                                        sTitle = HttpUtility.HtmlDecode(h1.InnerText.Trim());
                                    }

                                    HtmlNode uc = hnLi.SelectSingleNode(hnLi.XPath + "//a[@data-user_id]");
                                    if (uc != null)
                                    {
                                        sAuthor = HttpUtility.HtmlDecode(uc.InnerText.Trim());
                                    }

                                    if (!string.IsNullOrEmpty(sUri) && !string.IsNullOrEmpty(sID))
                                    {
                                        PictureModel pm = new PictureModel(sUri, sTitle, sAuthor, sID)
                                        {
                                            Category = category,
                                            _ugo = ugoira,
                                            _mul = multi
                                        };
                                        lpic.Add(pm);
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                }

                if (lpic.Count > 0)
                {
                    pic.Clear();
                    foreach (PictureModel pm in lpic)
                    {
                        pic.Add(pm);
                    }
                }
            }
            catch
            {
                isGrouped = false;
            }
        }

        //메인 페이지 핫 태그
        public static void HotTags(ref ObservableCollection<TagItem> tags, string stuff)
        {
            List<TagItem> ltag = new List<TagItem>();

            HtmlDocument hd = new HtmlDocument();
            hd.LoadHtml(stuff);
            HtmlNodeCollection hnc = hd.DocumentNode.SelectNodes("//a[@href]");
            if (hnc != null)
            {
                HtmlNode hnImgLink = null;
                foreach (HtmlNode hn in hnc)
                {
                    string href = hn.GetAttributeValue("href", "");
                    string label = href;
                    if (hn.ChildNodes["#text"] != null)
                    {
                        label = HttpUtility.HtmlDecode(hn.ChildNodes["#text"].InnerText);
                    }
                    string img = null;
                    if ((href.IndexOf("tags.php?") >= 0
                        || href.IndexOf("search.php?s_mode=s_tag") >= 0) &&
                        !hn.GetAttributeValue("class", "").Contains("tag-image"))
                    {
                        int tclass = 0;
                        string tname = HttpUtility.HtmlDecode(hn.InnerText);
                        int itag = href.IndexOf("tag=");
                        if (itag >= 0)
                        {
                            itag += 4;
                            tname = HttpUtility.UrlDecode(href.Substring(itag));
                        }
                        string uclass = "";
                        if (hn.ParentNode.Name == "li")
                        {
                            string sclass = hn.ParentNode.GetAttributeValue("class", "");
                            if (sclass.IndexOf("level") >= 0)
                            {
                                int iL = sclass.IndexOf("level") + 5;
                                if (iL < sclass.Length)
                                {
                                    int.TryParse(sclass.Substring(iL, 1), out tclass);
                                }
                            }

                            uclass = hn.ParentNode.ParentNode.GetAttributeValue("class", "");
                        }
                        else if (hn.ParentNode.Name == "div" && hnImgLink != null)
                        {
                            tclass = 1;

                            HtmlNode hnImg = hnImgLink.ChildNodes["img"];
                            if (hnImg != null)
                            {
                                img = hnImg.GetAttributeValue("src", null);
                            }
                            hnImgLink = null;
                        }
                        if (!string.IsNullOrEmpty(tname))
                        {
                            if (uclass.IndexOf("favorite") >= 0)
                            {
                                ltag.Insert(0, new TagItem() { _tagclass = -2, TagName = tname, TagImage = img, TagLabel = label, isLocalized = false });
                            }
                            else
                            {
                                ltag.Add(new TagItem() { _tagclass = tclass, TagName = tname, TagImage = img, TagLabel = label, isLocalized = (href.Contains("s_mode=s_tag") && !href.Contains("s_mode=s_tag_full")) });
                            }
                        }
                    }
                    else if (hn.GetAttributeValue("class", "").Contains("tag-image"))
                    {
                        hnImgLink = hn;
                    }
                }
            }

            if (ltag.Count > 0)
            {
                tags.Clear();
                foreach (TagItem ti in ltag)
                {
                    tags.Add(ti);
                }
            }
        }

        //검색 페이지 사전 유효성
        public static string isDicAvailable(string stuff)
        {
            int iSDic = CommonMethod.SearchTagAttributes("a", "href=\"http://dic.pixiv.net/a", 0, stuff);
            if (iSDic > 0)
            {
                int iEDic = stuff.IndexOf('>', iSDic);
                if (iEDic > iSDic)
                {
                    iEDic++;
                    string sampleDic = stuff.Substring(iSDic, iEDic - iSDic);
                    string m = CommonMethod.GetTagAttribute("href", sampleDic);
                    return m;
                }
            }
            return null;
        }

        //메인 페이지 pixiv 로고
        public static void TodayBanner(string stuff, Action<string, string> imgfunc)
        {
            /*
            int iSBan = CommonMethod.SearchTagAttributes("h1", "title", 0, stuff);
            if (iSBan > 0)
            {
                int iEBan = stuff.IndexOf("</h1>", iSBan);
                if (iEBan > iSBan)
                {
                    int iSImg = CommonMethod.SearchTagAttributes("img", "src=", iSBan, iEBan, stuff);
                    if (iSImg > iSBan)
                    {
                        int iEImg = stuff.IndexOf('>', iSImg);
                        if (iEImg > iSImg)
                        {
                            iEImg++;
                            string src = CommonMethod.GetTagAttribute("src", stuff.Substring(iSImg, iEImg - iSImg));
                            return string.IsNullOrEmpty(src) ? null : src;
                        }
                    }
                }
            }*/
            HtmlDocument hd = new HtmlDocument();
            hd.LoadHtml(stuff);
            var script = hd.DocumentNode.SelectSingleNode("//h1[contains(@class, 'header-logo')]//script[@src]");
            if (script != null)
            {
                string src = script.GetAttributeValue("src", "");
                if (!string.IsNullOrEmpty(src))
                {

                    Communication.Communicate(src, (a, b) =>
                    {
                        string url = null, tap = null;
                        if (a >= 0 && b != null && b.Contains("special-logo"))
                        {
                            int s = b.IndexOf("data-logo-img',");
                            if (s > 0)
                            {
                                s += 16;
                                int e = b.IndexOf("'", s);
                                if (e > s)
                                {
                                    url = b.Substring(s, e - s).Trim();
                                }
                            }
                            s = b.IndexOf("data-logo-link',");
                            if (s > 0)
                            {
                                s += 17;
                                int e = b.IndexOf("'", s);
                                if (e > s)
                                {
                                    tap = b.Substring(s, e - s).Trim();
                                }
                            }
                        }
                        if (imgfunc != null)
                        {
                            imgfunc(url, tap);
                        }
                    }, false);
                    return;
                }
            }
            imgfunc(null, null);
        }

        //검색 컬럼
        public static string SearchColumnTitle(string stuff)
        {
            if (!string.IsNullOrEmpty(stuff))
            {
                HtmlDocument hd = new HtmlDocument();
                hd.LoadHtml(stuff);
                HtmlNode hnH1 = hd.DocumentNode.SelectSingleNode("//h1[@class=\"column-title\"]");
                if (hnH1 != null)
                {
                    return HttpUtility.HtmlDecode(hnH1.InnerText.Trim());
                }
            }
            return string.Empty;
        }

        //검색 페이지
        public static bool SearchResult(ref ObservableCollection<PictureModel> pic, bool shouldClear, string stuff)
        {
            Communication.isLogin = (stuff.IndexOf("loggedIn = true") >= 0);

            List<PictureModel> lpic = InsideSearchResult(stuff);
            if (lpic.Count > 0 && pic != null)
            {
                if (shouldClear)
                {
                    pic.Clear();
                }
                foreach (PictureModel pm in lpic)
                {
                    if (!pic.Contains(pm))
                    {
                        pic.Add(pm);
                    }
                }
                return true;
            }
            return false;
        }

        public static List<PictureModel> InsideSearchResult(string stuff)
        {
            List<PictureModel> lpic = new List<PictureModel>();

            if (stuff.Length > 0)
            {
                HtmlDocument hd = new HtmlDocument();
                hd.LoadHtml(stuff);
                HtmlNodeCollection hncLi = hd.DocumentNode.SelectNodes("//li[contains(@class, 'image-item')]");
                if (hncLi != null)
                {
                    foreach (HtmlNode hnLi in hncLi)
                    {
                        string sID = "", sTitle = "", sAuthor = "", sUri = "", fav = "", resp = "";
                        bool ugoira = false, multi = false;

                        var hnA = hnLi.SelectSingleNode(".//a[@href]");
                        string href = hnA.GetAttributeValue("href", "");
                        if (href.Length > 0)
                        {
                            int iSid = href.IndexOf("illust_id=");
                            if (iSid >= 0)
                            {
                                iSid += 10;
                                int iEid = href.IndexOf('&', iSid);
                                if (iEid <= 0)
                                {
                                    iEid = href.Length;
                                }
                                sID = href.Substring(iSid, iEid - iSid);
                            }
                        }

                        var hnImg = hnLi.SelectSingleNode(".//img[@data-src or @src]");
                        if (hnImg != null)
                        {
                            string tag = hnImg.GetAttributeValue("data-tags", "");

                            ugoira = tag.Contains("うごイラ");

                            var hnUgoA = hnLi.SelectSingleNode(".//a[contains(@class, 'ugoku-illust')]");
                            if (hnUgoA != null)
                            {
                                ugoira = true;
                            }

                            var hnM = hnLi.SelectSingleNode(".//a[contains(@class, 'multiple')]");
                            if (hnM != null)
                            {
                                multi = true;
                            }

                            if (tag.Contains("R-18") && !Communication.Te)
                            {
                                sUri = "http://fake.dd/r-18";
                            }
                            else
                            {
                                sUri = hnImg.GetAttributeValue("data-src", "");
                                if (string.IsNullOrEmpty(sUri))
                                {
                                    sUri = hnImg.GetAttributeValue("src", "");
                                }
                            }
                        }

                        var hnTitle = hnLi.SelectSingleNode(".//h1[@class=\"title\"]");
                        if (hnTitle != null)
                        {
                            sTitle = HttpUtility.HtmlDecode(hnTitle.InnerText);
                        }

                        var hnAuthor = hnLi.SelectSingleNode(".//a[@data-user_name]");
                        if (hnAuthor != null)
                        {
                            sAuthor = HttpUtility.HtmlDecode(hnAuthor.InnerText);
                        }

                        var hnBook = hnLi.SelectSingleNode(".//a[contains(@class, 'bookmark-count')]");
                        if (hnBook != null)
                        {
                            fav = HttpUtility.HtmlDecode(hnBook.InnerText);
                        }

                        var hnResp = hnLi.SelectSingleNode(".//a[contains(@class, 'image-response-count')]");
                        if (hnResp != null)
                        {
                            resp = HttpUtility.HtmlDecode(hnResp.InnerText);
                        }

                        PictureModel pmnew = new PictureModel(sUri, sTitle, sAuthor, sID, fav, resp) { _ugo = ugoira, _mul = multi };
                        if (sUri.Length > 0 && sTitle.Length > 0)
                        {
                            lpic.Add(pmnew);
                        }
                    }
                }
            }
            return lpic;
        }

        //사용자 검색
        public static bool SearchUser(ref ObservableCollection<Contact> cm, bool shouldClear, string stuff)
        {
            List<Contact> imc = new List<Contact>();

            if (stuff.Length > 0)
            {
                HtmlDocument hd = new HtmlDocument();
                hd.LoadHtml(stuff);
                HtmlNodeCollection hncLi = hd.DocumentNode.SelectNodes("//li[@class=\"user-recommendation-item\"]");
                if (hncLi != null)
                {
                    foreach (HtmlNode hnLi in hncLi)
                    {
                        string sID = "", sPic = "", sName = "", sDesc = "";
                        HtmlNode hnA = hnLi.ChildNodes["a"];
                        if (hnA != null)
                        {
                            string memanc = hnA.GetAttributeValue("href", "");
                            if (memanc.Length > 0)
                            {
                                int iMemnum = memanc.IndexOf("?id=");
                                if (iMemnum > 0)
                                {
                                    iMemnum += 4;
                                    sID = memanc.Substring(iMemnum, memanc.Length - iMemnum);
                                }
                            }

                            sPic = hnA.GetAttributeValue("data-src", "");
                        }

                        HtmlNode hnH1 = hnLi.ChildNodes["h1"];
                        if (hnH1 != null)
                        {
                            HtmlNode hnAn = hnH1.ChildNodes["a"];
                            if (hnAn != null)
                            {
                                sName = HttpUtility.HtmlDecode(hnAn.InnerText);
                            }
                        }

                        HtmlNode hnP = hnLi.ChildNodes["p"];
                        if (hnP != null)
                        {
                            sDesc = HttpUtility.HtmlDecode(hnP.InnerText);
                        }

                        if (sID.Length > 0 && sName.Length > 0)
                        {
                            imc.Add(new Contact(sPic, sID, sName, sDesc));
                        }
                    }
                }
            }



            /*int iSSection = CommonMethod.SearchTagAttributesEnd("div", "user-search-result", 0, stuff);
            if (iSSection < 0)
            {
                iSSection = CommonMethod.SearchTagAttributesEnd("ul", "user-recommendation-items", 0, stuff);
            }
            if (iSSection > 0)
            {
                int iEUl = stuff.IndexOf("</nav>", iSSection);
                if (iEUl > iSSection)
                {
                    int iSSSection = CommonMethod.SearchTagAttributesEnd("ul", "user-recommendation-items", 0, stuff);
                    string sall = "";
                    if (iSSSection > 0)
                    {
                        sall = stuff.Substring(iSSSection, iEUl - iSSSection);
                    }
                    else
                    {
                        sall = stuff;
                    }
                    string[] items = sall.Split(new string[] { "<li class=\"user-recommendation-item" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string itt in items)
                    {
                        string memnum = "", pic = "", name = "", desc = "";
                        int isli = itt.IndexOf("<li");
                        int iAMem = CommonMethod.SearchTagAttributes("a", "href", 0, itt);
                        if (iAMem >= 0)
                        {
                            int iAE = itt.IndexOf('>', iAMem);
                            if (iAE > iAMem)
                            {
                                iAE++;
                                string member = itt.Substring(iAMem, iAE - iAMem);
                                string memanc = CommonMethod.GetTagAttribute("href", member);
                                if (memanc.Length > 0)
                                {
                                    int iMemnum = memanc.IndexOf("?id=");
                                    if (iMemnum > 0)
                                    {
                                        iMemnum += 4;
                                        memnum = memanc.Substring(iMemnum, memanc.Length - iMemnum);
                                    }
                                }
                            }

                            int iSImg = CommonMethod.SearchTagAttributes("a", "data-src", iAMem, itt);
                            if (iSImg < 0)
                            {
                                iSImg = CommonMethod.SearchTagAttributes("img", "src", iAMem, itt);
                            }
                            if (iSImg > 0)
                            {
                                int iEImg = itt.IndexOf('>', iSImg);
                                if (iEImg > iSImg)
                                {
                                    iEImg++;
                                    string imtag = itt.Substring(iSImg, iEImg - iSImg);
                                    string src = CommonMethod.GetTagAttribute("data-src", imtag);
                                    if (src.Length > 0)
                                    {
                                        pic = src;
                                    }
                                    else
                                    {
                                        pic = CommonMethod.GetTagAttribute("src", imtag);
                                        name = CommonMethod.GetTagAttribute("alt", imtag);
                                    }
                                }
                            }

                            int iSName = itt.IndexOf("<h", iAMem);
                            if (iSName > 0)
                            {
                                //iSName += 4;
                                int iEName = itt.IndexOf("</h", iSName);
                                if (iEName > iSName)
                                {
                                    name = itt.Substring(iSName, iEName - iSName);
                                    name = HttpUtility.HtmlDecode(CommonMethod.RemoveAllTags(name));
                                }
                            }

                            int iSDesc = itt.IndexOf("<p", iAMem);
                            if (iSDesc > 0)
                            {
                                int iEDesc = itt.IndexOf("</p", iSDesc);
                                if (iEDesc > iSDesc)
                                {
                                    desc = itt.Substring(iSDesc, iEDesc - iSDesc);
                                    desc = HttpUtility.HtmlDecode(CommonMethod.RemoveAllTags(desc).Replace("\n", "").Replace("\r",""));
                                }
                            }
                        }

                        if (memnum.Length > 0 && name.Length > 0)
                        {
                            imc.Add(new Contact(pic, memnum, name, desc));
                        }
                    }
                }
            }*/
            if (imc.Count > 0)
            {
                if (shouldClear)
                {
                    cm.Clear();
                }

                foreach (Contact ci in imc)
                {
                    cm.Add(ci);
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        // 즐겨찾기 유저
        public static bool ShowFavoriteUser(ref ObservableCollection<Contact> cm, bool shouldClear, string stuff)
        {
            List<Contact> imc = new List<Contact>();

            HtmlDocument hd = new HtmlDocument();
            hd.LoadHtml(stuff);
            HtmlNodeCollection hncA = hd.DocumentNode.SelectNodes("//ul/li//a[@class]");

            foreach (HtmlNode itt in hncA)
            {
                if (!itt.GetAttributeValue("class", "").Contains("ui-profile-popup"))
                {
                    continue;
                }

                string memnum = "", pic = "", name = "", desc = "";
                /*<a href="member.php?id=3386759" class="ui-profile-popup"
                 * data-user_id="3386759" data-profile_img="http://i2.pixiv.net/img82/profile/ykss35/mobile/7310071_80.jpg"
                 * data-user_name="うぐいす餅">うぐいす餅</a>*/
                memnum = itt.GetAttributeValue("data-user_id", "");
                pic = itt.GetAttributeValue("data-profile_img", "");
                if (string.IsNullOrEmpty(pic))
                {
                    HtmlNode hnImg = hd.DocumentNode.SelectSingleNode(itt.XPath + "//img");
                    if (hnImg != null)
                    {
                        pic = hnImg.GetAttributeValue("src", "");
                        if (string.IsNullOrEmpty(pic))
                        {
                            pic = hnImg.GetAttributeValue("data-src", "");
                        }
                    }
                }
                name = itt.GetAttributeValue("data-user_name", "");
                if (string.IsNullOrEmpty(name))
                {
                    HtmlNode hnName = hd.DocumentNode.SelectSingleNode(itt.XPath + "//h1[@class=\"user-name\"]");
                    name = HttpUtility.HtmlDecode(hnName.InnerText);
                }
                HtmlNode hnDesc = hd.DocumentNode.SelectSingleNode(itt.XPath + "//p[@class=\"user-comment\"]");
                if (hnDesc != null)
                {
                    desc = HttpUtility.HtmlDecode(hnDesc.InnerText);
                }

                if (memnum.Length > 0 && name.Length > 0)
                {
                    var m = new Contact(pic, memnum, HttpUtility.HtmlDecode(name), HttpUtility.HtmlDecode(desc));
                    if (!imc.Contains(m))
                    {
                        imc.Add(m);
                    }
                }
            }


            if (shouldClear)
            {
                cm.Clear();
            }

            if (imc.Count > 0)
            {
                foreach (Contact ci in imc)
                {
                    cm.Add(ci);
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        /*//즐겨찾기 유저 폴더
        public static bool FavoriteUserFolder()*/

        public static bool ViewDetail(DetailViewModel dvm, object idm, SwipeContentControl pv, double maxwidth, bool sibling,
            Action<int> after, string stuff)
        {
            try
            {
                bool r18 = false;
                Communication.isLogin = (stuff.IndexOf("loggedIn = true") >= 0);
                HtmlDocument hd = new HtmlDocument();
                hd.LoadHtml(stuff);

                dvm.ocTags.Clear();
                dvm.Prior = string.Empty;
                dvm.NextNum = string.Empty;
                dvm.Next = string.Empty;
                dvm.PriorNum = string.Empty;
                pv.CanSwipeLeft = false;
                pv.CanSwipeLeft = false;
                int iSWData = CommonMethod.SearchTagAttributesEnd("section", "work-info", 0, stuff);
                int iEWData = -1;
                #region 로그인 상태
                if (iSWData > 0)
                {
                    #region 앞뒤 작품
                    var hnBefore = hd.DocumentNode.SelectSingleNode("//li[@class=\"before\"]");
                    if (hnBefore != null)
                    {
                        var hnAB = hnBefore.SelectSingleNode(".//a[@href]");
                        if (hnAB != null)
                        {
                            string href = hnAB.GetAttributeValue("href", "");
                            int iSID = href.IndexOf("illust_id=");
                            if (iSID > 0)
                            {
                                iSID += "illust_id=".Length;
                                int iEID = href.IndexOf('&', iSID);
                                if (iEID <= iSID)
                                {
                                    iEID = href.Length - iSID;
                                }
                                else
                                {
                                    iEID = iEID - iSID;
                                }
                                if (sibling)
                                {
                                    dvm.PriorNum = href.Substring(iSID, iEID);
                                }

                                var hnIB = hnAB.SelectSingleNode(".//img[@src]");
                                if (hnIB != null)
                                {
                                    dvm.Prior = hnIB.GetAttributeValue("src", "");
                                }
                                pv.CanSwipeLeft = true;
                            }
                        }
                    }

                    var hnAfter = hd.DocumentNode.SelectSingleNode("//li[@class=\"after\"]");
                    if (hnAfter != null)
                    {
                        var hnAA = hnAfter.SelectSingleNode(".//a[@href]");
                        if (hnAA != null)
                        {
                            string href = hnAA.GetAttributeValue("href", "");
                            int iSID = href.IndexOf("illust_id=");
                            if (iSID > 0)
                            {
                                iSID += "illust_id=".Length;
                                int iEID = href.IndexOf('&', iSID);
                                if (iEID <= iSID)
                                {
                                    iEID = href.Length - iSID;
                                }
                                else
                                {
                                    iEID = iEID - iSID;
                                }
                                dvm.NextNum = href.Substring(iSID, iEID);

                                var hnIB = hnAA.SelectSingleNode(".//img[@src]");
                                if (hnIB != null)
                                {
                                    dvm.Next = hnIB.GetAttributeValue("src", "");
                                }

                                pv.CanSwipeRight = true;
                            }
                        }
                    }
                    #endregion
                    #region 메타
                    Border bddum = idm as Border;
                    bddum.Width = double.NaN;
                    bddum.Height = double.NaN;

                    var hnMetali = hd.DocumentNode.SelectNodes("//ul[@class=\"meta\"]/li");
                    if (hnMetali != null)
                    {
                        for (int i = 0; i < hnMetali.Count; i++)
                        {
                            var hnLi = hnMetali[i];
                            if (i == 0)
                            {
                                dvm.TimeStamp = HttpUtility.HtmlDecode(hnLi.InnerText);
                            }
                            else if (i == 1)
                            {
                                if (hnLi.InnerText.Contains('×'))
                                {
                                    string[] res = hnLi.InnerText.Split(new char[] { '×' });
                                    int wid = 0, hei = 0;
                                    if (res.Length == 2)
                                    {
                                        try
                                        {
                                            wid = Convert.ToInt32(res[0].Trim());
                                            string rehei = res[1].Trim();
                                            if (rehei.IndexOf('　') > 0)
                                            {
                                                rehei = rehei.Substring(0, rehei.IndexOf('　'));
                                            }
                                            hei = Convert.ToInt32(rehei);
                                        }
                                        catch { }
                                        if (idm != null)
                                        {
                                            bddum = idm as Border;
                                            bddum.Width = maxwidth;
                                            bddum.Height = hei * maxwidth / wid;
                                        }
                                    }
                                    dvm.isManga = false;
                                }
                                dvm.isManga = true;
                                dvm.TimeStamp += Environment.NewLine + HttpUtility.HtmlDecode(hnLi.InnerText);
                            }
                            else
                            {
                                var hncInLi = hnLi.SelectNodes(".//li");
                                if (hncInLi != null)
                                {
                                    foreach (var hnI in hncInLi)
                                    {
                                        if (string.IsNullOrEmpty(dvm.Tool))
                                        {
                                            dvm.Tool = HttpUtility.HtmlDecode(hnI.InnerText);
                                        }
                                        else
                                        {
                                            dvm.Tool += Environment.NewLine + HttpUtility.HtmlDecode(hnI.InnerText);
                                        }
                                    }
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(dvm.Tool))
                                    {
                                        dvm.Tool = HttpUtility.HtmlDecode(hnLi.InnerText);
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                    #region 점수
                    var hnScore = hd.DocumentNode.SelectSingleNode("//section[@class=\"score\"]/dl");
                    if (hnScore != null)
                    {
                        StringBuilder sb = new StringBuilder();
                        foreach (var hnSin in hnScore.ChildNodes)
                        {
                            if (hnSin.Name == "dd")
                            {
                                string cls = hnSin.GetAttributeValue("class", "");
                                if (cls == "rated-count")
                                {
                                    int ir = 0;
                                    if (int.TryParse(hnSin.InnerText, out ir) && ir > 0)
                                    {
                                        dvm.Rate_max = ir * 10;
                                    }
                                    else
                                    {
                                        dvm.Rate_max = 0;
                                    }
                                }
                                else if (cls == "score-count")
                                {
                                    int ir = 0;
                                    if (int.TryParse(hnSin.InnerText, out ir))
                                    {
                                        dvm.Rate_good = ir;
                                    }
                                    else
                                    {
                                        dvm.Rate_good = 0;
                                    }
                                }

                                sb.Append(" " + hnSin.InnerText);
                                sb.Append(Environment.NewLine);
                            }
                            else if (hnSin.Name == "dt")
                            {
                                sb.Append(HttpUtility.HtmlDecode(hnSin.InnerText));
                            }
                        }
                        dvm.Rate = sb.ToString();
                    }
                    #endregion

                    #region 제목
                    var hnTitle = hd.DocumentNode.SelectSingleNode("//section[contains(@class, 'work-info')]//h1[@class=\"title\"]");
                    if (hnTitle != null)
                    {
                        dvm.Title = HttpUtility.HtmlDecode(hnTitle.InnerText);
                    }
                    #endregion

                    #region 캡션
                    var hnCap = hd.DocumentNode.SelectSingleNode("//section[contains(@class, 'work-info')]//p[@class=\"caption\"]");
                    if (hnCap != null)
                    {
                        dvm.Caption = HttpUtility.HtmlDecode(hnCap.InnerHtml);
                    }
                    #endregion

                    bool _r18 = false;
                    #region 태그
                    var hncTags = hd.DocumentNode.SelectNodes("//ul[@class=\"tags\"]/li");
                    if (hncTags != null)
                    {
                        foreach (var hnTag in hncTags)
                        {
                            var hnText = hnTag.SelectSingleNode(".//*[@class=\"text\"]");
                            if (hnText != null)
                            {
                                string sEachTag = HttpUtility.HtmlDecode(hnText.InnerText);

                                if ((sEachTag == "R-18" || sEachTag == "R-18G") && !Communication.Te)
                                {
                                    _r18 = true;
                                }

                                bool isSelf = false;
                                var hnSelf = hnTag.SelectSingleNode(".//*[@class=\"self-tag\"]");
                                if (hnSelf != null)
                                {
                                    isSelf = hnSelf.InnerText.Length > 0;
                                }

                                dvm.ocTags.Add(new TagItem() { TagName = sEachTag, isnSelf = !isSelf });
                            }
                        }
                    }
                    #endregion

                    #region 그림
                    if (_r18)
                    {
                        dvm.Pic = "http://fake.dd/r-18";
                    }
                    else
                    {
                        var hnImg = hd.DocumentNode.SelectSingleNode("//div[contains(@class, '_layout-thumbnail')]//img[@src or @data-src]");
                        if (hnImg != null)
                        {
                            dvm.Pic = hnImg.GetAttributeValue("data-src", "");
                            if (string.IsNullOrEmpty(dvm.Pic))
                            {
                                dvm.Pic = hnImg.GetAttributeValue("src", "");
                            }

                            //<img width="4092" height="2893" class="big" alt="12月" data-src="http://i3.pixiv.net/img-original/img/2014/12/12/09/31/55/47516478_p0.jpg">
                            //var hnBig = hd.DocumentNode.SelectSingleNode("//div[contains(@class, '_layout-thumbnail')]//img[@class=\"big\"]");
                            var hnBig = hd.DocumentNode.SelectSingleNode("//img[contains(@class, 'original-image')]");
                            dvm.Type = "error";
                            if (hnBig != null)
                            {
                                string dummy = hnBig.GetAttributeValue("data-src", "");
                                dvm.Type = "?mode=single&fullsize=" + HttpUtility.UrlEncode(HttpUtility.HtmlDecode(dummy));
                            }
                            else
                            {
                                var hnIA = hd.DocumentNode.SelectSingleNode("//div[contains(@class, 'works_display')]//a[@href]");
                                if (hnIA != null)
                                {
                                    string dummy = hnIA.GetAttributeValue("href", "");
                                    int iQ = dummy.IndexOf('?');
                                    if (iQ >= 0)
                                    {
                                        dvm.Type = HttpUtility.HtmlDecode(dummy.Substring(iQ, dummy.Length - iQ));
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    #region 프로필
                    var hnPA = hd.DocumentNode.SelectSingleNode("//div[contains(@class, 'profile-unit')]//a[@href]");
                    if (hnPA != null)
                    {
                        string sHref = hnPA.GetAttributeValue("href", "");
                        if (sHref.Length > 0)
                        {
                            int iSNum = sHref.IndexOf("id=");
                            if (iSNum > 0)
                            {
                                iSNum += 3;
                                int iENum = sHref.IndexOf('&');
                                string sID = "";
                                if (iENum > iSNum)
                                {
                                    sID = sHref.Substring(iSNum, iENum - iSNum);
                                }
                                sID = sHref.Substring(iSNum, sHref.Length - iSNum);
                                dvm.membernum = sID;
                            }
                        }
                        dvm.Name = HttpUtility.HtmlDecode(hnPA.InnerText);
                    }
                    #endregion
                }
                #endregion
                #region 비로그인 상태
                else
                {
                    HtmlNode hnTitle = hd.DocumentNode.SelectSingleNode("//div[@class=\"userdata\"]/h1[@class=\"title\"]");
                    if (hnTitle != null)
                    {
                        dvm.Title = HttpUtility.HtmlDecode(hnTitle.InnerText);
                    }

                    HtmlNode hnName = hd.DocumentNode.SelectSingleNode("//div[@class=\"userdata\"]/h2[@class=\"name\"]");
                    if (hnName != null)
                    {
                        dvm.Name = HttpUtility.HtmlDecode(hnName.InnerText);
                    }

                    HtmlNode hnDate = hd.DocumentNode.SelectSingleNode("//div[@class=\"userdata\"]/span[@class=\"date\"]");
                    if (hnDate != null)
                    {
                        dvm.TimeStamp = HttpUtility.HtmlDecode(hnDate.InnerText);
                    }

                    HtmlNode hnImg = hd.DocumentNode.SelectSingleNode("//div[@class=\"img-container\"]//img");
                    if (hnImg != null)
                    {
                        dvm.Pic = hnImg.GetAttributeValue("src", "");
                        if (hnImg.ParentNode != null && hnImg.ParentNode.Name == "a")
                        {
                            string dummy = hnImg.ParentNode.GetAttributeValue("href", "");
                            int iQ = dummy.IndexOf('?');
                            if (iQ >= 0)
                            {
                                dvm.Type = HttpUtility.HtmlDecode(dummy.Substring(iQ, dummy.Length - iQ));
                            }
                        }
                    }

                    HtmlNode hnCap = hd.DocumentNode.SelectSingleNode("//div[contains(@class,'work-main')]//*[@class=\"caption\"]");
                    if (hnCap != null)
                    {
                        dvm.Caption = HttpUtility.HtmlDecode(hnCap.InnerHtml);
                    }

                    dvm.ocTags.Clear();
                    HtmlNodeCollection hncTags = hd.DocumentNode.SelectNodes("//li[@class=\"tag\"]");
                    if (hncTags != null)
                    {
                        foreach (HtmlNode hnI in hncTags)
                        {
                            HtmlNode hnTagTitle = hd.DocumentNode.SelectSingleNode(hnI.XPath + "//a[@class=\"text\"]");
                            if (hnTagTitle != null)
                            {
                                string ntag = HttpUtility.HtmlDecode(hnTagTitle.InnerText);
                                dvm.ocTags.Add(new TagItem() { TagName = ntag });
                            }
                        }
                    }

                    dvm.Tool = "";
                    HtmlNodeCollection hncInfo = hd.DocumentNode.SelectNodes("//li[@class=\"info\"]");
                    if (hncInfo != null)
                    {
                        foreach (HtmlNode hnI in hncInfo)
                        {
                            if (!string.IsNullOrEmpty(dvm.Tool))
                            {
                                dvm.Tool += Environment.NewLine;
                            }
                            dvm.Tool += HttpUtility.HtmlDecode(hnI.InnerText.Trim());
                        }
                    }

                    HtmlNode hnOld = hd.DocumentNode.SelectSingleNode("//li[@class=\"older\"]/a");
                    if (hnOld != null)
                    {
                        string href = hnOld.GetAttributeValue("href", "");
                        int iSID = href.IndexOf("illust_id=");
                        if (iSID > 0)
                        {
                            iSID += "illust_id=".Length;
                            int iEID = href.IndexOf('&', iSID);
                            if (iEID <= iSID)
                            {
                                iEID = href.Length - iSID;
                            }
                            else
                            {
                                iEID = iEID - iSID;
                            }
                            dvm.NextNum = href.Substring(iSID, iEID);
                        }
                    }

                    HtmlNode hnNew = hd.DocumentNode.SelectSingleNode("//li[@class=\"newer\"]/a");
                    if (hnNew != null)
                    {
                        string href = hnNew.GetAttributeValue("href", "");
                        int iSID = href.IndexOf("illust_id=");
                        if (iSID > 0)
                        {
                            iSID += "illust_id=".Length;
                            int iEID = href.IndexOf('&', iSID);
                            if (iEID <= iSID)
                            {
                                iEID = href.Length - iSID;
                            }
                            else
                            {
                                iEID = iEID - iSID;
                            }
                            dvm.PriorNum = href.Substring(iSID, iEID);
                        }
                    }
                }
                #endregion
                #region 에러 처리
                var hnError = hd.DocumentNode.SelectSingleNode("//*[contains(@class, 'error')]");
                if (dvm.Pic == null && hnError != null)
                {
                    MessageBox.Show(HttpUtility.HtmlDecode(hnError.InnerText), Res.LocalString.errorserver, MessageBoxButton.OK);
                    dvm.Caption = "error";
                    return true;
                }
                HtmlNode hnD = hd.DocumentNode.SelectSingleNode("//section[@class=\"restricted-content\"]");
                if (hnD != null)
                {
                    MessageBox.Show(Res.LocalString.errorprivate, Res.LocalString.errorpixifeed, MessageBoxButton.OK);
                    dvm.Caption = "error";
                }
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while parsing the detail." + Environment.NewLine + ex.GetType().ToString(), "Pixifeed error",
                            MessageBoxButton.OK);
                dvm.Caption = "error";
            }

            return true;
        }

        public static bool ViewProfile(ref ContactModel cm, string stuff)
        {
            int iSProfile = CommonMethod.SearchTagAttributesEnd("div", "profile-unit", 0, stuff);
            if (iSProfile >= 0)
            {
                int iSImg = CommonMethod.SearchTagAttributes("img", "src", iSProfile, stuff);
                if (iSImg > iSProfile)
                {
                    int iEImg = stuff.IndexOf('>', iSImg);
                    if (iEImg > iSImg)
                    {
                        iEImg++;
                        string sImg = CommonMethod.GetTagAttribute("src", stuff.Substring(iSImg, iEImg - iSImg));
                        if (sImg.Length > 0)
                        {
                            cm.Pic = sImg;
                        }
                    }
                }

                int iSH2 = stuff.IndexOf("<h", iSProfile + 1);
                if (iSH2 > iSProfile)
                {
                    int iEH2 = stuff.IndexOf("</h", iSH2);
                    if (iEH2 > iSH2)
                    {
                        //iSH2 += 4;
                        string sNick = stuff.Substring(iSH2, iEH2 - iSH2);
                        if (sNick.Length > 0)
                        {
                            cm.Nick = HttpUtility.HtmlDecode(CommonMethod.RemoveAllTags(sNick));
                        }
                    }
                }
            }
            else
            {
                return false;
            }

            int iSTable = CommonMethod.SearchTagAttributes("table", "ws_table", 0, stuff);
            if (iSTable >= 0)
            {
                int iSTd = CommonMethod.SearchTagAttributesEnd("td", "td1", iSTable + 1, stuff);
                while (iSTd > iSTable)
                {
                    int iETr = stuff.IndexOf("</tr>", iSTd);
                    if (iETr > iSTd)
                    {
                        int iETd = stuff.IndexOf("</td>", iSTd);
                        if (iETd > iSTd)
                        {
                            string stitle = stuff.Substring(iSTd, iETd - iSTd);
                            int iSTd2 = CommonMethod.SearchTagAttributesEnd("td", "td2", iETd, stuff);
                            int iETd3 = stuff.IndexOf("</tr>", iETd);
                            if (iSTd2 > iETd)
                            {
                                int iETd2 = stuff.IndexOf("</td>", iSTd2);
                                if (iETd3 < iETd2 && iETd3 >= 0)
                                {
                                    iETd2 = Math.Min(iETd2, iETd3);
                                }
                                if (iETd2 > iSTd2)
                                {
                                    string scap = stuff.Substring(iSTd2, iETd2 - iSTd2).Replace("<br>", Environment.NewLine);
                                    scap = HttpUtility.HtmlDecode(CommonMethod.RemoveAllTags(scap));
                                    if (stitle.Length > 0 && scap.Length > 0)
                                    {
                                        cm.ocContactInfo.Add(new ContactItem() { Caption = stitle, Content = scap });
                                    }
                                }
                            }
                        }
                    }
                    iSTd = CommonMethod.SearchTagAttributesEnd("td", "td1", iSTd + 1, stuff);
                }
                //iSTable = CommonMethod.SearchTagAttributesEnd("table", "ws_table", iSTable + 1, stuff);
            }

            if (cm.ocContactInfo.Count == 0)
            {
                cm.ocContactInfo.Add(new ContactItem() { Caption = Res.LocalString.errorprofile });
            }
            return true;
        }

        public static void ShowComments(ref ObservableCollection<CommentItem> ci, string stuff)
        {
            List<CommentItem> lci = new List<CommentItem>();

            #region 이전 방식
            string sA = "", sDate = "", sHref = "", sComment = "";
            int iSArr = stuff.IndexOf("html");
            if (iSArr >= 0)
            {
                int iSSArr = stuff.IndexOf("[", iSArr);
                if (iSSArr > iSArr)
                {
                    int iESArr = stuff.IndexOf("]", iSSArr);
                    if (iESArr > iSSArr + 2)
                    {
                        Regex rx = new Regex(@"\\[uU]([0-9A-Fa-f]{4})");
                        string nst = HttpUtility.HtmlDecode(rx.Replace(stuff.Substring(iSSArr + 2, iESArr - iSSArr - 2), match =>
                            ((char)Int32.Parse(match.Value.Substring(2), NumberStyles.HexNumber)).ToString())).Replace(@"\/", "/");
                        //string nst = HttpUtility.HtmlDecode();

                        int iSP = CommonMethod.SearchTagAttributes("li", "comment-item", 0, nst);

                        while (iSP >= 0)
                        {
                            sA = sDate = sHref = sComment = "";
                            int iEP = nst.IndexOf("</li>", iSP);
                            int iSSP = iSP;
                            if (iEP >= 0)
                            {
                                int iSA = CommonMethod.SearchTagAttributes("a", "href=\"member.php", iSP, nst);
                                if (iSA > iSP)
                                {
                                    int iSEA = nst.IndexOf('>', iSA);
                                    if (iSEA > iSA)
                                    {
                                        iSEA++;
                                        sHref = CommonMethod.GetTagAttribute("href", nst.Substring(iSA, iSEA - iSA));
                                        if (sHref.Length > 0)
                                        {
                                            int iSID = sHref.IndexOf("id=");
                                            if (iSID >= 0)
                                            {
                                                iSID += 3;
                                                sHref = sHref.Substring(iSID, sHref.Length - iSID);
                                            }
                                        }

                                        int iEA = nst.IndexOf("</a>", iSEA);
                                        if (iSEA >= iSA)
                                        {
                                            sA = HttpUtility.HtmlDecode(nst.Substring(iSEA, iEA - iSEA));
                                            int iSDate = CommonMethod.SearchTagAttributesEnd("span", "comment-date", iEA, nst);
                                            if (iSDate >= iEA)
                                            {
                                                int iEDate = nst.IndexOf("</span>", iSDate);
                                                if (iEDate >= iSDate)
                                                {
                                                    sDate = nst.Substring(iSDate, iEDate - iSDate);
                                                    iSSP = iEDate + 7;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            if (iSSP < iEP)
                            {
                                sComment = CommonMethod.RemoveAllTags(nst.Substring(iSSP, iEP - iSSP)).Replace(@"\r", "").Replace(@"\n", " ");
                                if (sComment.Length > 0)
                                {
                                    sComment = HttpUtility.HtmlDecode(sComment);
                                    lci.Add(new CommentItem() { Name = sA, _nameid = sHref, TimeStamp = sDate, Body = sComment });
                                }
                            }

                            iSP = CommonMethod.SearchTagAttributes("li", "comment-item", iSP + 1, nst);
                        }
                    }
                    else
                    {
                        lci.Add(new CommentItem() { Body = Res.LocalString.nocom });
                    }
                }
            }

            if (lci.Count > 0)
            {
                ci.Clear();
                foreach (CommentItem each in lci)
                {
                    ci.Add(each);
                }
            }
            #endregion
        }

        public static List<CommentItem> ShowComments(string stuff, out bool isMore)
        {
            if (string.IsNullOrEmpty(stuff) || !stuff.EndsWith("}"))
            {
                isMore = false;
                return null;
            }
            JObject json = (JObject)JsonConvert.DeserializeObject(stuff);
            isMore = (bool?)json["body"]["more"] == true;
            string htmlbody = json["body"]["html"].ToString();
            stuff = null;

            HtmlDocument hd = new HtmlDocument();
            hd.OptionAutoCloseOnEnd = true;
            hd.LoadHtml(htmlbody);

            List<CommentItem> lc = new List<CommentItem>();
            HtmlNodeCollection hncComments = hd.DocumentNode.SelectNodes("//div[@class]");
            if (hncComments != null)
            {
                foreach (HtmlNode hnC in hncComments)
                {
                    string dc = hnC.GetAttributeValue("class", "");
                    string profile = null, name = null, nameurl = null, body = null, stamp = "", date = "";
                    if (dc.StartsWith("_comment-item"))
                    {
                        bool host = dc.IndexOf("host-user") >= 0;

                        
                        if (hnC.HasChildNodes)
                        {
                            foreach (HtmlNode hnCpart in hnC.ChildNodes)
                            {
                                if (hnCpart != null)
                                {
                                    if (hnCpart.Name == "a" && hnCpart.GetAttributeValue("class", "").IndexOf("user-icon-container") >= 0)
                                    {
                                        #region 프로필 이미지 컨테이너 div/a/img
                                        HtmlNode hnI = hnCpart.ChildNodes["img"];
                                        if (hnI != null)
                                        {
                                            profile = hnI.GetAttributeValue("data-src", null);//
                                        }
                                        #endregion
                                    }
                                    else if (hnCpart.Name == "div" && hnCpart.GetAttributeValue("class", "").IndexOf("comment") >= 0)
                                    {
                                        #region 코멘트 본 컨테이너 div/div
                                        if (hnCpart.HasChildNodes)
                                        {
                                            foreach (HtmlNode hnDpart in hnCpart.ChildNodes)
                                            {
                                                if (hnDpart != null)
                                                {
                                                    if (hnDpart.Name == "div" && hnDpart.GetAttributeValue("class", "").IndexOf("meta") >= 0)
                                                    {
                                                        //메타 컨테이너 div/div/div->a | span
                                                        HtmlNode hnA = hnDpart.ChildNodes["a"];
                                                        if (hnA != null)
                                                        {
                                                            string link = hnA.GetAttributeValue("href", "");
                                                            int iid = link.IndexOf("id=");
                                                            if (iid >= 0)
                                                            {
                                                                iid += 3;
                                                                nameurl = link.Substring(iid, link.Length - iid);//
                                                            }
                                                            name = HttpUtility.HtmlDecode(hnA.InnerText);//
                                                        }

                                                        HtmlNode hnS = hnDpart.ChildNodes["span"];
                                                        if (hnS != null)
                                                        {
                                                            date = HttpUtility.HtmlDecode(hnS.InnerText);//
                                                        }
                                                    }
                                                    else if (hnDpart.Name == "div" && hnDpart.GetAttributeValue("class", "").IndexOf("body") >= 0)
                                                    {
                                                        string rawbody = hnDpart.InnerText.Replace("\r", "").Replace("\n", "");
                                                        body = HttpUtility.HtmlDecode(rawbody.Trim().Replace("<br>", Environment.NewLine));//
                                                    }
                                                    else if (hnDpart.Name == "div" && hnDpart.GetAttributeValue("class", "").IndexOf("sticker-container") >= 0)
                                                    {
                                                        //스탬프 컨테이너 div/div/div
                                                        HtmlNode hnI = hnDpart.ChildNodes["img"];
                                                        if (hnI != null)
                                                        {
                                                            stamp = hnI.GetAttributeValue("data-src", null);//
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion
                                    }
                                }
                            }
                        }
                        if (!string.IsNullOrEmpty(body) || !string.IsNullOrEmpty(stamp))
                        {
                            lc.Add(new CommentItem()
                            {
                                _nameid = nameurl,
                                Body = body,
                                Name = name,
                                Profile = profile,
                                Stamp = stamp,
                                TimeStamp = date,
                                Host = host
                            });
                        }
                    }
                    else if(dc.StartsWith("_comment-sticker"))
                    {
                        var hncS = hnC.SelectNodes(".//div[@class=\"sticker-item\"]");
                        if(hncS != null)
                        {
                            foreach(var hnS in hncS)
                            {
                                name = ""; profile = ""; nameurl = ""; stamp = ""; date = ""; body = "";
                                var hnA = hnS.SelectSingleNode(".//a");
                                if(hnA != null)
                                {
                                    name = hnA.GetAttributeValue("data-user_name", "");
                                    profile = hnA.GetAttributeValue("data-profile_img", "");
                                    nameurl = hnA.GetAttributeValue("data-user_id", "");
                                }
                                var hnI = hnS.SelectSingleNode(".//img[contains(@class,'sticker')]");
                                if (hnI != null)
                                {
                                    stamp = hnI.GetAttributeValue("data-src", "");
                                }
                                if (!string.IsNullOrEmpty(body) || !string.IsNullOrEmpty(stamp))
                                {
                                    lc.Add(new CommentItem()
                                    {
                                        _nameid = nameurl,
                                        Body = body,
                                        Name = name,
                                        Profile = profile,
                                        Stamp = stamp,
                                        TimeStamp = date
                                    });
                                }
                            }
                        }
                        
                    }
                }
            }
            return lc;
        }

        public static List<EmojiModel> EnumEmoji(string stuff)
        {
            List<EmojiModel> lm = new List<EmojiModel>();

            string[] m = stuff.Split(';');
            if (m.Length > 1)
            {
                if (m[1].Contains("emoji"))
                {
                    int iS = m[1].IndexOf('[');
                    if (iS > 0)
                    {
                        iS++;
                        int iE = m[1].Length - 1;
                        if (iE > iS)
                        {
                            string n = m[1].Substring(iS, iE - iS);
                            List<EmojiInfo> lei = JSONParser.JSONToEmoji(n);
                            if (lei != null && lei.Count > 0)
                            {
                                foreach (var o in lei)
                                {
                                    lm.Add(new EmojiModel()
                                    {
                                        Name = o.name,
                                        Pic = string.Format("http://source.pixiv.net/common/images/emoji/{0}.png", o.id)
                                    });
                                }
                            }
                        }
                    }
                }
            }

            return lm;
        }

        public static List<StampModel> EnumStamps(string stuff)
        {
            List<StampModel> lm = new List<StampModel>();

            string[] m = stuff.Split(';');
            if (m.Length > 1)
            {
                if (m[0].Contains("stamp"))
                {
                    int iS = m[0].IndexOf('[');
                    if (iS > 0)
                    {
                        iS++;
                        int iE = m[0].Length - 1;
                        if (iE > iS)
                        {
                            string n = m[0].Substring(iS, iE - iS);
                            List<StampInfo> lei = JSONParser.JSONToStamp(n);
                            if (lei != null && lei.Count > 0)
                            {
                                foreach (var o in lei)
                                {
                                    foreach (var p in o.stamps)
                                    {
                                        lm.Add(new StampModel()
                                        {
                                            Id = p.ToString(),
                                            Pic = string.Format("http://source.pixiv.net/common//images/stamp/stamps/{0}_s.jpg", p)
                                        });
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return lm;
        }

        public static bool ShowWorks(ref ObservableCollection<PictureModel> pc, bool shouldClean, string stuff)
        {
            HtmlDocument hd = new HtmlDocument();
            hd.LoadHtml(stuff);
            List<PictureModel> pm = new List<PictureModel>();

            var hncLis = hd.DocumentNode.SelectNodes("//li[contains(@class, 'image-item')]");
            if (hncLis != null)
            {

                foreach (var hnLi in hncLis)
                {
                    string mem = "", uri = "", title = ""; bool ugoira = false, multi = false;
                    var hnA = hnLi.SelectSingleNode(".//a[@href]");
                    if (hnA != null)
                    {
                        mem = hnA.GetAttributeValue("href", "");
                    }
                    int iID = mem.IndexOf("id=");
                    if (iID > 0)
                    {
                        iID += 3;
                        mem = mem.Substring(iID, mem.Length - iID);
                    }
                    var hnImg = hnLi.SelectSingleNode(".//img[contains(@class, 'thumbnail')]");
                    if (hnImg != null)
                    {
                        uri = hnImg.GetAttributeValue("src", "");
                        if (string.IsNullOrEmpty(uri))
                        {
                            uri = hnImg.GetAttributeValue("data-src", "");
                        }
                        var tag = hnImg.GetAttributeValue("data-tags", "");
                        ugoira = tag.Contains("うごイラ");

                        var hnUgoA = hnLi.SelectSingleNode(".//a[contains(@class, 'ugoku-illust')]");
                        if (hnUgoA != null)
                        {
                            ugoira = true;
                        }

                        var hnM = hnLi.SelectSingleNode(".//a[contains(@class, 'multiple')]");
                        if (hnM != null)
                        {
                            multi = true;
                        }

                        if (tag.Contains("R-18") && !Communication.Te)
                        {
                            uri = "http://fake.dd/r-18";
                        }
                    }

                    var hnTitle = hnLi.SelectSingleNode(".//*[@class=\"title\"]");
                    if (hnTitle != null)
                    {
                        title = HttpUtility.HtmlDecode(hnTitle.InnerText).Trim();
                    }
                    PictureModel pnew = new PictureModel(uri, title, string.Empty, mem) { _ugo = ugoira, _mul = multi };
                    if (!pc.Contains(pnew))
                    {
                        pm.Add(pnew);
                    }
                }
            }
            if (pm.Count > 0)
            {
                if (shouldClean)
                {
                    pc.Clear();
                }
                foreach (PictureModel each in pm)
                {
                    pc.Add(each);
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string SingleBigImage(string stuff)
        {
            int iSImg = CommonMethod.SearchTagAttributes("img", "src", 0, stuff);
            if (iSImg > 0)
            {
                int iEImg = stuff.IndexOf('>', iSImg);
                if (iEImg > iSImg)
                {
                    iEImg++;
                    string sTag = stuff.Substring(iSImg, iEImg - iSImg);
                    if (sTag.Length > 0)
                    {
                        string sSrc = CommonMethod.GetTagAttribute("src", sTag);
                        return sSrc;
                    }
                }
            }
            return string.Empty;
        }

        public static List<string> EnumBigImages(string stuff)
        {
            List<string> ling = new List<string>();


            HtmlDocument hd = new HtmlDocument();
            hd.LoadHtml(stuff);

            var col = hd.DocumentNode.SelectNodes("//img[@class]");
            if (col == null)
            {
                int i = stuff.IndexOf("pixiv.context.images[");
                while (i > 0)
                {
                    int j = stuff.IndexOf('"', i);
                    if (j > i)
                    {
                        j++;
                        int k = stuff.IndexOf("\";", j);
                        if (k > j)
                        {
                            var str = Regex.Unescape(stuff.Substring(j, k - j));
                            ling.Add(str);
                        }
                    }
                    if (j > stuff.Length)
                    {
                        break;
                    }
                    i = stuff.IndexOf("pixiv.context.images[", j);
                }
            }
            else if (col != null)
            {
                foreach (var c in col)
                {
                    if (c.GetAttributeValue("data-filter", "").Contains("manga") && c.GetAttributeValue("data-src", "").Length > 0)
                    {
                        ling.Add(c.GetAttributeValue("data-src", ""));
                    }
                    else if (c.GetAttributeValue("class", "").Contains("ui-scroll-view") && c.GetAttributeValue("src", "").Length > 0)
                    {
                        ling.Add(c.GetAttributeValue("src", ""));
                    }
                }
            }

            return ling;
        }

        public static bool AppendRank(ref ObservableCollection<RankItem> ric, bool shouldClean, string stuff)
        {
            List<RankItem> lri = new List<RankItem>();

            HtmlDocument hd = new HtmlDocument();
            hd.LoadHtml(stuff);

            HtmlNodeCollection hncSec = hd.DocumentNode.SelectNodes("//section[@class=\"ranking-item\"]");
            if (hncSec != null)
            {
                foreach (HtmlNode hnS in hncSec)
                {
                    string num = "", img = "", title = "", artist = "", date = "";
                    decimal now = -1, last = 0;
                    bool ugoira = false, multi = false;
                    HtmlNode hnRank = hnS.SelectSingleNode(".//div[@class=\"rank\"]/h1/a");
                    if (hnRank != null)
                    {
                        now = CommonMethod.ExcludeExceptNum(HttpUtility.HtmlDecode(hnRank.InnerText));
                    }
                    HtmlNode hnLast = hnS.SelectSingleNode(".//div[@class=\"rank\"]/p/a");
                    if (hnLast != null)
                    {
                        last = CommonMethod.ExcludeExceptNum(HttpUtility.HtmlDecode(hnLast.InnerText));
                    }

                    HtmlNode hnImg = hnS.SelectSingleNode(".//img[@data-src]");
                    if (hnImg != null)
                    {
                        var tag = hnImg.GetAttributeValue("data-tags", "");
                        if (tag.Contains("うごイラ"))
                        {
                            ugoira = true;
                        }

                        var hnUgo = hnS.SelectSingleNode(".//a[contains(@class, 'ugoku-illust')]");
                        if (hnUgo != null)
                        {
                            ugoira = true;
                        }
                        var hnM = hnS.SelectSingleNode(".//a[contains(@class, 'multiple')]");
                        if (hnM != null)
                        {
                            multi = true;
                        }

                        if (tag.Contains("R-18") && !Communication.Te)
                        {
                            img = "http://fake.dd/r-18";
                        }
                        else
                        {
                            img = hnImg.GetAttributeValue("data-src", "");
                        }
                    }

                    HtmlNode hnTitle = hnS.SelectSingleNode(".//h2/a");
                    if (hnTitle != null)
                    {
                        string anchor = hnTitle.GetAttributeValue("href", "");
                        int iSID = anchor.IndexOf("id=");
                        if (iSID > 0)
                        {
                            iSID += 3;
                            //num
                            int iAfter = anchor.IndexOf('&', iSID);
                            if (iAfter >= iSID)
                            {
                                num = anchor.Substring(iSID, iAfter - iSID);
                            }
                            else
                            {
                                num = anchor.Substring(iSID, anchor.Length - iSID);
                            }
                        }
                        title = HttpUtility.HtmlDecode(hnTitle.InnerText);
                    }

                    HtmlNode hnMem = hnS.SelectSingleNode(".//a[@data-user_name]/span");
                    if (hnMem != null)
                    {
                        artist = HttpUtility.HtmlDecode(hnMem.InnerText);
                    }

                    date = hnS.GetAttributeValue("data-date", "");

                    if (!string.IsNullOrEmpty(num))
                    {
                        lri.Add(new RankItem(date, artist, img, num, title, now, last, ugoira, multi));
                    }
                }
            }

            if (lri.Count > 0)
            {
                if (shouldClean)
                {
                    ric.Clear();
                }

                //ric.AddRange(lri);
                foreach (RankItem ri in lri)
                {
                    ric.Add(ri);
                }

                return true;
            }
            return false;
        }

        public static bool SuggestUsers(ref ObservableCollection<Contact> csu, string stuff)
        {
            List<Contact> clst = new List<Contact>();

            int iSUser = CommonMethod.SearchTagAttributesEnd("ul", "class=\"users\"", 0, stuff);
            while (iSUser >= 0)
            {
                int iEUser = stuff.IndexOf("</ul>", iSUser);
                if (iEUser > iSUser)
                {
                    string[] users = stuff.Substring(iSUser, iEUser - iSUser).Split(new string[] { "</li>" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string usr in users)
                    {
                        int iSA = CommonMethod.SearchTagAttributes("a", "href=\"/member.php?id=", 0, usr);
                        if (iSA >= 0)
                        {
                            int iEA = usr.IndexOf('>', iSA);
                            if (iEA > iSA)
                            {
                                iEA++;
                                string anchor = usr.Substring(iSA, iEA - iSA);
                                string href = CommonMethod.GetTagAttribute("href", anchor);
                                string id = "";
                                if (href.Length > 0)
                                {
                                    int iSID = href.IndexOf("id=");
                                    if (iSID > 0)
                                    {
                                        iSID += 3;
                                        id = href.Substring(iSID, href.Length - iSID);
                                    }
                                }
                                string name = CommonMethod.GetTagAttribute("data-tooltip", anchor);
                                string img = "";
                                int iImg = usr.IndexOf("<img", iEA);
                                if (iImg >= iEA)
                                {
                                    int iEImg = usr.IndexOf('>', iImg);
                                    if (iEImg > iImg)
                                    {
                                        iEImg++;
                                        img = CommonMethod.GetTagAttribute("src", usr.Substring(iImg, iEImg - iImg));
                                    }
                                }
                                clst.Add(new Contact(img, id, name));
                            }
                        }
                    }
                }
                iSUser = CommonMethod.SearchTagAttributesEnd("ul", "class=\"users\"", iSUser + 1, stuff);
            }
            if (clst.Count > 0)
            {
                csu.Clear();

                foreach (Contact c in clst)
                {
                    csu.Add(c);
                }

                return true;
            }
            else
            {
                return false;
            }
        }

        public static string sugs = "", token = "";
        public static bool LoginTokens(string stuff)
        {
            int iSTok = stuff.IndexOf("pixiv.context.token");
            if (iSTok < 0)
            {
                return false;
            }
            else
            {
                iSTok += 19;
                int iETok = stuff.IndexOf('\n', iSTok);
                if (iETok > iSTok)
                {
                    string sTok = stuff.Substring(iSTok, iETok - iSTok);
                    sTok = Regex.Replace(sTok, @"[^\w]", "");
                    token = sTok;
                }
                else
                {
                    return false;
                }
            }

            int iSA = stuff.IndexOf("pixiv.context.userRecommendSampleUser");
            if (iSA < 0)
            {
                return false;
            }
            else
            {
                iSA += 37;
                int iEA = stuff.IndexOf('\n', iSA);
                if (iEA > iSA)
                {
                    string sRec = stuff.Substring(iSA, iEA - iSA);
                    sRec = Regex.Replace(sRec, @"[^\w,]", "");
                    sugs = sRec;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        public static bool IsUserFavorite(string stuff)
        {
            int iSTok = stuff.IndexOf("pixiv.context.favorite");
            if (iSTok < 0)
            {
                return false;
            }
            else
            {
                iSTok = stuff.IndexOf('=', iSTok);
                if (iSTok > 0)
                {
                    int iETok = stuff.IndexOf('\n', iSTok);
                    if (iETok > iSTok)
                    {
                        string sTok = stuff.Substring(iSTok, iETok - iSTok);
                        sTok = Regex.Replace(sTok, @"[^\w]", "");
                        return Convert.ToBoolean(sTok.Trim().ToLower());
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
        }

        public static bool GetUserBookmarks(ref ObservableCollection<PictureModel> pc, bool shouldClean, string stuff)
        {
            List<PictureModel> lpm = new List<PictureModel>();

            HtmlDocument hd = new HtmlDocument();
            hd.LoadHtml(stuff);

            HtmlNodeCollection hncLi = hd.DocumentNode.SelectNodes("//li[@class=\"image-item\"]");
            if (hncLi != null)
            {

                foreach (HtmlNode hn in hncLi)
                {
                    string uri = "", title = "", author = "", id = "", fav = "", resp = "";
                    bool ugoira = false, multi = false;
                    #region 이미지 부분
                    HtmlNode hnA = hn.ChildNodes["a"];
                    if (hnA != null)
                    {
                        string link = hnA.GetAttributeValue("href", "");
                        int iId = link.IndexOf("illust_id=");
                        if (iId >= 0)
                        {
                            iId += 10;
                            int iEd = link.IndexOf("&", iId);
                            if (iEd < iId)
                            {
                                iEd = link.Length;
                            }
                            id = link.Substring(iId, iEd - iId);
                        }

                        var hnTitle = hn.SelectSingleNode(".//h1[@class=\"title\"]");
                        if (hnTitle != null)
                        {
                            title = HttpUtility.HtmlDecode(hnTitle.InnerText);
                        }

                        HtmlNode img = hn.SelectSingleNode(".//img[@data-src or @src]");
                        if (img != null)
                        {
                            string tag = img.GetAttributeValue("data-tags", "");
                            if (tag.Contains("うごイラ"))
                            {
                                ugoira = true;
                            }

                            var hnUgoA = hn.SelectSingleNode(".//a[contains(@class, 'ugoku-illust')]");
                            if (hnUgoA != null)
                            {
                                ugoira = true;
                            }

                            var hnM = hn.SelectSingleNode(".//a[contains(@class, 'multiple')]");
                            if (hnM != null)
                            {
                                multi = true;
                            }

                            if (tag.Contains("R-18") && !Communication.Te)
                            {
                                uri = "http://fake.dd/r-18";
                            }
                            else
                            {
                                uri = img.GetAttributeValue("data-src", "");
                                if (string.IsNullOrEmpty(uri))
                                {
                                    uri = img.GetAttributeValue("src", "");
                                }
                            }
                        }
                    }
                    #endregion

                    #region 작성자 부분
                    HtmlNode hnAuthor = hn.SelectSingleNode(".//a[@data-user_name]");
                    if (hnAuthor != null)
                    {
                        author = HttpUtility.HtmlDecode(hnAuthor.InnerText);
                    }
                    #endregion

                    #region 북마크 부분
                    HtmlNodeCollection hncI = hn.SelectNodes(".//i[@class]");
                    if (hncI != null)
                    {
                        foreach (HtmlNode hnI in hncI)
                        {
                            if (hnI.GetAttributeValue("class", "").IndexOf("bookmark-") >= 0)
                            {
                                HtmlNode p = hnI.ParentNode;
                                fav = HttpUtility.HtmlDecode(p.InnerText);
                            }
                            else if (hnI.GetAttributeValue("class", "").IndexOf("response-") >= 0)
                            {
                                HtmlNode p = hnI.ParentNode;
                                resp = HttpUtility.HtmlDecode(p.InnerText);
                            }
                        }
                    }
                    #endregion

                    if (!string.IsNullOrEmpty(uri) && !string.IsNullOrEmpty(title))
                    {
                        PictureModel pm = null;
                        pm = new PictureModel(uri, title, author, id, fav, resp) { _ugo = ugoira, _mul = multi };
                        lpm.Add(pm);
                    }
                }
            }

            if (shouldClean)
            {
                pc.Clear();
            }

            if (lpm.Count > 0)
            {
                foreach (PictureModel pm in lpm)
                {
                    pc.Add(pm);
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsR_18(string stuff)
        {
            if (Communication.Te && stuff.IndexOf("<a href=\"/cate_r18.php") > 0)
            {
                return true;
            }
            return false;
        }

        public static string GetJProperty(string keyword, string stuff)
        {
            int iSTok = stuff.IndexOf(keyword);
            if (iSTok >= 0)
            {
                iSTok += keyword.Length;
                int iETok = stuff.IndexOf(';', iSTok);
                if (iETok > iSTok)
                {
                    string sdef = stuff.Substring(iSTok, iETok - iSTok);
                    //                  var reg = new Regex("\".*?\"");
                    var reg = new Regex("\".*?\"");
                    var matches = reg.Match(sdef);
                    if (!matches.Success)
                    {
                        reg = new Regex("\'.*?\'");
                        matches = reg.Match(sdef);
                    }
                    if (matches.Success && matches.Value.Length >= 2)
                    {
                        return matches.Value.Substring(1, matches.Value.Length - 2);
                    }
                }
            }
            return "";
        }

        public static IllustData GetUgoiraData(string stuff, bool isFull = false)
        {
            string keyword = isFull ? "pixiv.context.ugokuIllustFullscreenData" : "pixiv.context.ugokuIllustData";
            int iSTok = stuff.IndexOf(keyword);
            if (iSTok >= 0)
            {
                iSTok += keyword.Length;
                int iETok = stuff.IndexOf(';', iSTok);
                if (iETok > iSTok)
                {
                    string sdef = stuff.Substring(iSTok, iETok + 1 - iSTok);
                    //                  var reg = new Regex("\".*?\"");
                    var reg = new Regex("{.*?};");
                    var matches = reg.Match(sdef);
                    if (matches.Success)
                    {
                        string mv = matches.Value.Substring(0, matches.Value.Length - 1);
                        IllustData idata = JSONParser.JSONToUgoira(mv);
                        Debug.WriteLine(idata.frames);
                        return idata;
                    }
                }
            }
            return null;
        }
    }
}
