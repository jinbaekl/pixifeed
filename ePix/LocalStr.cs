﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace ePix
{
    public class LocalStr
    {
        public LocalStr()
        {

        }

        private static Res.LocalString localized = new Res.LocalString();

        public Res.LocalString localRes { get { return localized; } }
    }
}
