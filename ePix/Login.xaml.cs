﻿using HtmlAgilityPack;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System;
using System.Diagnostics;
using System.IO.IsolatedStorage;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ePix
{
    public partial class Login : PhoneApplicationPage
    {
        public Login()
        {
            InitializeComponent();

            this.Language = System.Windows.Markup.XmlLanguage.GetLanguage(System.Globalization.CultureInfo.CurrentUICulture.Name);

            if (ApplicationBar.Buttons.Count >= 2)
            {
                (ApplicationBar.Buttons[0] as ApplicationBarIconButton).Text = Res.LocalString.login;
                (ApplicationBar.Buttons[1] as ApplicationBarIconButton).Text = Res.LocalString.back;
            }
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if ((Application.Current.Resources["PhoneDarkThemeVisibility"] as Visibility?) == System.Windows.Visibility.Visible ^ Communication.isInvert)
            {
                LayoutRoot.Background = new SolidColorBrush(Color.FromArgb(255, 37, 39, 41));
                SystemTray.BackgroundColor = Color.FromArgb(255, 37, 39, 41);
                SystemTray.ForegroundColor = Colors.White;
            }

            if (IsolatedStorageSettings.ApplicationSettings.Contains("id") && IsolatedStorageSettings.ApplicationSettings.Contains("pass")
                && IsolatedStorageSettings.ApplicationSettings["id"] != null && IsolatedStorageSettings.ApplicationSettings["pass"] != null)
            {
                string id = HttpUtility.UrlDecode(IsolatedStorageSettings.ApplicationSettings["id"] as string);
                string pass = HttpUtility.UrlDecode(IsolatedStorageSettings.ApplicationSettings["pass"] as string);

                //NavigationService.Navigate(new Uri("/Login.xaml?id={0}&pass={1}", UriKind.Relative));
                etbID.Text = id;
                etbPass.Password = pass;
            }
        }

        private void abibLogin_Click(object sender, EventArgs e)
        {
            string id = etbID.Text;
            string pass = etbPass.Password;
            LoginNow(sender, pp, id, pass);
        }

        HttpWebRequest hwr = null; bool working = false;
        public void LoginNow(object sender, object prog, string id, string pass)
        {
            if (id.Length > 0 && pass.Length > 0)
            {


                /*
                 * <div id="contents">
                    <div class="one_column">
                    <div class="one_column_top"> </div>
                    <div class="one_column_body">
                    <div class="errorArea">
                    <h2>로그인을 제한했습니다</h2>
                    잘못된 입력이 계속되어 계정 잠금 상태가 되었습니다. 잠시 기다린 후에 로그인을 시도하십시오.<br>
                                        <a href="/">홈</a><br>
                                            <br>
                                            <div class="finish_upload">
                                                <iframe src="http://serv.ads.pixiv.org/get.php?zone_id=bigbanner404&amp;page_id=all&amp;segment=noseg&amp;pla_referer_page_name=pixiv&amp;pla_mf=notknown&amp;num=53e112fa317" width="500" height="520" marginwidth="0" marginheight="0" frameborder="0" allowtransparency="true" scrolling="no" class="ad-bigbanner" style="display: none !important;"></iframe>                        </div>
                    </div>
                    </div>
                    <div class="onecolumnBottom">&nbsp;</div>
                    </div>
                    </div>
                 */

                if (sender != null)
                {
                    (sender as ApplicationBarIconButton).IsEnabled = false;
                }

                working = true;
                (prog as ProgressBar).Visibility = System.Windows.Visibility.Visible;
                (prog as ProgressBar).IsIndeterminate = true;
                hwr = Communication.CommunicateLogin("http://www.pixiv.net/login.php", "mode=login&return_to=%2F&pixiv_id="
                    + HttpUtility.UrlEncode(id) + "&pass="
                    + HttpUtility.UrlEncode(pass),
                    (re, stuff) =>
                    {
                        if (re >= 0)
                        {
                            HtmlDocument hd = new HtmlDocument();
                            hd.LoadHtml(stuff);
                            HtmlNode hn = hd.DocumentNode.SelectSingleNode("//span[@class=\"error\"]");
                            if(hn == null)
                            {
                                hn = hd.DocumentNode.SelectSingleNode("//div[@class=\"errorArea\"]");
                            }
                            if(hn != null)
                            {
                                string errormsg = null;

                                foreach(var t in hn.ChildNodes)
                                {
                                    if(t.Name == "#text")
                                    {
                                        errormsg += HttpUtility.HtmlDecode(t.InnerText).Trim();
                                    }
                                }

                                this.Dispatcher.BeginInvoke(() =>
                                {
                                    working = false;
                                    pp.Visibility = System.Windows.Visibility.Collapsed;
                                    pp.IsIndeterminate = false;
                                    if (sender != null)
                                    {
                                        (sender as ApplicationBarIconButton).IsEnabled = true;
                                    }
                                    MessageBox.Show(errormsg, Res.LocalString.errorserver, MessageBoxButton.OK);
                                });
                            }
                            else
                            {
                                Communication.isLogin = true;
                                Communication.needRefresh = true;
                                Interpreter.LoginTokens(stuff);
                                this.Dispatcher.BeginInvoke(() =>
                                {
                                    if (cbAuto.IsChecked == true)
                                    {
                                        IsolatedStorageSettings.ApplicationSettings["id"] = id;
                                        IsolatedStorageSettings.ApplicationSettings["pass"] = pass;
                                        IsolatedStorageSettings.ApplicationSettings.Save();
                                    }
                                    /*else
                                    {
                                        if (IsolatedStorageSettings.ApplicationSettings.Contains("id"))
                                        {
                                            IsolatedStorageSettings.ApplicationSettings.Remove("id");
                                        }
                                        if (IsolatedStorageSettings.ApplicationSettings.Contains("pass"))
                                        {
                                            IsolatedStorageSettings.ApplicationSettings.Remove("pass");
                                        }
                                    }*/
                                    pp.Visibility = System.Windows.Visibility.Collapsed;
                                    pp.IsIndeterminate = false;
                                    working = false;
                                    if (NavigationService.CanGoBack)
                                    {
                                        NavigationService.GoBack();
                                    }

                                });
                            }
                        }
                        else
                        {
                            this.Dispatcher.BeginInvoke(() =>
                            {
                                (sender as ApplicationBarIconButton).IsEnabled = true;
                                pp.Visibility = System.Windows.Visibility.Collapsed;
                                MessageBox.Show(Res.LocalString.errorbang + Environment.NewLine +
                                (stuff != "UnknownError" ? stuff : Res.LocalString.noconnection),
                                Res.LocalString.errorpixifeed, MessageBoxButton.OK);
                            });
                        }
                    });
            }
        }

        public static void LoginSilent(string id, string pass, DependencyObject m, Action after)
        {
            if (id.Length > 0 && pass.Length > 0)
            {
                SystemTray.SetProgressIndicator(m, new ProgressIndicator()
                {
                    IsIndeterminate = true,
                    IsVisible = true,
                    Text = Res.LocalString.retlogin
                });
                Communication.CommunicateLogin("http://www.pixiv.net/login.php", "mode=login&pixiv_id="
                    + id + "&pass="
                    + pass,
                    (re, stuff) =>
                    {
                        if (re >= 0)
                        {
                            HtmlDocument hd = new HtmlDocument();
                            hd.LoadHtml(stuff);

                            HtmlNode hn = hd.DocumentNode.SelectSingleNode("//span[@class=\"error\"]");
                            if(hn == null)
                            {
                                hn = hd.DocumentNode.SelectSingleNode("//div[@class=\"errorArea\"]");
                            }
                            if (hn != null)
                            {
                                IsolatedStorageSettings.ApplicationSettings.Remove("id");
                                IsolatedStorageSettings.ApplicationSettings.Remove("pass");
                                IsolatedStorageSettings.ApplicationSettings.Save();
                            }
                            else
                            {
                                Communication.isLogin = true;
                                Interpreter.LoginTokens(stuff);
                            }
                        }
                        else
                        {
                        }
                        after();
                    });
            }
        }

        private void abibCancel_Click(object sender, EventArgs e)
        {
            if(hwr != null && working)
            {
                hwr.Abort();
                working = false;
            }
            else if (NavigationService.CanGoBack)
            {
                pp.Visibility = System.Windows.Visibility.Collapsed;
                NavigationService.GoBack();
            }
        }
    }
}