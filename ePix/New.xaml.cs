﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Collections.ObjectModel;
using ePix.Model;
using Microsoft.Phone.Shell;
using System.Collections;
using System.Windows.Controls.Primitives;

namespace ePix
{
    public partial class New : PhoneApplicationPage
    {
        public New()
        {
            InitializeComponent();

            this.Language = System.Windows.Markup.XmlLanguage.GetLanguage(System.Globalization.CultureInfo.CurrentUICulture.Name);

            if (ApplicationBar.Buttons.Count >= 1)
            {
                (ApplicationBar.Buttons[0] as ApplicationBarIconButton).Text = Res.LocalString.refresh;
            }
        }

        ObservableCollection<PictureModel> ocPic = new ObservableCollection<PictureModel>();
        ObservableCollection<PictureModel> ocFav = new ObservableCollection<PictureModel>();
        int nextpg = 1;
        private void btnBack_Click(object sender, EventArgs e)
        {
            if (NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if ((Application.Current.Resources["PhoneDarkThemeVisibility"] as Visibility?) == System.Windows.Visibility.Visible ^ Communication.isInvert)
            {
                LayoutRoot.Background = new SolidColorBrush(Color.FromArgb(255, 37, 39, 41));
                pivLat.Background = LayoutRoot.Background;
                SystemTray.BackgroundColor = Color.FromArgb(255, 37, 39, 41);
                SystemTray.ForegroundColor = Colors.White;
            }

            if (ocPic.Count == 0)
            {
                GetLatestItems(1);
                lbLatest.ItemsSource = ocPic;
            }
        }

        const string baseuri = "http://www.pixiv.net/new_illust.php";
        const string favuri = "http://www.pixiv.net/bookmark_new_illust.php";
        const string mpuri = "http://www.pixiv.net/mypixiv_new_illust.php";
        bool working = false;
        private int flast;
        private void GetLatestItems(int page)
        {
            if (working)
            {
                return;
            }
            working = true;
            SystemTray.SetProgressIndicator(this, new ProgressIndicator()
            {
                IsIndeterminate = true,
                IsVisible = true,
                Text = Res.LocalString.retlatest
            });

            string targeturi = baseuri;
            switch (pivLat.SelectedIndex)
            {
                case 1: targeturi = favuri; break;
                case 2: targeturi = mpuri; break;
            }
            if (page > 1)
            {
                targeturi += "?p=" + page.ToString();
            }

            Communication.Communicate(targeturi, (suc, res) =>
            {
                if (suc >= 0)
                {
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        if (Interpreter.SearchResult(ref ocPic, (page == 1), res))
                        {
                            if (page == 1 && ocPic.Count > 0)
                            {
                                lbLatest.ScrollTo(ocPic[0]);
                            }

                            nextpg = page + 1;
                        }
                    });
                }
                else
                {
                    try
                    {
                        this.Dispatcher.BeginInvoke(() =>
                        {
                            MessageBox.Show(Res.LocalString.errorbang + Environment.NewLine +
                                (res != "UnknownError" ? res : Res.LocalString.noconnection),
                                Res.LocalString.errorpixifeed, MessageBoxButton.OK);
                            if (NavigationService.CanGoBack)
                            {
                                NavigationService.GoBack();
                            }
                        });
                    }
                    catch { }
                }

                this.Dispatcher.BeginInvoke(() =>
                {
                    SystemTray.SetProgressIndicator(this, null);
                    working = false;
                });
            });
        }

        /*private void GetFavoriteItems(int page)
        {
            if (working)
            {
                return;
            }
            working = true;
            SystemTray.SetProgressIndicator(this, new ProgressIndicator()
            {
                IsIndeterminate = true,
                IsVisible = true,
                Text = Res.LocalString.retlatest
            });

            string targeturi = favuri;
            if (page > 1)
            {
                targeturi += "?p=" + page.ToString();
            }

            Communication.Communicate(targeturi, (suc, res) =>
            {
                if (suc >= 0)
                {
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        if (Interpreter.SearchResult(ref ocFav, (page == 1), res))
                        {
                            nextpg2 = page + 1;
                        }
                    });
                }
                else
                {
                    try
                    {
                        this.Dispatcher.BeginInvoke(() =>
                        {
                            MessageBox.Show(Res.LocalString.errorbang + Environment.NewLine +
                                (res != "UnknownError" ? res : Res.LocalString.noconnection),
                                Res.LocalString.errorpixifeed, MessageBoxButton.OK);
                            if (NavigationService.CanGoBack)
                            {
                                NavigationService.GoBack();
                            }
                        });
                    }
                    catch { }
                }

                this.Dispatcher.BeginInvoke(() =>
                {
                    SystemTray.SetProgressIndicator(this, null);
                    working = false;
                });
            });
        }*/

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            nextpg = 1;
            GetLatestItems(1);
            lbLatest.ItemsSource = ocPic;
        }

        private void pivPeople_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            nextpg = 0;
            flast = 0;
            ocPic.Clear();
            if (pivLat.SelectedIndex >= 0)
            {
                PivotItem li = e.RemovedItems[0] as PivotItem;
                PivotItem pi = e.AddedItems[0] as PivotItem;
                if (li != null)
                {
                    Grid gpi = li.Content as Grid;

                    if (gpi != null)
                    {
                        if (!gpi.Name.StartsWith("gdList"))
                        {
                            gpi = gpi.Children.FirstOrDefault((obj) => { return (obj as FrameworkElement).Name.StartsWith("gdList"); }) as Grid;
                        }
                        if (gpi != null && gpi.Children.Count > 0)
                        {
                            gpi.Children.Clear();
                        }
                        gpi.InvalidateArrange();
                        gpi.InvalidateMeasure();
                        gpi.OnApplyTemplate();

                        if (pi != null)
                        {
                            gpi = pi.Content as Grid;
                            if (gpi != null)
                            {
                                if (!gpi.Name.StartsWith("gdList"))
                                {
                                    gpi = gpi.Children.FirstOrDefault((obj) => { return (obj as FrameworkElement).Name.StartsWith("gdList"); }) as Grid;
                                }
                                if (gpi != null)
                                {
                                    if (gpi.Children.Count > 0)
                                    {
                                        gpi.Children.Clear();
                                    }
                                    gpi.Children.Add(lbLatest);
                                    gpi.InvalidateArrange();
                                    gpi.InvalidateMeasure();
                                    gpi.OnApplyTemplate();
                                }
                            }
                        }
                    }
                }
            }

            GetLatestItems(1);
        }

        private void ViewportControl_ViewportChanged(object sender, ViewportChangedEventArgs e)
        {
            ViewportControl vc = sender as ViewportControl;
            //lastview = vc.Viewport;
            if (ocPic != null && ocPic.Count > 0 && vc.Viewport.Bottom >= vc.Bounds.Bottom - 140 && flast != nextpg)
            {
                //int ipg = pivLat.SelectedIndex;
                try
                {
                    flast = nextpg;
                    GetLatestItems(nextpg);
                }
                catch { }
            }
        }
    }
}