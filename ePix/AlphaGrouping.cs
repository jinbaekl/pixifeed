﻿using System.Collections.Generic;
using System.Globalization;
using Microsoft.Phone.Globalization;

namespace LLSSample
{
    public class AlphaGrouping<T> : List<T>
    {
        /// <summary>
        /// The delegate that is used to get the key information.
        /// </summary>
        /// <param name="item">An object of type T</param>
        /// <returns>The key value to use for this object</returns>
        public delegate string GetKeyDelegate(T item);

        /// <summary>
        /// The Key of this group.
        /// </summary>
        public string Key { get; private set; }

        /// <summary>
        /// Public constructor.
        /// </summary>
        /// <param name="key">The key for this group.</param>
        public AlphaGrouping(string key)
        {
            Key = key;
        }

        /// <summary>
        /// Create a list of AlphaGroup<T> with keys set by a SortedLocaleGrouping.
        /// </summary>
        /// <param name="slg">The </param>
        /// <returns>Theitems source for a LongListSelector</returns>
        private static List<AlphaGrouping<T>> CreateGroups(SortedLocaleGrouping slg)
        {
            List<AlphaGrouping<T>> list = new List<AlphaGrouping<T>>();

            foreach (string key in slg.GroupDisplayNames)
            {
                list.Add(new AlphaGrouping<T>(key));
            }

            return list;
        }

        public override bool Equals(object obj)
        {
            if ((obj as AlphaGrouping<T>) != null)
            {
                if ((obj as AlphaGrouping<T>).Key == Key)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Create a list of AlphaGroup<T> with keys set by a SortedLocaleGrouping.
        /// </summary>
        /// <param name="items">The items to place in the groups.</param>
        /// <param name="ci">The CultureInfo to group and sort by.</param>
        /// <param name="getKey">A delegate to get the key from an item.</param>
        /// <param name="sort">Will sort the data if true.</param>
        /// <returns>An items source for a LongListSelector</returns>
        public static List<AlphaGrouping<T>> CreateGroups(IEnumerable<T> items, CultureInfo ci, GetKeyDelegate getKey, bool sort)
        {
            SortedLocaleGrouping slg = new SortedLocaleGrouping(ci);
            SortedLocaleGrouping slg2 = new SortedLocaleGrouping(new CultureInfo("ja-JP"));
            List<AlphaGrouping<T>> list = CreateGroups(slg);

            if (slg.CultureInfo != new CultureInfo("ja-JP"))
            {
                List<AlphaGrouping<T>> list2 = CreateGroups(slg2);
                foreach (AlphaGrouping<T> m in list2)
                {
                    if (!list.Contains(m))
                    {
                        list.Add(m);
                    }
                }
            }

            foreach (T item in items)
            {
                int index = 0;
                //if (slg.SupportsPhonetics)
                //{
                    //check if your database has yomi string for item
                    //if it does not, then do you want to generate Yomi or ask the user for this item.
                    //index = slg.GetGroupIndex(getKey(Yomiof(item)));
                //}
                //else
                //{
                index = slg.GetGroupIndex(getKey(item));

                if (index >= slg.GroupDisplayNames.Count - 1)
                {
                    index = slg.GroupDisplayNames.Count + slg2.GetGroupIndex(getKey(item));
                }
                //}
                if (index >= 0 && index < list.Count)
                {
                    list[index].Add(item);
                }
            }

            if (sort)
            {
                foreach (AlphaGrouping<T> group in list)
                {
                    group.Sort((c0, c1) => { return ci.CompareInfo.Compare(getKey(c0), getKey(c1)); });
                }
            }

            /*int i = 0;
            while (i < list.Count)
            {
                if (list[i].Count == 0)
                {
                    list.RemoveAt(i);
                }
                else
                {
                    i++;
                }
            }*/

            return list;
        }

    }
}