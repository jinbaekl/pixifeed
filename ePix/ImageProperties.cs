﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Phone;
using System.Diagnostics;
using Microsoft.Phone.Info;
using System.Reflection;
using Microsoft.Expression.Interactivity.Core;
using System.Windows.Media.Animation;
using Windows.Storage;

namespace ePix
{
    public static class ImageProperties
    {
        public static double scale = 1.0;

        #region SourceWithCustomReferer Property
        public static Dictionary<Uri, BitmapImage> imageCache = new Dictionary<Uri, BitmapImage>();

        public static readonly DependencyProperty SourceWithCustomRefererProperty =
            DependencyProperty.RegisterAttached(
                "SourceWithCustomReferer",
                typeof(Uri),
                typeof(ImageProperties),
                new PropertyMetadata(OnSourceWithCustomRefererChanged));

        private static void OnSourceWithCustomRefererChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            if (Communication.isAllowed < 1)
            {
                return;
            }

            var image = (Image)o;
            var olduri = (Uri)e.OldValue;
            var uri = (Uri)e.NewValue;

            if (DesignerProperties.IsInDesignTool)
            {
                // for the design surface we just load the image straight up
                image.Source = new BitmapImage(uri);
            }
            else
            {
                if (uri != olduri)
                {
                    image.Source = null;
                }
                if (uri == null)
                {
                    return;
                }

                if (uri.OriginalString == "http://fake.dd/r-18")
                {
                    /*StorageFolder local = Windows.Storage.ApplicationData.Current.I;
                    var file = await local.GetFileAsync("Images/r18_block140.png");*/
                    var st = File.OpenRead("Images/r18_block140.png");
                    //var st = (await file.OpenReadAsync()).AsStream();
                    var bmp = new BitmapImage();
                    bmp.SetSource(st);
                    image.Source = bmp;
                    image.Opacity = 1;
                    return;
                }

                if (imageCache.ContainsKey(uri))
                {
                    image.Source = imageCache[uri];
                    try
                    {
                        Border b = VisualTreeHelper.GetParent(VisualTreeHelper.GetParent(image)) as Border;
                        if (b == null)
                        {
                            image.Opacity = 1;
                        }
                        else
                        {
                            if (image.Name == "imgMain")
                            {
                                Storyboard st = b.Resources["storyMe"] as Storyboard;
                                if (st.GetCurrentState() != ClockState.Active)
                                {
                                    st.Begin();
                                }
                            }
                            else
                            {
                                image.Opacity = 1;
                                if (!double.IsNaN(image.Height) && image.Height < (imageCache[uri]).PixelHeight)
                                {
                                    image.Height = (imageCache[uri]).PixelHeight;
                                    image.Width = (imageCache[uri]).PixelWidth;
                                }
                            }
                        }
                    }
                    catch
                    {
                        image.Opacity = 1;
                    }
                    //image.Opacity = 1;
                    return;
                }
                if (olduri == uri && image.Opacity == 1)
                {
                    return;
                }

                /*if (Microsoft.Phone.Info.DeviceStatus.ApplicationMemoryUsageLimit <= DeviceStatus.ApplicationCurrentMemoryUsage + 52428800)
                {
                    Debug.WriteLine("No memory!!");
                    return;
                }*/
                
                HttpWebRequest request = HttpWebRequest.Create(uri) as HttpWebRequest;
                //request.Headers["Referer"] = "http://www.pixiv.net"; // or your custom referer string here
                request.CookieContainer = Communication.ccMain;
                request.Headers[HttpRequestHeader.CacheControl] = "max-age=0";
                request.Headers[HttpRequestHeader.AcceptEncoding] = "gzip";
                request.UserAgent = Communication.useragent;
                request.Headers[HttpRequestHeader.Referer] = "http://www.pixiv.net";
                request.Headers[HttpRequestHeader.AcceptLanguage] = CultureInfo.CurrentCulture.Name;
                //request.Headers[HttpRequestHeader.IfModifiedSince] = DateTime.UtcNow.ToString("r");
                request.BeginGetResponse((result) =>
                {
                    try
                    {
                        var response = request.EndGetResponse(result);
                        if (response.Headers[HttpRequestHeader.ContentEncoding] != null && response.Headers[HttpRequestHeader.ContentEncoding].Contains("gzip"))
                            response = new GZipWebResponse(response); //If gzipped response, uncompress
                        Stream imageStream = response.GetResponseStream();
                        bool isGIF = response.ResponseUri.LocalPath.EndsWith(".gif");
                        Deployment.Current.Dispatcher.BeginInvoke(() =>
                        {
                            try
                            {
                                BitmapImage bitmapImage = new BitmapImage();
                                bitmapImage.CreateOptions = BitmapCreateOptions.BackgroundCreation;
                                bitmapImage.DecodePixelWidth = 800;
                                /*if (isGIF)
                                {
                                    // Create an empty ExtendedImage instance.
                                    ExtendedImage exImage = new ExtendedImage();
                                    // Create an instance of GIF decoder.
                                    GifDecoder gifDecoder = new GifDecoder();

                                    // Interpret the stream as GIF and set the result
                                    // to the ExtendedImage instance.
                                    gifDecoder.Decode(exImage, imageStream);

                                    using (var stream = exImage.ToStream())
                                    {
                                        bitmapImage.SetSource(stream);
                                    }
                                }
                                else
                                {*/
                                    bitmapImage.SetSource(imageStream);
                                /*}*/
                                image.Source = bitmapImage;
                                while (Math.Min(90000000, Microsoft.Phone.Info.DeviceStatus.ApplicationMemoryUsageLimit) <= Microsoft.Phone.Info.DeviceStatus.ApplicationCurrentMemoryUsage + 40000000
                                    && imageCache.Count >= 1)
                                {
                                    Dictionary<Uri,BitmapImage>.KeyCollection.Enumerator em = imageCache.Keys.GetEnumerator();
                                    em.MoveNext();
                                    imageCache.Remove(em.Current);
                                    GC.Collect();
                                    return;
                                }
                                if (!imageCache.ContainsKey(uri))
                                {
                                    imageCache.Add(uri, bitmapImage);
                                }
                            }
                            catch(Exception ex)
                            {
                                Debug.WriteLine("Bitmap/Cache Error "+ex.GetType().ToString()+":"+uri);
                            }
                        });
                    }
                    catch (WebException)
                    {
                        // add error handling
                    }
                    catch (Exception)
                    {

                    }
                } , null);
            }
        }

        public static Uri GetSourceWithCustomReferer(Image image)
        {
            if (image == null)
            {
                throw new ArgumentNullException("Image");
            }
            return (Uri)image.GetValue(SourceWithCustomRefererProperty);
        }

        public static void SetSourceWithCustomReferer(Image image, Uri value)
        {
            if (image == null)
            {
                throw new ArgumentNullException("Image");
            }
            image.SetValue(SourceWithCustomRefererProperty, value);
        }
#endregion

        #region SmallSourceWCR Property
        public static readonly DependencyProperty SmallSourceWCRProperty =
            DependencyProperty.RegisterAttached(
                "SmallSourceWCR",
                typeof(Uri),
                typeof(ImageProperties),
                new PropertyMetadata(OnSmallSourceWCRChanged));

        async private static void OnSmallSourceWCRChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (Communication.isAllowed < 1)
            {
                return;
            }

            GC.Collect();

            var image = (Image)d;
            var olduri = (Uri)e.OldValue;
            var uri = (Uri)e.NewValue;
            scale = ScaleFactor.Scale;

            if (DesignerProperties.IsInDesignTool)
            {
                // for the design surface we just load the image straight up
                image.Source = new BitmapImage(uri);
            }
            else
            {
                if (olduri == uri && image.Opacity == 1)
                {
                    return;
                }

                image.Opacity = 0;
                DependencyObject fe = VisualTreeHelper.GetParent(image);
                if (fe != null)
                {
                    DependencyObject ff = VisualTreeHelper.GetParent(fe);
                    if (ff is ContentPresenter)
                    {
                        ExtendedVisualStateManager.GoToElementState(fe as FrameworkElement, "Normal", false);
                    }
                    else if (ff is Border)
                    {
                        ExtendedVisualStateManager.GoToElementState(ff as FrameworkElement, "Normal", false);
                        if (fe is Grid)
                        {
                            Grid gdon = fe as Grid;
                            gdon.Background = new SolidColorBrush(Color.FromArgb(255, 192, 192, 192));
                        }
                    }
                    else
                    {
                        image.Opacity = 1;
                    }
                }
                image.Source = null;
                if (uri == null)
                {
                    return;
                }

                if (uri.OriginalString == "http://fake.dd/r-18")
                {
                    /*StorageFolder local = Windows.Storage.ApplicationData.Current.I;
                    var file = await local.GetFileAsync("Images/r18_block140.png");*/
                    var st = File.OpenRead("Images/r18_block140.png");
                    //var st = (await file.OpenReadAsync()).AsStream();
                    var bmp = new BitmapImage();
                    bmp.SetSource(st);
                    image.Source = bmp;
                    image.Opacity = 1;
                    return;
                }

                if (imageCache.ContainsKey(uri))
                {
                    image.Source = imageCache[uri];
                    image.Opacity = 1;
                    return;
                }

                if (Microsoft.Phone.Info.DeviceStatus.ApplicationMemoryUsageLimit <= DeviceStatus.ApplicationCurrentMemoryUsage + 52428800)
                {
                    Debug.WriteLine("No memory!! - 1");
                    return;
                }

                HttpWebRequest request = HttpWebRequest.Create(uri) as HttpWebRequest;
                //request.Headers["Referer"] = "http://www.pixiv.net"; // or your custom referer string here
                request.CookieContainer = Communication.ccMain;
                request.UserAgent = Communication.useragent;
                request.Headers[HttpRequestHeader.AcceptEncoding] = "gzip";
                request.Headers[HttpRequestHeader.CacheControl] = "max-age=0";
                request.Headers[HttpRequestHeader.Referer] = "http://www.pixiv.net";
                request.Headers[HttpRequestHeader.AcceptLanguage] = CultureInfo.CurrentCulture.Name;
                //request.Headers[HttpRequestHeader.IfModifiedSince] = DateTime.UtcNow.ToString("r");
                request.BeginGetResponse((result) =>
                {
                    try
                    {
                        var response = request.EndGetResponse(result);
                        if (Microsoft.Phone.Info.DeviceStatus.ApplicationMemoryUsageLimit <= DeviceStatus.ApplicationCurrentMemoryUsage + 41943040)
                        {
                            Debug.WriteLine("No memory!! - 2");
                            return;
                        }

                        if (response.Headers[HttpRequestHeader.ContentEncoding] != null && response.Headers[HttpRequestHeader.ContentEncoding].Contains("gzip"))
                            response = new GZipWebResponse(response); //If gzipped response, uncompress
                        Stream imageStream = response.GetResponseStream();
                        bool isGIF = response.ResponseUri.LocalPath.EndsWith(".gif");
                        Deployment.Current.Dispatcher.BeginInvoke(() =>
                        {
                            try
                            {
                                BitmapImage bitmapImage = new BitmapImage();
                                bitmapImage.CreateOptions = BitmapCreateOptions.BackgroundCreation;

                                /*if (isGIF)
                                {
                                    // Create an empty ExtendedImage instance.
                                    ExtendedImage exImage = new ExtendedImage();
                                    // Create an instance of GIF decoder.
                                    GifDecoder gifDecoder = new GifDecoder();

                                    // Interpret the stream as GIF and set the result
                                    // to the ExtendedImage instance.
                                    gifDecoder.Decode(exImage, imageStream);

                                    using (var stream = exImage.ToStream())
                                    {
                                        bitmapImage.SetSource(stream);                                        
                                    }
                                }
                                else
                                {*/
                                    WriteableBitmap wb = 
                                    PictureDecoder.DecodeJpeg(imageStream, Convert.ToInt32(130 * scale), Convert.ToInt32(170 * scale));
                                    using(MemoryStream ms = new MemoryStream())
                                    {
                                    ms.Position = 0;
                                    wb.SaveJpeg(ms, wb.PixelWidth, wb.PixelHeight, 0, 85);
                                    ms.Position = 0;
                                    bitmapImage.SetSource(ms);
                                    }
                                /*}*/
                                
                                image.Source = bitmapImage;
                                while(Microsoft.Phone.Info.DeviceStatus.ApplicationMemoryUsageLimit <= Microsoft.Phone.Info.DeviceStatus.ApplicationCurrentMemoryUsage + 40000000
                                    && imageCache.Count >= 1)
                                //while (180*1024*1024 <= Microsoft.Phone.Info.DeviceStatus.ApplicationCurrentMemoryUsage + 10000000
                                //    && imageCache.Count >= 1)
                                {
                                    Debug.WriteLine("Less memory...");
                                    Dictionary<Uri, BitmapImage>.KeyCollection.Enumerator em = imageCache.Keys.GetEnumerator();
                                    em.MoveNext();
                                    imageCache.Remove(em.Current);
                                    GC.Collect();
                                    return;
                                }
                                if (!imageCache.ContainsKey(uri) && DeviceStatus.ApplicationMemoryUsageLimit - DeviceStatus.ApplicationCurrentMemoryUsage > 20971520)
                                {
                                    imageCache.Add(uri, bitmapImage);
                                }
                            }
                            catch (Exception ex)
                            {
                                Debug.WriteLine("Bitmap/Cache Error " + ex.GetType().ToString() + ":" + uri);
                            }
                        });
                    }
                    catch (WebException)
                    {
                        // add error handling
                    }
                    catch (Exception)
                    {

                    }
                }, null);
            }
        }

        public static Uri GetSmallSourceWCR(Image image)
        {
            if (image == null)
            {
                throw new ArgumentNullException("Image");
            }
            return (Uri)image.GetValue(SourceWithCustomRefererProperty);
        }

        public static void SetSmallSourceWCR(Image image, Uri value)
        {
            if (image == null)
            {
                throw new ArgumentNullException("Image");
            }
            image.SetValue(SourceWithCustomRefererProperty, value);
        }

        #endregion
    }
}
