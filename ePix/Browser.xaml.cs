﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Text;
using System.IO.IsolatedStorage;

namespace ePix
{
    public partial class Browser : PhoneApplicationPage
    {
        bool nav = false;
        public Browser()
        {
            InitializeComponent();
        }

        string target = "";
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (e.NavigationMode == NavigationMode.New || !nav)
            {
                if (NavigationContext.QueryString.ContainsKey("uri"))
                {
                    target = NavigationContext.QueryString["uri"];
                    StringBuilder sb = new StringBuilder();
                    if(!string.IsNullOrEmpty(Communication.lasturl))
                    {
                        sb.AppendLine("Referer: "+Communication.lasturl);
                    }
                    if(Communication.ccMain.Count > 0)
                    {
                        var cc = Communication.ccMain.GetCookies(new Uri("http://www.pixiv.net"));
                        if(cc.Count > 0)
                        {
                            sb.Append("Cookie: ");
                            foreach(Cookie c in cc)
                            {
                                if(sb[sb.Length - 1] == ' ')
                                {
                                    sb.Append(string.Format("{0}={1}", c.Name, c.Value));
                                }
                                else
                                {
                                    sb.Append(string.Format("; {0}={1}", c.Name, c.Value));
                                }
                            }
                            sb.AppendLine();
                        }
                    }
                    wbMain.Navigate(new Uri(target, UriKind.Absolute), null, sb.ToString());
                }
            }
        }

        private void wbMain_Navigating(object sender, NavigatingEventArgs e)
        {
            //http://www.pixiv.net/member_illust.php?mode=medium&illust_id=32458212
            if (e.Uri.Host.IndexOf("pixiv") < 0 || e.Uri.Host.IndexOf("ad") >= 0)
            {
                e.Cancel = true;
                return;
            }

            string m = e.Uri.AbsoluteUri;
            int iSid = m.IndexOf("illust_id=");
            if (iSid > 0)
            {
                iSid += 10;
                int iEid = m.IndexOf('&', iSid);
                if (iEid < 0)
                {
                    iEid = m.Length;
                }
                if (iEid > iSid)
                {
                    string num = m.Substring(iSid, iEid - iSid);
                    e.Cancel = true;
                    NavigationService.Navigate(new Uri(string.Format("/ViewDetail.xaml?id={0}", num), UriKind.Relative));
                    return;
                }
            }

            int iSSearch = m.IndexOf("search.php?");
            if (iSSearch > 0)
            {
                string n = e.Uri.Query;
                n = n.Replace("s_mode=", "m=");
                n = n.Replace("word=", "q=");
                n += "&tag=1";
                e.Cancel = true;
                NavigationService.Navigate(new Uri("/SearchResult.xaml" + n, UriKind.Relative));
                return;
            }
            tbLoad.Text = HttpUtility.UrlDecode(m);
            pbLoad.IsIndeterminate = true;
            gLoad.Visibility = System.Windows.Visibility.Visible;
        }

        private void wbMain_LoadCompleted(object sender, NavigationEventArgs e)
        {
            pbLoad.IsIndeterminate = false;
            gLoad.Visibility = System.Windows.Visibility.Collapsed;
            wbMain.Visibility = System.Windows.Visibility.Visible;
        }

        private void wbMain_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            pbLoad.IsIndeterminate = false;
            gLoad.Visibility = System.Windows.Visibility.Collapsed;
            wbMain.Visibility = System.Windows.Visibility.Visible;
            e.Handled = true;
            MessageBox.Show(Res.LocalString.errorpcon, Res.LocalString.errorserver, MessageBoxButton.OK);
            /*if (wbMain.CanGoBack)
            {
                wbMain.GoBack();
            }
            else if (NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }*/
        }

        private void PhoneApplicationPage_BackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (wbMain.CanGoBack)
            {
                wbMain.GoBack();
                e.Cancel = true;
            }
        }

        private void gLoad_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            pbLoad.IsIndeterminate = false;
            gLoad.Visibility = System.Windows.Visibility.Collapsed;
            wbMain.Visibility = System.Windows.Visibility.Visible;
        }

        private void wbMain_Navigated(object sender, NavigationEventArgs e)
        {
            if (IsolatedStorageSettings.ApplicationSettings.Contains("id") && IsolatedStorageSettings.ApplicationSettings.Contains("pass")
                && IsolatedStorageSettings.ApplicationSettings["id"] != null && IsolatedStorageSettings.ApplicationSettings["pass"] != null)
            {
                string id = HttpUtility.UrlDecode(IsolatedStorageSettings.ApplicationSettings["id"] as string);
                string pass = HttpUtility.UrlDecode(IsolatedStorageSettings.ApplicationSettings["pass"] as string);
                try
                {
                    wbMain.IsScriptEnabled = true;
                    System.Threading.ThreadPool.QueueUserWorkItem(obj =>
                    {
                        System.Threading.Thread.Sleep(700);

                        Dispatcher.BeginInvoke(() =>
                        {
                            try
                            {
                                wbMain.InvokeScript("eval", "document.getElementById('login_pixiv_id').value = '" + id + "';");
                                wbMain.InvokeScript("eval", "document.getElementById('login_password').value = '" + pass + "';");
                                wbMain.InvokeScript("eval", "document.getElementById('login_pixiv_id').parentNode.parentNode.submit();");
                            }
                            catch { }
                        });
                    });
                }
                catch
                { }
                nav = true;
            }
        }

    }
}