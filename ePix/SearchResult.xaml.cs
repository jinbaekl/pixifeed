﻿using ePix.Model;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO.IsolatedStorage;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Linq;
using System.Windows.Media.Imaging;
using System.IO;
using System.Diagnostics;

namespace ePix
{
    public partial class SearchResult : PhoneApplicationPage
    {
        public SearchResult()
        {
            InitializeComponent();

            this.Language = System.Windows.Markup.XmlLanguage.GetLanguage(System.Globalization.CultureInfo.CurrentUICulture.Name);

            if (ApplicationBar.Buttons.Count >= 3)
            {
                (ApplicationBar.Buttons[0] as ApplicationBarIconButton).Text = Res.LocalString.refresh;
                (ApplicationBar.Buttons[2] as ApplicationBarIconButton).Text = Res.LocalString.pin;
            }

            if (ApplicationBar.MenuItems.Count >= 3)
            {
                (ApplicationBar.MenuItems[0] as ApplicationBarMenuItem).Text = Res.LocalString.orderpopularity;
                (ApplicationBar.MenuItems[1] as ApplicationBarMenuItem).Text = Res.LocalString.orderlatest;
                (ApplicationBar.MenuItems[2] as ApplicationBarMenuItem).Text = Res.LocalString.orderoldest;
            }
        }

        string method = "s_tag";
        string query = string.Empty;
        string order = "date_d";
        int scd = -1;
        //int[] lastpg = new int[] { 1, 1, 1 };
        int lastpg = 1;
        bool tag = false;
        bool working = false;

        ObservableCollection<PictureModel> ocAll = new ObservableCollection<PictureModel>();
        //ObservableCollection<PictureModel> ocIll = new ObservableCollection<PictureModel>();
        //ObservableCollection<PictureModel> ocMan = new ObservableCollection<PictureModel>();

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if ((Application.Current.Resources["PhoneDarkThemeVisibility"] as Visibility?) == System.Windows.Visibility.Visible ^ Communication.isInvert)
            {
                pvResult.Background = new SolidColorBrush(Color.FromArgb(255, 37, 39, 41));
                SystemTray.BackgroundColor = Color.FromArgb(255, 37, 39, 41);
                SystemTray.ForegroundColor = Colors.White;
            }

            if (ocAll != null && ocAll.Count == 0)
            {
                if (NavigationContext.QueryString.ContainsKey("tag"))
                {
                    if (NavigationContext.QueryString["tag"] == "1")
                    {
                        tag = true;
                    }
                }
                if (NavigationContext.QueryString.ContainsKey("m"))
                {
                    method = NavigationContext.QueryString["m"];
                }
                if (NavigationContext.QueryString.ContainsKey("q"))
                {
                    query = NavigationContext.QueryString["q"];
                }

                if(query.ToUpper().Contains("R-18") && !Communication.Te)
                {
                    tbNothing.Text = Res.LocalString.r18_warning;
                    pvResult.IsEnabled = false;
                    this.ApplicationBar.IsVisible = false;
                    return;
                }

                pvResult.Title = string.Format(tag ? Res.LocalString.resultexact : Res.LocalString.resultinclude,
                    query);
                if (lbAll.ItemsSource == null)
                {
                    lbAll.ItemsSource = ocAll;

                    if (IsolatedStorageSettings.ApplicationSettings.Contains("scd") && IsolatedStorageSettings.ApplicationSettings.Contains("order"))
                    {
                        scd = (int)IsolatedStorageSettings.ApplicationSettings["scd"];
                        order = IsolatedStorageSettings.ApplicationSettings["order"] as string;
                    }

                    SearchStart(0, 1, false);
                }
            }
        }

        private void SearchStart(int filter, int pg, bool noback)
        {
            if (working)
            {
                return;
            }
            working = true;

            if (query.ToUpper().Contains("R-18") && !Communication.Te)
            {
                tbNothing.Text = Res.LocalString.r18_warning;
                pvResult.IsEnabled = false;
                this.ApplicationBar.IsVisible = false;
                return;
            }

            tbNothing.Visibility = System.Windows.Visibility.Collapsed;
            SystemTray.SetProgressIndicator(this, new ProgressIndicator()
            {
                IsVisible = true,
                IsIndeterminate = true,
                Text = Res.LocalString.retimage
            });

            string exuri = "";
            if (tag)
            {
                exuri = string.Format("http://www.pixiv.net/search.php?word={0}&s_mode=s_tag_full", HttpUtility.UrlEncode(query));
            }
            else
            {
                exuri = string.Format("http://www.pixiv.net/search.php?s_mode={0}&word={1}", method, HttpUtility.UrlEncode(query));
            }
            switch (filter)
            {
                case 1: exuri += "&type=illust"; break;
                case 2: exuri += "&type=manga"; break;
                case 3: exuri += "&type=ugoira"; break;
            }

            if(pg == 1)
            {
                flast = 0;
                lastpg = 1;
            }

            if (pg > 1)
            {
                exuri += "&p=" + pg.ToString();
            }
            if(order != "date_d")
            {
                exuri += "&order=" + order;
            }

            switch(order)
            {
                case "popular_d": tbFilter.Text = Res.LocalString.orderpopularity; break;
                case "date_d": tbFilter.Text = Res.LocalString.orderlatest; break;
                case "date": tbFilter.Text = Res.LocalString.orderoldest; break;
            }

            if(scd >= 0)
            {
                var dtFirst = DateTime.Now.AddDays(-scd);

                string toscd = dtFirst.ToString("yyyy-MM-dd");
                exuri += "&scd=" + toscd;

                string slabel = Res.LocalString.list_all;
                switch(scd)
                {
                    case 1: slabel = Res.LocalString.list_day; break;
                    case 7: slabel = Res.LocalString.list_week; break;
                    case 30: slabel = Res.LocalString.list_month; break;
                }

                tbFilter.Text += string.Format(" ({0})", slabel);
            }
            IsolatedStorageSettings.ApplicationSettings["scd"] = scd;
            IsolatedStorageSettings.ApplicationSettings["filter"] = filter;
            IsolatedStorageSettings.ApplicationSettings["order"] = order;
            IsolatedStorageSettings.ApplicationSettings.Save();
            Debug.WriteLine(exuri);
            Communication.Communicate(exuri, (suc, result) =>
            {
                if (suc >= 0)
                {
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        Communication.isLogin = (result.IndexOf("user.loggedIn = true") > 0);
                        if (!Communication.isLogin && IsolatedStorageSettings.ApplicationSettings.Contains("id") && IsolatedStorageSettings.ApplicationSettings.Contains("pass"))
                        {
                            string mid = HttpUtility.UrlEncode(IsolatedStorageSettings.ApplicationSettings["id"] as string);
                            string pass = HttpUtility.UrlEncode(IsolatedStorageSettings.ApplicationSettings["pass"] as string);
                            Login.LoginSilent(mid, pass, this, () =>
                            {
                                this.Dispatcher.BeginInvoke(() =>
                                {
                                    Communication.Communicate(exuri, (com2, result2) =>
                                    {
                                        this.Dispatcher.BeginInvoke(() =>
                                        {
                                            ReadyResult(pg, result2);
                                        });
                                    });
                                });
                            });
                        }
                        else
                        {
                            ReadyResult(pg, result);
                        }
                    });
                }
                else
                {
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        try
                        {
                            flast = lastpg - 1;
                            MessageBox.Show(Res.LocalString.errorbang + Environment.NewLine +
                                (result != "UnknownError" ? result : Res.LocalString.noconnection),
                                Res.LocalString.errorpixifeed, MessageBoxButton.OK);
                            if (NavigationService.CanGoBack && !noback)
                            {
                                NavigationService.GoBack();
                            }
                        }
                        catch { }
                    });
                }
                this.Dispatcher.BeginInvoke(() =>
                    {
                        SystemTray.SetProgressIndicator(this, null);
                    });
                working = false;
            });
        }

        private void ReadyResult(int pg, string result)
        {
            if (ApplicationBar.MenuItems.Count >= 1)
            {
                (ApplicationBar.MenuItems[0] as ApplicationBarMenuItem).IsEnabled = result.IndexOf("pixiv.user.premium = true") > 0;
            }

            string title = Interpreter.SearchColumnTitle(result);
            if(!string.IsNullOrEmpty(title))
            {
                pvResult.Title = string.Format(tag ? Res.LocalString.resultexact : Res.LocalString.resultinclude,
                    title);
            }

            if (Interpreter.SearchResult(ref ocAll, pg == 1, result))
            {
                lastpg = pg + 1;
                dicbtn.Tag = Interpreter.isDicAvailable(result);
                dicbtn.Visibility = dicbtn.Tag == null ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
            }
            else
            {
                lastpg = pg;
                flast = lastpg - 1;
                if (pg == 1)
                {
                    tbNothing.Visibility = System.Windows.Visibility.Visible;
                    ocAll.Clear();
                }
            }
            if (pg == 1)
            {
                if (ocAll.Count > 0 && lbAll.ItemsSource == ocAll)
                {
                    lbAll.ScrollTo(ocAll[0]);
                }
                else
                {
                    lbAll.ItemsSource = ocAll;
                }
            }
        }

        private void pvResult_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            /*if (ocAll.Count > 0)
            {
                lbAll.ScrollTo(ocAll[0]);
                lbAll.InvalidateArrange();
                lbAll.InvalidateMeasure();
                lbAll.OnApplyTemplate();
                lastview = new Rect(0, 0, lastview.Width, lastview.Height);
            }*/
            ocAll.Clear();
            GC.Collect();
            lastpg = 0;
            flast = 0;
            if (pvResult.SelectedIndex >= 0)
            {
                PivotItem li = e.RemovedItems[0] as PivotItem;
                PivotItem pi = e.AddedItems[0] as PivotItem;
                if (li != null)
                {
                    Grid gpi = li.Content as Grid;

                    if (gpi != null)
                    {
                        if (!gpi.Name.StartsWith("gdList"))
                        {
                            gpi = gpi.Children.FirstOrDefault((obj) => { return (obj as FrameworkElement).Name.StartsWith("gdList"); }) as Grid;
                        }
                        if (gpi != null && gpi.Children.Count > 0)
                        {
                            gpi.Children.Clear();
                        }
                        gpi.InvalidateArrange();
                        gpi.InvalidateMeasure();
                        gpi.OnApplyTemplate();
                    }

                    if (pi != null)
                    {
                        gpi = pi.Content as Grid;
                        if (gpi != null)
                        {
                            if (!gpi.Name.StartsWith("gdList"))
                            {
                                gpi = gpi.Children.FirstOrDefault((obj) => { return (obj as FrameworkElement).Name.StartsWith("gdList"); }) as Grid;
                            }
                            if (gpi != null)
                            {
                                if (gpi.Children.Count > 0)
                                {
                                    gpi.Children.Clear();
                                }
                                gpi.Children.Add(lbAll);
                                gpi.InvalidateArrange();
                                gpi.InvalidateMeasure();
                                gpi.OnApplyTemplate();
                            }
                        }
                    }
                }

                bool shouldRef = (ocAll.Count == 0);
                if (shouldRef)
                {
                    SearchStart(pvResult.SelectedIndex, 1, true);
                }
                else
                {
                    tbNothing.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
        }

        private void FlyoutMethod()
        {
            ApplicationBar.IsVisible = false;
            lsMethod.Items.Clear();
            lsMethod.Items.Add(Res.LocalString.list_all);
            lsMethod.Items.Add(Res.LocalString.list_day);
            lsMethod.Items.Add(Res.LocalString.list_week);
            lsMethod.Items.Add(Res.LocalString.list_month);
            pvResult.IsHitTestVisible = false;
            gdMethod.Visibility = System.Windows.Visibility.Visible;            
        }

        private void CloseFlyout()
        {
            if(lsMethod.SelectedIndex < 0)
            {
                order = lorder;
            }

            gdMethod.Visibility = System.Windows.Visibility.Collapsed;
            pvResult.IsHitTestVisible = true;

            ApplicationBar.IsVisible = true;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            if (NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            if (pvResult.SelectedIndex >= 0)
            {
                SearchStart(pvResult.SelectedIndex, 1, true);
            }
        }

        string lorder = "";
        private void abmPop_Click(object sender, EventArgs e)
        {
            if (pvResult.SelectedIndex >= 0)
            {
                lorder = order;
                order = "popular_d";
                FlyoutMethod();
            }
        }

        private void dicbtn_Click(object sender, RoutedEventArgs e)
        {
            if (dicbtn.Tag != null)
            {
                string uri = (string)dicbtn.Tag;
                if (uri.StartsWith("http://"))
                {
                    NavigationService.Navigate(new Uri("/Browser.xaml?uri=" + HttpUtility.UrlEncode(uri), UriKind.Relative));
                    /*
                    WebBrowserTask wbt = new WebBrowserTask();
                    wbt.Uri = new Uri(uri, UriKind.Absolute);
                    wbt.Show();
                     */
                }
            }
        }

        Rect lastview = Rect.Empty;
        int flast = 0;
        DateTime last = DateTime.Now;
        private void ViewportControl_ViewportChanged(object sender, ViewportChangedEventArgs e)
        {
            ViewportControl vc = sender as ViewportControl;
            lastview = vc.Viewport;
            if (ocAll != null && ocAll.Count > 0 && vc.Viewport.Bottom >= vc.Bounds.Bottom - 140 && flast != lastpg)
            {
                int ipg = pvResult.SelectedIndex;
                try
                {
                    if ((DateTime.Now - last).TotalSeconds > 2)
                    {
                        flast = lastpg;
                        last = DateTime.Now;
                        SearchStart(ipg, lastpg, true);
                    }
                }
                catch { }
            }
        }

        private void btnFav_Click(object sender, EventArgs e)
        {

        }

        private void btnPin_Click(object sender, EventArgs e)
        {
            if (ocAll != null && ocAll.Count > 0)
            {
                string picurl = lbAll.SelectedItem != null ? (lbAll.SelectedItem as PictureModel).Pic : ocAll[0].Pic;

                if (!string.IsNullOrEmpty(picurl))
                {
                    WebClient wc = new WebClient();
                    wc.Headers[HttpRequestHeader.Referer] = Communication.lasturl;
                    wc.OpenReadCompleted += new OpenReadCompletedEventHandler(wc_OpenReadCompleted);
                    wc.OpenReadAsync(new Uri(picurl));
                }
            }
        }

        void wc_OpenReadCompleted(object sender, OpenReadCompletedEventArgs e)
        {
            try
            {
                if (e.Error != null)
                {
                    if (e.Error is WebException)
                    {
                        MessageBox.Show(Res.LocalString.errortileun + Environment.NewLine + Environment.NewLine
                    + Res.LocalString.noconnection, Res.LocalString.errortile, MessageBoxButton.OK);
                    }
                    else
                    {
                        MessageBox.Show(Res.LocalString.errortileun + Environment.NewLine + Environment.NewLine
                        + e.Error.GetType().ToString(), Res.LocalString.errortile, MessageBoxButton.OK);
                    }
                    return;
                }

                var file = IsolatedStorageFile.GetUserStoreForApplication();

                if (!file.DirectoryExists(@"Shared\ShellContent\Search"))
                {
                    file.CreateDirectory(@"Shared\ShellContent\Search");
                }

                string title = NavigationContext.QueryString.ContainsKey("tag") ? Res.LocalString.tag : Res.LocalString.keyword;

                BitmapImage bi = new BitmapImage();
                bi.SetSource(e.Result);
                WriteableBitmap wbs = CreateTileImage(bi, query, title, true);
                SaveTileImage(wbs, string.Format(@"Shared\ShellContent\Search\{0}_{1}_{2}_s.png",
                    query, title, method));
                WriteableBitmap wb = CreateTileImage(bi, query, title);
                SaveTileImage(wb, string.Format(@"Shared\ShellContent\Search\{0}_{1}_{2}.png",
                    query,title,method));

                file.Dispose();
                FlipTileData ftd = new FlipTileData()
                {
                    BackTitle = string.Empty,
                    BackContent = string.Empty,
                    Title = query,
                    SmallBackgroundImage = new Uri(string.Format(@"isostore:/Shared/ShellContent/Search/{0}_{1}_{2}_s.png",
                        query, title, method)),
                    BackgroundImage = new Uri(string.Format(@"isostore:/Shared/ShellContent/Search/{0}_{1}_{2}.png",
                    query, title, method))
                };

                string param = string.Format("/MainPage.xaml?search={0}|{1}|{2}"
                    , tag ? 1 : 0, method, query);

                ShellTile.Create(new Uri(param, UriKind.Relative), ftd, false);
            }
            catch (InvalidOperationException)
            {
                MessageBox.Show(Res.LocalString.errortiledup, Res.LocalString.errortile, MessageBoxButton.OK);
            }
            catch (Exception ex)
            {
                MessageBox.Show(Res.LocalString.errortileun + Environment.NewLine + Environment.NewLine
                    + ex.GetType().ToString(), Res.LocalString.errortile, MessageBoxButton.OK);
            }
        }

        private WriteableBitmap CreateTileImage(BitmapImage img, string text, string title, bool isSmall = false)
        {
            int maxsize = isSmall ? 159 : 336;
            var image = new Image { Source = img, Stretch = Stretch.UniformToFill, Width = maxsize, Height = maxsize };
            var icon = new Image { Source = new BitmapImage() { CreateOptions = BitmapCreateOptions.None }, Stretch = Stretch.Fill, Width = 62, Height = 62 };

            System.Windows.Resources.StreamResourceInfo sri = Application.GetResourceStream(new Uri("ApplicationIcon.png", UriKind.Relative));
            (icon.Source as BitmapImage).SetSource(sri.Stream);

            Canvas.SetLeft(image, 0);
            Canvas.SetTop(image, 0);

            TextBlock tb = new TextBlock()
            {
                Text = text,
                FontSize = 25
            };

            System.Windows.Shapes.Rectangle rec = new System.Windows.Shapes.Rectangle()
            {
                Width = maxsize,
                Height = tb.ActualHeight + 40,
                Fill = new SolidColorBrush(Color.FromArgb(127, 0, 0, 0)),
                Stroke = new SolidColorBrush(Colors.Transparent),
                StrokeThickness = 0
            };

            //b.Child = tb;
            rec.UpdateLayout();
            image.UpdateLayout();

            // Measure the actual size of the TextBlock, this is with
            // the characters full hight/width (includning char spaceing)
            var width = tb.ActualWidth;
            var height = tb.ActualHeight;

            // Place the text in the lower right corner
            Canvas.SetLeft(rec, 0);
            Canvas.SetTop(rec, maxsize - height - 40);

            Canvas.SetLeft(icon, 0);
            Canvas.SetTop(icon, maxsize - height - 40 - icon.ActualHeight);

            TextBlock tbp = new TextBlock()
            {
                Text = title,
                FontSize = 32,
                Foreground = new SolidColorBrush(Colors.White)
            };

            System.Windows.Shapes.Rectangle recs = new System.Windows.Shapes.Rectangle()
            {
                Width = tbp.ActualWidth + 20,
                Height = tbp.ActualHeight + 20,
                Fill = new SolidColorBrush(Color.FromArgb(127, 0, 0, 0)),
                Stroke = new SolidColorBrush(Colors.Transparent),
                StrokeThickness = 0
            };

            Canvas.SetLeft(tbp, icon.ActualWidth + 10);
            Canvas.SetTop(tbp, maxsize - height - 40 - tbp.ActualHeight - 10);
            Canvas.SetLeft(recs, icon.ActualWidth);
            Canvas.SetTop(recs, maxsize - height - 40 - tbp.ActualHeight - 20);

            // Create a canvas and place the image and text on it
            var canvas = new Canvas
            {
                Height = 176,
                Width = 176
            };

            // This is where the images is placed
            // based on the Canvas.SetLeft &amp; SetTop values
            canvas.Children.Add(image);
            if (!isSmall)
            {
                canvas.Children.Add(icon);
                canvas.Children.Add(recs);
                canvas.Children.Add(tbp);
                canvas.Children.Add(rec);
            }
            canvas.UpdateLayout();

            // Render all of it to the WritableBitmap
            var wbmp = new WriteableBitmap(maxsize, maxsize);

            wbmp.Render(canvas, null);
            wbmp.Invalidate();

            CompensateForRender(wbmp.Pixels);
            wbmp.Invalidate();

            return wbmp;
        }

        public void CompensateForRender(int[] bitmapPixels)
        {
            if (bitmapPixels.Length == 0) return;

            for (int i = 0; i > bitmapPixels.Length; i++)
            {
                uint pixel = unchecked((uint)bitmapPixels[i]);

                double a = (pixel >> 24) & 255;
                if ((a == 255) || (a == 0)) continue;

                double r = (pixel >> 16) & 255;
                double g = (pixel >> 8) & 255;
                double b = (pixel) & 255;

                double factor = 255 / a;
                uint newR = (uint)Math.Round(r * factor);
                uint newG = (uint)Math.Round(g * factor);
                uint newB = (uint)Math.Round(b * factor);

                // compose
                bitmapPixels[i] = unchecked((int)((pixel & 0xFF000000) | (newR << 16) | (newG << 8) | newB));
            }
        }

        private void SaveTileImage(WriteableBitmap wbmp, string path)
        {
            using (var store = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (store.FileExists(path))
                    store.DeleteFile(path);
                var stream = store.OpenFile(path, FileMode.OpenOrCreate);
                wbmp.WritePNG(stream);
                stream.Close();
            }
        }

        private void lsMethod_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(e.AddedItems.Count > 0)
            {
                switch(lsMethod.SelectedIndex)
                {
                    case 0:
                        scd = -1;
                        break;
                    case 1:
                        scd = 1;
                        break;
                    case 2:
                        scd = 7;
                        break;
                    case 3:
                        scd = 30;
                        break;
                }

                CloseFlyout();
                SearchStart(pvResult.SelectedIndex, 1, true);
            }
        }

        private void abmDN_Click(object sender, EventArgs e)
        {
            if (pvResult.SelectedIndex >= 0)
            {
                lorder = order;
                order = "date_d";
                scd = -1;
                SearchStart(pvResult.SelectedIndex, 1, true);
            }
        }

        private void abmDO_Click(object sender, EventArgs e)
        {
            if (pvResult.SelectedIndex >= 0)
            {
                lorder = order;
                order = "date";
                FlyoutMethod();
            }
        }

        private void gdMCancel_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            e.Handled = true;
            CloseFlyout();
        }

        private void PhoneApplicationPage_BackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if(gdMethod.Visibility == System.Windows.Visibility.Visible)
            {
                e.Cancel = true;
                CloseFlyout();
            }
        }

        private void btnEye_Click(object sender, EventArgs e)
        {
            if (lbAll.GridCellSize == new Size(215, 260))
            {
                (ApplicationBar.Buttons[1] as ApplicationBarIconButton).IconUri = new Uri("/Images/outline.squares.png", UriKind.Relative);
                lbAll.GridCellSize = new Size(147, 175);
                lbAll.ItemTemplate = App.Current.Resources["PictureTemplate"] as DataTemplate;
            }
            else if (lbAll.GridCellSize == new Size(147, 175))
            {
                (ApplicationBar.Buttons[1] as ApplicationBarIconButton).IconUri = new Uri("/Images/outline.squares.png", UriKind.Relative);
                lbAll.GridCellSize = new Size(108, 108);
                lbAll.ItemTemplate = App.Current.Resources["PictureTemplateIcon"] as DataTemplate;
            }
            else
            {
                (ApplicationBar.Buttons[1] as ApplicationBarIconButton).IconUri = new Uri("/Images/outline.squares.3.png", UriKind.Relative);
                lbAll.GridCellSize = new Size(215, 260);
                lbAll.ItemTemplate = App.Current.Resources["PictureTemplate"] as DataTemplate;
            }
            lbAll.ApplyTemplate();
        }
    }
}