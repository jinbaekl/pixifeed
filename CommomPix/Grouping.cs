﻿using System.Collections.Generic;

namespace ePix
{
    public class Grouping<T> : List<T>
    {
        /// <summary>
        /// The delegate that is used to get the key information.
        /// </summary>
        /// <param name="item">An object of type T</param>
        /// <returns>The key value to use for this object</returns>
        public delegate string GetKeyDelegate(T item);

        /// <summary>
        /// The Key of this group.
        /// </summary>
        public string Key { get; private set; }

        /// <summary>
        /// Public constructor.
        /// </summary>
        /// <param name="key">The key for this group.</param>
        public Grouping(string key)
        {
            Key = key;
        }

        public override bool Equals(object obj)
        {
            if (obj != null)
            {
                if (obj is Grouping<T>)
                {
                    return (obj as Grouping<T>).Key == this.Key;
                }
            }
            return false;
        }


        public static List<Grouping<T>> CreateGroups(IEnumerable<T> items, GetKeyDelegate getKey)
        {
            List<Grouping<T>> list = new List<Grouping<T>>();

            foreach (T item in items)
            {
                string key = getKey(item);
                int i = list.IndexOf(new Grouping<T>(key));
                if (i >= 0 && i < list.Count)
                {
                    list[i].Add(item);
                }
                else
                {
                    list.Add(new Grouping<T>(key));
                    list[list.Count - 1].Add(item);
                }
            }

            return list;
        }
    }
}