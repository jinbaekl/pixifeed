﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Globalization;
using System.IO;
using System.Text;
using Microsoft.Phone.Marketplace;
using System.IO.IsolatedStorage;
using Microsoft.Phone.Tasks;

namespace ePix
{
    internal class GZipWebResponse : WebResponse
    {
        WebResponse response;
        internal GZipWebResponse(WebResponse resp)
        {
            response = resp;
        }
        public override System.IO.Stream GetResponseStream()
        {
            Stream st = response.GetResponseStream();
            //st.Position = 0;
            Stream sy = new Ionic.Zlib.GZipStream(st,Ionic.Zlib.CompressionMode.Decompress); //Uncompress
            //sy.Position = 0;
            return sy;
        }

        public override Uri ResponseUri
        {
            get
            {
                return response.ResponseUri;
            }
        }
    }

    public static class Communication
    {
        public static string useragent = string.Format("Pixifeed/{0} (Windows Phone OS 8.0; {1} {2}; CF {3})", "1.0", Environment.OSVersion.Platform, Environment.OSVersion.Version, Environment.Version);
        private const string accpt = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
        public static string lasturl = "http://www.pixiv.net";
        public static int isAllowed = 1;
        public static int _isFull = -1;
        public static bool isFull
        {
            get
            {
                if (_isFull < 0)
                {
                    /*if (System.Diagnostics.Debugger.IsAttached)
                    {
                        _isFull = 0;
                    }
                    else
                    {*/
                        LicenseInformation li = new LicenseInformation();
                        _isFull = li.IsTrial() ? 0 : 1;
                    /*}*/
                }
                return (_isFull == 1);
            }
        }

        public static int _isInvert = -1;
        public static bool isInvert
        {
            get
            {
                /*if (_isInvert < 0)
                {
                    if (IsolatedStorageSettings.ApplicationSettings.Contains("invert"))
                    {
                        bool invert = (bool)IsolatedStorageSettings.ApplicationSettings["invert"];
                        _isInvert = invert ? 1 : 0;
                    }
                }
                return (_isInvert == 1);*/
                return false;
            }
        }

        private static bool _refresh = false;
        public static bool needRefresh { get { return _refresh; } set { _refresh = value; } }
        private static bool _login = false;
        public static bool isLogin { get { return _login; } set { _refresh = true; _login = value; } }
        public static CookieContainer ccMain = new CookieContainer();
        public static NetworkCredential ncMain = new NetworkCredential();
        public static HttpWebRequest Communicate(string target, Action<int, string> after)
        {
            return Communicate(target, after, true, string.Empty);
        }

        public static HttpWebRequest Communicate(string target, Action<int, string> after, bool updateReferer)
        {
            return Communicate(target, after, updateReferer, string.Empty);
        }

        public static HttpWebRequest Communicate(string target, Action<int, string> after, bool updateReferer, string speReferer)
        {
            if (isAllowed < 1)
            {
                after(-1, Res.LocalString.restrictedconnection);
                return null;
            }
            try
            {
                HttpWebRequest hwr = WebRequest.CreateHttp(target);
                //hwr.AllowAutoRedirect
                hwr.Method = "GET";
                hwr.Accept = accpt;
                hwr.CookieContainer = ccMain;
                hwr.Headers[HttpRequestHeader.CacheControl] = "max-age=0";
                hwr.Headers[HttpRequestHeader.AcceptEncoding] = "gzip";
                hwr.UserAgent = useragent;
                hwr.Credentials = ncMain;
                if(speReferer.Length > 0)
                {
                    hwr.Headers[HttpRequestHeader.Referer] = speReferer;
                }
                else
                {
                    hwr.Headers[HttpRequestHeader.Referer] = lasturl;
                }
                hwr.Headers[HttpRequestHeader.AcceptLanguage] = CultureInfo.CurrentCulture.Name;
                hwr.Headers[HttpRequestHeader.IfModifiedSince] = DateTime.UtcNow.ToString("r");
                hwr.BeginGetResponse((a) =>
                {
                    try
                    {
                        if (a == null || hwr == null)
                        {
                            return;
                        }
                        if (a.IsCompleted)
                        {
                            var resp = hwr.EndGetResponse(a);
                            if (resp.Headers[HttpRequestHeader.ContentEncoding] != null && resp.Headers[HttpRequestHeader.ContentEncoding].Contains("gzip"))
                                resp = new GZipWebResponse(resp); //If gzipped response, uncompress
                            if (updateReferer)
                            {
                                lasturl = resp.ResponseUri.OriginalString;
                            }
                            using (var stream = resp.GetResponseStream())
                            {
                                using (StreamReader sr = new StreamReader(stream))
                                {
                                    string got = sr.ReadToEnd();
                                    after(0, got);
                                }
                            }
                        }
                    }
                    catch (WebException ex)
                    {
                        after(-1, ex.Status.ToString());
                    }
                    catch (Exception ex)
                    {
                        after(-2, ex.GetType().ToString());
                    }
                }, null);
                return hwr;
            }
            catch (WebException ex)
            {
                after(-1, ex.Status.ToString());
            }
            catch (Exception ex)
            {
                after(-2, ex.GetType().ToString());
            }
            return null;
        }

        public static HttpWebRequest CommunicatePOST(string target, string postdata, Action<int, string> after)
        {
            return CommunicatePOST(target, postdata, after, true, false);
        }

        public static HttpWebRequest CommunicatePOST(string target, string postdata, Action<int, string> after, bool updateReferer, bool json)
        {
            if (isAllowed < 1)
            {
                after(-1, Res.LocalString.restrictedconnection);
                return null;
            }
            try
            {
                HttpWebRequest hwr = WebRequest.CreateHttp(target);
                //hwr.AllowAutoRedirect
                hwr.Method = "POST";
                hwr.CookieContainer = ccMain;
                hwr.UserAgent = useragent;
                //hwr.Accept = accpt;
                hwr.Credentials = ncMain;
                hwr.Headers[HttpRequestHeader.Referer] = lasturl;
                hwr.Headers[HttpRequestHeader.AcceptEncoding] = "gzip";
                hwr.Headers["Origin"] = "http://www.pixiv.net";
                hwr.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
                if (json)
                {
                    hwr.Accept = "application/json, text/javascript, */*; q=0.01";
                    hwr.Headers["X-Requested-With"] = "XMLHttpRequest";
                }
                else
                {
                    hwr.Headers[HttpRequestHeader.CacheControl] = "max-age=0";
                    hwr.Accept = accpt;
                    hwr.Headers[HttpRequestHeader.IfModifiedSince] = DateTime.UtcNow.ToString("r");
                }
                hwr.Headers[HttpRequestHeader.AcceptLanguage] = CultureInfo.CurrentCulture.Name;
                hwr.BeginGetRequestStream((n) =>
                {
                    using (var reqstream = hwr.EndGetRequestStream(n))
                    {
                        using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(postdata)))
                        {
                            ms.Position = 0;
                            ms.CopyTo(reqstream);
                        }
                    }

                    hwr.BeginGetResponse((a) =>
                    {
                        try
                        {
                            if (a == null || hwr == null)
                            {
                                return;
                            }
                            if (a.IsCompleted)
                            {
                                var resp = hwr.EndGetResponse(a);
                                if (resp.Headers[HttpRequestHeader.ContentEncoding] != null && resp.Headers[HttpRequestHeader.ContentEncoding].Contains("gzip"))
                                    resp = new GZipWebResponse(resp); //If gzipped response, uncompress
                                if (updateReferer)
                                {
                                    lasturl = resp.ResponseUri.OriginalString;
                                }
                                using (var stream = resp.GetResponseStream())
                                {
                                    using (StreamReader sr = new StreamReader(stream))
                                    {
                                        string got = sr.ReadToEnd();
                                        after(0, got);
                                    }
                                }
                            }
                        }
                        catch (WebException ex)
                        {
                            after(-1, ex.Status.ToString());
                        }
                        catch (Exception ex)
                        {
                            after(-2, ex.GetType().ToString());
                        }
                    }, null);
                }, null);
                return hwr;
            }
            catch (WebException ex)
            {
                after(-1, ex.Status.ToString());
            }
            catch (Exception ex)
            {
                after(-2, ex.GetType().ToString());
            }
            return null;
        }

        public static HttpWebRequest CommunicateBinary(string target, Action<int, Stream, string> after)
        {
            if (isAllowed < 1)
            {
                after(-1, null, Res.LocalString.restrictedconnection);
                return null;
            }

            try
            {
                HttpWebRequest hwr = WebRequest.CreateHttp(target);
                //hwr.AllowAutoRedirect
                hwr.Method = "GET";
                hwr.Accept = accpt;
                hwr.CookieContainer = ccMain;
                hwr.Headers[HttpRequestHeader.AcceptEncoding] = "gzip";
                hwr.Headers[HttpRequestHeader.CacheControl] = "max-age=0";
                hwr.UserAgent = useragent;
                hwr.Headers[HttpRequestHeader.Referer] = lasturl;
                hwr.Headers[HttpRequestHeader.AcceptLanguage] = CultureInfo.CurrentCulture.Name;
                //hwr.Headers[HttpRequestHeader.IfModifiedSince] = DateTime.UtcNow.ToString("r");
                hwr.BeginGetResponse((a) =>
                {
                    try
                    {
                        if (a == null || hwr == null)
                        {
                            return;
                        }
                        if (a.IsCompleted)
                        {
                            var resp = hwr.EndGetResponse(a);
                            if (resp.Headers[HttpRequestHeader.ContentEncoding] != null && resp.Headers[HttpRequestHeader.ContentEncoding].Contains("gzip"))
                                resp = new GZipWebResponse(resp); //If gzipped response, uncompress
                            //lasturl = resp.ResponseUri.OriginalString;
                            using (var stream = resp.GetResponseStream())
                            {
                                MemoryStream ms = new MemoryStream();
                                stream.CopyTo(ms);
                                ms.Position = 0;
                                after(0, ms, resp.ResponseUri.OriginalString);
                            }
                        }
                    }
                    catch (WebException)
                    {
                        after(-1, null, string.Empty);
                    }
                    catch (Exception)
                    {
                        after(-2, null, string.Empty);
                    }
                }, null);
                return hwr;
            }
            catch (WebException)
            {
                after(-1, null, string.Empty);
            }
            catch (Exception)
            {
                after(-2, null, string.Empty);
            }
            return null;
        }

        public static HttpWebRequest CommunicateJSON(string target, Action<int, string> after)
        {
            if (isAllowed < 1)
            {
                after(-1, Res.LocalString.restrictedconnection);
                return null;
            }
            try
            {
                HttpWebRequest hwr = WebRequest.CreateHttp(target);
                //hwr.AllowAutoRedirect
                hwr.Method = "GET";
                hwr.Accept = "application/json, text/javascript, */*; q=0.01";
                hwr.CookieContainer = ccMain;
                hwr.Headers[HttpRequestHeader.CacheControl] = "max-age=0";
                hwr.Headers[HttpRequestHeader.AcceptEncoding] = "gzip";
                hwr.UserAgent = useragent;
                hwr.Headers["X-Requested-With"] = "XMLHttpRequest";
                hwr.Headers[HttpRequestHeader.Referer] = lasturl;
                hwr.Headers[HttpRequestHeader.AcceptLanguage] = CultureInfo.CurrentCulture.Name;
                hwr.Headers[HttpRequestHeader.IfModifiedSince] = DateTime.UtcNow.ToString("r");
                hwr.BeginGetResponse((a) =>
                {
                    try
                    {
                        if (a == null || hwr == null)
                        {
                            return;
                        }
                        if (a.IsCompleted)
                        {
                            var resp = hwr.EndGetResponse(a);
                            if (resp.Headers[HttpRequestHeader.ContentEncoding] != null && resp.Headers[HttpRequestHeader.ContentEncoding].Contains("gzip"))
                                resp = new GZipWebResponse(resp); //If gzipped response, uncompress
                            lasturl = resp.ResponseUri.OriginalString;
                            using (var stream = resp.GetResponseStream())
                            {
                                using (StreamReader sr = new StreamReader(stream))
                                {
                                    string got = sr.ReadToEnd();
                                    after(0, got);
                                }
                            }
                        }
                    }
                    catch (WebException ex)
                    {
                        after(-1, ex.Status.ToString());
                    }
                    catch (Exception ex)
                    {
                        after(-2, ex.GetType().ToString());
                    }
                }, null);
                return hwr;
            }
            catch (WebException ex)
            {
                after(-1, ex.Status.ToString());
            }
            catch (Exception ex)
            {
                after(-2, ex.GetType().ToString());
            }
            return null;
        }

        const string translateuri = "http://translate.google.com/#auto/{0}/{1}";
        public static void TranslateModal(string todo)
        {
            WebBrowserTask wbt = new WebBrowserTask();
            string code = CultureInfo.CurrentUICulture.TwoLetterISOLanguageName;
            wbt.Uri = new Uri(string.Format(translateuri, code, HttpUtility.UrlEncode(todo)), UriKind.Absolute);
            wbt.Show();
        }
    }
}
