﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace ePix.Model
{
    public class ContactModel
    {
        public string Pic { get; set; }
        public string Nick { get; set; }

        public ObservableCollection<ContactItem> ocContactInfo = new ObservableCollection<ContactItem>();
        public ObservableCollection<PictureModel> ocWorks = new ObservableCollection<PictureModel>();
    }

    public class ContactItem
    {
        public string Caption { get; set; }
        public string Content { get; set; }
    }
}
