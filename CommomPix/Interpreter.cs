﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using ePix.Model;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Data;
using System.IO;
using System.Windows.Media.Imaging;
using System.IO.IsolatedStorage;
using System.Text.RegularExpressions;
using System.Globalization;
using Microsoft.Phone.Controls;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using HtmlAgilityPack;

namespace ePix
{
    public static class Interpreter
    {
        //메인 페이지 최근 항목
        public static void LatestImages(ref ObservableCollection<PictureModel> pic, string stuff, out bool isGrouped)
        {
            List<PictureModel> lpic = new List<PictureModel>();
            isGrouped = false;

            HtmlDocument hd = new HtmlDocument();
            hd.LoadHtml(stuff);

            int iSUl = CommonMethod.SearchTagAttributes("ul", "linkStyleWorks", 0, stuff);
            if (iSUl < 0)
            {
                iSUl = CommonMethod.SearchTagAttributes("div", "newindex-bg-container", 0, stuff);
            }

            #region 비회원
            if (iSUl >= 0)
            {
                //비회원

                iSUl = CommonMethod.SearchTagAttributes("ul", "ui-brick", iSUl, stuff);


                int iEUl = stuff.IndexOf("</ul>", iSUl);
                if (iEUl > iSUl)
                {
                    string part = stuff.Substring(iSUl, iEUl - iSUl);
                    string[] pats = part.Split(new string[] { "</li>" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string pa in pats)
                    {
                        string sID = "", sTitle = "", sUri = "";
                        #region 앵커 위치
                        int iSA = pa.IndexOf("<a");
                        if (iSA >= 0)
                        {

                            int iEA = pa.IndexOf('>', iSA + 1);
                            if (iEA > iSA)
                            {
                                string href = CommonMethod.GetTagAttribute("href", pa.Substring(iSA, iEA - iSA + 1));
                                if (href.Length > 0)
                                {
                                    int iSid = href.IndexOf("illust_id=");
                                    if (iSid >= 0)
                                    {
                                        iSid += 10;
                                        int iEid = href.IndexOf('&', iSid);
                                        if (iEid <= 0)
                                        {
                                            iEid = href.Length;
                                        }
                                        sID = href.Substring(iSid, iEid - iSid);
                                    }
                                }
                            }
                        }
                        #endregion
                        #region 이미지 위치
                        int iSI = pa.IndexOf("<img");
                        if (iSI >= 0)
                        {

                            int iEI = pa.IndexOf('>', iSI + 1);
                            if (iEI > iSI)
                            {
                                string inimg = pa.Substring(iSI, iEI - iSI + 1);
                                string src = CommonMethod.GetTagAttribute("src", inimg);
                                if (src.Length > 0)
                                {
                                    if (src.IndexOf("http://") < 0)
                                    {
                                        src = src.Insert(0, "http://www.pixiv.net/");
                                    }
                                }
                                sUri = src;
                                string alt = CommonMethod.GetTagAttribute("alt", inimg);
                                sTitle = HttpUtility.HtmlDecode(alt);
                            }
                        }
                        #endregion

                        if (sUri.Length > 0)
                        {
                            lpic.Add(new PictureModel(sUri, sTitle, string.Empty, sID));
                        }
                    }
                }
            }
            #endregion
            else
            {
                #region 회원

                //int iSDiv = CommonMethod.SearchTagAttributes("ul", "class=\"image-items\"", 0, stuff);

                HtmlNodeCollection hnc = hd.DocumentNode.SelectNodes("//div[@id=\"item-container\"]/section[@class=\"item\"]");
                if (hnc != null)
                {
                    foreach (HtmlNode hnSec in hnc)
                    {
                        string category = "";
                        HtmlNode hnHeader = hd.DocumentNode.SelectSingleNode(hnSec.XPath + "/header/h1/a");
                        if (hnHeader != null)
                        {
                            category = HttpUtility.HtmlDecode(hnHeader.InnerText.Trim());
                            isGrouped = true;
                        }

                        HtmlNodeCollection hncSec = hnSec.SelectNodes(hnSec.XPath + "//li[@class=\"image-item\"]");
                        if (hncSec != null)
                        {
                            foreach (HtmlNode hnLi in hncSec)
                            {
                                string sID = "", sTitle = "", sAuthor = "", sUri = "";
                                HtmlNode ic = hnLi.SelectSingleNode(hnLi.XPath + "//a[@class=\"work\"]");
                                if (ic != null)
                                {
                                    string href = ic.GetAttributeValue("href", "");
                                    if (href.Length > 0)
                                    {
                                        int iSid = href.IndexOf("illust_id=");
                                        if (iSid >= 0)
                                        {
                                            iSid += 10;
                                            int iEid = href.IndexOf('&', iSid);
                                            if (iEid <= 0)
                                            {
                                                iEid = href.Length;
                                            }
                                            sID = href.Substring(iSid, iEid - iSid);
                                        }
                                    }

                                    HtmlNode div = ic.ChildNodes["div"];
                                    if (div != null)
                                    {
                                        HtmlNode img = div.ChildNodes["img"];
                                        if (img != null)
                                        {
                                            sUri = img.GetAttributeValue("data-src", "");
                                            if (string.IsNullOrEmpty(sUri))
                                            {
                                                sUri = img.GetAttributeValue("src", "");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        HtmlNode img = ic.ChildNodes["img"];
                                        if (img != null)
                                        {
                                            sUri = img.GetAttributeValue("data-src", "");
                                            if (string.IsNullOrEmpty(sUri))
                                            {
                                                sUri = img.GetAttributeValue("src", "");
                                            }
                                        }
                                    }
                                    HtmlNode h1 = ic.ChildNodes["h1"];
                                    sTitle = HttpUtility.HtmlDecode(h1.InnerText.Trim());
                                }

                                HtmlNode uc = hnLi.SelectSingleNode(hnLi.XPath + "//a[@class=\"user\"]");
                                if (uc != null)
                                {
                                    sAuthor = HttpUtility.HtmlDecode(uc.InnerText.Trim());
                                }

                                if (!string.IsNullOrEmpty(sUri) && !string.IsNullOrEmpty(sID))
                                {
                                    PictureModel pm = new PictureModel(sUri, sTitle, sAuthor, sID) { Category = category };
                                    lpic.Add(pm);
                                }
                            }
                        }
                    }
                }
                #endregion
            }

            if (lpic.Count > 0)
            {
                pic.Clear();
                foreach (PictureModel pm in lpic)
                {
                    pic.Add(pm);
                }
            }
        }

        //메인 페이지 핫 태그
        public static void HotTags(ref ObservableCollection<TagItem> tags, string stuff)
        {
            List<TagItem> ltag = new List<TagItem>();

            HtmlDocument hd = new HtmlDocument();
            hd.LoadHtml(stuff);
            HtmlNodeCollection hnc = hd.DocumentNode.SelectNodes("//a[@href]");
            if (hnc != null)
            {
                foreach (HtmlNode hn in hnc)
                {
                    if (hn.GetAttributeValue("href", "").IndexOf("tags.php?") >= 0
                        || hn.GetAttributeValue("href", "").IndexOf("search.php?s_mode=s_tag_full") >= 0)
                    {
                        int tclass = 0;
                        string tname = "";
                        if (hn.ParentNode.Name == "li")
                        {
                            string sclass = hn.ParentNode.GetAttributeValue("class", "");
                            if (sclass.IndexOf("level") >= 0)
                            {
                                int iL = sclass.IndexOf("level") + 5;
                                if (iL < sclass.Length)
                                {
                                    int.TryParse(sclass.Substring(iL, 1), out tclass);
                                }
                            }

                            string uclass = hn.ParentNode.ParentNode.GetAttributeValue("class","");

                            tname = HttpUtility.HtmlDecode(hn.InnerText);
                            if (!string.IsNullOrEmpty(tname))
                            {
                                if (uclass.IndexOf("favorite") >= 0)
                                {
                                    ltag.Insert(0,new TagItem() { _tagclass = -2, TagName = tname });
                                }
                                else
                                {
                                    ltag.Add(new TagItem() { _tagclass = tclass, TagName = tname });
                                }
                            }
                        }
                    }
                }
            }

            if (ltag.Count > 0)
            {
                tags.Clear();
                foreach (TagItem ti in ltag)
                {
                    tags.Add(ti);
                }
            }
        }

        //검색 페이지 사전 유효성
        public static string isDicAvailable(string stuff)
        {
            int iSDic = CommonMethod.SearchTagAttributes("a", "href=\"http://dic.pixiv.net/a", 0, stuff);
            if (iSDic > 0)
            {
                int iEDic = stuff.IndexOf('>', iSDic);
                if (iEDic > iSDic)
                {
                    iEDic++;
                    string sampleDic = stuff.Substring(iSDic, iEDic - iSDic);
                    string m = CommonMethod.GetTagAttribute("href", sampleDic);
                    return m;
                }
            }
            return null;
        }

        //메인 페이지 pixiv 로고
        public static string TodayBanner(string stuff)
        {
            int iSBan = CommonMethod.SearchTagAttributes("h1", "title", 0, stuff);
            if (iSBan > 0)
            {
                int iEBan = stuff.IndexOf("</h1>", iSBan);
                if (iEBan > iSBan)
                {
                    int iSImg = CommonMethod.SearchTagAttributes("img", "src=", iSBan, iEBan, stuff);
                    if (iSImg > iSBan)
                    {
                        int iEImg = stuff.IndexOf('>', iSImg);
                        if (iEImg > iSImg)
                        {
                            iEImg++;
                            string src = CommonMethod.GetTagAttribute("src", stuff.Substring(iSImg, iEImg - iSImg));
                            return src;
                        }
                    }
                }
            }
            return null;
        }

        //검색 페이지
        public static bool SearchResult(ref ObservableCollection<PictureModel> pic, bool shouldClear, string stuff)
        {
            Communication.isLogin = (stuff.IndexOf("loggedIn = true") >= 0);

            List<PictureModel> lpic = InsideSearchResult(stuff);
            if (lpic.Count > 0 && pic != null)
            {
                if (shouldClear)
                {
                    pic.Clear();
                }
                foreach (PictureModel pm in lpic)
                {
                    if (!pic.Contains(pm))
                    {
                        pic.Add(pm);
                    }
                }
                return true;
            }
            return false;
        }

        public static List<PictureModel> InsideSearchResult(string stuff)
        {
            List<PictureModel> lpic = new List<PictureModel>();
            if (stuff.Length > 0)
            {
                int iSDiv = CommonMethod.SearchTagAttributes("ul", "class=\"image-items", 0, stuff);
                if (iSDiv >= 0)
                {
                retry:
                    int iEDiv = stuff.IndexOf("</section>", iSDiv);
                    if (iEDiv < 0)
                    {
                        iEDiv = stuff.IndexOf("</ul>", iSDiv);
                    }
                    if (iEDiv > iSDiv)
                    {
                        string inside = stuff.Substring(iSDiv, iEDiv - iSDiv);
                        string[] ins = inside.Split(new string[] { "</li>" }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string each in ins)
                        {
                            string sID = "", sTitle = "", sAuthor = "", sUri = "", fav = "", resp = "";
                            int iSA = CommonMethod.SearchTagAttributes("a", "href", 0, each);
                            if (iSA >= 0)
                            {
                                int iEA = CommonMethod.SearchTagAttributesEnd("a", "href", 0, each);
                                if (iEA > iSA)
                                {
                                    string an = each.Substring(iSA, iEA - iSA);
                                    if (an.Length > 0)
                                    {
                                        string href = CommonMethod.GetTagAttribute("href", an);
                                        if (href.Length > 0)
                                        {
                                            int iSid = href.IndexOf("illust_id=");
                                            if (iSid >= 0)
                                            {
                                                iSid += 10;
                                                int iEid = href.IndexOf('&', iSid);
                                                if (iEid <= 0)
                                                {
                                                    iEid = href.Length;
                                                }
                                                sID = href.Substring(iSid, iEid - iSid);
                                            }
                                        }

                                        int iSCSpan = CommonMethod.SearchTagAttributes("i", "bookmark-badge", iEA, each);
                                        if (iSCSpan > 0)
                                        {
                                            int iECSpan = each.IndexOf("</i>", iSCSpan);
                                            if (iECSpan > iSCSpan)
                                            {
                                                int iEEA = each.IndexOf("</a>", iECSpan);
                                                if (iEEA > iECSpan)
                                                {
                                                    fav = HttpUtility.HtmlDecode(CommonMethod.RemoveAllTags(each.Substring(iECSpan, iEEA - iECSpan).Trim()));
                                                }
                                            }
                                        }

                                        int iSDSpan = CommonMethod.SearchTagAttributes("i", "response-", iEA, each);
                                        if (iSDSpan > 0)
                                        {
                                            int iEDSpan = each.IndexOf("</i>", iSDSpan);
                                            if (iEDSpan > iSDSpan)
                                            {
                                                int iEEA = each.IndexOf("</a>", iEDSpan);
                                                if (iEEA > iEDSpan)
                                                {
                                                    resp = HttpUtility.HtmlDecode(CommonMethod.RemoveAllTags(each.Substring(iEDSpan, iEEA - iEDSpan).Trim()));
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            int iSImg = CommonMethod.SearchTagAttributes("img", "src", 0, each);
                            if (iSImg >= 0)
                            {
                                int iEImg = CommonMethod.SearchTagAttributesEnd("img", "src", 0, each);
                                if (iEImg >= 0)
                                {
                                    string imgstr = each.Substring(iSImg, iEImg - iSImg);
                                    if (imgstr.Length > 0)
                                    {
                                        sUri = CommonMethod.GetTagAttribute("src", imgstr);
                                    }
                                    //sUri = CommonMethod.
                                }
                            }

                            int iSTitle = each.IndexOf("<h");
                            if (iSTitle >= 0)
                            {
                                //iSTitle += 4;
                                int iETitle = each.IndexOf("</h", iSTitle);
                                if (iETitle > iSTitle)
                                {
                                    sTitle = HttpUtility.HtmlDecode(CommonMethod.RemoveAllTags(each.Substring(iSTitle, iETitle - iSTitle)));
                                }

                                int iSNTag = CommonMethod.SearchTagAttributesEnd("a", "user", iSTitle, each);
                                if (iSNTag >= 0)
                                {
                                    int iENTag = each.IndexOf("</a>", iSNTag);
                                    if (iENTag > iSNTag)
                                    {
                                        sAuthor = HttpUtility.HtmlDecode(CommonMethod.RemoveAllTags(each.Substring(iSNTag, iENTag - iSNTag)));
                                    }
                                }
                            }

                            PictureModel pmnew = new PictureModel(sUri, sTitle, sAuthor, sID, fav, resp);
                            if (sUri.Length > 0 && sTitle.Length > 0)
                            {
                                lpic.Add(pmnew);
                            }
                        }
                    }
                    iSDiv = CommonMethod.SearchTagAttributes("ul", "class=\"images", iSDiv + 1, stuff);
                    if (iSDiv > 0)
                    {
                        goto retry;
                    }
                }
            }
            return lpic;
        }

        //사용자 검색
        public static bool SearchUser(ref ObservableCollection<Contact> cm, bool shouldClear, string stuff)
        {
            List<Contact> imc = new List<Contact>();

            int iSSection = CommonMethod.SearchTagAttributesEnd("div", "user-search-result", 0, stuff);
            if (iSSection < 0)
            {
                iSSection = CommonMethod.SearchTagAttributesEnd("ul", "user-recommendation-items", 0, stuff);
            }
            if (iSSection > 0)
            {
                int iEUl = stuff.IndexOf("</nav>", iSSection);
                if (iEUl > iSSection)
                {
                    int iSSSection = CommonMethod.SearchTagAttributesEnd("ul", "user-recommendation-items", 0, stuff);
                    string sall = "";
                    if (iSSSection > 0)
                    {
                        /*int iEEUl = stuff.IndexOf("</ul>", iSSSection);
                        if (iEEUl > 0)
                        {
                            iEUl = iEEUl;
                        }
                        sall = stuff.Substring(iSSSection, iEUl - iSSSection);*/
                        sall = stuff.Substring(iSSSection, iEUl - iSSSection);
                    }
                    else
                    {
                        sall = stuff;
                    }
                    string[] items = sall.Split(new string[] { "<li class=\"user-recommendation-item" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string itt in items)
                    {
                        string memnum = "", pic = "", name = "", desc = "";
                        int isli = itt.IndexOf("<li");
                        int iAMem = CommonMethod.SearchTagAttributes("a", "href", 0, itt);
                        if (iAMem >= 0)
                        {
                            int iAE = itt.IndexOf('>', iAMem);
                            if (iAE > iAMem)
                            {
                                iAE++;
                                string member = itt.Substring(iAMem, iAE - iAMem);
                                string memanc = CommonMethod.GetTagAttribute("href", member);
                                if (memanc.Length > 0)
                                {
                                    int iMemnum = memanc.IndexOf("?id=");
                                    if (iMemnum > 0)
                                    {
                                        iMemnum += 4;
                                        memnum = memanc.Substring(iMemnum, memanc.Length - iMemnum);
                                    }
                                }
                            }

                            int iSImg = CommonMethod.SearchTagAttributes("a", "data-src", iAMem, itt);
                            if (iSImg < 0)
                            {
                                iSImg = CommonMethod.SearchTagAttributes("img", "src", iAMem, itt);
                            }
                            if (iSImg > 0)
                            {
                                int iEImg = itt.IndexOf('>', iSImg);
                                if (iEImg > iSImg)
                                {
                                    iEImg++;
                                    string imtag = itt.Substring(iSImg, iEImg - iSImg);
                                    string src = CommonMethod.GetTagAttribute("data-src", imtag);
                                    if (src.Length > 0)
                                    {
                                        pic = src;
                                    }
                                    else
                                    {
                                        pic = CommonMethod.GetTagAttribute("src", imtag);
                                        name = CommonMethod.GetTagAttribute("alt", imtag);
                                    }
                                }
                            }

                            int iSName = itt.IndexOf("<h", iAMem);
                            if (iSName > 0)
                            {
                                //iSName += 4;
                                int iEName = itt.IndexOf("</h", iSName);
                                if (iEName > iSName)
                                {
                                    name = itt.Substring(iSName, iEName - iSName);
                                    name = HttpUtility.HtmlDecode(CommonMethod.RemoveAllTags(name));
                                }
                            }

                            int iSDesc = itt.IndexOf("<p", iAMem);
                            if (iSDesc > 0)
                            {
                                int iEDesc = itt.IndexOf("</p", iSDesc);
                                if (iEDesc > iSDesc)
                                {
                                    desc = itt.Substring(iSDesc, iEDesc - iSDesc);
                                    desc = HttpUtility.HtmlDecode(CommonMethod.RemoveAllTags(desc).Replace("\n", "").Replace("\r",""));
                                }
                            }
                        }

                        if (memnum.Length > 0 && name.Length > 0)
                        {
                            imc.Add(new Contact(pic, memnum, name, desc));
                        }
                    }
                }
            }
            if (imc.Count > 0)
            {
                if (shouldClear)
                {
                    cm.Clear();
                }

                foreach (Contact ci in imc)
                {
                    cm.Add(ci);
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        //
        public static bool ShowFavoriteUser(ref ObservableCollection<Contact> cm, bool shouldClear, string stuff)
        {
            List<Contact> imc = new List<Contact>();

            int iSSection = CommonMethod.SearchTagAttributesEnd("section", "search-result", 0, stuff);
            if (iSSection < 0)
            {
                iSSection = CommonMethod.SearchTagAttributesEnd("ul", "user-items", 0, stuff);
            }
            if (iSSection > 0)
            {
                int iEUl = stuff.IndexOf("</ul>", iSSection);
                if (iEUl > iSSection)
                {
                    int iSSSection = stuff.IndexOf("<ul", iSSection);
                    string sall = "";
                    if (iSSSection > 0)
                    {
                        int iEEUl = stuff.IndexOf("</ul>", iSSSection);
                        if (iEEUl > 0)
                        {
                            iEUl = iEEUl;
                        }
                        sall = stuff.Substring(iSSSection, iEUl - iSSSection);
                    }
                    else
                    {
                        sall = stuff;
                    }
                    string[] items = sall.Split(new string[] { "</li>" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string itt in items)
                    {
                        string memnum = "", pic = "", name = "", desc = "";
                        int isli = itt.IndexOf("<li");
                        int iAMem = CommonMethod.SearchTagAttributes("a", "href", Math.Max(isli, 0), itt);
                        if (iAMem >= 0)
                        {
                            int iAE = itt.IndexOf('>', iAMem);
                            if (iAE > iAMem)
                            {
                                iAE++;
                                string member = itt.Substring(iAMem, iAE - iAMem);
                                string memanc = CommonMethod.GetTagAttribute("href", member);
                                if (memanc.Length > 0)
                                {
                                    int iMemnum = memanc.IndexOf("?id=");
                                    if (iMemnum > 0)
                                    {
                                        iMemnum += 4;
                                        memnum = memanc.Substring(iMemnum, memanc.Length - iMemnum);
                                    }
                                }
                            }

                            int iSImg = CommonMethod.SearchTagAttributes("img", "data-src", iAMem, itt);
                            if (iSImg < 0)
                            {
                                iSImg = CommonMethod.SearchTagAttributes("img", "src", iAMem, itt);
                            }
                            if (iSImg > 0)
                            {
                                int iEImg = itt.IndexOf('>', iSImg);
                                if (iEImg > iSImg)
                                {
                                    iEImg++;
                                    string imtag = itt.Substring(iSImg, iEImg - iSImg);
                                    string src = CommonMethod.GetTagAttribute("data-src", imtag);
                                    if (src.Length > 0)
                                    {
                                        pic = src;
                                    }
                                    else
                                    {
                                        pic = CommonMethod.GetTagAttribute("src", imtag);
                                        name = CommonMethod.GetTagAttribute("alt", imtag);
                                    }
                                }
                            }

                            int iSName = itt.IndexOf("<h", iAMem);
                            if (iSName > 0)
                            {
                                //iSName += 4;
                                int iEName = itt.IndexOf("</h", iSName);
                                if (iEName > iSName)
                                {
                                    name = itt.Substring(iSName, iEName - iSName);
                                    name = HttpUtility.HtmlDecode(CommonMethod.RemoveAllTags(name));
                                }
                            }

                            int iSDesc = itt.IndexOf("<p", iAMem);
                            if (iSDesc > 0)
                            {
                                int iEDesc = itt.IndexOf("</p", iSDesc);
                                if (iEDesc > iSDesc)
                                {
                                    desc = itt.Substring(iSDesc, iEDesc - iSDesc);
                                    desc = HttpUtility.HtmlDecode(CommonMethod.RemoveAllTags(desc));
                                }
                            }
                        }

                        if (memnum.Length > 0 && name.Length > 0)
                        {
                            imc.Add(new Contact(pic, memnum, HttpUtility.HtmlDecode(name), HttpUtility.HtmlDecode(desc)));
                        }
                    }
                }
            }

            if (shouldClear)
            {
                cm.Clear();
            }

            if (imc.Count > 0)
            {
                foreach (Contact ci in imc)
                {
                    cm.Add(ci);
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool ViewDetail(DetailViewModel dvm, object idm, SwipeContentControl pv, double maxwidth, bool sibling,
            Action<int> after, string stuff)
        {
            try
            {
                Communication.isLogin = (stuff.IndexOf("loggedIn = true") >= 0);

                dvm.ocTags.Clear();
                dvm.Prior = string.Empty;
                dvm.NextNum = string.Empty;
                dvm.Next = string.Empty;
                dvm.PriorNum = string.Empty;
                int iSWData = CommonMethod.SearchTagAttributesEnd("section", "work-info", 0, stuff);
                int iEWData = -1;
                #region 로그인 상태
                if (iSWData > 0)
                {
                    //int iEMWData = stuff.IndexOf("</section>", iSWData);
                    int iEMWData = CommonMethod.SearchTagAttributes("ul", "meta", iSWData + 1, stuff);
                    #region 평점
                    if (iEMWData > iSWData)
                    {
                        int iERate = stuff.IndexOf("</dl>", iSWData);
                        if (iERate > iSWData)
                        {
                            string sRData = stuff.Substring(iSWData, iERate - iSWData);
                            //sRData = sRData.Replace("<dt>", "*");
                            sRData = sRData.Replace("</dt>", " ");
                            sRData = sRData.Replace("</dd>", Environment.NewLine);
                            dvm.Rate = HttpUtility.HtmlDecode(CommonMethod.RemoveAllTags(sRData));
                        }
                    }
                    #endregion
                    if (iEMWData > iSWData)
                    {
                        iEWData = stuff.IndexOf("</section>", iEMWData + 1);
                        if (iEWData > iEMWData)
                        {
                            #region 본문 헤더
                            string sWData = stuff.Substring(iEMWData, iEWData - iEMWData);
                            int iEP = sWData.IndexOf("</ul>");
                            if (iEP > 0)
                            {
                                #region 수치 헤더
                                string nWData = HttpUtility.HtmlDecode(sWData.Substring(0, iEP));
                                string[] tWData = nWData.Split(new string[] { "</li>" }, StringSplitOptions.RemoveEmptyEntries);
                                if (tWData.Length >= 2)
                                {
                                    dvm.TimeStamp = CommonMethod.RemoveAllTags(tWData[0].Trim());
                                    string ssize = CommonMethod.RemoveAllTags(tWData[1].Trim());
                                    dvm.TimeStamp += Environment.NewLine+ ssize;
                                    string[] res = ssize.Split(new char[] { '×' });
                                    int wid = 0, hei = 0;
                                    if (res.Length == 2)
                                    {
                                        try
                                        {
                                            wid = Convert.ToInt32(res[0].Trim());
                                            string rehei = res[1].Trim();
                                            if (rehei.IndexOf('　') > 0)
                                            {
                                                rehei = rehei.Substring(0, rehei.IndexOf('　'));
                                            }
                                            hei = Convert.ToInt32(rehei);
                                        }
                                        catch { }
                                        if (idm != null)
                                        {
                                            Border bddum = idm as Border;
                                            bddum.Width = maxwidth;
                                            bddum.Height = hei * maxwidth / wid;
                                        }
                                    }
                                    if (tWData.Length >= 3)
                                    {
                                        dvm.Tool = string.Empty;
                                        for (int i = 2; i < tWData.Length; i++)
                                        {
                                            dvm.Tool += CommonMethod.RemoveAllTags(tWData[i].Trim())+Environment.NewLine;
                                        }
                                    }
                                }
                                #endregion
                            }

                            #region 작가의 전후 작품'
                            
                            int iSSib = CommonMethod.SearchTagAttributes("ul", "sibling-items", 0, stuff);
                            if (iSSib > 0)
                            {
                                int iESib = stuff.IndexOf("</ul>", iSSib);
                                if (iESib > iSSib)
                                {
                                    int iSB = CommonMethod.SearchTagAttributesEnd("li", "before", iSSib, stuff);
                                    if (iSB > iSSib)
                                    {
                                        int iSBB = stuff.IndexOf("</li>", iSB, StringComparison.InvariantCultureIgnoreCase);
                                        if (iSBB > iSB)
                                        {
                                            int iSA = CommonMethod.SearchTagAttributes("a", "illust_id=", iSB, iSBB, stuff);
                                            if (iSA > 0)
                                            {
                                                int iEA = stuff.IndexOf('>', iSA);
                                                if (iEA > iSA)
                                                {
                                                    iEA++;
                                                    string href = "http://www.pixiv.net" + CommonMethod.GetTagAttribute("href", stuff.Substring(iSA, iEA - iSA));
                                                    int iSID = href.IndexOf("illust_id=");
                                                    if (iSID > 0)
                                                    {
                                                        iSID += "illust_id=".Length;
                                                        int iEID = href.IndexOf('&', iSID);
                                                        if (iEID <= iSID)
                                                        {
                                                            iEID = href.Length - iSID;
                                                        }
                                                        else
                                                        {
                                                            iEID = iEID - iSID;
                                                        }
                                                        if (sibling)
                                                        {
                                                            dvm.PriorNum = href.Substring(iSID, iEID);
                                                        }
                                                    }
                                                    if (sibling)
                                                    {
                                                        Communication.Communicate(href, (a, b) =>
                                                        {
                                                            DetailViewModel dvmnew = new DetailViewModel();
                                                            if (Interpreter.ViewDetail(dvmnew, null, null, maxwidth, false, null, b))
                                                            {
                                                                dvm.Prior = dvmnew.Pic;
                                                                dvm.PriorModel = dvmnew;
                                                                if (after != null)
                                                                {
                                                                    after(0);
                                                                }
                                                            }
                                                        }, false);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    int iSN = CommonMethod.SearchTagAttributesEnd("li", "after", iSSib, stuff);
                                    if (iSN > iSSib)
                                    {
                                        int iSNN = stuff.IndexOf("</li>", iSN, StringComparison.InvariantCultureIgnoreCase);
                                        if (iSNN > iSN)
                                        {
                                            int iSA = CommonMethod.SearchTagAttributes("a", "illust_id=", iSN, iSNN, stuff);
                                            if (iSA > 0)
                                            {
                                                int iEA = stuff.IndexOf('>', iSA);
                                                if (iEA > iSA)
                                                {
                                                    iEA++;
                                                    string href = "http://www.pixiv.net" + CommonMethod.GetTagAttribute("href", stuff.Substring(iSA, iEA - iSA));
                                                    int iSID = href.IndexOf("illust_id=");
                                                    if (iSID > 0)
                                                    {
                                                        iSID += "illust_id=".Length;
                                                        int iEID = href.IndexOf('&', iSID);
                                                        if (iEID <= iSID)
                                                        {
                                                            iEID = href.Length - iSID;
                                                        }
                                                        else
                                                        {
                                                            iEID = iEID - iSID;
                                                        }
                                                        if (sibling)
                                                        {
                                                            dvm.NextNum = href.Substring(iSID, iEID);
                                                        }
                                                    }
                                                    if (sibling)
                                                    {
                                                        Communication.Communicate(href, (a, b) =>
                                                        {
                                                            DetailViewModel dvmnewm = new DetailViewModel();
                                                            if (Interpreter.ViewDetail(dvmnewm, null, null, maxwidth, false, null, b))
                                                            {
                                                                dvm.Next = dvmnewm.Pic;
                                                                dvm.NextModel = dvmnewm;
                                                                if (after != null)
                                                                {
                                                                    after(1);
                                                                }
                                                            }
                                                        }, false);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            
                            #endregion

                            #region 제목
                            int iSTitle = CommonMethod.SearchTagAttributesEnd("h1", "title", 0, sWData);
                            if (iSTitle >= 0)
                            {
                                int iETitle = sWData.IndexOf("</h1>", iSTitle);
                                if (iETitle > iSTitle)
                                {
                                    dvm.Title = HttpUtility.HtmlDecode(sWData.Substring(iSTitle, iETitle - iSTitle));
                                }
                            }
                            #endregion
                            #endregion
                        }
                    }


                    #region 프로필
                    //int iSProfile = CommonMethod.SearchTagAttributes("div", "profile_area", 0, stuff);
                    int iSProfile = CommonMethod.SearchTagAttributes("div", "profile-unit", 0, stuff);
                    if (iSProfile >= 0)
                    {
                        int iSName = stuff.IndexOf("<h1", iSProfile + 1);
                        if (iSName > iSProfile)
                        {
                            int iEName = stuff.IndexOf("</h1", iSName);
                            if (iEName > iSName)
                            {
                                string sName = stuff.Substring(iSName, iEName - iSName);
                                dvm.Name = HttpUtility.HtmlDecode(CommonMethod.RemoveAllTags(sName));
                            }

                            #region 멤버 링크
                            int iSLink = CommonMethod.SearchTagAttributes("a", "member.php?id=", iSProfile, stuff);
                            if (iSLink > iSProfile)
                            {
                                int iELink = stuff.IndexOf('>', iSLink);
                                if (iELink > iSLink)
                                {
                                    iELink++;
                                    string sLink = stuff.Substring(iSLink, iELink - iSLink);
                                    if (sLink.Length > 0)
                                    {
                                        string sHref = CommonMethod.GetTagAttribute("href", sLink);
                                        if (sHref.Length > 0)
                                        {
                                            int iSNum = sHref.IndexOf("id=");
                                            if (iSNum > 0)
                                            {
                                                iSNum += 3;
                                                int iENum = sHref.IndexOf('&');
                                                string sID = "";
                                                if (iENum > iSNum)
                                                {
                                                    sID = sHref.Substring(iSNum, iENum - iSNum);
                                                }
                                                sID = sHref.Substring(iSNum, sHref.Length - iSNum);
                                                dvm.membernum = sID;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                    }
                    #endregion

                    #region 본문, 태그
                    int iSWCaption = -1;
                    if (iSWData > 0 && iEWData > iSWData)
                    {
                        iSWCaption = CommonMethod.SearchTagAttributesEnd("p", "caption", iSWData, iEWData, stuff);
                    }
                    if (iSWCaption >= 0)
                    {
                        int iEWCaption = stuff.IndexOf("</p>", iSWCaption);
                        if (iEWCaption > iSWCaption)
                        {
                            string sCaption = stuff.Substring(iSWCaption, iEWCaption - iSWCaption);
                            if (sCaption.Length > 0)
                            {
                                dvm.Caption = HttpUtility.HtmlDecode(CommonMethod.RemoveAllTags(sCaption));
                            }
                        }

                    }
                    #endregion

                    #region 태그
                    int iSTag = CommonMethod.SearchTagAttributesEnd("section", "work-tags", Math.Max(0, iSWCaption), stuff);
                    if (iSTag >= 0)
                    {
                        int iETag = stuff.IndexOf("</ul>", iSTag, StringComparison.InvariantCultureIgnoreCase);
                        if (iETag > iSTag)
                        {
                            int iSEach = CommonMethod.SearchTagAttributes("a", "tags.php?", iSTag, iETag, stuff);
                            while (iSEach > iSTag)
                            {
                                int iSSEach = CommonMethod.SearchTagAttributes("a", "class=\"text\"", iSEach, stuff);
                                if (iSSEach > iSEach)
                                {
                                    int iSAEnd = stuff.IndexOf('>', iSSEach + 1);
                                    /*string sTagHref = "";
                                    if (iSAEnd > iSEach)
                                    {
                                        iSAEnd++;
                                        string sTag = stuff.Substring(iSEach, iSAEnd - iSEach);
                                        sTagHref = CommonMethod.GetTagAttribute("href", sTag);
                                    }*/
                                    if (iSAEnd > iSSEach)
                                    {
                                        iSAEnd++;
                                        int iSAQuit = stuff.IndexOf("</a>", iSAEnd);
                                        if (iSAQuit > iSAEnd)
                                        {
                                            string sEachTag = HttpUtility.HtmlDecode(stuff.Substring(iSAEnd, iSAQuit - iSAEnd));
                                            bool isSelf = CommonMethod.SearchTagAttributes("span", "self-tag", iSEach, iSSEach, stuff) >= iSTag;
                                            dvm.ocTags.Add(new TagItem() { TagName = sEachTag, isnSelf = !isSelf });
                                        }
                                    }
                                }

                                if (iSEach + 1 < iETag)
                                {
                                    iSEach = CommonMethod.SearchTagAttributes("a", "tags.php?", iSEach + 1, iETag, stuff);
                                }
                                else
                                {
                                    iSEach = -1;
                                }
                            }
                        }
                    }
                    #endregion

                    #region 그림 출력
                    int iSWDisplay = CommonMethod.SearchTagAttributesEnd("div", "works_display", 0, stuff);
                    if (iSWDisplay >= 0)
                    {
                        int iEWDisplay = stuff.IndexOf("</div>", iSWDisplay);
                        if (iEWDisplay > iSWDisplay)
                        {
                            int iSAnc = CommonMethod.SearchTagAttributes("a", "href", iSWDisplay, iEWDisplay, stuff);
                            if (iSAnc >= iSWDisplay)
                            {
                                int iEAnc = stuff.IndexOf('>', iSAnc);
                                if (iEAnc > iSAnc)
                                {
                                    iEAnc++;
                                    string atag = stuff.Substring(iSAnc, iEAnc - iSAnc);
                                    if (atag.Length > 0)
                                    {
                                        string btag = HttpUtility.HtmlDecode(CommonMethod.GetTagAttribute("href", atag));
                                        if (btag.Length > 0)
                                        {
                                            int iparam = btag.IndexOf('?');
                                            if (iparam > 0)
                                            {
                                                dvm.Type = btag.Substring(iparam, btag.Length - iparam);
                                            }
                                        }
                                    }
                                }
                            }

                            int iSImg = stuff.IndexOf("<img", iSWDisplay, iEWDisplay - iSWDisplay);
                            if (iSImg > iSWDisplay)
                            {
                                int iEImg = stuff.IndexOf('>', iSImg);
                                if (iEImg > iSImg)
                                {
                                    iEImg++;
                                    string imgstr = stuff.Substring(iSImg, iEImg - iSImg);
                                    dvm.Pic = CommonMethod.GetTagAttribute("src", imgstr);
                                }
                            }
                        }
                    }
                    #endregion
                }
                #endregion
                #region 비로그인 상태
                else
                {
                    int iSFront = CommonMethod.SearchTagAttributes("div", "cool-work-main", 0, stuff);
                    if (iSFront >= 0)
                    {
                        int iSAnc = CommonMethod.SearchTagAttributes("a", "href", iSFront, stuff);
                        if (iSAnc >= iSFront)
                        {
                            int iEAnc = stuff.IndexOf('>', iSAnc);
                            if (iEAnc > iSAnc)
                            {
                                iEAnc++;
                                string atag = stuff.Substring(iSAnc, iEAnc - iSAnc);
                                if (atag.Length > 0)
                                {
                                    string btag = CommonMethod.GetTagAttribute("href", atag);
                                    if (btag.Length > 0)
                                    {
                                        int iparam = btag.IndexOf('?');
                                        if (iparam > 0)
                                        {
                                            dvm.Type = HttpUtility.HtmlDecode(btag.Substring(iparam, btag.Length - iparam));
                                        }
                                    }
                                }
                            }
                        }

                        int iSTitle = CommonMethod.SearchTagAttributesEnd("h1", "class=\"title", iSFront, stuff);
                        if (iSTitle >= 0)
                        {
                            int iETitle = stuff.IndexOf("</h1>", iSTitle);
                            if (iETitle > iSTitle)
                            {
                                dvm.Title = HttpUtility.HtmlDecode(stuff.Substring(iSTitle, iETitle - iSTitle));
                            }
                        }

                        int iSImg = CommonMethod.SearchTagAttributes("img", "src", iSTitle, stuff);
                        if (iSImg > iSTitle)
                        {
                            int iSImgE = stuff.IndexOf('>', iSImg + 1);
                            if (iSImgE > iSImg)
                            {
                                iSImgE++;
                                string imgtag = stuff.Substring(iSImg, iSImgE - iSImg);
                                if (imgtag.Length > 0)
                                {
                                    dvm.Pic = CommonMethod.GetTagAttribute("src", imgtag);
                                }
                            }
                        }

                    }

                    int iSCap = CommonMethod.SearchTagAttributesEnd("div", "caption_long", 0, stuff);
                    if (iSCap >= 0)
                    {
                        int iECap = stuff.IndexOf("</div>", iSCap);
                        if (iECap > iSCap)
                        {
                            dvm.Caption = HttpUtility.HtmlDecode(CommonMethod.RemoveAllTags(stuff.Substring(iSCap, iECap - iSCap)));
                        }

                        int iSTag = CommonMethod.SearchTagAttributesEnd("div", "tag_area", iSCap, stuff);
                        //int iETag = CommonMethod.SearchTagAttributes("ul", "inline-list", iSCap, stuff);
                        int iETag = stuff.IndexOf("</div", iSTag + 1);
                        if (iSTag >= 0)
                        {
                            int iSFake = CommonMethod.SearchTagAttributesEnd("a", "tag-icon", iSTag, stuff);
                            if (iSFake >= iSTag)
                            {
                                int iSEach = CommonMethod.SearchTagAttributes("a", "tags.php?", iSFake, stuff);
                                while (iSEach > iSTag)
                                {
                                    int iSAEnd = stuff.IndexOf('>', iSEach + 1);
                                    /*string sTagHref = "";
                                    if (iSAEnd > iSEach)
                                    {
                                        iSAEnd++;
                                        string sTag = stuff.Substring(iSEach, iSAEnd - iSEach);
                                        sTagHref = CommonMethod.GetTagAttribute("href", sTag);
                                    }*/
                                    if (iSAEnd > iSEach)
                                    {
                                        iSAEnd++;
                                        int iSAQuit = stuff.IndexOf("</a>", iSAEnd);
                                        if (iSAQuit > iSAEnd)
                                        {
                                            string sEachTag = HttpUtility.HtmlDecode(CommonMethod.RemoveAllTags(stuff.Substring(iSAEnd, iSAQuit - iSAEnd)));
                                            dvm.ocTags.Add(new TagItem() { TagName = sEachTag });
                                        }
                                    }

                                    if (iETag > iSEach + 1)
                                    {
                                        iSFake = CommonMethod.SearchTagAttributesEnd("a", "tag-icon", iSEach + 1, stuff);
                                        if (iSFake > iSEach)
                                        {
                                            iSEach = iSFake;
                                        }
                                        iSEach = CommonMethod.SearchTagAttributes("a", "tags.php?", iSEach + 1, iETag, stuff);
                                    }
                                    else
                                    {
                                        iSFake = CommonMethod.SearchTagAttributesEnd("a", "tag-icon", iSEach + 1, stuff);
                                        if (iSFake > iSEach)
                                        {
                                            iSEach = iSFake;
                                        }
                                        iSEach = CommonMethod.SearchTagAttributes("a", "tags.php?", iSEach + 1, stuff);
                                    }
                                }
                            }
                        }
                    }

                    int iSSub = CommonMethod.SearchTagAttributes("div", "userdata", 0, stuff);
                    if (iSSub >= 0)
                    {
                        int iEPNick = stuff.IndexOf("</h2>", iSSub + 1);
                        int iENick = 0;
                        if (iEPNick > iSSub)
                        {
                            iENick = stuff.LastIndexOf("</a>", iEPNick);
                        }
                        if (iENick > iSSub)
                        {
                            int iSA = CommonMethod.SearchTagAttributes("a", "member.php?", iSSub, stuff);
                            if (iSA >= 0)
                            {
                                int iEA = stuff.IndexOf('>', iSA);
                                if (iEA > iSA)
                                {
                                    iEA++;
                                    string sA = stuff.Substring(iSA, iEA - iSA);
                                    if (sA.Length > 0)
                                    {
                                        string shref = CommonMethod.GetTagAttribute("href", sA);
                                        int iSHid = shref.IndexOf("?id=");
                                        if (iSHid >= 0)
                                        {
                                            iSHid += 4;
                                            dvm.membernum = shref.Substring(iSHid, shref.Length - iSHid);
                                        }
                                    }

                                    int iETitle = stuff.IndexOf("</a>", iEA);
                                    if (iETitle > iEA)
                                    {
                                        dvm.Name = HttpUtility.HtmlDecode(stuff.Substring(iEA, iETitle - iEA));
                                    }
                                }
                            }
                        }

                        if (iEPNick > 0)
                        {
                            int iESum = stuff.IndexOf("</ul>", iEPNick);
                            if (iESum > 0)
                            {
                                int iSSum = stuff.LastIndexOf("<ul>", iESum);
                                if (iSSum < iESum && iSSum > 0)
                                {
                                    iSSum += 4;
                                    string sum = stuff.Substring(iSSum, iESum - iSSum);
                                    string[] sums = sum.Split(new string[] { "</li>" }, StringSplitOptions.RemoveEmptyEntries);
                                    for (int n = 0; n < sums.Length; n++)
                                    {
                                        string single = sums[n];
                                        int i = single.LastIndexOf("<span");
                                        if (i >= 0)
                                        {
                                            int j = single.IndexOf('>', i);
                                            if (j > i)
                                            {
                                                j++;
                                                int k = single.IndexOf("</span>", j);
                                                if (k > j)
                                                {
                                                    //string part = HttpUtility.HtmlDecode(single.Substring(j, k - j));
                                                    string part = HttpUtility.HtmlDecode(CommonMethod.RemoveAllTags(single));
                                                    switch (n)
                                                    {
                                                        case 0: dvm.TimeStamp = part; break;
                                                        case 1:
                                                            //dvm.TimeStamp = part; 
                                                            string[] res = part.Trim().Split(new char[] { '×' });
                                                            int wid = 0, hei = 0;
                                                            if (res.Length == 2)
                                                            {
                                                                try
                                                                {
                                                                    wid = Convert.ToInt32(res[0].Trim());
                                                                    string rehei = res[1].Trim();
                                                                    if (rehei.IndexOf('　') > 0)
                                                                    {
                                                                        rehei = rehei.Substring(0, rehei.IndexOf('　'));
                                                                    }
                                                                    hei = Convert.ToInt32(rehei);
                                                                }
                                                                catch { }
                                                                if (idm != null)
                                                                {
                                                                    Border bddum = idm as Border;
                                                                    bddum.Width = maxwidth;
                                                                    bddum.Height = hei * maxwidth / wid;
                                                                }
                                                            }
                                                            break;
                                                        case 2: dvm.Tool = part; break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }

                }
                #endregion
                #region 에러 처리
                int iSError = CommonMethod.SearchTagAttributesEnd("span", "error", 0, stuff);
                int iEError = stuff.IndexOf("</span>", iSError + 1);
                if (iSError < 0)
                {
                    iSError = CommonMethod.SearchTagAttributesEnd("div", "error", 0, stuff);
                    iEError = stuff.IndexOf("</div>", iSError + 1);
                }
                if (iSError >= 0)
                {
                    if (iEError > iSError)
                    {
                        MessageBox.Show(HttpUtility.HtmlDecode(CommonMethod.RemoveAllTags(stuff.Substring(iSError, iEError - iSError))),                Res.LocalString.errorserver,
                            MessageBoxButton.OK);
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while parsing the detail." + Environment.NewLine + ex.GetType().ToString(), "Pixifeed error",
                            MessageBoxButton.OK);
            }

            return true;
        }

        public static bool ViewProfile(ref ContactModel cm, string stuff)
        {
            int iSProfile = CommonMethod.SearchTagAttributesEnd("div", "profile-unit", 0, stuff);
            if (iSProfile >= 0)
            {
                int iSImg = CommonMethod.SearchTagAttributes("img", "src", iSProfile, stuff);
                if (iSImg > iSProfile)
                {
                    int iEImg = stuff.IndexOf('>', iSImg);
                    if (iEImg > iSImg)
                    {
                        iEImg++;
                        string sImg = CommonMethod.GetTagAttribute("src", stuff.Substring(iSImg, iEImg - iSImg));
                        if (sImg.Length > 0)
                        {
                            cm.Pic = sImg;
                        }
                    }
                }

                int iSH2 = stuff.IndexOf("<h", iSProfile + 1);
                if (iSH2 > iSProfile)
                {
                    int iEH2 = stuff.IndexOf("</h", iSH2);
                    if (iEH2 > iSH2)
                    {
                        //iSH2 += 4;
                        string sNick = stuff.Substring(iSH2, iEH2 - iSH2);
                        if (sNick.Length > 0)
                        {
                            cm.Nick = HttpUtility.HtmlDecode(CommonMethod.RemoveAllTags(sNick));
                        }
                    }
                }
            }
            else
            {
                return false;
            }

            int iSTable = CommonMethod.SearchTagAttributes("table", "ws_table", 0, stuff);
            if (iSTable >= 0)
            {
                int iSTd = CommonMethod.SearchTagAttributesEnd("td", "td1", iSTable + 1, stuff);
                while (iSTd > iSTable)
                {
                    int iETr = stuff.IndexOf("</tr>", iSTd);
                    if (iETr > iSTd)
                    {
                        int iETd = stuff.IndexOf("</td>", iSTd);
                        if (iETd > iSTd)
                        {
                            string stitle = stuff.Substring(iSTd, iETd - iSTd);
                            int iSTd2 = CommonMethod.SearchTagAttributesEnd("td", "td2", iETd, stuff);
                            int iETd3 = stuff.IndexOf("</tr>", iETd);
                            if (iSTd2 > iETd)
                            {
                                int iETd2 = stuff.IndexOf("</td>", iSTd2);
                                if (iETd3 < iETd2 && iETd3 >= 0)
                                {
                                    iETd2 = Math.Min(iETd2, iETd3);
                                }
                                if (iETd2 > iSTd2)
                                {
                                    string scap = stuff.Substring(iSTd2, iETd2 - iSTd2).Replace("<br>", Environment.NewLine);
                                    scap = HttpUtility.HtmlDecode(CommonMethod.RemoveAllTags(scap));
                                    if (stitle.Length > 0 && scap.Length > 0)
                                    {
                                        cm.ocContactInfo.Add(new ContactItem() { Caption = stitle, Content = scap });
                                    }
                                }
                            }
                        }
                    }
                    iSTd = CommonMethod.SearchTagAttributesEnd("td", "td1", iSTd + 1, stuff);
                }
                //iSTable = CommonMethod.SearchTagAttributesEnd("table", "ws_table", iSTable + 1, stuff);
            }

            if (cm.ocContactInfo.Count == 0)
            {
                cm.ocContactInfo.Add(new ContactItem() { Caption = Res.LocalString.errorprofile });
            }
            return true;
        }

        public static void ShowComments(ref ObservableCollection<CommentItem> ci, string stuff)
        {
            List<CommentItem> lci = new List<CommentItem>();

            #region 이전 방식
            string sA = "", sDate = "", sHref = "", sComment = "";
            int iSArr = stuff.IndexOf("html");
            if (iSArr >= 0)
            {
                int iSSArr = stuff.IndexOf("[", iSArr);
                if (iSSArr > iSArr)
                {
                    int iESArr = stuff.IndexOf("]", iSSArr);
                    if (iESArr > iSSArr + 2)
                    {
                        Regex rx = new Regex(@"\\[uU]([0-9A-Fa-f]{4})");
                        string nst = HttpUtility.HtmlDecode(rx.Replace(stuff.Substring(iSSArr + 2, iESArr - iSSArr - 2), match =>
                            ((char)Int32.Parse(match.Value.Substring(2), NumberStyles.HexNumber)).ToString())).Replace(@"\/", "/");
                        //string nst = HttpUtility.HtmlDecode();

                        int iSP = CommonMethod.SearchTagAttributes("li", "comment-item", 0, nst);

                        while (iSP >= 0)
                        {
                            sA = sDate = sHref = sComment = "";
                            int iEP = nst.IndexOf("</li>", iSP);
                            int iSSP = iSP;
                            if (iEP >= 0)
                            {
                                int iSA = CommonMethod.SearchTagAttributes("a", "href=\"member.php", iSP, nst);
                                if (iSA > iSP)
                                {
                                    int iSEA = nst.IndexOf('>', iSA);
                                    if (iSEA > iSA)
                                    {
                                        iSEA++;
                                        sHref = CommonMethod.GetTagAttribute("href", nst.Substring(iSA, iSEA - iSA));
                                        if (sHref.Length > 0)
                                        {
                                            int iSID = sHref.IndexOf("id=");
                                            if (iSID >= 0)
                                            {
                                                iSID += 3;
                                                sHref = sHref.Substring(iSID, sHref.Length - iSID);
                                            }
                                        }

                                        int iEA = nst.IndexOf("</a>", iSEA);
                                        if (iSEA >= iSA)
                                        {
                                            sA = HttpUtility.HtmlDecode(nst.Substring(iSEA, iEA - iSEA));
                                            int iSDate = CommonMethod.SearchTagAttributesEnd("span", "comment-date", iEA, nst);
                                            if (iSDate >= iEA)
                                            {
                                                int iEDate = nst.IndexOf("</span>", iSDate);
                                                if (iEDate >= iSDate)
                                                {
                                                    sDate = nst.Substring(iSDate, iEDate - iSDate);
                                                    iSSP = iEDate + 7;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            if (iSSP < iEP)
                            {
                                sComment = CommonMethod.RemoveAllTags(nst.Substring(iSSP, iEP - iSSP)).Replace(@"\r","").Replace(@"\n"," ");
                                if (sComment.Length > 0)
                                {
                                    sComment = HttpUtility.HtmlDecode(sComment);
                                    lci.Add(new CommentItem() { Name = sA, _nameid = sHref, TimeStamp = sDate, Body = sComment });
                                }
                            }

                            iSP = CommonMethod.SearchTagAttributes("li", "comment-item", iSP + 1, nst);
                        }
                    }
                    else
                    {
                        lci.Add(new CommentItem() { Body = Res.LocalString.nocom });
                    }
                }
            }

            if (lci.Count > 0)
            {
                ci.Clear();
                foreach (CommentItem each in lci)
                {
                    ci.Add(each);
                }
            }
            #endregion
        }

        public static List<CommentItem> ShowComments(string stuff, out bool isMore)
        {
            if (string.IsNullOrEmpty(stuff) || !stuff.EndsWith("}"))
            {
                isMore = false;
                return null;
            }
            JObject json = (JObject)JsonConvert.DeserializeObject(stuff);
            isMore = (bool?)json["body"]["more"] == true;
            string htmlbody = json["body"]["html"].ToString();
            stuff = null;

            HtmlDocument hd = new HtmlDocument();
            hd.OptionAutoCloseOnEnd = true;
            hd.LoadHtml(htmlbody);

            List<CommentItem> lc = new List<CommentItem>();
            HtmlNodeCollection hncComments = hd.DocumentNode.SelectNodes("//div[@class]");
            if (hncComments != null)
            {
                foreach (HtmlNode hnC in hncComments)
                {
                    string dc = hnC.GetAttributeValue("class", "");
                    if (!dc.StartsWith("_comment-item"))
                    {
                        continue;
                    }

                    bool host = dc.IndexOf("host-user") >= 0;

                    string profile = null, name = null, nameurl = null, body = null, stamp = "", date = "";
                    if (hnC.HasChildNodes)
                    {
                        foreach (HtmlNode hnCpart in hnC.ChildNodes)
                        {
                            if (hnCpart != null)
                            {
                                if (hnCpart.Name == "a" && hnCpart.GetAttributeValue("class", "").IndexOf("user-icon-container") >= 0)
                                {
                                    #region 프로필 이미지 컨테이너 div/a/img
                                    HtmlNode hnI = hnCpart.ChildNodes["img"];
                                    if (hnI != null)
                                    {
                                        profile = hnI.GetAttributeValue("data-src", null);//
                                    }
                                    #endregion
                                }
                                else if (hnCpart.Name == "div" && hnCpart.GetAttributeValue("class", "").IndexOf("comment") >= 0)
                                {
                                    #region 코멘트 본 컨테이너 div/div
                                    if (hnCpart.HasChildNodes)
                                    {
                                        foreach (HtmlNode hnDpart in hnCpart.ChildNodes)
                                        {
                                            if (hnDpart != null)
                                            {
                                                if (hnDpart.Name == "div" && hnDpart.GetAttributeValue("class", "").IndexOf("meta") >= 0)
                                                {
                                                    //메타 컨테이너 div/div/div->a | span
                                                    HtmlNode hnA = hnDpart.ChildNodes["a"];
                                                    if (hnA != null)
                                                    {
                                                        string link = hnA.GetAttributeValue("href", "");
                                                        int iid = link.IndexOf("id=");
                                                        if (iid >= 0)
                                                        {
                                                            iid += 3;
                                                            nameurl = link.Substring(iid, link.Length - iid);//
                                                        }
                                                        name = HttpUtility.HtmlDecode(hnA.InnerText);//
                                                    }

                                                    HtmlNode hnS = hnDpart.ChildNodes["span"];
                                                    if (hnS != null)
                                                    {
                                                        date = HttpUtility.HtmlDecode(hnS.InnerText);//
                                                    }
                                                }
                                                else if (hnDpart.Name == "div" && hnDpart.GetAttributeValue("class", "").IndexOf("body") >= 0)
                                                {
                                                    string rawbody = hnDpart.InnerText.Replace("\r", "").Replace("\n", "");
                                                    body = HttpUtility.HtmlDecode(rawbody.Trim().Replace("<br>", Environment.NewLine));//
                                                }
                                                else if (hnDpart.Name == "div" && hnDpart.GetAttributeValue("class", "").IndexOf("sticker-container") >= 0)
                                                {
                                                    //스탬프 컨테이너 div/div/div
                                                    HtmlNode hnI = hnDpart.ChildNodes["img"];
                                                    if (hnI != null)
                                                    {
                                                        stamp = hnI.GetAttributeValue("data-src", null);//
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion
                                }
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(body) || !string.IsNullOrEmpty(stamp))
                    {
                        lc.Add(new CommentItem()
                        {
                            _nameid = nameurl,
                            Body = body,
                            Name = name,
                            Profile = profile,
                            Stamp = stamp,
                            TimeStamp = date,
                            Host = host
                        });
                    }
                }
            }
            return lc;
        }

        public static List<EmojiModel> EnumEmoji(string stuff)
        {
            List<EmojiModel> lm = new List<EmojiModel>();

            HtmlDocument hd = new HtmlDocument();
            hd.LoadHtml(stuff);
            HtmlNodeCollection hnUS = hd.DocumentNode.SelectNodes("//div/ul[@class]");
            if (hnUS != null)
            {
                foreach (HtmlNode hnU in hnUS)
                {
                    if (hnU.GetAttributeValue("class", "").StartsWith("emoji-list")
                        && hnU.HasChildNodes)
                    {
                        IEnumerable<HtmlNode> hncLi = hnU.ChildNodes.Where((a) => { return a.Name == "li"; });
                        if (hncLi != null)
                        {
                            foreach (HtmlNode hnLi in hncLi)
                            {
                                if (hnLi != null && hnLi.HasChildNodes)
                                {
                                    HtmlNode hnImg = hnLi.ChildNodes["img"];
                                    string id = hnImg.GetAttributeValue("data-name", "");
                                    if (hnImg != null)
                                    {
                                        string sImg = hnImg.GetAttributeValue("data-src", "");
                                        lm.Add(new EmojiModel() { Pic = sImg, Name = id });
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return lm;
        }

        public static List<StampModel> EnumStamps(string stuff)
        {
            List<StampModel> lm = new List<StampModel>();

            HtmlDocument hd = new HtmlDocument();
            hd.LoadHtml(stuff);
            HtmlNodeCollection hnUS = hd.DocumentNode.SelectNodes("//div/ul[@class]");
            if (hnUS != null)
            {
                foreach (HtmlNode hnU in hnUS)
                {
                    if (hnU.GetAttributeValue("class", "").StartsWith("sticker-list")
                        && hnU.HasChildNodes)
                    {
                        IEnumerable<HtmlNode> hncLi = hnU.ChildNodes.Where((a) => { return a.Name == "li"; });
                        if (hncLi != null)
                        {
                            foreach (HtmlNode hnLi in hncLi)
                            {
                                if (hnLi != null && hnLi.GetAttributeValue("class", "") == "sticker" && hnLi.HasChildNodes)
                                {
                                    string id = hnLi.GetAttributeValue("data-id", "");
                                    HtmlNode hnImg = hnLi.ChildNodes["img"];
                                    if (hnImg != null)
                                    {
                                        string sImg = hnImg.GetAttributeValue("data-src", "");
                                        lm.Add(new StampModel() { Pic = sImg, Id =id });
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return lm;
        }

        public static bool ShowWorks(ref ObservableCollection<PictureModel> pc, bool shouldClean, string stuff)
        {
            List<PictureModel> pm = new List<PictureModel>();
            int iSDiv = CommonMethod.SearchTagAttributes("div", "display_works", 0, stuff);
            if (iSDiv >= 0)
            {
                int iEUl = stuff.IndexOf("</ul>", iSDiv, StringComparison.InvariantCultureIgnoreCase);
                int iEDiv = -1;
                if (iEUl > iSDiv)
                {
                    iEDiv = stuff.IndexOf("</div>", iEUl);
                }
                else
                {
                    iEDiv = stuff.IndexOf("</div>", iSDiv + 1);
                }
                if (iEDiv > iSDiv)
                {
                    int iSA = CommonMethod.SearchTagAttributes("a", "member_illust.php?", iSDiv, iEDiv, stuff);
                    while (iSA > 0)
                    {
                        int iEA = stuff.IndexOf('>', iSA);
                        if (iEA >= iSA)
                        {
                            iEA++;
                            string memstr = CommonMethod.GetTagAttribute("href", stuff.Substring(iSA, iEA - iSA));
                            if (memstr.Length > 0)
                            {
                                int iID = memstr.IndexOf("id=");
                                if (iID > 0)
                                {
                                    iID += 3;
                                    string mem = memstr.Substring(iID, memstr.Length - iID);
                                    int iSImg = CommonMethod.SearchTagAttributes("img", "src", iSA, stuff);
                                    if (iSImg >= iSA)
                                    {
                                        int iEImg = stuff.IndexOf('>', iSImg);
                                        if (iEImg > iSImg)
                                        {
                                            iEImg++;
                                            string imgstr = stuff.Substring(iSImg, iEImg - iSImg);
                                            if (imgstr.Length > 0)
                                            {
                                                string title = "";
                                                int iEEA = stuff.IndexOf("</a>", iEImg);
                                                if (iEEA > iEImg)
                                                {
                                                    title = HttpUtility.HtmlDecode(CommonMethod.RemoveAllTags(stuff.Substring(iEImg, iEEA - iEImg)));
                                                    /*if (title.Length > 0)
                                                    {
                                                        title = title.Remove(title.Length - 1);
                                                    }*/
                                                }
                                                /*string title = HttpUtility.HtmlDecode(CommonMethod.GetTagAttribute("alt", imgstr));
                                                if (title.Length > 0)
                                                {
                                                    title = title.Remove(title.Length - 1);
                                                }*/
                                                string uri = CommonMethod.GetTagAttribute("src", imgstr);
                                                PictureModel pnew = new PictureModel(uri, title, string.Empty, mem);
                                                if (!pc.Contains(pnew))
                                                {
                                                    pm.Add(pnew);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        iSA = CommonMethod.SearchTagAttributes("a", "member_illust.php?", iSA + 1, iEDiv, stuff);
                    }
                }
            }
            if (pm.Count > 0)
            {
                if (shouldClean)
                {
                    pc.Clear();
                }
                foreach (PictureModel each in pm)
                {
                    pc.Add(each);
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string SingleBigImage(string stuff)
        {
            int iSImg = CommonMethod.SearchTagAttributes("img", "src", 0, stuff);
            if (iSImg > 0)
            {
                int iEImg = stuff.IndexOf('>', iSImg);
                if (iEImg > iSImg)
                {
                    iEImg++;
                    string sTag = stuff.Substring(iSImg, iEImg - iSImg);
                    if (sTag.Length > 0)
                    {
                        string sSrc = CommonMethod.GetTagAttribute("src", sTag);
                        return sSrc;
                    }
                }
            }
            return string.Empty;
        }

        public static List<string> EnumBigImages(string stuff)
        {
            List<string> ling = new List<string>();
            int iSDivCont = CommonMethod.SearchTagAttributes("section", "manga", 0, stuff);
            if (iSDivCont >= 0)
            {
                int iSScript = stuff.IndexOf("<script>", iSDivCont);
                if (iSScript >= iSDivCont)
                {
                    int iUnshift = stuff.IndexOf("pixiv.context.images", iSScript);
                    while (iUnshift >= iSScript)
                    {
                        iUnshift = stuff.IndexOf('\'', iUnshift);
                        iUnshift++;

                        int iEUnshift = stuff.IndexOf('\'', iUnshift);
                        if (iEUnshift > iUnshift)
                        {
                            string sImg = stuff.Substring(iUnshift, iEUnshift - iUnshift);
                            if (sImg.Length > 0)
                            {
                                ling.Add(sImg);
                            }
                            iUnshift = stuff.IndexOf("pixiv.context.images", iEUnshift + 1);
                        }
                        else
                        {
                            iUnshift = -1;
                        }
                    }
                }
            }
            return ling;
        }

        public static bool AppendRank(ref ObservableCollection<RankItem> ric, bool shouldClean, string stuff)
        {
            List<RankItem> lri = new List<RankItem>();

            int iSSection = CommonMethod.SearchTagAttributesEnd("div", "class=\"ranking-items", 0, stuff);
            if (iSSection >= 0)
            {
                //int iESection = stuff.IndexOf("</div>", iSSection);
                int iESection = CommonMethod.SearchTagAttributes("div", "loading-indicator", iSSection, stuff);
                if (iESection > iSSection)
                {
                    /*string[] articles = stuff.Substring(iSSection, iESection - iSSection).Split(new string[] { "</article>" }, StringSplitOptions.RemoveEmptyEntries);*/
                    string[] articles = stuff.Substring(iSSection, iESection - iSSection).Split(new string[] { "<section id=" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string art in articles)
                    {
                        string num = "", img = "", title = "", artist = "", date = "";
                        decimal now = -1, last = 0;
                        int iSRank = CommonMethod.SearchTagAttributesEnd("h1", "", 0, art);
                        if (iSRank >= 0)
                        {
                            int iERank = art.IndexOf("</h1>", iSRank);
                            if (iERank > iSRank)
                            {
                                //now
                                now = CommonMethod.ExcludeExceptNum(HttpUtility.HtmlDecode(CommonMethod.RemoveAllTags(art.Substring(iSRank, iERank - iSRank))));
                                int iSLast = CommonMethod.SearchTagAttributesEnd("a", "href=\"ranking.php", iERank, art);
                                if (iSLast >= iERank)
                                {
                                    int iELast = art.IndexOf("</a>", iSLast);
                                    if (iELast > iSLast)
                                    {
                                        //last
                                        last = CommonMethod.ExcludeExceptNum(HttpUtility.HtmlDecode(CommonMethod.RemoveAllTags(art.Substring(iSLast, iELast - iSLast))));
                                    }
                                }
                                int iSImg = CommonMethod.SearchTagAttributes("img", "data-src", iERank, art);
                                //int iSImg = CommonMethod.SearchTagAttributes("img", "src", iERank, art);
                                if (iSImg > iERank)
                                {
                                    int iEImg = art.IndexOf('>', iSImg);
                                    if (iEImg > iSImg)
                                    {
                                        iEImg++;
                                        img = CommonMethod.GetTagAttribute("data-src", art.Substring(iSImg, iEImg - iSImg));
                                        int iSTLine = art.IndexOf("<h2>");
                                        if (iSTLine >= 0)
                                        {
                                            iSTLine += 4;
                                            int iSA = CommonMethod.SearchTagAttributes("a", "href=\"member_", iSTLine, art);
                                            if (iSA >= iSTLine)
                                            {
                                                int iEA = art.IndexOf('>', iSA);
                                                if (iEA > iSA)
                                                {
                                                    iEA++;
                                                    string anchor = HttpUtility.UrlDecode(CommonMethod.GetTagAttribute("href",
                                                        art.Substring(iSA, iEA - iSA)));
                                                    if (anchor.Length > 0)
                                                    {
                                                        /*if (anchor.IndexOf('&') > 0)
                                                        {
                                                            anchor = anchor.Remove(anchor.IndexOf('&'));
                                                        }*/
                                                        int iSID = anchor.IndexOf("id=");
                                                        if (iSID > 0 && art.Length > iSID)
                                                        {
                                                            iSID += 3;
                                                            //num
                                                            int iAfter = anchor.IndexOf('&', iSID);
                                                            if (iAfter >= iSID)
                                                            {
                                                                num = anchor.Substring(iSID, iAfter - iSID);
                                                            }
                                                            else
                                                            {
                                                                num = anchor.Substring(iSID, anchor.Length - iSID);
                                                            }
                                                            int iETitle = art.IndexOf("</a>", iEA);
                                                            if (iETitle >= iEA)
                                                            {
                                                                //title
                                                                title = HttpUtility.HtmlDecode(CommonMethod.RemoveAllTags(art.Substring(iEA, iETitle - iEA)));
                                                                int iSMem = CommonMethod.SearchTagAttributes("a", "href=\"member.php", iETitle, art);
                                                                if (iSMem >= 0)
                                                                {
                                                                    int iEMem = art.IndexOf("</a>", iSMem);
                                                                    if (iEMem > iSMem)
                                                                    {
                                                                        //artist
                                                                        artist = HttpUtility.HtmlDecode(CommonMethod.RemoveAllTags(art.Substring(iSMem, iEMem - iSMem)));
                                                                        int iSPDate = CommonMethod.SearchTagAttributes("dt", "", iEMem, art);
                                                                        if (iSPDate >= iEMem)
                                                                        {
                                                                            int iSDate = art.IndexOf("<dd>", iSPDate);
                                                                            if (iSDate >= iSPDate)
                                                                            {
                                                                                iSDate += 4;
                                                                                int iEDate = art.IndexOf("</dd>", iSDate);
                                                                                if (iEDate > iSDate)
                                                                                {
                                                                                    //date
                                                                                    date = HttpUtility.HtmlDecode(CommonMethod.RemoveAllTags(art.Substring(iSDate, iEDate - iSDate)));
                                                                                }
                                                                            }
                                                                        }
                                                                        lri.Add(new RankItem(date, artist, img, num, title,
                                                                                        now,
                                                                                        last));
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }


                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (lri.Count > 0)
            {
                if (shouldClean)
                {
                    ric.Clear();
                }

                //ric.AddRange(lri);
                foreach (RankItem ri in lri)
                {
                    ric.Add(ri);
                }

                return true;
            }
            return false;
        }

        public static bool SuggestUsers(ref ObservableCollection<Contact> csu, string stuff)
        {
            List<Contact> clst = new List<Contact>();

            int iSUser = CommonMethod.SearchTagAttributesEnd("ul", "class=\"users\"", 0, stuff);
            while (iSUser >= 0)
            {
                int iEUser = stuff.IndexOf("</ul>", iSUser);
                if (iEUser > iSUser)
                {
                    string[] users = stuff.Substring(iSUser, iEUser - iSUser).Split(new string[] { "</li>" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string usr in users)
                    {
                        int iSA = CommonMethod.SearchTagAttributes("a", "href=\"/member.php?id=", 0, usr);
                        if (iSA >= 0)
                        {
                            int iEA = usr.IndexOf('>', iSA);
                            if (iEA > iSA)
                            {
                                iEA++;
                                string anchor = usr.Substring(iSA, iEA - iSA);
                                string href = CommonMethod.GetTagAttribute("href", anchor);
                                string id = "";
                                if (href.Length > 0)
                                {
                                    int iSID = href.IndexOf("id=");
                                    if (iSID > 0)
                                    {
                                        iSID += 3;
                                        id = href.Substring(iSID, href.Length - iSID);
                                    }
                                }
                                string name = CommonMethod.GetTagAttribute("data-tooltip", anchor);
                                string img = "";
                                int iImg = usr.IndexOf("<img", iEA);
                                if (iImg >= iEA)
                                {
                                    int iEImg = usr.IndexOf('>', iImg);
                                    if (iEImg > iImg)
                                    {
                                        iEImg++;
                                        img = CommonMethod.GetTagAttribute("src", usr.Substring(iImg, iEImg - iImg));
                                    }
                                }
                                clst.Add(new Contact(img, id, name));
                            }
                        }
                    }
                }
                iSUser = CommonMethod.SearchTagAttributesEnd("ul", "class=\"users\"", iSUser + 1, stuff);
            }
            if (clst.Count > 0)
            {
                csu.Clear();

                foreach (Contact c in clst)
                {
                    csu.Add(c);
                }

                return true;
            }
            else
            {
                return false;
            }
        }

        public static string sugs = "", token = "";
        public static bool LoginTokens(string stuff)
        {
            int iSTok = stuff.IndexOf("pixiv.context.token");
            if (iSTok < 0)
            {
                return false;
            }
            else
            {
                iSTok += 19;
                int iETok = stuff.IndexOf('\n', iSTok);
                if (iETok > iSTok)
                {
                    string sTok = stuff.Substring(iSTok, iETok - iSTok);
                    sTok = Regex.Replace(sTok, @"[^\w]","");
                    token = sTok;
                }
                else
                {
                    return false;
                }
            }

            int iSA = stuff.IndexOf("pixiv.context.userRecommendSampleUser");
            if (iSA < 0)
            {
                return false;
            }
            else
            {
                iSA += 37;
                int iEA = stuff.IndexOf('\n', iSA);
                if (iEA > iSA)
                {
                    string sRec = stuff.Substring(iSA, iEA - iSA);
                    sRec = Regex.Replace(sRec, @"[^\w,]","");
                    sugs = sRec;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        public static bool IsUserFavorite(string stuff)
        {
            int iSTok = stuff.IndexOf("pixiv.context.favorite");
            if (iSTok < 0)
            {
                return false;
            }
            else
            {
                iSTok = stuff.IndexOf('=', iSTok);
                if (iSTok > 0)
                {
                    int iETok = stuff.IndexOf('\n', iSTok);
                    if (iETok > iSTok)
                    {
                        string sTok = stuff.Substring(iSTok, iETok - iSTok);
                        sTok = Regex.Replace(sTok, @"[^\w]", "");
                        return Convert.ToBoolean(sTok.Trim().ToLower());
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
        }

        public static bool GetUserBookmarks(ref ObservableCollection<PictureModel> pc, bool shouldClean, string stuff)
        {
            List<PictureModel> lpm = new List<PictureModel>();

            HtmlDocument hd = new HtmlDocument();
            hd.LoadHtml(stuff);

            HtmlNodeCollection hncLi = hd.DocumentNode.SelectNodes("//div[@class=\"display_works linkStyleWorks\"]/ul/li");
            if (hncLi != null)
            {
                
                foreach (HtmlNode hn in hncLi)
                {
                    string uri = "", title = "", author = "", id = "", fav = "", resp = "";
                    #region 이미지 부분
                    HtmlNode hnA = hn.ChildNodes["a"];
                    if (hnA != null)
                    {
                        string link = hnA.GetAttributeValue("href", "");
                        int iId = link.IndexOf("illust_id=");
                        if (iId >= 0)
                        {
                            iId += 10;
                            int iEd = link.IndexOf("&", iId);
                            if (iEd < iId)
                            {
                                iEd = link.Length;
                            }
                            id = link.Substring(iId, iEd - iId);
                        }

                        title = HttpUtility.HtmlDecode(hnA.InnerText);

                        HtmlNode hnIDiv = hnA.ChildNodes["div"];
                        if (hnIDiv != null)
                        {
                            HtmlNode img = hnIDiv.ChildNodes["img"];
                            if (img != null)
                            {
                                string simg = img.GetAttributeValue("data-src", "");
                                if (simg.Length == 0)
                                {
                                    simg = img.GetAttributeValue("src", "");
                                }

                                uri = simg;
                            }
                        }
                    }
                    #endregion

                    #region 작성자 부분
                    HtmlNode hnAuthor = hn.ChildNodes.FirstOrDefault((a) => { return a.GetAttributeValue("class", "").StartsWith("f10"); });
                    if (hnAuthor != null)
                    {
                        author = HttpUtility.HtmlDecode(hnAuthor.InnerText);
                    }
                    #endregion

                    #region 북마크 부분
                    HtmlNodeCollection hncI = hd.DocumentNode.SelectNodes(hn.XPath + "//i");
                    if (hncI != null)
                    {
                        foreach (HtmlNode hnI in hncI)
                        {
                            if (hnI.GetAttributeValue("class", "").IndexOf("bookmark-") >= 0)
                            {
                                HtmlNode p = hnI.ParentNode;
                                fav = HttpUtility.HtmlDecode(p.InnerText);
                            }
                            else if (hnI.GetAttributeValue("class", "").IndexOf("response-") >= 0)
                            {
                                HtmlNode p = hnI.ParentNode;
                                resp = HttpUtility.HtmlDecode(p.InnerText);
                            }
                        }
                    }
                    #endregion

                    if (!string.IsNullOrEmpty(uri) && !string.IsNullOrEmpty(title))
                    {
                        PictureModel pm = null;
                        pm = new PictureModel(uri, title, author, id, fav, resp);
                        lpm.Add(pm);
                    }
                }
            }

            if (shouldClean)
            {
                pc.Clear();
            }

            if (lpm.Count > 0)
            {
                foreach (PictureModel pm in lpm)
                {
                    pc.Add(pm);
                }
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
