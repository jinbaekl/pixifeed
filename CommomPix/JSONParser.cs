﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ePix
{
    class JSONParser
    {
        public static List<Contact> JSONToContacts(string JStr)
        {
            List<Contact> lc = new List<Contact>();
            try
            {
                JObject json = (JObject)JsonConvert.DeserializeObject(JStr);
                foreach (var f in json["recommend_users"])
                {
                    /*
                     * "recommend_users": [{
                            "user_id": "335493",
                            "user_name": "くろぬこネーロ",
                            "img": "http:\/\/i2.pixiv.net\/img18\/profile\/kuronekone_ro\/5915575_s.jpg",
                            "user_comment": "今月の抱負\r\n\r\n「とりあえずペン握ろ？」\r\n\r\nブログ\r\nhttp:\/\/kuronekone-ro.jugem...."
                        },
                     * 
                     */

                    Contact c = new Contact(f["img"].ToString(), f["user_id"].ToString(),
                        f["user_name"].ToString(), Regex.Replace(f["user_comment"].ToString(),@"[\s\n]+"," "));
                    lc.Add(c);
                }
            }
            catch
            {
                return lc;
            }
            return lc;
        }

        public static List<string> JSONToKeywords(string JStr, string prior)
        {
            List<string> lc = new List<string>();
            try
            {
                JObject json = (JObject)JsonConvert.DeserializeObject(JStr);
                foreach (var f in json["candidates"])
                {
                    string c = (prior.Length > 0) ? prior + " " : string.Empty;
                    c += f["tag_name"].ToString();
                    lc.Add(c);
                }
            }
            catch
            {
                return lc;
            }
            return lc;
        }
    }
}
