﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
using ePix.Model;

namespace ePix
{

    public class SerializeJSON
    {
        public static string Serialize<T>(T obj)
        {
            System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(obj.GetType());
            MemoryStream ms = new MemoryStream();
            serializer.WriteObject(ms, obj);
            string retVal = Encoding.UTF8.GetString(ms.ToArray(),0,Convert.ToInt32(ms.Length));
            ms.Dispose();
            return retVal;
        }

        public static T Deserialize<T>(string json)
        {
            T obj = Activator.CreateInstance<T>();
            MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(json));
            System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(obj.GetType());
            obj = (T)serializer.ReadObject(ms);
            ms.Close();
            ms.Dispose();
            return obj;
        }
    }

    /// Our Person object to Serialize/Deserialize to JSON
    [DataContract]
    public class Contact
    {
        public Contact() { }
        public Contact(string pic, string memnum, string nick)
        {
            this.img = pic;
            this.user_id = memnum;
            this.user_name = nick;
        }

        public Contact(string pic, string memnum, string nick, string des)
        {
            this.img = pic;
            this.user_id = memnum;
            this.user_name = nick;
            this.desc = des;
        }

        [DataMember]
        public string img { get; set; }

        [DataMember]
        public string user_id { get; set; }

        [DataMember]
        public string user_name { get; set; }

        public string desc { get; set; }

        public override bool Equals(object obj)
        {
            Contact c = obj as Contact;
            if (c != null)
            {
                return user_id == c.user_id;
            }
            return false;
        }
    }

    public class Keyword
    {
        public string tag_name { get; set; }
        public string access_cnt { get; set; }

        public override string ToString()
        {
            return tag_name;
        }
    }
}
